package com.khwy158.ldxdemo.service;

import com.khwy158.ldxdemo.dto.ClientExecution;
import com.khwy158.ldxdemo.pojo.LdxCompanyClient;
import org.springframework.web.multipart.MultipartFile;

public interface ClientService {

    /**
     * 新建客户
     * @param ldxCompanyClient
     * @return
     */
    ClientExecution savePersonalClient(LdxCompanyClient ldxCompanyClient, MultipartFile file);


    
}
