package com.khwy158.ldxdemo.service.serviceImpl;


import com.khwy158.ldxdemo.Utils.FileUploadUtil;
import com.khwy158.ldxdemo.config.FileConfig;
import com.khwy158.ldxdemo.dao.LdxCompanyClientMapper;
import com.khwy158.ldxdemo.dto.ClientExecution;
import com.khwy158.ldxdemo.enums.ClientStatusEnum;
import com.khwy158.ldxdemo.pojo.LdxCompanyClient;
import com.khwy158.ldxdemo.service.ClientService;
import jdk.nashorn.internal.runtime.regexp.joni.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    @SuppressWarnings("all")
    private LdxCompanyClientMapper lccm;

    @Autowired
    private FileConfig fileConfig;

    @Override
    public ClientExecution savePersonalClient(LdxCompanyClient ldxCompanyClient, MultipartFile file) {


        //录入的客户为空
        if (ldxCompanyClient==null){
            return new ClientExecution(ClientStatusEnum.CLIENT_NULL);
        }
        //客户名字是否为空
        if (ldxCompanyClient.getCompanyClientName()==null){
            return new ClientExecution(ClientStatusEnum.CLIENT_NAME_NULL);
        }
        //客户邮箱是否为空
        if (ldxCompanyClient.getContactsEmail()==null){
            return new ClientExecution(ClientStatusEnum.CLIENT_EMAIL_NULL);
        }
        //客户性别为空
        if (ldxCompanyClient.getContactsSex()==null){
            return new ClientExecution(ClientStatusEnum.CLIENT_SEX_NULL);
        }
        //客户所在地是否为空
        if (ldxCompanyClient.getCityId()==null){
            return new ClientExecution(ClientStatusEnum.CLIENT_CITY_NULL);
        }

        //获得客户时间是否为空
        if (ldxCompanyClient.getAcquisitionTime()==null){
            return new ClientExecution(ClientStatusEnum.ACQUISITIONTIME_NULL);
        }
        //跟进员工是否为空
        if (ldxCompanyClient.getEmplId()==null){
            return new ClientExecution(ClientStatusEnum.EMPL_NULL);
        }
        //其他必要信息验证
        if (ldxCompanyClient.getClientCategoryId()==null
                ||ldxCompanyClient.getClientStatusId()==null
                ||ldxCompanyClient.getClientGradeId()==null
                ||ldxCompanyClient.getContactsPhone()==null
                ||ldxCompanyClient.getRegisterId()==null
                ||ldxCompanyClient.getEmplId()==null){
            return new ClientExecution(ClientStatusEnum.INFORMATION_NEEDED_EMPTY);
        }

        //检测录入客户为公司客户还是个人客户
        if(ldxCompanyClient.getClientComOrPerson()==1){
            if (ldxCompanyClient.getContactsName()==null){
                return new ClientExecution(ClientStatusEnum.COMPANY_CONTACTS_NULL);
            }
        }
        //上传文件
        if (file!=null&&!file.isEmpty()){
            String file1 = FileUploadUtil.upLoadFile(file, fileConfig.getConfig());
            ldxCompanyClient.setRelatedDocuments(file1);

        }



        int res=lccm.insertSelective(ldxCompanyClient);
        //新增失败
        if (res<=0){
            return new ClientExecution(ClientStatusEnum.INSERT_FAILED);
        }

        //新建成功
        return new ClientExecution(ClientStatusEnum.INSERT_SUCCESS);



    }
}
