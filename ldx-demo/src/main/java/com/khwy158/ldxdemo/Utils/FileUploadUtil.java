package com.khwy158.ldxdemo.Utils;


import com.khwy158.ldxdemo.config.FileConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;


@Component
public class FileUploadUtil {



    //上传文件
    public static String upLoadFile(MultipartFile file,String path){
        // 得到文件名字
        String fileName = file.getOriginalFilename();
        // 得到后缀
        String ext = fileName.substring(fileName.lastIndexOf("."));

        String tempName=UUID.randomUUID().toString().replaceAll("-",".");

        File file1=new File(path+"/"+tempName+ext);

        /**
         * 判断文件夹是否存在
         */
        if (!file1.getParentFile().exists()){
            file1.getParentFile().mkdir();
        }
        //上传
        try {
            file.transferTo(file1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path+"/"+tempName+ext;
    }


}
