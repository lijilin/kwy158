package com.khwy158.ldxdemo.controller;

import com.khwy158.ldxdemo.Utils.JsonResult;
import com.khwy158.ldxdemo.config.FileConfig;
import com.khwy158.ldxdemo.dto.ClientExecution;
import com.khwy158.ldxdemo.pojo.LdxCompanyClient;
import com.khwy158.ldxdemo.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ClientController {

    //客户服务
    @Autowired
    private ClientService clientService;

    //文件路劲
    @Autowired
    private FileConfig fileConfig;


    @RequestMapping("/ldx/savepersonal")
    public JsonResult savePersonalClient(@RequestBody LdxCompanyClient ldxCompanyClient, @RequestParam(value = "file")MultipartFile file){
        ClientExecution clientExecution = clientService.savePersonalClient(ldxCompanyClient,file);



        if (clientExecution.getCode()!=201){
            return new JsonResult(clientExecution.getCode(),clientExecution.getMsg());
        }
        return new JsonResult(clientExecution.getCode(),clientExecution.getMsg());
    }


}
