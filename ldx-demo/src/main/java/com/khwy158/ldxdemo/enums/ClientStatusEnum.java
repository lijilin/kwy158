package com.khwy158.ldxdemo.enums;

public enum ClientStatusEnum {
    INFORMATION_NEEDED_EMPTY(1001,"需要的信息为空"),
    SERCH_SUCCESS(200,"查询成功"),
    INSERT_SUCCESS(201,"增加成功"),
    CLIENT_NULL(1002,"录入的客户为空"),
    CLIENT_NAME_NULL(1003,"录入的客户名字为空"),
    CLIENT_EMAIL_NULL(1004,"录入的客户邮箱为空"),
    CLIENT_SEX_NULL(1005,"录入的客户性别为空"),
    CLIENT_CITY_NULL(1006,"录入的客户邮箱为空"),
    SERCH_FAILED(1007,"查询失败"),
    INSERT_FAILED(1008,"新增失败"),
    ACQUISITIONTIME_NULL(1010,"获取时间为空"),
    EMPL_NULL(1011,"没有跟进员工"),
    COMPANY_CONTACTS_NULL(1009,"公司联系人姓名为空");

    //状态码
    private int code;

    //信息
    private String msg;

    ClientStatusEnum() {
    }

    ClientStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {

        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
