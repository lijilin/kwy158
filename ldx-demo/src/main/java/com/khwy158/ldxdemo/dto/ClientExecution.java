package com.khwy158.ldxdemo.dto;

import com.khwy158.ldxdemo.enums.ClientStatusEnum;
import com.khwy158.ldxdemo.pojo.LdxCompanyClient;

import java.util.List;

public class ClientExecution  {
    //状态码
    private Integer code;
    //状态信息
    private String msg;

    //公司客户实体
    private LdxCompanyClient ldxCompanyClient;

    //公司客户列表
    private List<LdxCompanyClient> ldxCompanyClients;


    /**
     * 操作失败构造器
     * @param clientStatusEnum
     */
    public ClientExecution(ClientStatusEnum clientStatusEnum) {
        this.code = clientStatusEnum.getCode();
        this.msg = clientStatusEnum.getMsg();
    }

    /**
     * 只对公司客户操作成功构造器
     * @param clientStatusEnum
     * @param ldxCompanyClient
     */
    public ClientExecution(ClientStatusEnum clientStatusEnum, LdxCompanyClient ldxCompanyClient) {
        this.code = clientStatusEnum.getCode();
        this.msg = clientStatusEnum.getMsg();
        this.ldxCompanyClient = ldxCompanyClient;
    }


    public ClientExecution(ClientStatusEnum clientStatusEnum, List<LdxCompanyClient> ldxCompanyClients) {
        this.code = clientStatusEnum.getCode();
        this.msg = clientStatusEnum.getMsg();
        this.ldxCompanyClients = ldxCompanyClients;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LdxCompanyClient getLdxCompanyClient() {
        return ldxCompanyClient;
    }

    public void setLdxCompanyClient(LdxCompanyClient ldxCompanyClient) {
        this.ldxCompanyClient = ldxCompanyClient;
    }




    public List<LdxCompanyClient> getLdxCompanyClients() {
        return ldxCompanyClients;
    }

    public void setLdxCompanyClients(List<LdxCompanyClient> ldxCompanyClients) {
        this.ldxCompanyClients = ldxCompanyClients;
    }



}
