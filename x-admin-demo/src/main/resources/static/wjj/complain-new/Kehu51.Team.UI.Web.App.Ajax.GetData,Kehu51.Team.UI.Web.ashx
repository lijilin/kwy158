if(typeof Kehu51 == "undefined") Kehu51={};
if(typeof Kehu51.Team == "undefined") Kehu51.Team={};
if(typeof Kehu51.Team.UI == "undefined") Kehu51.Team.UI={};
if(typeof Kehu51.Team.UI.Web == "undefined") Kehu51.Team.UI.Web={};
if(typeof Kehu51.Team.UI.Web.App == "undefined") Kehu51.Team.UI.Web.App={};
if(typeof Kehu51.Team.UI.Web.App.Ajax == "undefined") Kehu51.Team.UI.Web.App.Ajax={};
if(typeof Kehu51.Team.UI.Web.App.Ajax.GetData_class == "undefined") Kehu51.Team.UI.Web.App.Ajax.GetData_class={};
Kehu51.Team.UI.Web.App.Ajax.GetData_class = function() {};
Object.extend(Kehu51.Team.UI.Web.App.Ajax.GetData_class.prototype, Object.extend(new AjaxPro.AjaxClass(), {
	IsExistsContent: function(tablenameID, fieldname, datatype, text, value, currendIndexID, cusID) {
		return this.invoke("IsExistsContent", {"tablenameID":tablenameID, "fieldname":fieldname, "datatype":datatype, "text":text, "value":value, "currendIndexID":currendIndexID, "cusID":cusID}, this.IsExistsContent.getArguments().slice(7));
	},
	SetMessageRead: function(messageid) {
		return this.invoke("SetMessageRead", {"messageid":messageid}, this.SetMessageRead.getArguments().slice(1));
	},
	GetCity: function(parentID) {
		return this.invoke("GetCity", {"parentID":parentID}, this.GetCity.getArguments().slice(1));
	},
	GetLinkMan: function(cusID) {
		return this.invoke("GetLinkMan", {"cusID":cusID}, this.GetLinkMan.getArguments().slice(1));
	},
	GetSysParameter: function(parameterName, classID) {
		return this.invoke("GetSysParameter", {"parameterName":parameterName, "classID":classID}, this.GetSysParameter.getArguments().slice(2));
	},
	GetSysParameterNew: function(parameterName, classID, configGuid) {
		return this.invoke("GetSysParameterNew", {"parameterName":parameterName, "classID":classID, "configGuid":configGuid}, this.GetSysParameterNew.getArguments().slice(3));
	},
	GetRoleData: function() {
		return this.invoke("GetRoleData", {}, this.GetRoleData.getArguments().slice(0));
	},
	GetSysParamterHTML: function(parameterName, controlName, controlType, classID) {
		return this.invoke("GetSysParamterHTML", {"parameterName":parameterName, "controlName":controlName, "controlType":controlType, "classID":classID}, this.GetSysParamterHTML.getArguments().slice(4));
	},
	GetSysParamterNewHTML: function(parameterName, controlName, controlType, classID, configGuid) {
		return this.invoke("GetSysParamterNewHTML", {"parameterName":parameterName, "controlName":controlName, "controlType":controlType, "classID":classID, "configGuid":configGuid}, this.GetSysParamterNewHTML.getArguments().slice(5));
	},
	GetCusMaturityID: function(cusID) {
		return this.invoke("GetCusMaturityID", {"cusID":cusID}, this.GetCusMaturityID.getArguments().slice(1));
	},
	GetFilesHtml: function(idList, type, typeid, isShowCloseButton) {
		return this.invoke("GetFilesHtml", {"idList":idList, "type":type, "typeid":typeid, "isShowCloseButton":isShowCloseButton}, this.GetFilesHtml.getArguments().slice(4));
	},
	DeleteImageFile: function(path, fieldName, tableName, isCheck) {
		return this.invoke("DeleteImageFile", {"path":path, "fieldName":fieldName, "tableName":tableName, "isCheck":isCheck}, this.DeleteImageFile.getArguments().slice(4));
	},
	GetFolderList: function() {
		return this.invoke("GetFolderList", {}, this.GetFolderList.getArguments().slice(0));
	},
	ExistsCusName: function(cusName) {
		return this.invoke("ExistsCusName", {"cusName":cusName}, this.ExistsCusName.getArguments().slice(1));
	},
	ExistsMobilePhone: function(mobilePhone) {
		return this.invoke("ExistsMobilePhone", {"mobilePhone":mobilePhone}, this.ExistsMobilePhone.getArguments().slice(1));
	},
	GetUrlTitle: function(url) {
		return this.invoke("GetUrlTitle", {"url":url}, this.GetUrlTitle.getArguments().slice(1));
	},
	DateTimeDiff: function(startTime, endTime) {
		return this.invoke("DateTimeDiff", {"startTime":startTime, "endTime":endTime}, this.DateTimeDiff.getArguments().slice(2));
	},
	DateDiff: function(date) {
		return this.invoke("DateDiff", {"date":date}, this.DateDiff.getArguments().slice(1));
	},
	GetSMSSendCus: function(id) {
		return this.invoke("GetSMSSendCus", {"id":id}, this.GetSMSSendCus.getArguments().slice(1));
	},
	GetEmailSendCus: function(id) {
		return this.invoke("GetEmailSendCus", {"id":id}, this.GetEmailSendCus.getArguments().slice(1));
	},
	GetSMSKeyword: function(msg) {
		return this.invoke("GetSMSKeyword", {"msg":msg}, this.GetSMSKeyword.getArguments().slice(1));
	},
	ShiBieMobilePhone: function(phone) {
		return this.invoke("ShiBieMobilePhone", {"phone":phone}, this.ShiBieMobilePhone.getArguments().slice(1));
	},
	CheckUserMobilePhone: function() {
		return this.invoke("CheckUserMobilePhone", {}, this.CheckUserMobilePhone.getArguments().slice(0));
	},
	CheckSMSCount: function() {
		return this.invoke("CheckSMSCount", {}, this.CheckSMSCount.getArguments().slice(0));
	},
	GetEmailConfigList: function() {
		return this.invoke("GetEmailConfigList", {}, this.GetEmailConfigList.getArguments().slice(0));
	},
	GetSelectCusCount: function(selectUserID, typeid, parameter) {
		return this.invoke("GetSelectCusCount", {"selectUserID":selectUserID, "typeid":typeid, "parameter":parameter}, this.GetSelectCusCount.getArguments().slice(3));
	},
	GetFileContent: function(fileName, splitID) {
		return this.invoke("GetFileContent", {"fileName":fileName, "splitID":splitID}, this.GetFileContent.getArguments().slice(2));
	},
	GetTaskSendCus: function(id) {
		return this.invoke("GetTaskSendCus", {"id":id}, this.GetTaskSendCus.getArguments().slice(1));
	},
	SendOldMobilePhoneCode: function() {
		return this.invoke("SendOldMobilePhoneCode", {}, this.SendOldMobilePhoneCode.getArguments().slice(0));
	},
	IsBindMobilePhone: function(mobilephone) {
		return this.invoke("IsBindMobilePhone", {"mobilephone":mobilephone}, this.IsBindMobilePhone.getArguments().slice(1));
	},
	SendBindMobilePhoneVerifiedCode: function(mobilePhone, oldCode) {
		return this.invoke("SendBindMobilePhoneVerifiedCode", {"mobilePhone":mobilePhone, "oldCode":oldCode}, this.SendBindMobilePhoneVerifiedCode.getArguments().slice(2));
	},
	SendDynamicPassword: function(sendType) {
		return this.invoke("SendDynamicPassword", {"sendType":sendType}, this.SendDynamicPassword.getArguments().slice(1));
	},
	CloseDynamicPassword: function() {
		return this.invoke("CloseDynamicPassword", {}, this.CloseDynamicPassword.getArguments().slice(0));
	},
	GetSelectFollowTaskNodeString: function(tasknodeid) {
		return this.invoke("GetSelectFollowTaskNodeString", {"tasknodeid":tasknodeid}, this.GetSelectFollowTaskNodeString.getArguments().slice(1));
	},
	GetSelectUserList: function(IDList, selecttype, dialogName, tagid) {
		return this.invoke("GetSelectUserList", {"IDList":IDList, "selecttype":selecttype, "dialogName":dialogName, "tagid":tagid}, this.GetSelectUserList.getArguments().slice(4));
	},
	GetGroupList: function() {
		return this.invoke("GetGroupList", {}, this.GetGroupList.getArguments().slice(0));
	},
	GetRoleList: function() {
		return this.invoke("GetRoleList", {}, this.GetRoleList.getArguments().slice(0));
	},
	GetCommonMultiSelect: function(tableID, idlist) {
		return this.invoke("GetCommonMultiSelect", {"tableID":tableID, "idlist":idlist}, this.GetCommonMultiSelect.getArguments().slice(2));
	},
	GetDealProductSelect: function(productIDList, selectmode, dealID) {
		return this.invoke("GetDealProductSelect", {"productIDList":productIDList, "selectmode":selectmode, "dealID":dealID}, this.GetDealProductSelect.getArguments().slice(3));
	},
	url: '/ajaxpro/Kehu51.Team.UI.Web.App.Ajax.GetData,Kehu51.Team.UI.Web.ashx'
}));
Kehu51.Team.UI.Web.App.Ajax.GetData = new Kehu51.Team.UI.Web.App.Ajax.GetData_class();

