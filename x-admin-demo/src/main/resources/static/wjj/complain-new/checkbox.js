﻿var arrClass = [
    "checkbox-item",
    "checkbox-item checkbox-item-mouseleave",
    "checkbox-item checkbox-item-mouseover",
    "checkbox-item checkbox-item-checked",
    "checkbox-item checkbox-item-check-disabled",
    "checkbox-item checkbox-item-uncheck-disabled"
];

var arrClass2 = [
    "radiobox-item",
    "radiobox-item radiobox-item-mouseleave",
    "radiobox-item radiobox-item-mouseover",
    "radiobox-item radiobox-item-checked",
    "radiobox-item radiobox-item-check-disabled",
    "radiobox-item radiobox-item-uncheck-disabled"
];

$(function () {
    bindEvent();
});

function clearClass(obj, defaultClass) {
    var control = hasAttr(obj, "data-control");
    if (control == "radio") {
        if ($(obj).hasClass(arrClass2[1])) $(obj).removeClass(arrClass2[1]);
        if ($(obj).hasClass(arrClass2[2])) $(obj).removeClass(arrClass2[2]);
        if ($(obj).hasClass(arrClass2[3])) $(obj).removeClass(arrClass2[3]);
        if ($(obj).hasClass(arrClass2[4])) $(obj).removeClass(arrClass2[4]);
        if ($(obj).hasClass(arrClass2[5])) $(obj).removeClass(arrClass2[5]);
        if (defaultClass) {
            $(obj).addClass(arrClass2[0]);
        }
    } else {
        if ($(obj).hasClass(arrClass[1])) $(obj).removeClass(arrClass[1]);
        if ($(obj).hasClass(arrClass[2])) $(obj).removeClass(arrClass[2]);
        if ($(obj).hasClass(arrClass[3])) $(obj).removeClass(arrClass[3]);
        if ($(obj).hasClass(arrClass[4])) $(obj).removeClass(arrClass[4]);
        if ($(obj).hasClass(arrClass[5])) $(obj).removeClass(arrClass[5]);
        if (defaultClass) {
            $(obj).addClass(arrClass[0]);
        }
    }


}

function bindEvent() {
    $(".checkbox-item").bind("mouseover", function () {
        if (!hasDisabled(this)) {
            var selected = $(this).attr("data-selected");
            if (selected == 0) {
                clearClass(this, false);
                $(this).addClass(arrClass[2]);
            }
        }
    });

    $(".radiobox-item").bind("mouseover", function () {
        if (!hasDisabled(this)) {
            var selected = $(this).attr("data-selected");
            if (selected == 0) {
                clearClass(this, false);
                $(this).addClass(arrClass2[2]);
            }
        }
    });

    $(".checkbox-item").bind("mouseleave", function () {
        if (!hasDisabled(this)) {
            var selected = $(this).attr("data-selected");
            if (selected == 0) {
                clearClass(this, false);
                $(this).addClass(arrClass[1]);
            }
        }
    });

    $(".radiobox-item").bind("mouseleave", function () {
        if (!hasDisabled(this)) {
            var selected = $(this).attr("data-selected");
            if (selected == 0) {
                clearClass(this, false);
                $(this).addClass(arrClass2[1]);
            }
        }
    });

    //多选
    $(".checkbox-item").bind("click", function () {
        var id = $(this).attr("data-type");
        if (id != "chkall")
            initClickEvent(this);
        else
            initCheckAll(this);
    });
    //单选
    $(".radiobox-item").bind("click", function () {
        if (!hasDisabled(this)) {
            clearRadioClass(this);
            initClickEvent(this);
        }
    });
    //label的点击事件
    $(".inputcheckbox2").find("label").each(function (index) {
        $(this).css("cursor", "pointer");
        $(this).bind("click", function () {
            var objCheckBox = $(this).prev();
            if (!hasDisabled(objCheckBox)) {
                initClickEvent(objCheckBox);
            }
        });
    });

    bindDisabled();
}

function bindDisabled() {
    //如果是禁用的选择框，那么移除所有效果
    $("." + arrClass[4]).unbind("mouseover").unbind("mouseleave").unbind("click");
    $("." + arrClass[5]).unbind("mouseover").unbind("mouseleave").unbind("click");

    $("."+arrClass2[4]).unbind("mouseover").unbind("mouseleave").unbind("click");
    $("."+arrClass2[5]).unbind("mouseover").unbind("mouseleave").unbind("click");
}

//初始化click事件
function initClickEvent(obj) {
    if (!hasDisabled(obj)) {
        clearClass(obj);
        var selected = $(obj).attr("data-selected");
        var control = hasAttr(obj, "data-control");
        if (selected == 0) {
            if (control == "radio") {
                //如果是单选框，那么先取消所有的选中
                clearRadioClass(obj);
                $(obj).addClass(arrClass2[3]);
            }
            else
                $(obj).addClass(arrClass[3]);
            $(obj).unbind("mouseover");
            $(obj).unbind("mouseleave");
            $(obj).attr("data-selected", "1");
        } else {
            if (control == "radio") {
                $(obj).addClass(arrClass2[3]);
            }
            else
                $(obj).addClass(arrClass[0]);
            $(obj).attr("data-selected", "0");
            $(obj).bind("mouseover", function () {
                clearClass(obj, false);
                if (control == "radio")
                    $(obj).addClass(arrClass2[2]);
                else
                    $(obj).addClass(arrClass[2]);
            });
            $(obj).bind("mouseleave", function () {
                clearClass(obj, false);
                if (control == "radio")
                    $(obj).addClass(arrClass2[1]);
                else
                    $(obj).addClass(arrClass[1]);
            });
        }
    }
}
//多选的全选操作
function initCheckAll(obj) {
    var isallcheck = 0;
    var selected = $(obj).attr("data-selected");
    if (selected == "1") {
        $(".inputcheckbox2").find("span").each(function (index) {
            clearClass(this);
            $(this).attr("data-selected", "0");
            $(this).addClass(arrClass[0]);
            $(this).bind("mouseover", function () {
                clearClass(this, false);
                $(this).addClass(arrClass[2]);
            });
            $(this).bind("mouseleave", function () {
                clearClass(this, false);
                $(this).addClass(arrClass[1]);
            });
        });
    }
    else {
        $(".inputcheckbox2").find("span").each(function (index) {
            clearClass(this, false);
            $(this).attr("data-selected", "1");
            $(this).addClass(arrClass[3]);
            $(this).unbind("mouseover");
            $(this).unbind("mouseleave");
        });
    }
}
//清楚所有的radio的样式
function clearRadioClass(obj) {
    var dataType = $(obj).attr("data-type");
    $(".inputcheckbox2").find("span").each(function (index) {
        if (dataType == $(this).attr("data-type")) {
            clearClass(this, false);
            $(this).attr("data-selected", "0");
            $(this).addClass(arrClass2[0]);
        }
    });
}
//是否禁用
function hasDisabled(obj) {
    var className = $(obj).attr("class");
    if (className == arrClass2[4] || className == arrClass2[5]||className==arrClass[4]||className==arrClass[5]) {
        return true;
    }
    return false;
}


//获取选中的value
function GetValue(type) {
    var strValue = "";
    $(".checkbox-item-checked").each(function (index) {
        var thisType = $(this).attr("data-type");
        if (type == thisType) {
            var value = $(this).attr("data-value");
            strValue += value + ",";
        }
    });

    $(".checkbox-item-check-disabled").each(function (index) {
        var thisType = $(this).attr("data-type");
        if (type == thisType) {
            var value = $(this).attr("data-value");
            strValue += value + ",";
        }
    });

    if (strValue != "") strValue = strValue.substring(0, strValue.length - 1);
    return strValue;
}
//获取radio的value
function GetRadioValue(type) {
    var strValue = "";
    $(".radiobox-item-checked").each(function (index) {
        var thisType = $(this).attr("data-type");
        if (type == thisType) {
            var value = $(this).attr("data-value");
            strValue = value;
            return;
        }
    });

    $(".radiobox-item-check-disabled").each(function (index) {
        var thisType = $(this).attr("data-type");
        if (type == thisType) {
            var value = $(this).attr("data-value");
            strValue = value;
            return;
        }
    });
    
    return strValue;
}
//获取属性data-id的内容
function GetText(type) {
    var strValue = "";
    $(".checkbox-item-checked").each(function (index) {
        var thisType = $(this).attr("data-type");
        if (type == thisType) {
            var value = $(this).attr("data-id");
            strValue += value + ",";
        }
    });

    if (strValue != "") strValue = strValue.substring(0, strValue.length - 1);
    return strValue;
}
//获取radio的data-id的内容
function GetRadioText(type) {
    var strValue = "";
    $(".radiobox-item-checked").each(function (index) {
        var thisType = $(this).attr("data-type");
        if (type == thisType) {
            var value = $(this).attr("data-id");
            strValue = value;
            return;
        }
    });

    return strValue;
}

//设置checkbox选中项
function SetCheck(checkString) {
    $(".checkbox-item").each(function (index) {
        var value = $(this).attr("data-value");
        if (checkString.indexOf(value) > -1) {
            clearClass(this, false);
            $(this).addClass(arrClass[3]);
            $(this).attr("data-selected", "1");
        }
    });
}
//设置radio选中项
function SetRadio(checkString) {
    $(".radiobox-item").each(function (index) {
        clearClass(this, true);
        var value = $(this).attr("data-value");
        if (checkString.indexOf(value) > -1) {           
            $(this).addClass(arrClass2[3]);
            $(this).attr("data-selected", "1");
        }
    });
}

function SetRadioCheck(type, value, isenable) {
    $(".radiobox-item").each(function (index) {        
        var _value = $(this).attr("data-value");
        var _type = $(this).attr("data-type");
        if (_type == type) {
            clearClass(this, true);
            if (value == _value) {
                $(this).addClass(arrClass2[3]);
                $(this).attr("data-selected", "1");
                $(this).unbind("click");
            } else {
                if (isenable) {
                    $(this).addClass(arrClass2[5]);
                }
                $(this).attr("data-selected", "0");
            }
        }
    });
}

//判断对象属性是否存在，存在返回属性值，否则返回空
function hasAttr(obj, attrName) {
    if (typeof ($(obj).attr(attrName)) == "undefined") return "";
    else return $(obj).attr(attrName);
}

//覆盖common.js中的GetCheckedID
function GetCheckedID(type) {
    return GetValue(type);
}