    function AddLinkMan(formName) {

        var cusID = document.getElementById("rptCusID").value;
        if (cusID == "" || cusID == "0") {
            Dialog.alert("请先选择客户！");
        } else {

            var diag = new Dialog();
            diag.ID = "LinkManEdit";
            diag.Width = 800;
            diag.Height = 600;
            diag.MessageTitle = "添加新联系人";
            diag.Message = "您可以为一个公司添加多个联系人，如张经理，王总等！联系人是指您与该公司的谁进行联系。";
            diag.Title = document.getElementById("rptCusName").value + "-添加联系人";
            diag.URL = "/app/Customers/LinkMan/LinkManEdit.aspx?Mode=Add&CusID=" + document.getElementById("rptCusID").value + "&CloseType=UpdateSelect&FormName="+formName+"&ControlName=rptUpdateLinkMan";
            diag.show();
        }
    }

    // ACLOUD 常用JS函数
    function getBid(s) {
        return document.getElementById(s);
    }
    function getBmc(s) {
        return document.getElementByName(s);
    }

    //显示分类列表
    function SetLinkMan(tid, cid,formName,defaultid) {
        var slt = getBid("rptLinkMan");
        slt.style.maxWidth = "120px";
        var v = Kehu51.Team.UI.Web.App.Ajax.GetData.GetLinkMan(cid).value; // 类的名称
        if (v.Rows.length == 0) {
            slt.options.length = 0; //清空之前的
            Dialog.confirm('该客户还没有联系人，是否现在添加？', function () {
                var diag = new Dialog();
                diag.Width = 800;
                diag.Height = 382;
                diag.Title = document.getElementById("rptCusName").value + "-添加联系人";
                //diag.URL = "../LinkMan/LinkManEdit.aspx?Mode=Add&CusID=" + cid + "&CloseType=UpdateSelect&FormName="+formName;
				diag.URL = "/app/Customers/LinkMan/LinkManEdit.aspx?Mode=Add&CusID=" + cid + "&CloseType=UpdateSelect&FormName="+formName+"&ControlName=rptUpdateLinkMan";
                diag.show();
            }, function () { parent.Dialog.close(); });
            return;
        }

        if (v != null) {
            if (v != null && typeof (v) == "object" && v != null) {
                slt.length = 0;
                slt.options.length = 0;
                //slt.options.add(new Option("请选择",0));
            }
            for (var i = 0; i < v.Rows.length; i++) {
                var txt = v.Rows[i].RealName; //这个地方需要注意区分大小写
                //var ini = v.Rows[i].Name_Initial;
                var vol = v.Rows[i].LinkID;
                slt.options.add(new Option(txt, vol));
            }
			if(defaultid != 0) slt.value = defaultid;
        }
        showLinkManLoading(false);
    }



    function SetLinkManList(formName) {
        showLinkManLoading(true);
        var delay = setTimeout("SetLinkManList2('"+formName+"',0)", 100);
    }
    function SetLinkManListDefaultID(formName, defaultid) {
	    showLinkManLoading(true);
	    
        var delay = setTimeout("SetLinkManList2('"+formName+"',"+defaultid+")", 100);
    }

    function SetLinkManList2(formName, defaultid) {

        getBid("rptCusName").style.color = "#000000";//将文字颜色设置为黑色（默认显示的灰色）
        var cid = getBid("rptCusID").value;
        var tid = document.getElementById("rptCusType").value;

        if (tid == 2) {
            //如果是个人客户则联系人指定为客户本人
            var slt = getBid("rptLinkMan");
            var cname = getBid("rptCusName").value;
            slt.length = 0;
            slt.options.length = 0;
            slt.options.add(new Option(cname, 0));
            //slt.disabled = true;//禁用控件
            showLinkManLoading(false);
            IsShowLinkMan(false);
            showLinkManAddBotton(false);
        }
        else {
            SetLinkMan(tid, cid, formName, defaultid);
            IsShowLinkMan(true);
            showLinkManAddBotton(true);
        }
    }
    function IsShowLinkMan(yes) {
        try{
            if (yes) {
                getBid("rptLinkManTD").style.display = "";
                getBid("rptLinkManTD2").style.display = "";
            } else {
                getBid("rptLinkManTD").style.display = "none";
                getBid("rptLinkManTD2").style.display = "none";
            }
        } catch (err) { }
        StartSetSize();//重置窗口大小
        //StartAutoSize(fromName);
    }

    function ClickLinkMan(fromName) {
        var cid = getBid("rptCusID").value;
        var cusType = document.getElementById("rptCusType").value;
        if (cid == "") {
            Dialog.alert("请先选择客户！", function () { SelectCus(cusType,'1', 'rptCusID', 'rptCusName',fromName , 1) });
        }
    }


	function showLinkManLoading(show) {
        if (show) {
            document.getElementById("rptLinkManLoading").style.display = "";
        }
        else {
            document.getElementById("rptLinkManLoading").style.display = "none";
        }
    }

    function showLinkManAddBotton(show) {
        if (show) {
            document.getElementById("rptShowAddLinkManBotton").style.display = "";
        }
        else {
            document.getElementById("rptShowAddLinkManBotton").style.display = "none";
        }
    }

	function showLinkManView(show) {
        if (show) {
            document.getElementById("rptLinkManViewDetail").style.display = "";
        }
        else {
            document.getElementById("rptLinkManViewDetail").style.display = "none";
        }
    }

    function OpenLinkManDetail(){
        var cusid = getBid("rptLinkMan").value;
		if(cusid == "" || cusid == "0"){
			Dialog.alert("请先选择客户！");
			return;
		}
		LinkManDetail(cusid);
	}