document.domain = "kehu51.com";

$(function () {
    //判断是否是IE10及IE10以下，如果是，则引入JQUERY 1.X的版本
    var ieVersion = IEVersion();
    if (ieVersion >= 6 && ieVersion<=10) {
        var newscript = document.createElement('script');
        newscript.setAttribute('type', 'text/javascript');
        newscript.setAttribute('src', "/Resource/js/jquery/jquery.1.12.4.js");

        var head = document.getElementsByTagName('head')[0];
        head.appendChild(newscript);
    }
})

//判断IE版本
function IEVersion() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  
    var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器  
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    if (isIE) {
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if (fIEVersion == 7) {
            return 7;
        } else if (fIEVersion == 8) {
            return 8;
        } else if (fIEVersion == 9) {
            return 9;
        } else if (fIEVersion == 10) {
            return 10;
        } else {
            return 6;//IE版本<=7
        }
    } else if (isEdge) {
        return 0;//edge
    } else if (isIE11) {
        return 11; //IE11  
    } else {
        return -1;//不是ie浏览器
    }
}

//var $ = function (id) { return document.getElementById(id); };
function CheckAll(form) {
    for (var i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];
        if (e.name != 'chkall' && e.disabled != true)
            e.checked = form.chkall.checked;
    }
}
//弹出或关闭选择和排序框
function showWorkList(id, value) {
    //document.getElementById(id).style.display = value;
    //parent.ReSetIframeHeight();

    //2017-06-08 qianlong 重新调整
    var count = $("[id=" + id + "]").length;
    $("[id=" + id + "]").each(function (index) {
        if (count == 2 && index == 0) {
            $(this).hide();
        } else {
            if (value == "")
                $(this).show();
            else
                $(this).hide();
        }
    });
}
///提交按钮变灰
function CommitButtom(buttomID, buttomCSS, buttomText) {
    if (buttomID == null) buttomID = "rptCommit";
    if (buttomCSS == null) buttomCSS = "B";
    if (buttomText == null) buttomText = "保存";
    document.getElementById(buttomID).style.display = "none";
    document.getElementById("rptCommitHui").innerHTML = "<input type=\"button\" value=\"" + buttomText + "\" class=\"BigButton" + buttomCSS + "\" disabled=\"disabled\" style=\"color:#cccccc\" />";

    ShowDialogLoading();
}



//新的【2015-7-23】
function CommitButton(btnID) {
    if (btnID == null) btnID = "rptCommit";

    var btnCSS = document.getElementById(btnID).className;
    var btnText = document.getElementById(btnID).value;

    document.getElementById(btnID).style.display = "none";
    document.getElementById("rptCommitHui").innerHTML = "<input type=\"button\" value=\"" + btnText + "\" class=\"" + btnCSS + "\" disabled=\"disabled\"/>";

    ShowDialogLoading("正在保存，请等待...");
}


//[2016-10-11] 新建客户界面增加了保存后继续新建   按钮后加的，可以同时禁用多个按钮
function CommitButtonList(buttonList) {
    var btnID, btnObj;
    for (var i = 0; i < buttonList.length; i++) {

        btnID = buttonList[i];
        btnObj = document.getElementById(btnID);

        if (btnObj) {
            var btnCSS = btnObj.className;
            var btnText = btnObj.value;

            btnObj.style.display = "none";
            document.getElementById(btnID + "Hui").innerHTML = "<input type=\"button\" value=\"" + btnText + "\" class=\"" + btnCSS + "\" disabled=\"disabled\"/>";
        }
    }

    ShowDialogLoading("正在保存，请等待...");
}
//2017-09-28 qianlong 重置按钮
function ResetButtomList(buttonList) {
    var btnID, btnObj;
    for (var i = 0; i < buttonList.length; i++) {

        btnID = buttonList[i];
        btnObj = document.getElementById(btnID);

        if (btnObj) {
            var btnCSS = btnObj.className;
            var btnText = btnObj.value;
            btnObj.style.display = "";
            document.getElementById(btnID + "Hui").innerHTML = "";
        }
    }

    ShowDialogLoading("正在保存，请等待...");
}

function ShowDialogLoading(loadcontent) {
    try {
        //2017-09-06 qianlong 如果弹出3个以上的Dialog，那么loding无法显示
        /*var loadingObj = top.document.getElementById("zDialog_Iframe_Loading");
        loadingObj.style.display = "";//显示loading*/

        var loadingObj = top.document.getElementsByName("zDialog_Iframe_Loading");
        for (var i = 0; i < loadingObj.length; i++) {
            loadingObj[i].style.display = "";//显示loading
        }
        if (loadcontent) top.document.getElementById("dialog_loadcontent").innerHTML = loadcontent;
    } catch (err) { }
}


function GetMao() {
    var url = document.location.toString();
    var index = url.indexOf('#');
    if (index == -1) return "";
    url = url.replace(url.substr(0, index) + "#", "");


    var windex = url.indexOf('?');
    if (windex != -1) url = url.substr(0, windex);

    return url;
}

function getvalue(name) {
    var str = window.location.href;
    if (str.indexOf(name) != -1) {
        var pos_start = str.indexOf(name) + name.length + 1;
        var pos_end = str.indexOf("&", pos_start);
        if (pos_end == -1) {
            return str.substring(pos_start);
        }
        else {
            return str.substring(pos_start, pos_end)
        }
    }
    else {
        //return "没有这个name值";
        return "";
    }
}


function getOs() {
    var OsObject = "";
    if (navigator.userAgent.indexOf("MSIE") > 0) {
        return "MSIE";
    }
    if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
        return "Firefox";
    }
    if (isSafari = navigator.userAgent.indexOf("Safari") > 0) {
        return "Safari";
    }
    if (isCamino = navigator.userAgent.indexOf("Camino") > 0) {
        return "Camino";
    }
    if (isMozilla = navigator.userAgent.indexOf("Gecko/") > 0) {
        return "Gecko";
    }
    //2017-06-26 lijingbo IE11及以上版本，需如下判断
    if (!!window.ActiveXObject || "ActiveXObject" in window) {
        return "MSIE";
    }
}

/*function isExitsVariable(variableName) {

    try {
        if (typeof (variableName) == "undefined") {
            return false;
        } else {
            return true;
        }
    } catch (e) { }
    return false;
}*/






//表格隔行变色，鼠标经过变化，点击变色
//senfe("表格名称","奇数行背景","偶数行背景","鼠标经过背景","点击后背景");
function senfe(o, a, b, c, d) {
    var t = document.getElementById(o).getElementsByTagName("tr");
    for (var i = 0; i < t.length; i++) {
        var cls = t[i].getAttribute("class");
        if (cls == "TableFooter") continue;
        t[i].style.backgroundColor = (t[i].sectionRowIndex % 2 == 0) ? a : b;
        //t[i].onclick=function(){
        // if(this.x!="1"){
        //  this.x="1";//本来打算直接用背景色判断，FF获取到的背景是RGB值，不好判断
        //  this.style.backgroundColor=d;
        // }else{
        //  this.x="0";
        // this.style.backgroundColor=(this.sectionRowIndex%2==0)?a:b;
        //}
        //}


        /*t[i].onclick=function(){
        if(this.x!="1"){
         this.x="1";//本来打算直接用背景色判断，FF获取到的背景是RGB值，不好判断
         this.style.backgroundColor=d;
        }else{
         this.x="0";
         this.style.backgroundColor=(this.sectionRowIndex%2==0)?a:b;
        }
       }*/




        t[i].onmouseover = function () {
            if (this.x != "1") this.style.backgroundColor = c;
        }
        t[i].onmouseout = function () {
            if (this.x != "1") this.style.backgroundColor = (this.sectionRowIndex % 2 == 0) ? a : b;
        }
    }
}

//兼容firefox的js插入代码，替换掉IE中的 insertAdjacentHTML
//where：插入位置。包括beforeBegin,beforeEnd,afterBegin,afterEnd。
//el：用于参照插入位置的html元素对象
//html：要插入的html代码
function insertHtml(where, el, html) {
    where = where.toLowerCase();
    if (el.insertAdjacentHTML) {
        switch (where) {
            case "beforebegin":
                el.insertAdjacentHTML('BeforeBegin', html);
                return el.previousSibling;
            case "afterbegin":
                el.insertAdjacentHTML('AfterBegin', html);
                return el.firstChild;
            case "beforeend":
                el.insertAdjacentHTML('BeforeEnd', html);
                return el.lastChild;
            case "afterend":
                el.insertAdjacentHTML('AfterEnd', html);
                return el.nextSibling;
        }
        throw 'Illegal insertion point -> "' + where + '"';
    }
    var range = el.ownerDocument.createRange();
    var frag;
    switch (where) {
        case "beforebegin":
            range.setStartBefore(el);
            frag = range.createContextualFragment(html);
            el.parentNode.insertBefore(frag, el);
            return el.previousSibling;
        case "afterbegin":
            if (el.firstChild) {
                range.setStartBefore(el.firstChild);
                frag = range.createContextualFragment(html);
                el.insertBefore(frag, el.firstChild);
                return el.firstChild;
            } else {
                el.innerHTML = html;
                return el.firstChild;
            }
        case "beforeend":
            if (el.lastChild) {
                range.setStartAfter(el.lastChild);
                frag = range.createContextualFragment(html);
                el.appendChild(frag);
                return el.lastChild;
            } else {
                el.innerHTML = html;
                return el.lastChild;
            }
        case "afterend":
            range.setStartAfter(el);
            frag = range.createContextualFragment(html);
            el.parentNode.insertBefore(frag, el.nextSibling);
            return el.nextSibling;
    }
    throw 'Illegal insertion point -> "' + where + '"';
}


//返回指定checkbox名称的选中的value属性字符串，格式为 1,2,3
function GetCheckedID(controlName) {
    var strID = "";
    var idList = document.getElementsByName(controlName);
    for (var i = 0; i < idList.length; i++) {
        if (idList[i].checked) {
            strID += idList[i].value + ",";
        }
    }
    strID = strID.substring(0, strID.length - 1); //去掉最后的,
    return strID;
}


//type 1允许多选，2禁止多选 返回false表示条件为false
function GetCheckedTips(type, msg1, msg2) {
    var strID = GetCheckedID("rptID");
    if (strID == "") {
        Dialog.alert(msg1);
        return "false";
    } else {
        if (type == 2) {
            if (strID.split(',').length != 1) {
                Dialog.alert(msg2);
                return "false";
            }
        }
    }
    return strID;
}

//返回指定控件名称的value列表,以逗号隔开的
function GetValueList(controlName) {
    var valueList = "";
    var idList = document.getElementsByName(controlName);
    for (var i = 0; i < idList.length; i++) {
        valueList += idList[i].value + ",";
    }
    valueList = valueList.substring(0, valueList.length - 1); //去掉最后的,
    return valueList;
}



function CommonDelete() {
    parent.CloseLoading();
    var idList = GetCheckedID("rptID");
    if (idList == "") {
        Dialog.showError("请选择要删除的记录！");
        return;
    }
    if (document.getElementById("rptID"))
        document.getElementById("rptID").value = idList;
    if (!confirm('确定要删除选择的记录吗？')) return;

    document.getElementById("IsDelete").value = "true";
    document.forms[0].submit();
}


//获取指定iframe的高度
function GetCwinHeight(obj) {
    var cwin = obj;
    if (cwin && !window.opera) {
        if (cwin.contentDocument && cwin.contentDocument.body.offsetHeight)
            return cwin.contentDocument.body.offsetHeight;
        else if (cwin.Document && cwin.Document.body.scrollHeight)
            return cwin.Document.body.scrollHeight;
    }
}



//获取浏览器可用区域高度
function GetBrowserHeight() {
    var winHeight = 0;
    //获取窗口高度
    if (parent.window.innerHeight)
        winHeight = parent.window.innerHeight;
    else if ((parent.document.body) && (parent.document.body.clientHeight))
        winHeight = parent.document.body.clientHeight;

    /*nasty hack to deal with doctype swith in IE*/
    //通过深入Document内部对body进行检测，获取窗口大小
    if (parent.document.documentElement && parent.document.documentElement.clientHeight) {
        winHeight = parent.document.documentElement.clientHeight;
    }
    return winHeight;
}
//获取iframe内容高度，兼容chrome
function GetContentHeight() {
    if (getOs() == "MSIE") return document.body.scrollHeight;
    else return document.documentElement.scrollHeight;
}

//重新设置页面高度
function SetSize() {
    //2017-5-12 huangjinliang 直接在js中获取当前Dialog的ID，不用再传进来了，原来的StartSetSize()可以继续使用
    var dialogID = window.frameElement.id.replace("_DialogFrame_", "");//iFrame的ID，得到Dialog的ID
    autoSize(dialogID);
}
function StartSetSize() {

    //if (!document.getElementById("top1")) {//2017-5-16 huangjinliang 界面中包含有top1的div的，就是固定了底下的保存按钮行的，不执行
    //    setTimeout("SetSize()", 100);
    //}

    //某些界面如果不用执行这个方法，可以加上这个标签，比如客户导入界面选择所有者返回后 [huangjinliang 2018-1-30]
    if (!document.getElementById("NoSetDialogSizeSpan")) {

        setTimeout("SetDialogSize()", 100);
    }

}

//2018-4-20 lx 根据具体的ID来设置高度，比如弹出的二级页面中的formid
function StartSetSizeById(strFormId) {
    //某些界面如果不用执行这个方法，可以加上这个标签，比如客户导入界面选择所有者返回后 [huangjinliang 2018-1-30]
    if (!document.getElementById("NoSetDialogSizeSpan")) {
        setTimeout("SetDialogSizeById('" + strFormId + "')", 100);
    }

}

function StartSetSizeByIdNow(strFormId) {
    //某些界面如果不用执行这个方法，可以加上这个标签，比如客户导入界面选择所有者返回后 [huangjinliang 2018-1-30]
    if (!document.getElementById("NoSetDialogSizeSpan")) {
        SetDialogSizeById(strFormId);
    }

}

//2017-04-11 qianlong Begin
function autoSize(dialogID) {
    var h = GetContentHeight();
    var maxH = GetBrowserHeight() - 100;//浏览器的可用高度减掉窗口边框

    if (h > maxH) {
        h = maxH;//最高不超过浏览器的可用高度
    }
    var w = parent.window.document.getElementById("_Container_" + dialogID).style.width;
    var dialog = new Dialog();
    dialog.ID = dialogID;
    dialog.setSize(null, h);
}


//2017-10-25 hjl：使用这个统一设置Dialog高度，适用于包含div1的界面，保存界面使用最大高度，并且始终显示下面的按钮栏
function SetDialogSize() {

    //var dialogID = window.frameElement.id.replace("_DialogFrame_", "");//iFrame的ID，得到Dialog的ID
    //debugger;
    //2018-07-02 qianlong 获取dialogID
    var dialogID = top.arrDialog[top.arrDialog.length - 1];//获取数组中的最后一个ID

    //StartSetSizeMinHeight(dialogID);
    var contentH = GetContentHeight() + 1;//内容高度，不加1容易出现滚动条
    var maxH = GetBrowserHeight() - 100;//浏览器的可用高度减掉窗口边框

    var isH = false;
    if (contentH > maxH) {
        contentH = maxH;//最高不超过浏览器的可用高度
        isH = true;
    }

    //如果是详情界面，限制最小高度不小于Dialog原始设置的高度，避免有的时候内容太少，导致窗口很小
    if (dialogID.indexOf('Detail') != -1) {
        //2017-10-25 hjl 最小高度不小于原始高度
        var oldHeight = parent.window.document.getElementById("_Container_" + dialogID).style.height;
        var oldH = oldHeight.replace("px", "");
        if (contentH < oldH) {
            contentH = oldH;
            isH = true;
        }
    }
    var dialog = new Dialog();
    dialog.ID = dialogID;

    dialog.setSize(null, contentH);

    if (isH) {
        if (document.getElementById("top1")) {
            var bottomHeight = 40;
            SetHeight(dialogID, bottomHeight);
        }
    }


}

function SetDialogSizeById(strFormId) {
    var dialogID = window.frameElement.id.replace("_DialogFrame_", "");//iFrame的ID，得到Dialog的ID

    //StartSetSizeMinHeight(dialogID);
    var contentH = $("#" + strFormId+"").height() + 1;//GetContentHeight() + 1;//内容高度，不加1容易出现滚动条
    var maxH = GetBrowserHeight() - 100;//浏览器的可用高度减掉窗口边框

    var isH = false;
    if (contentH > maxH) {
        contentH = maxH;//最高不超过浏览器的可用高度
        isH = true;
    }

    //如果是详情界面，限制最小高度不小于Dialog原始设置的高度，避免有的时候内容太少，导致窗口很小
    if (dialogID.indexOf('Detail') != -1 && dialogID.toLowerCase() != "worklistdetail") {
        //2017-10-25 hjl 最小高度不小于原始高度
        var oldHeight = parent.window.document.getElementById("_Container_" + dialogID).style.height;
        var oldH = oldHeight.replace("px", "");
        if (contentH < oldH) {
            contentH = oldH;
            isH = true;
        }
    }
    var dialog = new Dialog();
    dialog.ID = dialogID;

    dialog.setSize(null, contentH);

    if (isH) {
        if (document.getElementById("top1")) {
            var bottomHeight = 40;
            SetHeight(dialogID, bottomHeight);
        }
    }


}

/*function StartSetSizeMinHeight(dialogID) {


    var h = GetContentHeight()+1;//不加1容易出现滚动条
    var maxH = GetBrowserHeight() - 100;//浏览器的可用高度减掉窗口边框

    if (h > maxH) {
        h = maxH;//最高不超过浏览器的可用高度
    }

    //2017-12-25 hjl 最小高度不小于原始高度
    var oldHeight = parent.window.document.getElementById("_Container_" + dialogID).style.height;
    var oldH = oldHeight.replace("px", "");
    if (h < oldH) h = oldH;

    var dialog = new Dialog();
    dialog.ID = dialogID;

    dialog.setSize(null, h);
}*/

//2016-12-28 qianlong 设置div的高度
function SetHeight(dialog, h) {

    var topDiv = document.getElementById("top1");
    var parentH = parent.window.document.getElementById("_Container_" + dialog).style.height;

    var docH = parseInt(parentH) - h;
    topDiv.style.height = docH + "px";
}

function StartAutoSize(dialogID) {
    if (!document.getElementById("top1")) {//2017-5-16 huangjinliang 界面中包含有top1的div的，就是固定了底下的保存按钮行的，不执行
        setTimeout("autoSize('" + dialogID + "')", 100);
    }
}
//End

function ShowUserInfo(userid) {
    //阻止事件冒泡
    if (typeof (event) != "undefined") {
        event.stopPropagation();
    }

    if (userid == 0)
        return;

    var diag = new Dialog();
    diag.ID = "ShowUserInfo";
    diag.Width = 700;
    diag.Height = 460;
    diag.Title = "用户基本信息";
    diag.URL = "/app/team/member/showuserinfo.aspx?UserID=" + userid + "&dialog=ShowUserInfo";
    diag.show();
}

//2017-05-18 qianlong 固定
function fixedToolbar() {
    return;
    //暂时注释下面内容
    var topH = $(".detailTop").height();
    var winH = $(window).height();
    var h = winH - topH - 10;
    $(".detailContent").height(h);

    $(window).resize(function () {

    });

}

//2017-5-23 设置为空的提示信息
function SetContentEmptyTips(isAllContent, recordCount, tips1, tips2, addText, addClickFunction, importText, importClickFunction) {
    if (recordCount == 0) {
        var tipsHTML;
        if (isAllContent) {
            tipsHTML = "<center><div style=\"margin-top:80px;font-size:17px;color:#666666;\"><img src=\"/theme/default/images/ku_100.png\" style=\"vertical-align:middle;margin-right:20px\"/><br><br><br>" + tips1;
            if (addText != "") {
                tipsHTML += "<input type=\"button\" style=\"margin-left:10px\" value=\"" + addText + "\" onclick=\"" + addClickFunction + ";\" class=\"btn2 btn2-commit\" />";
            }
            if (importText != "") {
                tipsHTML += "&nbsp;&nbsp;或&nbsp;&nbsp;<input type=\"button\" value=\"" + importText + "\" onclick=\"" + importClickFunction + ";\" class=\"btn2 btn2-commit\" />";
            }
            tipsHTML += "</div></center>";



        } else {

            if (addText == "手动录入客户") addText = "录客户";
            if (addText == "手动录入产品") addText = "录产品";

            tipsHTML = "<center><div style=\"margin-top:80px;font-size:17px;color:#666666;\"><img src=\"/theme/default/images/ku_100.png\" style=\"vertical-align:middle;margin-right:20px\"/><br><br><br>" + tips2;

            if (addText != "") {
                tipsHTML += "<input type=\"button\" style=\"margin-left:10px\" value=\"" + addText + "\" onclick=\"" + addClickFunction + ";\" class=\"btn2 btn2-commit\" />";
            }

            tipsHTML += "</div></center>";
        }
        document.getElementById("rptContentDiv").innerHTML = tipsHTML;

        var tableToolsObj = document.getElementById("rptTableTools");
        if (tableToolsObj) tableToolsObj.style.display = "none";//不显示工具栏

        //不显示底下的按首字母查找
        var buttonTools = document.getElementById("rptBottomTools");
        if (buttonTools) buttonTools.style.display = "none";


        //不显示指定成员
        /*var userListLiObj = document.getElementById("userListLI");
        if (userListLiObj) {
            userListLiObj.style.display = "none";
            document.getElementById("listLi").style.display = "none";
        }*/
    }
}
//2017-06-08 qianlong 固定列表界面的表头 滚动时

$(window).scroll(function (event) {
    var scTop = $(document).scrollTop();
    if ($("#rptList").length > 0 && $("#rptList").find("thead").length > 0) {
        if (scTop > 140) {
            fixedHead();
        } else {
            $("#stickyHead").fadeOut(50);
            $("#stickyHead").remove();
        }
    }

});

//$(function () {
//    scrollback();//找到滚动条的位置
//});

function fixedHead() {
    var tID = 0;
    if (typeof (tableID) != "undefined") tID = tableID;
    if (tID == 53 || tID == 88) {
        //工作日志、审批
        fixedHead_Other();
    }
    else {
        fixedHeadNormal();
    }
}
//工作日志
function fixedHead_Other() {
    //顶部的内容
    var dvHead = $(".pagecaption2_fz").clone();
    //工具栏
    var dvTools = $(".apptools").clone();

    var th = $("#rptList").find("thead").clone();
    var thr = th[0].rows[0];
    thr.style.backgroundColor = "#ffffff";
    var table = document.getElementById("rptList");
    var tr = table.rows[0];
    for (var i = 0; i < tr.cells.length; i++) {
        var td = tr.cells[i];
        var w = td.offsetWidth;
        var thc = thr.cells[i];
        thc.style.width = w + "px";
        if (i == 0) {
            var sp = thc.childNodes[0].childNodes[0];
            var cls = sp.getAttribute("class");
            var sel = sp.getAttribute("data-selected");
            thc.innerHTML = '<span class="inputcheckbox2 list"><span class="' + cls + '" data-type="chkall" data-id="chkall" data-value="" data-selected="' + sel + '" onclick="initCheckAll(this)"></span></span>'
        }
    }

    if ($("#stickyHead").length == 0) {
        $(document.body).append("<div id=\"stickyHead\" style='position: fixed;top: 0px;left: 0px;display: none;margin-left: 10px;margin-right: 10px;padding-top: 10px;background-color:#ffffff;'></div>");
        $("#stickyHead").fadeIn(10);
        /*$("#stickyHead").append("<div class=\"clearfix pagecaption2_fz\">" + $(dvHead).html() + "</div>");*/
        if ($(".editordialogtab").length > 0) {
            var dvTab = $(".editordialogtab").clone();
            $("#stickyHead").append("<div class=\"editordialogtab\" style=\"margin-bottom:6px\">" + $(dvTab).html() + "</div>");
        }
        $("#stickyHead").append("<div class=\"apptools\">" + $(dvTools).html() + "</div>");
        $("#stickyHead").append("<table id=\"stickyTable\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"TableList\">" + $(th).html() + "</table>");
        $("#stickyHead").find("span").each(function (index) {
            var id = $(this).attr("id");
            if (id == "listLi") {
                $(this).parent().parent().hide();
                $(this).remove();

            }
        });

        removeObject();
    }
    else {
        $("#stickyHead").fadeIn(10);
    }
}

//默认
function fixedHeadNormal() {
    //顶部的内容
    var dvHead = $(".pagecaption2_fz").clone();
    //工具栏
    var dvTools = $(".tools").clone();
    var th = $("#rptList").find("thead").clone();
    var thr = th[0].rows[0];
    thr.style.backgroundColor = "#ffffff";
    var table = document.getElementById("rptList");
    var tr = table.rows[0];
    for (var i = 0; i < tr.cells.length; i++) {
        var td = tr.cells[i];
        var w = td.offsetWidth;
        var thc = thr.cells[i];
        thc.style.width = w + "px";
        //thc.style.color = "#476074";
        //thc.style.color = "#ffffff";
        if (i == 0) {
            var sp = thc.childNodes[0].childNodes[0];
            if (typeof (sp) != "undefined") {
                var cls = sp.getAttribute("class");
                var sel = sp.getAttribute("data-selected");
                thc.innerHTML = '<span class="inputcheckbox2 list"><span class="' + cls + '" data-type="chkall" data-id="chkall" data-value="" data-selected="' + sel + '" onclick="initCheckAll(this)"></span></span>';
            }
        }
    }

    if ($("#stickyHead").length == 0) {
        $("#form1").append("<div id=\"stickyHead\" style='position: fixed;top: 0px;left: 0px;border-top:1px solid #e2e6ec;margin-left:10px;background-color:#f4f6f9;display:none;'></div>");

        $("#stickyHead").fadeIn(10);
        //$("#stickyHead").append("<div class=\"clearfix pagecaption2_fz\">" + $(dvHead).html() + "</div>");
        $("#stickyHead").append("<div class=\"tools\">" + $(dvTools).html() + "</div>");
        $("#stickyHead").append("<table id=\"stickyTable\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\" class=\"TableList\">" + $(th).html() + "</table>");

        $("#stickyHead").find("span").each(function (index) {
            var id = $(this).attr("id");
            if (id == "listLi") {
                $(this).parent().parent().hide();
                $(this).remove();
            }
        });

        removeObject();
    }
    else {
        $("#stickyHead").fadeIn(10);
    }
}

function removeObject() {
    var arrID = ["selectedItem", "selectuserid", "selectType", "arrow_down"];

    for (var i = 0; i < arrID.length; i++) {
        var id = arrID[i];
        var count = $("[id=" + id + "]").length;
        $("[id=" + id + "]").each(function (index) {
            if (count == 2 && index == 1)
                $(this).remove();
        });
    }
}


//2017-09-26 qianlong
function CloseAndRefresh() {
    //try { parent.window.frames["rptContentIframe"].location = parent.window.frames["rptContentIframe"].location; } catch (err) { parent.location.reload(); }
    var iframeobj = parent.window.frames["_Slider_DialogFrame"];
    if (iframeobj) { if (typeof (iframeobj.contentWindow) != "undefined") { iframeobj.contentWindow.location.href = iframeobj.contentWindow.location.href; } else { iframeobj.location.href = iframeobj.location.href; } parent.document.getElementById("rptContentLoading").style.display = "none"; } else {
        try {
            parent.window.frames["rptContentIframe"].location = parent.window.frames["rptContentIframe"].location;
        }
        catch (err) { parent.location.reload(); }
    }
    parent.Dialog.close();
}
function CloseWin() {
    parent.Dialog.close();
}


//设置地址栏中的参数
function GetParamets(args, key, value) {

    var newParam = "";
    if (args.indexOf(key) == -1) {
        var newKey = key.replace("&", "");
        newParam = newKey + "=" + value + "&" + args;
    } else {
        var arrParam = args.split('&');
        if (arrParam.length > 0) {
            for (var i = 0; i < arrParam.length; i++) {
                var param = arrParam[i];
                if (param == "") continue;
                var arr = param.split('=');
                var k = arr[0];
                var v = "";
                if (arr.length > 2) {
                    for (var j = 1; j < arr.length; j++) {
                        v += arr[j] + "=";
                    }
                    if (v != "") {
                        v = v.substring(0, v.length - 1);
                    }

                } else {
                    v = arr[1];
                }
                //一些参数可能存在特殊情况 例如：name和viewname，检索时都会检索到name，所有对参数名称进行特殊处理
                if (key.indexOf("&") > -1) {
                    key = key.replace("&", "");
                }
                if (key == k) {
                    newParam += k + "=" + value + "&";
                } else {
                    newParam += k + "=" + v + "&";
                }
            }
        }
    }
    //if (newParam != "") newParam = newParam.substring(0, newParam.length - 1);
    return newParam;
}




//获取第2个站点host [2018-1-23 huangjinliang：因为所有的功能面面都集中在一个站点，一个应用程序池压力太大，逐步将部分功能分散到第一个应用池]
function Get2Host() {
    var host = window.location.host.toLowerCase();

    var protocol = window.location.protocol.replace(":", "");

    if (host == "local.kehu51.com") {
        return protocol + "://local.kehu51.com";
    } else {
        if (host == "s27.kehu51.com" || host == "s23.kehu51.com" || host == "s25.kehu51.com" || host == "s18.kehu51.com" || host == "s28.kehu51.com" || host == "s26.kehu51.com") {
            host = host.replace(".kehu51.com", "-2.kehu51.com");
            return protocol + "://" + host;
        } else {
            return "";
        }
    }
}

//2018-05-15 LX post提交URL
function PostRefresh(url, params) {
    var temp = document.createElement("form");
    temp.action = url;
    temp.method = "post";
    temp.style.display = "none";

    for (var x in params) {
        var opt = document.createElement("textarea");
        opt.name = x;
        opt.value = params[x];
        temp.appendChild(opt);
    }

    document.body.appendChild(temp);
    temp.submit();

    return temp;
}

//2018-07-12 LX 获取URL上的参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var reg_rewrite = new RegExp("(^|/)" + name + "/([^/]*)(/|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    var q = window.location.pathname.substr(1).match(reg_rewrite);
    if (r != null) {
        return unescape(r[2]);
    } else if (q != null) {
        return unescape(q[2]);
    } else {
        return null;
    }
}