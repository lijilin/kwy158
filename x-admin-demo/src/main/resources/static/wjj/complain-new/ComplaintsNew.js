﻿function ComplaintsAddNew(cusid) {
    var diag = new Dialog();
    diag.ID = "ComplaintsEdit";
    diag.Width = 1000;
    diag.Height = 700;
    diag.MessageTitle = "新建一个投诉";
    diag.Message = "在这里，您可以将您觉得可能需要投诉的一个客户记录下来，这样日后可以对这个客户进行跟踪";
    diag.URL = "/app/Customers/ComplaintsNew/ComplaintsEdit.aspx?Mode=Add&CusID=" + cusid;
    diag.OnLoad = function () {
        var h = GetCwinHeight(diag.innerFrame);
        diag.setSize(null, h);
    };
    diag.show();
}
function ComplaintsDetailNew(oid) {
    var diag = new Dialog();
    diag.ID = "ComplaintsDetail";
    diag.Width = 1050;
    diag.Height = 680;
    diag.URL = "/app/customers/complaintsnew/ComplaintsDetail.aspx?cid=" + oid;

    diag.show();
}
function ComplaintsCheckedDeleteNew() {
    parent.CloseLoading();
    var idList = GetCheckedID("rptID");
    if (idList == "") {
        Dialog.showError("请选择要删除的记录！");
        return;
    }
    ComplaintsDelete(idList, 'refresh');
}
function ComplaintsDelete(idlist, returnAction) {
    parent.CloseLoading();
    if (!confirm('删除后将无法恢复，确定要删除吗？')) return;

    var diag = new Dialog();
    diag.ID = "ComplaintsDelete";
    diag.Width = 300;
    diag.Height = 110;
    diag.URL = "/app/Customers/complaintsnew/Complaintsdelete.aspx?idlist=" + idlist + "&returnaction=" + returnAction + "&DialogID=";
    diag.show();
}
function GotoSelectUser() {
    window.location.href = "/app/customers/complaintsnew/index.aspx?action=" + action + "&selectUserID=" + $("rptUserList").value;
}

function ComplaintsParentEditNew() {
    var id = GetCheckedID("rptID");
    if (id == "") {
        Dialog.showError("请选择要编辑的记录！");
        return;
    } else if (id.indexOf(',') != -1) {
        Dialog.showError("同时只能编辑一条记录，请只选择一个！");
        return;
    } else {
        ComplaintsEditNew('', id);
    }
}

function ComplaintsEditNew(dialogid, oid) {
    var diag = new Dialog();
    diag.ID = "ComplaintsEdit";
    diag.Width = 1000;
    diag.Height = 700;
    diag.URL = "/app/Customers/complaintsnew/ComplaintsEdit.aspx?DialogID=" + dialogid + "&Mode=Edit&CID=" + oid;
    diag.show();
}

function EditNew() {
    var diag = new Dialog();
    diag.ID = "ComplaintsEdit";
    diag.Width = 1000;
    diag.Height = 700;
    diag.URL = "/app/Customers/complaintsnew/ComplaintsEdit.aspx?DialogID=ComplaintsDetail&Mode=Edit&CID=" + detailCId + "&CusType=" + detailCusType;
    diag.show();
}

function Delete(followid) {
    parent.parent.CloseLoading();
    if (!confirm('删除将不可恢复，确定要删除吗？')) return;
    document.getElementById("rptDeleteID").value = followid;
    document.forms[0].submit();
}

function FollowAdd_ComplaintsNew(cusid, linkmanid, tasknodeid, complatinsid, DialogId) {
    var diag = new Dialog();
    diag.ID = "FollowEdit";
    diag.Width = 900;
    diag.Height = 560;
    diag.Title = "新建联系记录";
    diag.MessageTitle = "新建联系记录";
    diag.Message = "您可以在这里把每次与客户联系的内容记录下来，这样日后可以随时回来查询";
    //diag.URL = "/app/Customers/follow/FollowEdit.aspx?Mode=Add&CusID=" + cusid + "&linkmanid=" + linkmanid + "&tasknodeid=" + tasknodeid + "&ComplaintsID=" + complatinsid + "&DialogID=" + DialogId;
    diag.URL = "/app/Customers/follow/FollowEditNew.aspx?Mode=Add&CusID=" + cusid + "&linkmanid=" + linkmanid + "&tasknodeid=" + tasknodeid + "&ComplaintsID=" + complatinsid + "&DialogID=" + DialogId;
    diag.show();
}

function FollowAdd_ComplaintsIndex() {
    var complatinsid = GetCheckedID("rptID");
    if (complatinsid == "") {
        Dialog.showError("还没有选择客户投诉记录");
        return;
    }
    if (complatinsid.indexOf(",") > -1) {
        Dialog.showError("不能为多个客户投诉创建跟进，请选择一条记录");
        return;
    }

    var diag = new Dialog();
    diag.ID = "FollowEdit";
    diag.Width = 900;
    diag.Height = 560;
    diag.Title = "新建联系记录";
    diag.MessageTitle = "新建联系记录";
    diag.Message = "您可以在这里把每次与客户联系的内容记录下来，这样日后可以随时回来查询";
    //diag.URL = "/app/Customers/follow/FollowEdit.aspx?Mode=Add&CusID=0&linkmanid=0&tasknodeid=0&ComplaintsID=" + complatinsid + "&DialogID=";
    diag.URL = "/app/Customers/follow/FollowEditNew.aspx?Mode=Add&CusID=0&linkmanid=0&tasknodeid=0&ComplaintsID=" + complatinsid + "&DialogID=";
    diag.show();
}

//2017-09-06 qianlong 
function WaitTaskAdd_Complaints(cusid, linkmanid, complatinsid, DialogId) {
    var diag = new Dialog();
    diag.ID = "WaitTaskEdit";
    diag.Width = 900;
    diag.Height = 510;
    diag.Title = "创建客户投诉待办任务";
    diag.MessageTitle = "新建一个客户投诉待办任务";
    diag.Message = "您可以将要办的事情在这里新建为任务，到时系统就会提醒您，也可以给下级分配工作任务(团队版)。<a href=\"http://bbs.kehu51.com/Notice/thread-3267-1-1.html\" target=\"_blank\">初次使用请查看帮助</a>";
    diag.URL = "/app/Customers/ComplaintsNew/Complaints_Task.aspx?Mode=Add&CusID=" + cusid + "&linkmanid=" + linkmanid + "&complatinsid=" + complatinsid + "&srcFrom=" + DialogId;
    diag.OnLoad = function () { var h = GetCwinHeight(diag.innerFrame); diag.setSize(null, h); };
    diag.show();
}
//如果从列表界面进行添加任务
function WaitTaskAdd_ComplaintsIndex() {

    var complatinsid = GetCheckedID("rptID");
    if (complatinsid == "") {
        Dialog.showError("还没有选择客户投诉记录");
        return;
    }
    if (complatinsid.indexOf(",") > -1) {
        Dialog.showError("不能为多个客户投诉创建任务，请选择一条记录");
        return;
    }
    var diag = new Dialog();
    diag.ID = "WaitTaskEdit";
    diag.Width = 900;
    diag.Height = 510;
    diag.Title = "创建客户投诉待办任务";
    diag.MessageTitle = "新建一个客户投诉待办任务";
    diag.Message = "您可以将要办的事情在这里新建为任务，到时系统就会提醒您，也可以给下级分配工作任务(团队版)。<a href=\"http://bbs.kehu51.com/Notice/thread-3267-1-1.html\" target=\"_blank\">初次使用请查看帮助</a>";
    diag.URL = "/app/Customers/ComplaintsNew/Complaints_Task.aspx?Mode=Add&CusID=0&linkmanid=0&complatinsid=" + complatinsid + "&srcFrom=";
    diag.OnLoad = function () { var h = GetCwinHeight(diag.innerFrame); diag.setSize(null, h); };
    diag.show();
}

function clickOpenTaskTask() {
    var openvalue = GetValue("openwaittask");
    if (openvalue == "1") {
        $("#trOpenWaitTask").css("backgroundColor", "#F4F9F9");
        $("#showWaitTask").show();
    }
    else {
        $("#trOpenWaitTask").css("backgroundColor", "#ffffff");
        $("#showWaitTask").hide();
    }

    StartAutoSize("ComplaintsEdit");
}

function checkForm() {
    if (!ShowFieldCheck()) return false;
    var openvalue = GetValue("openwaittask");
    $("#rptIsOpenWaitTask").val(openvalue);

    var batchexecuteman = GetRadioValue("batchexecuteman");
    $("#rptExecuteMan").val(batchexecuteman);

    if (parseInt(openvalue) == 1) {
        if ($("#rptXianQiStartDate").val() == "") {
            Dialog.showError("请选择任务的执行开始时间", function () {
                $("#rptXianQiStartDate").focus();
            });
            return false;
        }
        if ($("#rptXianQiEndDate").val() == "") {
            Dialog.showError("请选择任务的执行开始时间", function () {
                $("#rptXianQiEndDate").focus();
            });
            return false;
        }

        if ($("#rptTaskContent").val() == "") {
            Dialog.showError("请填写任务内容", function () {
                $("#rptTaskContent").focus();
            });
            return false;
        }
    }

    CommitButton(null);
    return true;
}