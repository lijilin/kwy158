/*!
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2015
 * @version 1.3.3
 *
 * Date formatter utility library that allows formatting date/time variables or Date objects using PHP DateTime format.
 * @see http://php.net/manual/en/function.date.php
 *
 * For more JQuery plugins visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */
var DateFormatter;
(function () {
    "use strict";

    var _compare, _lpad, _extend, defaultSettings, DAY, HOUR;
    DAY = 1000 * 60 * 60 * 24;
    HOUR = 3600;

    _compare = function (str1, str2) {
        return typeof (str1) === 'string' && typeof (str2) === 'string' && str1.toLowerCase() === str2.toLowerCase();
    };
    _lpad = function (value, length, char) {
        var chr = char || '0', val = value.toString();
        return val.length < length ? _lpad(chr + val, length) : val;
    };
    _extend = function (out) {
        var i, obj;
        out = out || {};
        for (i = 1; i < arguments.length; i++) {
            obj = arguments[i];
            if (!obj) {
                continue;
            }
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if (typeof obj[key] === 'object') {
                        _extend(out[key], obj[key]);
                    } else {
                        out[key] = obj[key];
                    }
                }
            }
        }
        return out;
    };
    defaultSettings = {
        dateSettings: {
            days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            months: [
                'January', 'February', 'March', 'April', 'May', 'June', 'July',
                'August', 'September', 'October', 'November', 'December'
            ],
            monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            meridiem: ['AM', 'PM'],
            ordinal: function (number) {
                var n = number % 10, suffixes = { 1: 'st', 2: 'nd', 3: 'rd' };
                return Math.floor(number % 100 / 10) === 1 || !suffixes[n] ? 'th' : suffixes[n];
            }
        },
        separators: /[ \-+\/\.T:@]/g,
        validParts: /[dDjlNSwzWFmMntLoYyaABgGhHisueTIOPZcrU]/g,
        intParts: /[djwNzmnyYhHgGis]/g,
        tzParts: /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        tzClip: /[^-+\dA-Z]/g
    };

    DateFormatter = function (options) {
        var self = this, config = _extend(defaultSettings, options);
        self.dateSettings = config.dateSettings;
        self.separators = config.separators;
        self.validParts = config.validParts;
        self.intParts = config.intParts;
        self.tzParts = config.tzParts;
        self.tzClip = config.tzClip;
    };

    DateFormatter.prototype = {
        constructor: DateFormatter,
        parseDate: function (vDate, vFormat) {
            var self = this, vFormatParts, vDateParts, i, vDateFlag = false, vTimeFlag = false, vDatePart, iDatePart,
                vSettings = self.dateSettings, vMonth, vMeriIndex, vMeriOffset, len, mer,
                out = { date: null, year: null, month: null, day: null, hour: 0, min: 0, sec: 0 };
            if (!vDate) {
                return undefined;
            }
            if (vDate instanceof Date) {
                return vDate;
            }
            if (typeof vDate === 'number') {
                return new Date(vDate);
            }
            if (vFormat === 'U') {
                i = parseInt(vDate);
                return i ? new Date(i * 1000) : vDate;
            }
            if (typeof vDate !== 'string') {
                return '';
            }
            vFormatParts = vFormat.match(self.validParts);
            if (!vFormatParts || vFormatParts.length === 0) {
                throw new Error("Invalid date format definition.");
            }
            vDateParts = vDate.replace(self.separators, '\0').split('\0');
            for (i = 0; i < vDateParts.length; i++) {
                vDatePart = vDateParts[i];
                iDatePart = parseInt(vDatePart);
                switch (vFormatParts[i]) {
                    case 'y':
                    case 'Y':
                        len = vDatePart.length;
                        if (len === 2) {
                            out.year = parseInt((iDatePart < 70 ? '20' : '19') + vDatePart);
                        } else if (len === 4) {
                            out.year = iDatePart;
                        }
                        vDateFlag = true;
                        break;
                    case 'm':
                    case 'n':
                    case 'M':
                    case 'F':
                        if (isNaN(vDatePart)) {
                            vMonth = vSettings.monthsShort.indexOf(vDatePart);
                            if (vMonth > -1) {
                                out.month = vMonth + 1;
                            }
                            vMonth = vSettings.months.indexOf(vDatePart);
                            if (vMonth > -1) {
                                out.month = vMonth + 1;
                            }
                        } else {
                            if (iDatePart >= 1 && iDatePart <= 12) {
                                out.month = iDatePart;
                            }
                        }
                        vDateFlag = true;
                        break;
                    case 'd':
                    case 'j':
                        if (iDatePart >= 1 && iDatePart <= 31) {
                            out.day = iDatePart;
                        }
                        vDateFlag = true;
                        break;
                    case 'g':
                    case 'h':
                        vMeriIndex = (vFormatParts.indexOf('a') > -1) ? vFormatParts.indexOf('a') :
                            (vFormatParts.indexOf('A') > -1) ? vFormatParts.indexOf('A') : -1;
                        mer = vDateParts[vMeriIndex];
                        if (vMeriIndex > -1) {
                            vMeriOffset = _compare(mer, vSettings.meridiem[0]) ? 0 :
                                (_compare(mer, vSettings.meridiem[1]) ? 12 : -1);
                            if (iDatePart >= 1 && iDatePart <= 12 && vMeriOffset > -1) {
                                out.hour = iDatePart + vMeriOffset;
                            } else if (iDatePart >= 0 && iDatePart <= 23) {
                                out.hour = iDatePart;
                            }
                        } else if (iDatePart >= 0 && iDatePart <= 23) {
                            out.hour = iDatePart;
                        }
                        vTimeFlag = true;
                        break;
                    case 'G':
                    case 'H':
                        if (iDatePart >= 0 && iDatePart <= 23) {
                            out.hour = iDatePart;
                        }
                        vTimeFlag = true;
                        break;
                    case 'i':
                        if (iDatePart >= 0 && iDatePart <= 59) {
                            out.min = iDatePart;
                        }
                        vTimeFlag = true;
                        break;
                    case 's':
                        if (iDatePart >= 0 && iDatePart <= 59) {
                            out.sec = iDatePart;
                        }
                        vTimeFlag = true;
                        break;
                }
            }
            if (vDateFlag === true && out.year && out.month && out.day) {
                out.date = new Date(out.year, out.month - 1, out.day, out.hour, out.min, out.sec, 0);
            } else {
                if (vTimeFlag !== true) {
                    return false;
                }
                out.date = new Date(0, 0, 0, out.hour, out.min, out.sec, 0);
            }
            return out.date;
        },
        guessDate: function (vDateStr, vFormat) {
            if (typeof vDateStr !== 'string') {
                return vDateStr;
            }
            var self = this, vParts = vDateStr.replace(self.separators, '\0').split('\0'), vPattern = /^[djmn]/g,
                vFormatParts = vFormat.match(self.validParts), vDate = new Date(), vDigit = 0, vYear, i, iPart, iSec;

            if (!vPattern.test(vFormatParts[0])) {
                return vDateStr;
            }

            for (i = 0; i < vParts.length; i++) {
                vDigit = 2;
                iPart = vParts[i];
                iSec = parseInt(iPart.substr(0, 2));
                switch (i) {
                    case 0:
                        if (vFormatParts[0] === 'm' || vFormatParts[0] === 'n') {
                            vDate.setMonth(iSec - 1);
                        } else {
                            vDate.setDate(iSec);
                        }
                        break;
                    case 1:
                        if (vFormatParts[0] === 'm' || vFormatParts[0] === 'n') {
                            vDate.setDate(iSec);
                        } else {
                            vDate.setMonth(iSec - 1);
                        }
                        break;
                    case 2:
                        vYear = vDate.getFullYear();
                        if (iPart.length < 4) {
                            vDate.setFullYear(parseInt(vYear.toString().substr(0, 4 - iPart.length) + iPart));
                            vDigit = iPart.length;
                        } else {
                            vDate.setFullYear = parseInt(iPart.substr(0, 4));
                            vDigit = 4;
                        }
                        break;
                    case 3:
                        vDate.setHours(iSec);
                        break;
                    case 4:
                        vDate.setMinutes(iSec);
                        break;
                    case 5:
                        vDate.setSeconds(iSec);
                        break;
                }
                if (iPart.substr(vDigit).length > 0) {
                    vParts.splice(i + 1, 0, iPart.substr(vDigit));
                }
            }
            return vDate;
        },
        parseFormat: function (vChar, vDate) {
            var self = this, vSettings = self.dateSettings, fmt, backspace = /\\?(.?)/gi, doFormat = function (t, s) {
                return fmt[t] ? fmt[t]() : s;
            };
            fmt = {
                /////////
                // DAY //
                /////////
                /**
                 * Day of month with leading 0: `01..31`
                 * @return {string}
                 */
                d: function () {
                    return _lpad(fmt.j(), 2);
                },
                /**
                 * Shorthand day name: `Mon...Sun`
                 * @return {string}
                 */
                D: function () {
                    return vSettings.daysShort[fmt.w()];
                },
                /**
                 * Day of month: `1..31`
                 * @return {number}
                 */
                j: function () {
                    return vDate.getDate();
                },
                /**
                 * Full day name: `Monday...Sunday`
                 * @return {number}
                 */
                l: function () {
                    return vSettings.days[fmt.w()];
                },
                /**
                 * ISO-8601 day of week: `1[Mon]..7[Sun]`
                 * @return {number}
                 */
                N: function () {
                    return fmt.w() || 7;
                },
                /**
                 * Day of week: `0[Sun]..6[Sat]`
                 * @return {number}
                 */
                w: function () {
                    return vDate.getDay();
                },
                /**
                 * Day of year: `0..365`
                 * @return {number}
                 */
                z: function () {
                    var a = new Date(fmt.Y(), fmt.n() - 1, fmt.j()), b = new Date(fmt.Y(), 0, 1);
                    return Math.round((a - b) / DAY);
                },

                //////////
                // WEEK //
                //////////
                /**
                 * ISO-8601 week number
                 * @return {number}
                 */
                W: function () {
                    var a = new Date(fmt.Y(), fmt.n() - 1, fmt.j() - fmt.N() + 3), b = new Date(a.getFullYear(), 0, 4);
                    return _lpad(1 + Math.round((a - b) / DAY / 7), 2);
                },

                ///////////
                // MONTH //
                ///////////
                /**
                 * Full month name: `January...December`
                 * @return {string}
                 */
                F: function () {
                    return vSettings.months[vDate.getMonth()];
                },
                /**
                 * Month w/leading 0: `01..12`
                 * @return {string}
                 */
                m: function () {
                    return _lpad(fmt.n(), 2);
                },
                /**
                 * Shorthand month name; `Jan...Dec`
                 * @return {string}
                 */
                M: function () {
                    return vSettings.monthsShort[vDate.getMonth()];
                },
                /**
                 * Month: `1...12`
                 * @return {number}
                 */
                n: function () {
                    return vDate.getMonth() + 1;
                },
                /**
                 * Days in month: `28...31`
                 * @return {number}
                 */
                t: function () {
                    return (new Date(fmt.Y(), fmt.n(), 0)).getDate();
                },

                //////////
                // YEAR //
                //////////
                /**
                 * Is leap year? `0 or 1`
                 * @return {number}
                 */
                L: function () {
                    var Y = fmt.Y();
                    return (Y % 4 === 0 && Y % 100 !== 0 || Y % 400 === 0) ? 1 : 0;
                },
                /**
                 * ISO-8601 year
                 * @return {number}
                 */
                o: function () {
                    var n = fmt.n(), W = fmt.W(), Y = fmt.Y();
                    return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
                },
                /**
                 * Full year: `e.g. 1980...2010`
                 * @return {number}
                 */
                Y: function () {
                    return vDate.getFullYear();
                },
                /**
                 * Last two digits of year: `00...99`
                 * @return {string}
                 */
                y: function () {
                    return fmt.Y().toString().slice(-2);
                },

                //////////
                // TIME //
                //////////
                /**
                 * Meridian lower: `am or pm`
                 * @return {string}
                 */
                a: function () {
                    return fmt.A().toLowerCase();
                },
                /**
                 * Meridian upper: `AM or PM`
                 * @return {string}
                 */
                A: function () {
                    var n = fmt.G() < 12 ? 0 : 1;
                    return vSettings.meridiem[n];
                },
                /**
                 * Swatch Internet time: `000..999`
                 * @return {string}
                 */
                B: function () {
                    var H = vDate.getUTCHours() * HOUR, i = vDate.getUTCMinutes() * 60, s = vDate.getUTCSeconds();
                    return _lpad(Math.floor((H + i + s + HOUR) / 86.4) % 1000, 3);
                },
                /**
                 * 12-Hours: `1..12`
                 * @return {number}
                 */
                g: function () {
                    return fmt.G() % 12 || 12;
                },
                /**
                 * 24-Hours: `0..23`
                 * @return {number}
                 */
                G: function () {
                    return vDate.getHours();
                },
                /**
                 * 12-Hours with leading 0: `01..12`
                 * @return {string}
                 */
                h: function () {
                    return _lpad(fmt.g(), 2);
                },
                /**
                 * 24-Hours w/leading 0: `00..23`
                 * @return {string}
                 */
                H: function () {
                    return _lpad(fmt.G(), 2);
                },
                /**
                 * Minutes w/leading 0: `00..59`
                 * @return {string}
                 */
                i: function () {
                    return _lpad(vDate.getMinutes(), 2);
                },
                /**
                 * Seconds w/leading 0: `00..59`
                 * @return {string}
                 */
                s: function () {
                    return _lpad(vDate.getSeconds(), 2);
                },
                /**
                 * Microseconds: `000000-999000`
                 * @return {string}
                 */
                u: function () {
                    return _lpad(vDate.getMilliseconds() * 1000, 6);
                },

                //////////////
                // TIMEZONE //
                //////////////
                /**
                 * Timezone identifier: `e.g. Atlantic/Azores, ...`
                 * @return {string}
                 */
                e: function () {
                    var str = /\((.*)\)/.exec(String(vDate))[1];
                    return str || 'Coordinated Universal Time';
                },
                /**
                 * Timezone abbreviation: `e.g. EST, MDT, ...`
                 * @return {string}
                 */
                T: function () {
                    var str = (String(vDate).match(self.tzParts) || [""]).pop().replace(self.tzClip, "");
                    return str || 'UTC';
                },
                /**
                 * DST observed? `0 or 1`
                 * @return {number}
                 */
                I: function () {
                    var a = new Date(fmt.Y(), 0), c = Date.UTC(fmt.Y(), 0),
                        b = new Date(fmt.Y(), 6), d = Date.UTC(fmt.Y(), 6);
                    return ((a - c) !== (b - d)) ? 1 : 0;
                },
                /**
                 * Difference to GMT in hour format: `e.g. +0200`
                 * @return {string}
                 */
                O: function () {
                    var tzo = vDate.getTimezoneOffset(), a = Math.abs(tzo);
                    return (tzo > 0 ? '-' : '+') + _lpad(Math.floor(a / 60) * 100 + a % 60, 4);
                },
                /**
                 * Difference to GMT with colon: `e.g. +02:00`
                 * @return {string}
                 */
                P: function () {
                    var O = fmt.O();
                    return (O.substr(0, 3) + ':' + O.substr(3, 2));
                },
                /**
                 * Timezone offset in seconds: `-43200...50400`
                 * @return {number}
                 */
                Z: function () {
                    return -vDate.getTimezoneOffset() * 60;
                },

                ////////////////////
                // FULL DATE TIME //
                ////////////////////
                /**
                 * ISO-8601 date
                 * @return {string}
                 */
                c: function () {
                    return 'Y-m-d\\TH:i:sP'.replace(backspace, doFormat);
                },
                /**
                 * RFC 2822 date
                 * @return {string}
                 */
                r: function () {
                    return 'D, d M Y H:i:s O'.replace(backspace, doFormat);
                },
                /**
                 * Seconds since UNIX epoch
                 * @return {number}
                 */
                U: function () {
                    return vDate.getTime() / 1000 || 0;
                }
            };
            return doFormat(vChar, vChar);
        },
        formatDate: function (vDate, vFormat) {
            var self = this, i, n, len, str, vChar, vDateStr = '';
            if (typeof vDate === 'string') {
                vDate = self.parseDate(vDate, vFormat);
                if (vDate === false) {
                    return false;
                }
            }
            if (vDate instanceof Date) {
                len = vFormat.length;
                for (i = 0; i < len; i++) {
                    vChar = vFormat.charAt(i);
                    if (vChar === 'S') {
                        continue;
                    }
                    str = self.parseFormat(vChar, vDate);
                    if (i !== (len - 1) && self.intParts.test(vChar) && vFormat.charAt(i + 1) === 'S') {
                        n = parseInt(str);
                        str += self.dateSettings.ordinal(n);
                    }
                    vDateStr += str;
                }
                return vDateStr;
            }
            return '';
        }
    };
})();/**
 * @preserve jQuery DateTimePicker plugin v2.5.4
 * @homepage http://xdsoft.net/jqplugins/datetimepicker/
 * @author Chupurnov Valeriy (<chupurnov@gmail.com>)
 */
/*global DateFormatter, document,window,jQuery,setTimeout,clearTimeout,HighlightedDate,getCurrentValue*/
; (function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'jquery-mousewheel'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    'use strict';

    var currentlyScrollingTimeDiv = false;
    //qianlong
    var currentlyScrollingYearDiv = false;
    var currentlyScrollingYearMonthDiv = false;
    var currentlyScrollingWeekDiv = false;

    var default_options = {
        i18n: {
            en: { // English
                months: [
					"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                ],
                dayOfWeekShort: [
					"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
                ],
                dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
            },
            ch: { // Simplified Chinese
                months: [
					"一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
                ],
                dayOfWeekShort: [
					"日", "一", "二", "三", "四", "五", "六"
                ]
            },
            'en-GB': { //English (British)
                months: [
					"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                ],
                dayOfWeekShort: [
					"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
                ],
                dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
            },
            'zh-TW': { //Traditional Chinese (繁體中文)
                months: [
					"一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
                ],
                dayOfWeekShort: [
					"日", "一", "二", "三", "四", "五", "六"
                ],
                dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
            },
            zh: { //Simplified Chinese (简体中文)
                months: [
					"一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
                ],
                dayOfWeekShort: [
					"日", "一", "二", "三", "四", "五", "六"
                ],
                dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
            },
        },
        value: '',// 设置当前datetimepicker的值
        rtl: false, // false   默认显示方式   true timepicker和datepicker位置变换

        format: 'Y/m/d H:i',// 设置时间年月日时分的格式 如: 2016/11/15 18:00
        formatTime: 'H:i',// 设置时间时分的格式
        formatDate: 'Y/m/d', // 设置时间年月日的格式

        startDate: false, // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
        step: 30,// 设置时间时分的间隔
        monthChangeSpinner: true,

        closeOnDateSelect: false,// true 设置datepicker可点击   false 设置datepicker不可点击 实际上可以双击
        closeOnTimeSelect: true,// true 设置timepicker可点击   false 设置timepicker不可点击 
        closeOnYearSelect: true,//qianlong true 设置yearpicker可点击   false 设置yearpicker不可点击
        closeOnYearMonthSelect: true,//qianlong true 设置yearmonthpicker可点击   false 设置yearmonthpicker不可点击
        closeOnWeekSelect: true,
        closeOnWithoutClick: true,// true 设置点击input可以隐藏datetimepicker   false 设置点击input不可以隐藏datetimepicker  
        closeOnInputClick: true,// true 设置点击input可以隐藏datetimepicker   false 设置点击input不可以隐藏datetimepicker  (会有闪动 先隐藏 再显示)

        timepicker: true,// true 显示timepicker   false 隐藏timepicker
        datepicker: true,// true 显示datepicker   false 隐藏datepicker        
        yearpicker: false,// true 显示yearpicker   false 隐藏yearpicker
        yearmonthpicker: false,//qianlong true 显示yearmonthpicker   false 隐藏yearmonthpicker
        weekpicker: false,//qianlong  true 显示weekpicker   false 隐藏weekpicker

        weeks: false,// true 显示周数   false 隐藏周数
        defaultTime: false,	// // 如果输入值为空 可用来设置默认显示时间 use formatTime format (ex. '10:00' for formatTime:    'H:i') 
        defaultDate: false,	// 如果输入值为空 可用来设置默认显示日期 use formatDate format (ex new Date() or '1986/12/08' or '-1970/01/05' or '-1970/01/05')

        minDate: false,// 设置datepicker最小的限制日期   如：2016/08/15
        maxDate: false,// 设置datepicker最大的限制日期   如：2016/11/15
        minTime: false,// 设置timepicker最小的限制时间   如：08:00
        maxTime: false,// 设置timepicker最大的限制时间   如：18:00
        disabledMinTime: false,
        disabledMaxTime: false,

        allowTimes: [],// 设置timepicker显示的时间   如：allowTimes:['09:00','11:00','12:00','21:00']
        opened: false,// false默认打开datetimepicker可关闭  true打开datetimepicker后不可关闭
        initTime: true,// 设置timepicker默认时间   如：08:00
        inline: false,// ture设置datetimepicker一直显示
        theme: '',// ture设置datetimepicker显示样式 如: 'dark'

        onSelectDate: function () { },
        onSelectTime: function () { },
        onSelectYear: function () { },//qianlong
        onSelectYearMonth: function () { },//qianlong
        onSelectWeek: function () { },//qianlong
        onChangeMonth: function () { },
        onGetWeekOfYear: function () { },
        onChangeYear: function () { },
        onChangeDateTime: function () { },
        onShow: function () { },
        onClose: function () { },
        onGenerate: function () { },

        withoutCopyright: true,// ture默认隐藏左下角'xdsoft.net'链接  false 显示左下角'xdsoft.net'链接 
        inverseButton: false,// false 默认   true datepicker的上一月和下一月功能互换  timepicker的上下可点击按钮功能互换
        hours12: false,// true设置12小时格式  false设置24小时格式
        next: 'xdsoft_next',// 设置datepicker上一月按钮的样式
        prev: 'xdsoft_prev',// 设置datepicker下一月按钮的样式
        dayOfWeekStart: 0,// 设置默认第-列为周几 如：0 周日  1 周一
        parentID: 'body',// 设置父级选择器
        timeHeightInTimePicker: 25,// 设置timepicker的行高
        timepickerScrollbar: true,// ture设置timepicker显示滑动条  false设置timepicker不显示滑动条
        todayButton: true,// ture显示今天按钮  false不显示今天按钮   位置在datepicker左上角
        prevButton: true,// ture显示上一月按钮  false不显示上一月按钮   位置在datepicker左上角
        nextButton: true,// ture显示下一月按钮  false不显示下一月按钮   位置在datepicker又上角
        defaultSelect: true,

        scrollMonth: false,// ture 设置datepicker的月份可以滑动  false设置datepicker的月份不可以滑动
        scrollTime: true,
        scrollInput: true,
        readOnly: "readonly",//qianlong 是否可以输入内容  readonly:只读，为空：非只读

        lazyInit: false,// 翻译： 初始化插件发生只有当用户交互。大大加速插件与大量的领域的工作
        mask: false,// 使用输入掩码。真正的-自动生成一个字段的“格式”的面具，从0到9的数字，设置为值的最高可能的数字。例如：第一个小时的数字不能大于2，而第一位数字不能大于5  如：{mask:'9999/19/39 29:59',format:'Y/m/d H:i'}
        validateOnBlur: true,// 失去焦点时验证datetime值输入,。如果值是无效的datetime,然后插入当前日期时间值
        allowBlank: true,
        yearStart: 1950,// 设置最小的年份
        yearEnd: 2050,// 设置最大的年份
        monthStart: 0,// 设置最小的月份
        monthEnd: 11,// 设置最大的月份
        style: '',
        id: '',
        fixed: false,
        roundTime: 'round', // 设置timepicker的计算方式  round四舍五入 ceil向上取整 floor向下取整
        className: '',
        weekends: [],
        highlightedDates: [],
        highlightedPeriods: [],
        allowDates: [],// 设置正则表达式检查日期 如：{format:'Y-m-d',allowDateRe:'\d{4}-(03-31|06-30|09-30|12-31)' }
        allowDateRe: null,
        disabledDates: [],// 设置不可点击的日期  如：disabledDates: ['21.11.2016','22.11.2016','23.11.2016','24.11.2016','25.11.2016','26.11.2016']
        disabledWeekDays: [],// 设置不可点击的星期  如：disabledWeekDays:[0,3,4]
        yearOffset: 0,// 设置偏移年份  如：2 代表当前年份加2  -2  代表当前年份减2
        beforeShowDay: null,// 显示datetimepicker之前可调用的方法  {beforeShowDay:function(d) {console.log("bsd"); } }

        enterLikeTab: true,// tab按键均可使datetimepicker关闭  true点击回车键可使datetimepicker关闭 false点击回车键不可使datetimepicker关闭 
        showApplyButton: false// 相当于确定按钮  true显示  false隐藏
    };

    var dateHelper = null,
		globalLocaleDefault = 'ch',
		globalLocale = 'ch';

    var dateFormatterOptionsDefault = {
        meridiem: ['AM', 'PM']
    };

    var initDateFormatter = function () {
        var locale = default_options.i18n[globalLocale],
			opts = {
			    days: locale.dayOfWeek,
			    daysShort: locale.dayOfWeekShort,
			    months: locale.months,
			    monthsShort: $.map(locale.months, function (n) { return n.substring(0, 3) }),
			};

        dateHelper = new DateFormatter({
            dateSettings: $.extend({}, dateFormatterOptionsDefault, opts)
        });
    };

    // for locale settings
    $.datetimepicker = {
        setLocale: function (locale) {
            var newLocale = default_options.i18n[locale] ? locale : globalLocaleDefault;
            if (globalLocale != newLocale) {
                globalLocale = newLocale;
                // reinit date formatter
                initDateFormatter();
            }
        },
        setDateFormatter: function (dateFormatter) {
            dateHelper = dateFormatter;
        },
        RFC_2822: 'D, d M Y H:i:s O',
        ATOM: 'Y-m-d\TH:i:sP',
        ISO_8601: 'Y-m-d\TH:i:sO',
        RFC_822: 'D, d M y H:i:s O',
        RFC_850: 'l, d-M-y H:i:s T',
        RFC_1036: 'D, d M y H:i:s O',
        RFC_1123: 'D, d M Y H:i:s O',
        RSS: 'D, d M Y H:i:s O',
        W3C: 'Y-m-d\TH:i:sP'
    };

    // first init date formatter
    initDateFormatter();

    // fix for ie8
    if (!window.getComputedStyle) {
        window.getComputedStyle = function (el, pseudo) {
            this.el = el;
            this.getPropertyValue = function (prop) {
                var re = /(\-([a-z]){1})/g;
                if (prop === 'float') {
                    prop = 'styleFloat';
                }
                if (re.test(prop)) {
                    prop = prop.replace(re, function (a, b, c) {
                        return c.toUpperCase();
                    });
                }
                return el.currentStyle[prop] || null;
            };
            return this;
        };
    }
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (obj, start) {
            var i, j;
            for (i = (start || 0), j = this.length; i < j; i += 1) {
                if (this[i] === obj) { return i; }
            }
            return -1;
        };
    }
    Date.prototype.countDaysInMonth = function () {
        return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate();
    };
    $.fn.xdsoftScroller = function (percent) {
        return this.each(function () {
            var timeboxparent = $(this),
				pointerEventToXY = function (e) {
				    var out = { x: 0, y: 0 },
						touch;
				    if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend' || e.type === 'touchcancel') {
				        touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				        out.x = touch.clientX;
				        out.y = touch.clientY;
				    } else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove' || e.type === 'mouseover' || e.type === 'mouseout' || e.type === 'mouseenter' || e.type === 'mouseleave') {
				        out.x = e.clientX;
				        out.y = e.clientY;
				    }
				    return out;
				},
				timebox,
				parentHeight,
				height,
				scrollbar,
				scroller,
				maximumOffset = 100,
				start = false,
				startY = 0,
				startTop = 0,
				h1 = 0,
				touchStart = false,
				startTopScroll = 0,
				calcOffset = function () { };
            if (percent === 'hide') {
                timeboxparent.find('.xdsoft_scrollbar').hide();
                return;
            }
            if (!$(this).hasClass('xdsoft_scroller_box')) {
                timebox = timeboxparent.children().eq(0);
                parentHeight = timeboxparent[0].clientHeight;
                height = timebox[0].offsetHeight;
                scrollbar = $('<div class="xdsoft_scrollbar"></div>');
                scroller = $('<div class="xdsoft_scroller"></div>');
                scrollbar.append(scroller);

                timeboxparent.addClass('xdsoft_scroller_box').append(scrollbar);
                calcOffset = function calcOffset(event) {
                    var offset = pointerEventToXY(event).y - startY + startTopScroll;
                    if (offset < 0) {
                        offset = 0;
                    }
                    if (offset + scroller[0].offsetHeight > h1) {
                        offset = h1 - scroller[0].offsetHeight;
                    }
                    timeboxparent.trigger('scroll_element.xdsoft_scroller', [maximumOffset ? offset / maximumOffset : 0]);
                };

                scroller
					.on('touchstart.xdsoft_scroller mousedown.xdsoft_scroller', function (event) {
					    if (!parentHeight) {
					        timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
					    }

					    startY = pointerEventToXY(event).y;
					    startTopScroll = parseInt(scroller.css('margin-top'), 10);
					    h1 = scrollbar[0].offsetHeight;

					    if (event.type === 'mousedown' || event.type === 'touchstart') {
					        if (document) {
					            $(document.body).addClass('xdsoft_noselect');
					        }
					        $([document.body, window]).on('touchend mouseup.xdsoft_scroller', function arguments_callee() {
					            $([document.body, window]).off('touchend mouseup.xdsoft_scroller', arguments_callee)
									.off('mousemove.xdsoft_scroller', calcOffset)
									.removeClass('xdsoft_noselect');
					        });
					        $(document.body).on('mousemove.xdsoft_scroller', calcOffset);
					    } else {
					        touchStart = true;
					        event.stopPropagation();
					        event.preventDefault();
					    }
					})
					.on('touchmove', function (event) {
					    if (touchStart) {
					        event.preventDefault();
					        calcOffset(event);
					    }
					})
					.on('touchend touchcancel', function () {
					    touchStart = false;
					    startTopScroll = 0;
					});

                timeboxparent
					.on('scroll_element.xdsoft_scroller', function (event, percentage) {
					    if (!parentHeight) {
					        timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percentage, true]);
					    }
					    percentage = percentage > 1 ? 1 : (percentage < 0 || isNaN(percentage)) ? 0 : percentage;

					    scroller.css('margin-top', maximumOffset * percentage);

					    setTimeout(function () {
					        timebox.css('marginTop', -parseInt((timebox[0].offsetHeight - parentHeight) * percentage, 10));
					    }, 10);
					})
					.on('resize_scroll.xdsoft_scroller', function (event, percentage, noTriggerScroll) {
					    var percent, sh;
					    parentHeight = timeboxparent[0].clientHeight;
					    height = timebox[0].offsetHeight;
					    percent = parentHeight / height;
					    sh = percent * scrollbar[0].offsetHeight;
					    if (percent > 1) {
					        scroller.hide();
					    } else {
					        scroller.show();
					        scroller.css('height', parseInt(sh > 10 ? sh : 10, 10));
					        maximumOffset = scrollbar[0].offsetHeight - scroller[0].offsetHeight;
					        if (noTriggerScroll !== true) {
					            timeboxparent.trigger('scroll_element.xdsoft_scroller', [percentage || Math.abs(parseInt(timebox.css('marginTop'), 10)) / (height - parentHeight)]);
					        }
					    }
					});

                timeboxparent.on('mousewheel', function (event) {
                    var top = Math.abs(parseInt(timebox.css('marginTop'), 10));

                    top = top - (event.deltaY * 20);
                    if (top < 0) {
                        top = 0;
                    }

                    timeboxparent.trigger('scroll_element.xdsoft_scroller', [top / (height - parentHeight)]);
                    event.stopPropagation();
                    return false;
                });

                timeboxparent.on('touchstart', function (event) {
                    start = pointerEventToXY(event);
                    startTop = Math.abs(parseInt(timebox.css('marginTop'), 10));
                });

                timeboxparent.on('touchmove', function (event) {
                    if (start) {
                        event.preventDefault();
                        var coord = pointerEventToXY(event);
                        timeboxparent.trigger('scroll_element.xdsoft_scroller', [(startTop - (coord.y - start.y)) / (height - parentHeight)]);
                    }
                });

                timeboxparent.on('touchend touchcancel', function () {
                    start = false;
                    startTop = 0;
                });
            }
            timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
        });
    };

    //qianlong
    $.fn.xdsoftScrollerYear = function (percent) {
        return this.each(function () {
            var yearboxparent = $(this),
				pointerEventToXY = function (e) {
				    var out = { x: 0, y: 0 },
						touch;
				    if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend' || e.type === 'touchcancel') {
				        touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				        out.x = touch.clientX;
				        out.y = touch.clientY;
				    } else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove' || e.type === 'mouseover' || e.type === 'mouseout' || e.type === 'mouseenter' || e.type === 'mouseleave') {
				        out.x = e.clientX;
				        out.y = e.clientY;
				    }
				    return out;
				},
                yearbox,
				parentHeight,
				height,
				scrollbar,
				scroller,
				maximumOffset = 100,
				start = false,
				startY = 0,
				startTop = 0,
				h1 = 0,
				touchStart = false,
				startTopScroll = 0,
				calcOffset = function () { };
            if (percent === 'hide') {
                yearboxparent.find('.xdsoft_scrollbar').hide();
                return;
            }
            if (!$(this).hasClass('xdsoft_scroller_box')) {
                yearbox = yearboxparent.children().eq(0);

                parentHeight = yearboxparent[0].clientHeight;
                height = yearbox[0].offsetHeight;
                scrollbar = $('<div class="xdsoft_scrollbar"></div>');
                scroller = $('<div class="xdsoft_scroller"></div>');
                scrollbar.append(scroller);

                yearboxparent.addClass('xdsoft_scroller_box').append(scrollbar);
                calcOffset = function calcOffset(event) {
                    var offset = pointerEventToXY(event).y - startY + startTopScroll;
                    if (offset < 0) {
                        offset = 0;
                    }
                    if (offset + scroller[0].offsetHeight > h1) {
                        offset = h1 - scroller[0].offsetHeight;
                    }
                    yearboxparent.trigger('scroll_element.xdsoft_scroller', [maximumOffset ? offset / maximumOffset : 0]);
                };

                scroller
					.on('touchstart.xdsoft_scroller mousedown.xdsoft_scroller', function (event) {
					    if (!parentHeight) {
					        yearboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
					    }

					    startY = pointerEventToXY(event).y;
					    startTopScroll = parseInt(scroller.css('margin-top'), 10);
					    h1 = scrollbar[0].offsetHeight;

					    if (event.type === 'mousedown' || event.type === 'touchstart') {
					        if (document) {
					            $(document.body).addClass('xdsoft_noselect');
					        }
					        $([document.body, window]).on('touchend mouseup.xdsoft_scroller', function arguments_callee() {
					            $([document.body, window]).off('touchend mouseup.xdsoft_scroller', arguments_callee)
									.off('mousemove.xdsoft_scroller', calcOffset)
									.removeClass('xdsoft_noselect');
					        });
					        $(document.body).on('mousemove.xdsoft_scroller', calcOffset);
					    } else {
					        touchStart = true;
					        event.stopPropagation();
					        event.preventDefault();
					    }
					})
					.on('touchmove', function (event) {
					    if (touchStart) {
					        event.preventDefault();
					        calcOffset(event);
					    }
					})
					.on('touchend touchcancel', function () {
					    touchStart = false;
					    startTopScroll = 0;
					});

                yearboxparent
					.on('scroll_element.xdsoft_scroller', function (event, percentage) {
					    if (!parentHeight) {
					        yearboxparent.trigger('resize_scroll.xdsoft_scroller', [percentage, true]);
					    }
					    percentage = percentage > 1 ? 1 : (percentage < 0 || isNaN(percentage)) ? 0 : percentage;

					    scroller.css('margin-top', maximumOffset * percentage);

					    setTimeout(function () {
					        var mTop = yearbox[0].offsetHeight - parentHeight;
					        yearbox.css('marginTop', -parseInt((mTop < 0 ? 0 : mTop) * percentage, 10));
					    }, 10);
					})
					.on('resize_scroll.xdsoft_scroller', function (event, percentage, noTriggerScroll) {
					    var percent, sh;
					    parentHeight = yearboxparent[0].clientHeight;
					    height = yearbox[0].offsetHeight;
					    percent = parentHeight / height;
					    sh = percent * scrollbar[0].offsetHeight;
					    if (percent > 1) {
					        scroller.hide();
					    } else {
					        scroller.show();
					        scroller.css('height', parseInt(sh > 10 ? sh : 10, 10));
					        maximumOffset = scrollbar[0].offsetHeight - scroller[0].offsetHeight;
					        if (noTriggerScroll !== true) {
					            yearboxparent.trigger('scroll_element.xdsoft_scroller', [percentage || Math.abs(parseInt(yearbox.css('marginTop'), 10)) / (height - parentHeight)]);
					        }
					    }
					});

                yearboxparent.on('mousewheel', function (event) {
                    var top = Math.abs(parseInt(yearbox.css('marginTop'), 10));

                    top = top - (event.deltaY * 20);
                    if (top < 0) {
                        top = 0;
                    }

                    yearboxparent.trigger('scroll_element.xdsoft_scroller', [top / (height - parentHeight)]);
                    event.stopPropagation();
                    return false;
                });

                yearboxparent.on('touchstart', function (event) {
                    start = pointerEventToXY(event);
                    startTop = Math.abs(parseInt(yearbox.css('marginTop'), 10));
                });

                yearboxparent.on('touchmove', function (event) {
                    if (start) {
                        event.preventDefault();
                        var coord = pointerEventToXY(event);
                        yearboxparent.trigger('scroll_element.xdsoft_scroller', [(startTop - (coord.y - start.y)) / (height - parentHeight)]);
                    }
                });

                yearboxparent.on('touchend touchcancel', function () {
                    start = false;
                    startTop = 0;
                });
            }
            yearboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
        });
    };

    $.fn.xdsoftScrollerYearMonth = function (percent) {
        return this.each(function () {
            var yearmonthboxparent = $(this),
				pointerEventToXY = function (e) {
				    var out = { x: 0, y: 0 },
						touch;
				    if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend' || e.type === 'touchcancel') {
				        touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				        out.x = touch.clientX;
				        out.y = touch.clientY;
				    } else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove' || e.type === 'mouseover' || e.type === 'mouseout' || e.type === 'mouseenter' || e.type === 'mouseleave') {
				        out.x = e.clientX;
				        out.y = e.clientY;
				    }
				    return out;
				},
                yearmonthbox,
				parentHeight,
				height,
				scrollbar,
				scroller,
				maximumOffset = 100,
				start = false,
				startY = 0,
				startTop = 0,
				h1 = 0,
				touchStart = false,
				startTopScroll = 0,
				calcOffset = function () { };
            if (percent === 'hide') {
                yearmonthboxparent.find('.xdsoft_scrollbar').hide();
                return;
            }
            if (!$(this).hasClass('xdsoft_scroller_box')) {
                yearmonthbox = yearmonthboxparent.children().eq(0);

                parentHeight = yearmonthboxparent[0].clientHeight;
                height = yearmonthbox[0].offsetHeight;
                scrollbar = $('<div class="xdsoft_scrollbar"></div>');
                scroller = $('<div class="xdsoft_scroller"></div>');
                scrollbar.append(scroller);

                yearmonthboxparent.addClass('xdsoft_scroller_box').append(scrollbar);
                calcOffset = function calcOffset(event) {
                    var offset = pointerEventToXY(event).y - startY + startTopScroll;
                    if (offset < 0) {
                        offset = 0;
                    }
                    if (offset + scroller[0].offsetHeight > h1) {
                        offset = h1 - scroller[0].offsetHeight;
                    }
                    yearmonthboxparent.trigger('scroll_element.xdsoft_scroller', [maximumOffset ? offset / maximumOffset : 0]);
                };

                scroller
					.on('touchstart.xdsoft_scroller mousedown.xdsoft_scroller', function (event) {
					    if (!parentHeight) {
					        yearmonthboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
					    }

					    startY = pointerEventToXY(event).y;
					    startTopScroll = parseInt(scroller.css('margin-top'), 10);
					    h1 = scrollbar[0].offsetHeight;

					    if (event.type === 'mousedown' || event.type === 'touchstart') {
					        if (document) {
					            $(document.body).addClass('xdsoft_noselect');
					        }
					        $([document.body, window]).on('touchend mouseup.xdsoft_scroller', function arguments_callee() {
					            $([document.body, window]).off('touchend mouseup.xdsoft_scroller', arguments_callee)
									.off('mousemove.xdsoft_scroller', calcOffset)
									.removeClass('xdsoft_noselect');
					        });
					        $(document.body).on('mousemove.xdsoft_scroller', calcOffset);
					    } else {
					        touchStart = true;
					        event.stopPropagation();
					        event.preventDefault();
					    }
					})
					.on('touchmove', function (event) {
					    if (touchStart) {
					        event.preventDefault();
					        calcOffset(event);
					    }
					})
					.on('touchend touchcancel', function () {
					    touchStart = false;
					    startTopScroll = 0;
					});

                yearmonthboxparent
					.on('scroll_element.xdsoft_scroller', function (event, percentage) {
					    if (!parentHeight) {
					        yearmonthboxparent.trigger('resize_scroll.xdsoft_scroller', [percentage, true]);
					    }
					    percentage = percentage > 1 ? 1 : (percentage < 0 || isNaN(percentage)) ? 0 : percentage;

					    scroller.css('margin-top', maximumOffset * percentage);
					    setTimeout(function () {
					        //qianlong
					        var mTop = yearmonthbox[0].offsetHeight - parentHeight;
					        yearmonthbox.css('marginTop', -parseInt((mTop < 0 ? 0 : mTop) * percentage, 10));
					    }, 10);
					})
					.on('resize_scroll.xdsoft_scroller', function (event, percentage, noTriggerScroll) {
					    var percent, sh;
					    parentHeight = yearmonthboxparent[0].clientHeight;
					    height = yearmonthbox[0].offsetHeight;
					    percent = parentHeight / height;
					    sh = percent * scrollbar[0].offsetHeight;
					    if (percent > 1) {
					        scroller.hide();
					    } else {
					        scroller.show();
					        scroller.css('height', parseInt(sh > 10 ? sh : 10, 10));
					        maximumOffset = scrollbar[0].offsetHeight - scroller[0].offsetHeight;
					        if (noTriggerScroll !== true) {
					            yearmonthboxparent.trigger('scroll_element.xdsoft_scroller', [percentage || Math.abs(parseInt(yearmonthbox.css('marginTop'), 10)) / (height - parentHeight)]);
					        }
					    }
					});

                yearmonthboxparent.on('mousewheel', function (event) {
                    var top = Math.abs(parseInt(yearmonthbox.css('marginTop'), 10));

                    top = top - (event.deltaY * 20);
                    if (top < 0) {
                        top = 0;
                    }

                    yearmonthboxparent.trigger('scroll_element.xdsoft_scroller', [top / (height - parentHeight)]);
                    event.stopPropagation();
                    return false;
                });

                yearmonthboxparent.on('touchstart', function (event) {
                    start = pointerEventToXY(event);
                    startTop = Math.abs(parseInt(yearmonthbox.css('marginTop'), 10));
                });

                yearmonthboxparent.on('touchmove', function (event) {
                    if (start) {
                        event.preventDefault();
                        var coord = pointerEventToXY(event);
                        yearmonthboxparent.trigger('scroll_element.xdsoft_scroller', [(startTop - (coord.y - start.y)) / (height - parentHeight)]);
                    }
                });

                yearmonthboxparent.on('touchend touchcancel', function () {
                    start = false;
                    startTop = 0;
                });
            }
            yearmonthboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
        });
    };

    $.fn.xdsoftScrollerWeek = function (percent) {
        return this.each(function () {
            var weekboxparent = $(this),
				pointerEventToXY = function (e) {
				    var out = { x: 0, y: 0 },
						touch;
				    if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend' || e.type === 'touchcancel') {
				        touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				        out.x = touch.clientX;
				        out.y = touch.clientY;
				    } else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove' || e.type === 'mouseover' || e.type === 'mouseout' || e.type === 'mouseenter' || e.type === 'mouseleave') {
				        out.x = e.clientX;
				        out.y = e.clientY;
				    }
				    return out;
				},
                weekbox,
				parentHeight,
				height,
				scrollbar,
				scroller,
				maximumOffset = 100,
				start = false,
				startY = 0,
				startTop = 0,
				h1 = 0,
				touchStart = false,
				startTopScroll = 0,
				calcOffset = function () { };
            if (percent === 'hide') {
                weekboxparent.find('.xdsoft_scrollbar').hide();
                return;
            }
            if (!$(this).hasClass('xdsoft_scroller_box')) {
                weekbox = weekboxparent.children().eq(0);

                parentHeight = weekboxparent[0].clientHeight;
                height = weekbox[0].offsetHeight;
                scrollbar = $('<div class="xdsoft_scrollbar"></div>');
                scroller = $('<div class="xdsoft_scroller"></div>');
                scrollbar.append(scroller);

                weekboxparent.addClass('xdsoft_scroller_box').append(scrollbar);
                calcOffset = function calcOffset(event) {
                    var offset = pointerEventToXY(event).y - startY + startTopScroll;
                    if (offset < 0) {
                        offset = 0;
                    }
                    if (offset + scroller[0].offsetHeight > h1) {
                        offset = h1 - scroller[0].offsetHeight;
                    }
                    weekboxparent.trigger('scroll_element.xdsoft_scroller', [maximumOffset ? offset / maximumOffset : 0]);
                };

                scroller
					.on('touchstart.xdsoft_scroller mousedown.xdsoft_scroller', function (event) {
					    if (!parentHeight) {
					        weekboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
					    }

					    startY = pointerEventToXY(event).y;
					    startTopScroll = parseInt(scroller.css('margin-top'), 10);
					    h1 = scrollbar[0].offsetHeight;

					    if (event.type === 'mousedown' || event.type === 'touchstart') {
					        if (document) {
					            $(document.body).addClass('xdsoft_noselect');
					        }
					        $([document.body, window]).on('touchend mouseup.xdsoft_scroller', function arguments_callee() {
					            $([document.body, window]).off('touchend mouseup.xdsoft_scroller', arguments_callee)
									.off('mousemove.xdsoft_scroller', calcOffset)
									.removeClass('xdsoft_noselect');
					        });
					        $(document.body).on('mousemove.xdsoft_scroller', calcOffset);
					    } else {
					        touchStart = true;
					        event.stopPropagation();
					        event.preventDefault();
					    }
					})
					.on('touchmove', function (event) {
					    if (touchStart) {
					        event.preventDefault();
					        calcOffset(event);
					    }
					})
					.on('touchend touchcancel', function () {
					    touchStart = false;
					    startTopScroll = 0;
					});

                weekboxparent
					.on('scroll_element.xdsoft_scroller', function (event, percentage) {
					    if (!parentHeight) {
					        weekboxparent.trigger('resize_scroll.xdsoft_scroller', [percentage, true]);
					    }
					    percentage = percentage > 1 ? 1 : (percentage < 0 || isNaN(percentage)) ? 0 : percentage;

					    scroller.css('margin-top', maximumOffset * percentage);
					    setTimeout(function () {
					        //qianlong
					        var mTop = weekbox[0].offsetHeight - parentHeight;
					        weekbox.css('marginTop', -parseInt((mTop < 0 ? 0 : mTop) * percentage, 10));
					    }, 10);
					})
					.on('resize_scroll.xdsoft_scroller', function (event, percentage, noTriggerScroll) {
					    var percent, sh;
					    parentHeight = weekboxparent[0].clientHeight;
					    height = weekbox[0].offsetHeight;
					    percent = parentHeight / height;
					    sh = percent * scrollbar[0].offsetHeight;
					    if (percent > 1) {
					        scroller.hide();
					    } else {
					        scroller.show();
					        scroller.css('height', parseInt(sh > 10 ? sh : 10, 10));
					        maximumOffset = scrollbar[0].offsetHeight - scroller[0].offsetHeight;
					        if (noTriggerScroll !== true) {
					            weekboxparent.trigger('scroll_element.xdsoft_scroller', [percentage || Math.abs(parseInt(weekbox.css('marginTop'), 10)) / (height - parentHeight)]);
					        }
					    }
					});

                weekboxparent.on('mousewheel', function (event) {
                    var top = Math.abs(parseInt(weekbox.css('marginTop'), 10));

                    top = top - (event.deltaY * 20);
                    if (top < 0) {
                        top = 0;
                    }

                    weekboxparent.trigger('scroll_element.xdsoft_scroller', [top / (height - parentHeight)]);
                    event.stopPropagation();
                    return false;
                });

                weekboxparent.on('touchstart', function (event) {
                    start = pointerEventToXY(event);
                    startTop = Math.abs(parseInt(weekbox.css('marginTop'), 10));
                });

                weekboxparent.on('touchmove', function (event) {
                    if (start) {
                        event.preventDefault();
                        var coord = pointerEventToXY(event);
                        weekboxparent.trigger('scroll_element.xdsoft_scroller', [(startTop - (coord.y - start.y)) / (height - parentHeight)]);
                    }
                });

                weekboxparent.on('touchend touchcancel', function () {
                    start = false;
                    startTop = 0;
                });
            }
            weekboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
        });
    };
    //End

    $.fn.datetimepicker = function (opt, opt2) {
        var result = this,
			KEY0 = 48,
			KEY9 = 57,
			_KEY0 = 96,
			_KEY9 = 105,
			CTRLKEY = 17,
			DEL = 46,
			ENTER = 13,
			ESC = 27,
			BACKSPACE = 8,
			ARROWLEFT = 37,
			ARROWUP = 38,
			ARROWRIGHT = 39,
			ARROWDOWN = 40,
			TAB = 9,
			F5 = 116,
			AKEY = 65,
			CKEY = 67,
			VKEY = 86,
			ZKEY = 90,
			YKEY = 89,
			ctrlDown = false,
			options = ($.isPlainObject(opt) || !opt) ? $.extend(true, {}, default_options, opt) : $.extend(true, {}, default_options),

			lazyInitTimer = 0,
			createDateTimePicker,
			destroyDateTimePicker,

			lazyInit = function (input) {
			    input
					.on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function initOnActionCallback() {
					    if (input.is(':disabled') || input.data('xdsoft_datetimepicker')) {
					        return;
					    }
					    clearTimeout(lazyInitTimer);
					    lazyInitTimer = setTimeout(function () {

					        if (!input.data('xdsoft_datetimepicker')) {
					            createDateTimePicker(input);
					        }
					        input
								.off('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', initOnActionCallback)
								.trigger('open.xdsoft');
					    }, 100);
					});
			};

        createDateTimePicker = function (input) {
            var datetimepicker = $('<div class="xdsoft_datetimepicker xdsoft_noselect"></div>'),
				xdsoft_copyright = $('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
				datepicker = $('<div class="xdsoft_datepicker active"></div>'),
				month_picker = $('<div class="xdsoft_monthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button" title="今天"></button><button type="button" class="xdsoft_clear_button" title="清除"></button>' +
					'<div class="xdsoft_label xdsoft_month"><span></span><i></i></div>' +
					'<div class="xdsoft_label xdsoft_year"><span></span><i></i></div>' +
					'<button type="button" class="xdsoft_next"></button></div>'),
				calendar = $('<div class="xdsoft_calendar"></div>'),
				timepicker = $('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button><button type="button" class="xdsoft_clear_button" title="清除"></button></div>'),
				timeboxparent = timepicker.find('.xdsoft_time_box').eq(0),
                timebox = $('<div class="xdsoft_time_variant"></div>'),

                //qianlong begin
                yearpicker = $('<div class="xdsoft_yearpicker"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_year_box"></div><button type="button" class="xdsoft_next"></button></div>'),
                yearboxparent = yearpicker.find('.xdsoft_year_box').eq(0),
                yearbox = $('<div class="xdsoft_year_variant"></div>'),

                yearmonthpicker = $('<div class="xdsoft_yearmonthpicker"><div class="xdsoft_label xdsoft_year"><span></span><i></i></div><div class="xdsoft_yearmonth_box"></div><button type="button" class="xdsoft_next"></button></div>'),
                yearmonthboxparent = yearmonthpicker.find('.xdsoft_yearmonth_box').eq(0),
                yearmonthbox = $('<div class="xdsoft_yearmonth_variant"></div>'),

                weekpicker = $('<div class="xdsoft_weekpicker"><div class="xdsoft_label xdsoft_year"><span></span><i></i></div><div class="xdsoft_week_box"></div><button type="button" class="xdsoft_next"></button></div>'),
                weekboxparent = weekpicker.find('.xdsoft_week_box').eq(0),
                weekbox = $('<div class="xdsoft_week_variant"></div>'),
                //end

				applyButton = $('<button type="button" class="xdsoft_save_selected blue-gradient-button">Save Selected</button>'),

				monthselect = $('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>'),
				yearselect = $('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>'),
                //qianlong
                yearmonth_yearselect = $('<div class="xdsoft_select xdsoft_yearmonth_yearselect"><div></div></div>'),
                week_yearselect = $('<div class="xdsoft_select xdsoft_week_yearselect"><div></div></div>'),

				triggerAfterOpen = false,
				XDSoft_datetime,

				xchangeTimer,
				timerclick,
				current_time_index,
				setPos,
				timer = 0,
				_xdsoft_datetime,
				forEachAncestorOf,
				throttle;

            if (options.id) {
                datetimepicker.attr('id', options.id);
            }
            if (options.style) {
                datetimepicker.attr('style', options.style);
            }
            if (options.weeks) {
                datetimepicker.addClass('xdsoft_showweeks');
            }
            if (options.rtl) {
                datetimepicker.addClass('xdsoft_rtl');
            }

            datetimepicker.addClass('xdsoft_' + options.theme);
            datetimepicker.addClass(options.className);

            month_picker
				.find('.xdsoft_month span')
					.after(monthselect);
            month_picker
				.find('.xdsoft_year span')
					.after(yearselect);
            //qianlong
            //if (options.yearmonthpicker) {
            yearmonthpicker.find('.xdsoft_year span').after(yearmonth_yearselect);
            yearmonthpicker.find('.xdsoft_label').css({ 'float': 'none', 'padding-left': '5px', 'padding-right': '5px' });
            yearmonthpicker.find('.xdsoft_yearmonth_box').css('margin-top', '2px');
            yearmonthpicker.find('.xdsoft_select').css('top', '25px');
            yearmonthpicker
            .find('.xdsoft_year')
                .on('touchstart mousedown.xdsoft', function (event) {
                    var select = $(this).find('.xdsoft_select').eq(0),
                        val = 0,
                        top = 0,
                        visible = select.is(':visible'),
                        items,
                        i;

                    yearmonthpicker
                        .find('.xdsoft_select')
                            .hide();
                    if (_xdsoft_datetime.currentTime) {
                        val = _xdsoft_datetime.currentTime['getFullYear']();
                    }

                    select[visible ? 'hide' : 'show']();
                    for (items = select.find('div.xdsoft_option'), i = 0; i < items.length; i += 1) {
                        if (items.eq(i).data('value') === val) {
                            break;
                        } else {
                            top += items[0].offsetHeight;
                        }
                    }

                    select.xdsoftScroller(top / (select.children()[0].offsetHeight - (select[0].clientHeight)));
                    event.stopPropagation();
                    return false;
                });

            weekpicker.find('.xdsoft_year span').after(week_yearselect);
            weekpicker.find('.xdsoft_label').css({ 'float': 'none', 'padding-left': '5px', 'padding-right': '5px' });
            weekpicker.find('.xdsoft_week_box').css('margin-top', '2px');
            weekpicker.find('.xdsoft_select').css('top', '25px');
            weekpicker
				.find('.xdsoft_year')
					.on('touchstart mousedown.xdsoft', function (event) {
					    var select = $(this).find('.xdsoft_select').eq(0),
                            val = 0,
                            top = 0,
                            visible = select.is(':visible'),
                            items,
                            i;

					    weekpicker
                            .find('.xdsoft_select')
                                .hide();
					    if (_xdsoft_datetime.currentTime) {
					        val = _xdsoft_datetime.currentTime['getFullYear']();
					    }

					    select[visible ? 'hide' : 'show']();
					    for (items = select.find('div.xdsoft_option'), i = 0; i < items.length; i += 1) {
					        if (items.eq(i).data('value') === val) {
					            break;
					        } else {
					            top += items[0].offsetHeight;
					        }
					    }

					    select.xdsoftScroller(top / (select.children()[0].offsetHeight - (select[0].clientHeight)));
					    event.stopPropagation();
					    return false;
					});
            //}

            month_picker
				.find('.xdsoft_month,.xdsoft_year')
					.on('touchstart mousedown.xdsoft', function (event) {
					    var select = $(this).find('.xdsoft_select').eq(0),
                            val = 0,
                            top = 0,
                            visible = select.is(':visible'),
                            items,
                            i;

					    month_picker
                            .find('.xdsoft_select')
                                .hide();
					    if (_xdsoft_datetime.currentTime) {
					        val = _xdsoft_datetime.currentTime[$(this).hasClass('xdsoft_month') ? 'getMonth' : 'getFullYear']();
					    }

					    select[visible ? 'hide' : 'show']();
					    for (items = select.find('div.xdsoft_option'), i = 0; i < items.length; i += 1) {
					        if (items.eq(i).data('value') === val) {
					            break;
					        } else {
					            top += items[0].offsetHeight;
					        }
					    }

					    select.xdsoftScroller(top / (select.children()[0].offsetHeight - (select[0].clientHeight)));
					    event.stopPropagation();
					    return false;
					});
            //qianlong
            month_picker
				.find('.xdsoft_select')
					.xdsoftScroller()
				.on('touchstart mousedown.xdsoft', function (event) {
				    event.stopPropagation();
				    event.preventDefault();
				})
				.on('touchstart mousedown.xdsoft', '.xdsoft_option', function () {
				    if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
				        _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
				    }

				    var year = _xdsoft_datetime.currentTime.getFullYear();
				    if (_xdsoft_datetime && _xdsoft_datetime.currentTime) {
				        _xdsoft_datetime.currentTime[$(this).parent().parent().hasClass('xdsoft_monthselect') ? 'setMonth' : 'setFullYear']($(this).data('value'));
				    }

				    $(this).parent().parent().hide();

				    datetimepicker.trigger('xchange.xdsoft');
				    if (options.onChangeMonth && $.isFunction(options.onChangeMonth)) {
				        options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
				    }

				    if (year !== _xdsoft_datetime.currentTime.getFullYear() && $.isFunction(options.onChangeYear)) {
				        options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
				    }
				});
            //qianlong
            yearmonthpicker
				.find('.xdsoft_select')
					.xdsoftScroller()
				.on('touchstart mousedown.xdsoft', function (event) {
				    event.stopPropagation();
				    event.preventDefault();
				})
				.on('touchstart mousedown.xdsoft', '.xdsoft_option', function () {
				    if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
				        _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
				    }

				    var year = _xdsoft_datetime.currentTime.getFullYear();
				    if (_xdsoft_datetime && _xdsoft_datetime.currentTime) {
				        _xdsoft_datetime.currentTime['setFullYear']($(this).data('value'));
				    }

				    $(this).parent().parent().hide();

				    datetimepicker.trigger('xchange.xdsoft');
				    if (year !== _xdsoft_datetime.currentTime.getFullYear() && $.isFunction(options.onChangeYear)) {
				        options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
				    }
				});
            //qianlong
            weekpicker
				.find('.xdsoft_select')
					.xdsoftScroller()
				.on('touchstart mousedown.xdsoft', function (event) {
				    event.stopPropagation();
				    event.preventDefault();
				})
				.on('touchstart mousedown.xdsoft', '.xdsoft_option', function () {
				    if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
				        _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
				    }

				    var year = _xdsoft_datetime.currentTime.getFullYear();
				    if (_xdsoft_datetime && _xdsoft_datetime.currentTime) {
				        _xdsoft_datetime.currentTime['setFullYear']($(this).data('value'));
				    }

				    $(this).parent().parent().hide();

				    datetimepicker.trigger('xchange.xdsoft');
				    if (year !== _xdsoft_datetime.currentTime.getFullYear() && $.isFunction(options.onChangeYear)) {
				        options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
				    }
				});

            datetimepicker.getValue = function () {
                return _xdsoft_datetime.getCurrentTime();
            };

            datetimepicker.setOptions = function (_options) {
                var highlightedDates = {};

                options = $.extend(true, {}, options, _options);

                if (_options.allowTimes && $.isArray(_options.allowTimes) && _options.allowTimes.length) {
                    options.allowTimes = $.extend(true, [], _options.allowTimes);
                }

                if (_options.weekends && $.isArray(_options.weekends) && _options.weekends.length) {
                    options.weekends = $.extend(true, [], _options.weekends);
                }

                if (_options.allowDates && $.isArray(_options.allowDates) && _options.allowDates.length) {
                    options.allowDates = $.extend(true, [], _options.allowDates);
                }

                if (_options.allowDateRe && Object.prototype.toString.call(_options.allowDateRe) === "[object String]") {
                    options.allowDateRe = new RegExp(_options.allowDateRe);
                }

                if (_options.highlightedDates && $.isArray(_options.highlightedDates) && _options.highlightedDates.length) {
                    $.each(_options.highlightedDates, function (index, value) {
                        var splitData = $.map(value.split(','), $.trim),
							exDesc,
							hDate = new HighlightedDate(dateHelper.parseDate(splitData[0], options.formatDate), splitData[1], splitData[2]), // date, desc, style
							keyDate = dateHelper.formatDate(hDate.date, options.formatDate);
                        if (highlightedDates[keyDate] !== undefined) {
                            exDesc = highlightedDates[keyDate].desc;
                            if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
                                highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
                            }
                        } else {
                            highlightedDates[keyDate] = hDate;
                        }
                    });

                    options.highlightedDates = $.extend(true, [], highlightedDates);
                }

                if (_options.highlightedPeriods && $.isArray(_options.highlightedPeriods) && _options.highlightedPeriods.length) {
                    highlightedDates = $.extend(true, [], options.highlightedDates);
                    $.each(_options.highlightedPeriods, function (index, value) {
                        var dateTest, // start date
							dateEnd,
							desc,
							hDate,
							keyDate,
							exDesc,
							style;
                        if ($.isArray(value)) {
                            dateTest = value[0];
                            dateEnd = value[1];
                            desc = value[2];
                            style = value[3];
                        }
                        else {
                            var splitData = $.map(value.split(','), $.trim);
                            dateTest = dateHelper.parseDate(splitData[0], options.formatDate);
                            dateEnd = dateHelper.parseDate(splitData[1], options.formatDate);
                            desc = splitData[2];
                            style = splitData[3];
                        }

                        while (dateTest <= dateEnd) {
                            hDate = new HighlightedDate(dateTest, desc, style);
                            keyDate = dateHelper.formatDate(dateTest, options.formatDate);
                            dateTest.setDate(dateTest.getDate() + 1);
                            if (highlightedDates[keyDate] !== undefined) {
                                exDesc = highlightedDates[keyDate].desc;
                                if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
                                    highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
                                }
                            } else {
                                highlightedDates[keyDate] = hDate;
                            }
                        }
                    });

                    options.highlightedDates = $.extend(true, [], highlightedDates);
                }

                if (_options.disabledDates && $.isArray(_options.disabledDates) && _options.disabledDates.length) {
                    options.disabledDates = $.extend(true, [], _options.disabledDates);
                }

                if (_options.disabledWeekDays && $.isArray(_options.disabledWeekDays) && _options.disabledWeekDays.length) {
                    options.disabledWeekDays = $.extend(true, [], _options.disabledWeekDays);
                }

                if ((options.open || options.opened) && (!options.inline)) {
                    input.trigger('open.xdsoft');
                }

                if (options.inline) {
                    triggerAfterOpen = true;
                    datetimepicker.addClass('xdsoft_inline');
                    input.after(datetimepicker).hide();
                }

                if (options.inverseButton) {
                    options.next = 'xdsoft_prev';
                    options.prev = 'xdsoft_next';
                }

                if (options.datepicker) {
                    datepicker.addClass('active');
                } else {
                    datepicker.removeClass('active');
                }

                if (options.timepicker) {
                    timepicker.addClass('active');
                } else {
                    timepicker.removeClass('active');
                }
                //qianlong
                if (options.yearpicker) {
                    yearpicker.addClass('active');
                } else {
                    yearpicker.removeClass('active');
                }

                if (options.yearmonthpicker) {
                    yearmonthpicker.addClass('active');
                } else {
                    yearmonthpicker.removeClass('active');
                }

                if (options.weekpicker) {
                    weekpicker.addClass('active');
                } else {
                    weekpicker.removeClass('active');
                }
                //End

                if (options.value) {
                    _xdsoft_datetime.setCurrentTime(options.value);
                    if (input && input.val) {
                        input.val(_xdsoft_datetime.str);
                    }
                }

                if (isNaN(options.dayOfWeekStart)) {
                    options.dayOfWeekStart = 0;
                } else {
                    options.dayOfWeekStart = parseInt(options.dayOfWeekStart, 10) % 7;
                }

                if (!options.timepickerScrollbar) {
                    timeboxparent.xdsoftScroller('hide');
                }

                if (options.minDate && /^[\+\-](.*)$/.test(options.minDate)) {
                    options.minDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.minDate), options.formatDate);
                }

                if (options.maxDate && /^[\+\-](.*)$/.test(options.maxDate)) {
                    options.maxDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.maxDate), options.formatDate);
                }

                applyButton.toggle(options.showApplyButton);

                month_picker
					.find('.xdsoft_today_button')
						.css('visibility', !options.todayButton ? 'hidden' : 'visible');
                //qianlong 清除按钮
                month_picker
					.find('.xdsoft_clear_button')
						.css('visibility', !options.todayButton ? 'hidden' : 'visible');

                month_picker
					.find('.' + options.prev)
						.css('visibility', !options.prevButton ? 'hidden' : 'visible');

                month_picker
					.find('.' + options.next)
						.css('visibility', !options.nextButton ? 'hidden' : 'visible');

                setMask(options);

                if (options.validateOnBlur) {
                    input
						.off('blur.xdsoft')
						.on('blur.xdsoft', function () {
						    if (options.allowBlank && (!$.trim($(this).val()).length || (typeof options.mask == "string" && $.trim($(this).val()) === options.mask.replace(/[0-9]/g, '_')))) {
						        $(this).val(null);
						        datetimepicker.data('xdsoft_datetime').empty();
						    } else {
						        var d = dateHelper.parseDate($(this).val(), options.format);
						        if (d) { // parseDate() may skip some invalid parts like date or time, so make it clear for user: show parsed date/time
						            $(this).val(dateHelper.formatDate(d, options.format));
						        } else {
						            //qianlong
						            if (options.timepicker) {
						                var splittedHours = +([$(this).val()[0], $(this).val()[1]].join('')),
                                            splittedMinutes = +([$(this).val()[2], $(this).val()[3]].join(''));

						                // parse the numbers as 0312 => 03:12
						                if (!options.datepicker && options.timepicker && splittedHours >= 0 && splittedHours < 24 && splittedMinutes >= 0 && splittedMinutes < 60) {
						                    $(this).val([splittedHours, splittedMinutes].map(function (item) {
						                        return item > 9 ? item : '0' + item;
						                    }).join(':'));
						                } else {
						                    $(this).val(dateHelper.formatDate(_xdsoft_datetime.now(), options.format));
						                }
						            }
						            if (options.datepicker || options.timepicker) {
						                Dialog.showError("输入的格式不正确");
						                $(this).val("");
						                $(this).focus();
						                datetimepicker.data('xdsoft_datetime').empty();
						            }
						        }
						        datetimepicker.data('xdsoft_datetime').setCurrentTime($(this).val());
						    }

						    datetimepicker.trigger('changedatetime.xdsoft');
						    datetimepicker.trigger('close.xdsoft');
						});
                }
                options.dayOfWeekStartPrev = (options.dayOfWeekStart === 0) ? 6 : options.dayOfWeekStart - 1;

                datetimepicker
					.trigger('xchange.xdsoft')
					.trigger('afterOpen.xdsoft');
            };

            datetimepicker
				.data('options', options)
				.on('touchstart mousedown.xdsoft', function (event) {
				    event.stopPropagation();
				    event.preventDefault();
				    yearselect.hide();
				    yearmonth_yearselect.hide();
				    monthselect.hide();
				    return false;
				});

            //scroll_element = timepicker.find('.xdsoft_time_box');
            timeboxparent.append(timebox);
            timeboxparent.xdsoftScroller();
            //qianlong
            yearboxparent.append(yearbox);
            yearboxparent.xdsoftScrollerYear();

            yearmonthboxparent.append(yearmonthbox);
            yearmonthboxparent.xdsoftScrollerYearMonth();

            weekboxparent.append(weekbox);
            weekboxparent.xdsoftScrollerWeek();
            //End

            datetimepicker.on('afterOpen.xdsoft', function () {
                timeboxparent.xdsoftScroller();
                //qianlong
                yearboxparent.xdsoftScrollerYear();
                yearmonthboxparent.xdsoftScrollerYearMonth();
                weekboxparent.xdsoftScrollerWeek();
                //End
            });

            datetimepicker
				.append(datepicker)
				.append(timepicker)
                .append(yearpicker)//qianlong
            .append(yearmonthpicker)//qianlong
            .append(weekpicker);//qianlong
            if (options.withoutCopyright !== true) {
                datetimepicker
					.append(xdsoft_copyright);
            }

            datepicker
				.append(month_picker)
				.append(calendar)
				.append(applyButton);

            $(options.parentID)
				.append(datetimepicker);

            XDSoft_datetime = function () {
                var _this = this;
                _this.now = function (norecursion) {
                    var d = new Date(),
						date,
						time;

                    if (!norecursion && options.defaultDate) {
                        date = _this.strToDateTime(options.defaultDate);
                        d.setFullYear(date.getFullYear());
                        d.setMonth(date.getMonth());
                        d.setDate(date.getDate());
                    }

                    if (options.yearOffset) {
                        d.setFullYear(d.getFullYear() + options.yearOffset);
                    }

                    if (!norecursion && options.defaultTime) {
                        time = _this.strtotime(options.defaultTime);
                        d.setHours(time.getHours());
                        d.setMinutes(time.getMinutes());
                    }
                    return d;
                };

                _this.isValidDate = function (d) {
                    if (Object.prototype.toString.call(d) !== "[object Date]") {
                        return false;
                    }
                    return !isNaN(d.getTime());
                };

                _this.setCurrentTime = function (dTime, requireValidDate) {
                    if (typeof dTime === 'string') {
                        _this.currentTime = _this.strToDateTime(dTime);
                    }
                    else if (_this.isValidDate(dTime)) {
                        _this.currentTime = dTime;
                    }
                    else if (!dTime && !requireValidDate && options.allowBlank) {
                        _this.currentTime = null;
                    }
                    else {
                        _this.currentTime = _this.now();
                    }

                    datetimepicker.trigger('xchange.xdsoft');
                };

                _this.empty = function () {
                    _this.currentTime = null;
                };

                _this.getCurrentTime = function (dTime) {
                    return _this.currentTime;
                };

                _this.nextMonth = function () {

                    if (_this.currentTime === undefined || _this.currentTime === null) {
                        _this.currentTime = _this.now();
                    }

                    var month = _this.currentTime.getMonth() + 1,
						year;
                    if (month === 12) {
                        _this.currentTime.setFullYear(_this.currentTime.getFullYear() + 1);
                        month = 0;
                    }

                    year = _this.currentTime.getFullYear();

                    _this.currentTime.setDate(
						Math.min(
							new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(),
							_this.currentTime.getDate()
						)
					);
                    _this.currentTime.setMonth(month);

                    if (options.onChangeMonth && $.isFunction(options.onChangeMonth)) {
                        options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
                    }

                    if (year !== _this.currentTime.getFullYear() && $.isFunction(options.onChangeYear)) {
                        options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
                    }

                    datetimepicker.trigger('xchange.xdsoft');
                    return month;
                };

                _this.prevMonth = function () {

                    if (_this.currentTime === undefined || _this.currentTime === null) {
                        _this.currentTime = _this.now();
                    }

                    var month = _this.currentTime.getMonth() - 1;
                    if (month === -1) {
                        _this.currentTime.setFullYear(_this.currentTime.getFullYear() - 1);
                        month = 11;
                    }
                    _this.currentTime.setDate(
						Math.min(
							new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(),
							_this.currentTime.getDate()
						)
					);
                    _this.currentTime.setMonth(month);
                    if (options.onChangeMonth && $.isFunction(options.onChangeMonth)) {
                        options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
                    }
                    datetimepicker.trigger('xchange.xdsoft');
                    return month;
                };

                _this.getWeekOfYear = function (datetime) {
                    if (options.onGetWeekOfYear && $.isFunction(options.onGetWeekOfYear)) {
                        var week = options.onGetWeekOfYear.call(datetimepicker, datetime);
                        if (typeof week !== 'undefined') {
                            return week;
                        }
                    }
                    var onejan = new Date(datetime.getFullYear(), 0, 1);
                    //First week of the year is th one with the first Thursday according to ISO8601
                    if (onejan.getDay() != 4)
                        onejan.setMonth(0, 1 + ((4 - onejan.getDay() + 7) % 7));
                    return Math.ceil((((datetime - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                };

                _this.strToDateTime = function (sDateTime) {
                    var tmpDate = [], timeOffset, currentTime;

                    if (sDateTime && sDateTime instanceof Date && _this.isValidDate(sDateTime)) {
                        return sDateTime;
                    }

                    tmpDate = /^(\+|\-)(.*)$/.exec(sDateTime);
                    if (tmpDate) {
                        tmpDate[2] = dateHelper.parseDate(tmpDate[2], options.formatDate);
                    }
                    if (tmpDate && tmpDate[2]) {
                        timeOffset = tmpDate[2].getTime() - (tmpDate[2].getTimezoneOffset()) * 60000;
                        currentTime = new Date((_this.now(true)).getTime() + parseInt(tmpDate[1] + '1', 10) * timeOffset);
                    } else {
                        currentTime = sDateTime ? dateHelper.parseDate(sDateTime, options.format) : _this.now();
                    }

                    if (!_this.isValidDate(currentTime)) {
                        currentTime = _this.now();
                    }

                    return currentTime;
                };

                _this.strToDate = function (sDate) {
                    if (sDate && sDate instanceof Date && _this.isValidDate(sDate)) {
                        return sDate;
                    }

                    var currentTime = sDate ? dateHelper.parseDate(sDate, options.formatDate) : _this.now(true);
                    if (!_this.isValidDate(currentTime)) {
                        currentTime = _this.now(true);
                    }
                    return currentTime;
                };

                _this.strtotime = function (sTime) {
                    if (sTime && sTime instanceof Date && _this.isValidDate(sTime)) {
                        return sTime;
                    }
                    var currentTime = sTime ? dateHelper.parseDate(sTime, options.formatTime) : _this.now(true);
                    if (!_this.isValidDate(currentTime)) {
                        currentTime = _this.now(true);
                    }
                    return currentTime;
                };

                _this.str = function () {
                    return dateHelper.formatDate(_this.currentTime, options.format);
                };
                _this.currentTime = this.now();
            };

            _xdsoft_datetime = new XDSoft_datetime();

            applyButton.on('touchend click', function (e) {//pathbrite
                e.preventDefault();
                datetimepicker.data('changed', true);
                _xdsoft_datetime.setCurrentTime(getCurrentValue());
                input.val(_xdsoft_datetime.str());
                datetimepicker.trigger('close.xdsoft');
            });
            month_picker
				.find('.xdsoft_today_button')
				.on('touchend mousedown.xdsoft', function () {
				    datetimepicker.data('changed', true);
				    _xdsoft_datetime.setCurrentTime(0, true);
				    datetimepicker.trigger('afterOpen.xdsoft');
				}).on('dblclick.xdsoft', function () {
				    var currentDate = _xdsoft_datetime.getCurrentTime(), minDate, maxDate;
				    currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
				    minDate = _xdsoft_datetime.strToDate(options.minDate);
				    minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());
				    if (currentDate < minDate) {
				        return;
				    }
				    maxDate = _xdsoft_datetime.strToDate(options.maxDate);
				    maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate());
				    if (currentDate > maxDate) {
				        return;
				    }
				    input.val(_xdsoft_datetime.str());
				    input.trigger('change');
				    datetimepicker.trigger('close.xdsoft');
				});
            //qianlong 清除事件
            month_picker
				.find('.xdsoft_clear_button')
				.on('touchend mousedown.xdsoft', function () {
				    input.val("");
				}).on('click.xdsoft', function () {
				    input.val("");
				    input.trigger('change');
				    datetimepicker.trigger('close.xdsoft');
				});

            month_picker
				.find('.xdsoft_prev,.xdsoft_next')
				.on('touchend mousedown.xdsoft', function () {
				    var $this = $(this),
						timer = 0,
						stop = false;

				    (function arguments_callee1(v) {
				        if ($this.hasClass(options.next)) {
				            _xdsoft_datetime.nextMonth();
				        } else if ($this.hasClass(options.prev)) {
				            _xdsoft_datetime.prevMonth();
				        }
				        if (options.monthChangeSpinner) {
				            if (!stop) {
				                timer = setTimeout(arguments_callee1, v || 100);
				            }
				        }
				    }(500));

				    $([document.body, window]).on('touchend mouseup.xdsoft', function arguments_callee2() {
				        clearTimeout(timer);
				        stop = true;
				        $([document.body, window]).off('touchend mouseup.xdsoft', arguments_callee2);
				    });
				});

            timepicker
				.find('.xdsoft_prev,.xdsoft_next')
				.on('touchend mousedown.xdsoft', function () {
				    var $this = $(this),
						timer = 0,
						stop = false,
						period = 110;
				    (function arguments_callee4(v) {
				        var pheight = timeboxparent[0].clientHeight,
							height = timebox[0].offsetHeight,
							top = Math.abs(parseInt(timebox.css('marginTop'), 10));
				        if ($this.hasClass(options.next) && (height - pheight) - options.timeHeightInTimePicker >= top) {
				            timebox.css('marginTop', '-' + (top + options.timeHeightInTimePicker) + 'px');
				        } else if ($this.hasClass(options.prev) && top - options.timeHeightInTimePicker >= 0) {
				            timebox.css('marginTop', '-' + (top - options.timeHeightInTimePicker) + 'px');
				        }
				        /**
                         * Fixed bug:
                         * When using css3 transition, it will cause a bug that you cannot scroll the timepicker list.
                         * The reason is that the transition-duration time, if you set it to 0, all things fine, otherwise, this
                         * would cause a bug when you use jquery.css method.
                         * Let's say: * { transition: all .5s ease; }
                         * jquery timebox.css('marginTop') will return the original value which is before you clicking the next/prev button,
                         * meanwhile the timebox[0].style.marginTop will return the right value which is after you clicking the
                         * next/prev button.
                         * 
                         * What we should do:
                         * Replace timebox.css('marginTop') with timebox[0].style.marginTop.
                         */
				        timeboxparent.trigger('scroll_element.xdsoft_scroller', [Math.abs(parseInt(timebox[0].style.marginTop, 10) / (height - pheight))]);
				        period = (period > 10) ? 10 : period - 10;
				        if (!stop) {
				            timer = setTimeout(arguments_callee4, v || period);
				        }
				    }(500));
				    $([document.body, window]).on('touchend mouseup.xdsoft', function arguments_callee5() {
				        clearTimeout(timer);
				        stop = true;
				        $([document.body, window])
							.off('touchend mouseup.xdsoft', arguments_callee5);
				    });
				});
            //qianlong 清除事件
            timepicker
				.find('.xdsoft_clear_button')
				.on('touchend mousedown.xdsoft', function () {
				    input.val("");
				}).on('click.xdsoft', function () {
				    input.val("");
				    input.trigger('change');
				    datetimepicker.trigger('close.xdsoft');
				});

            //qianlong
            yearpicker
				.find('.xdsoft_prev,.xdsoft_next')
				.on('touchend mousedown.xdsoft', function () {
				    var $this = $(this),
						timer = 0,
						stop = false,
						period = 110;
				    (function arguments_callee4(v) {
				        var pheight = yearboxparent[0].clientHeight,
							height = yearbox[0].offsetHeight,
							top = Math.abs(parseInt(yearbox.css('marginTop'), 10));
				        if ($this.hasClass(options.next) && (height - pheight) - options.timeHeightInTimePicker >= top) {
				            yearbox.css('marginTop', '-' + (top + options.timeHeightInTimePicker) + 'px');
				        } else if ($this.hasClass(options.prev) && top - options.timeHeightInTimePicker >= 0) {
				            yearbox.css('marginTop', '-' + (top - options.timeHeightInTimePicker) + 'px');
				        }
				        yearboxparent.trigger('scroll_element.xdsoft_scroller', [Math.abs(parseInt(yearbox[0].style.marginTop, 10) / (height - pheight))]);
				        period = (period > 10) ? 10 : period - 10;
				        if (!stop) {
				            timer = setTimeout(arguments_callee4, v || period);
				        }
				    }(500));
				    $([document.body, window]).on('touchend mouseup.xdsoft', function arguments_callee5() {
				        clearTimeout(timer);
				        stop = true;
				        $([document.body, window])
							.off('touchend mouseup.xdsoft', arguments_callee5);
				    });
				});

            yearmonthpicker
				.find('.xdsoft_prev,.xdsoft_next')
				.on('touchend mousedown.xdsoft', function () {
				    var $this = $(this),
						timer = 0,
						stop = false,
						period = 110;
				    (function arguments_callee4(v) {
				        var pheight = yearmonthboxparent[0].clientHeight,
							height = yearmonthbox[0].offsetHeight,
							top = Math.abs(parseInt(yearmonthbox.css('marginTop'), 10));
				        if ($this.hasClass(options.next) && (height - pheight) - options.timeHeightInTimePicker >= top) {
				            yearmonthbox.css('marginTop', '-' + (top + options.timeHeightInTimePicker) + 'px');
				        } else if ($this.hasClass(options.prev) && top - options.timeHeightInTimePicker >= 0) {
				            yearmonthbox.css('marginTop', '-' + (top - options.timeHeightInTimePicker) + 'px');
				        }
				        yearmonthboxparent.trigger('scroll_element.xdsoft_scroller', [Math.abs(parseInt(yearmonthbox[0].style.marginTop, 10) / (height - pheight))]);
				        period = (period > 10) ? 10 : period - 10;
				        if (!stop) {
				            timer = setTimeout(arguments_callee4, v || period);
				        }
				    }(500));
				    $([document.body, window]).on('touchend mouseup.xdsoft', function arguments_callee5() {
				        clearTimeout(timer);
				        stop = true;
				        $([document.body, window])
							.off('touchend mouseup.xdsoft', arguments_callee5);
				    });
				});

            weekpicker
				.find('.xdsoft_prev,.xdsoft_next')
				.on('touchend mousedown.xdsoft', function () {
				    var $this = $(this),
						timer = 0,
						stop = false,
						period = 110;
				    (function arguments_callee4(v) {
				        var pheight = weekboxparent[0].clientHeight,
							height = weekbox[0].offsetHeight,
							top = Math.abs(parseInt(weekbox.css('marginTop'), 10));
				        if ($this.hasClass(options.next) && (height - pheight) - options.timeHeightInTimePicker >= top) {
				            weekbox.css('marginTop', '-' + (top + options.timeHeightInTimePicker) + 'px');
				        } else if ($this.hasClass(options.prev) && top - options.timeHeightInTimePicker >= 0) {
				            weekbox.css('marginTop', '-' + (top - options.timeHeightInTimePicker) + 'px');
				        }
				        weekboxparent.trigger('scroll_element.xdsoft_scroller', [Math.abs(parseInt(weekbox[0].style.marginTop, 10) / (height - pheight))]);
				        period = (period > 10) ? 10 : period - 10;
				        if (!stop) {
				            timer = setTimeout(arguments_callee4, v || period);
				        }
				    }(500));
				    $([document.body, window]).on('touchend mouseup.xdsoft', function arguments_callee5() {
				        clearTimeout(timer);
				        stop = true;
				        $([document.body, window])
							.off('touchend mouseup.xdsoft', arguments_callee5);
				    });
				});
            //End

            xchangeTimer = 0;
            // base handler - generating a calendar and timepicker
            datetimepicker
				.on('xchange.xdsoft', function (event) {
				    clearTimeout(xchangeTimer);
				    xchangeTimer = setTimeout(function () {

				        if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
				            //In case blanks are allowed, delay construction until we have a valid date 
				            if (options.allowBlank)
				                return;

				            _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
				        }

				        var table = '',
							start = new Date(_xdsoft_datetime.currentTime.getFullYear(), _xdsoft_datetime.currentTime.getMonth(), 1, 12, 0, 0),
							i = 0,
							j,
							today = _xdsoft_datetime.now(),
							maxDate = false,
							minDate = false,
							hDate,
							day,
							d,
							y,
							m,
							w,
							classes = [],
							customDateSettings,
							newRow = true,
							time = '',

							h = '',
							line_time,
                            //qianlong
                            year = '',
                            line_year,
                            yearmonth = '',
                            line_yearmonth,
                            week = '',
                            line_week,
                            //End

							description;

				        while (start.getDay() !== options.dayOfWeekStart) {
				            start.setDate(start.getDate() - 1);
				        }

				        table += '<table><thead><tr>';

				        if (options.weeks) {
				            table += '<th>周</th>';
				        }

				        for (j = 0; j < 7; j += 1) {
				            table += '<th>' + options.i18n[globalLocale].dayOfWeekShort[(j + options.dayOfWeekStart) % 7] + '</th>';
				        }

				        table += '</tr></thead>';
				        table += '<tbody>';

				        if (options.maxDate !== false) {
				            maxDate = _xdsoft_datetime.strToDate(options.maxDate);
				            maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate(), 23, 59, 59, 999);
				        }

				        if (options.minDate !== false) {
				            minDate = _xdsoft_datetime.strToDate(options.minDate);
				            minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());
				        }

				        while (i < _xdsoft_datetime.currentTime.countDaysInMonth() || start.getDay() !== options.dayOfWeekStart || _xdsoft_datetime.currentTime.getMonth() === start.getMonth()) {
				            classes = [];
				            i += 1;

				            day = start.getDay();
				            d = start.getDate();
				            y = start.getFullYear();
				            m = start.getMonth();
				            w = _xdsoft_datetime.getWeekOfYear(start);
				            description = '';

				            classes.push('xdsoft_date');

				            if (options.beforeShowDay && $.isFunction(options.beforeShowDay.call)) {
				                customDateSettings = options.beforeShowDay.call(datetimepicker, start);
				            } else {
				                customDateSettings = null;
				            }

				            if (options.allowDateRe && Object.prototype.toString.call(options.allowDateRe) === "[object RegExp]") {
				                if (!options.allowDateRe.test(dateHelper.formatDate(start, options.formatDate))) {
				                    classes.push('xdsoft_disabled');
				                }
				            } else if (options.allowDates && options.allowDates.length > 0) {
				                if (options.allowDates.indexOf(dateHelper.formatDate(start, options.formatDate)) === -1) {
				                    classes.push('xdsoft_disabled');
				                }
				            } else if ((maxDate !== false && start > maxDate) || (minDate !== false && start < minDate) || (customDateSettings && customDateSettings[0] === false)) {
				                classes.push('xdsoft_disabled');
				            } else if (options.disabledDates.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
				                classes.push('xdsoft_disabled');
				            } else if (options.disabledWeekDays.indexOf(day) !== -1) {
				                classes.push('xdsoft_disabled');
				            } /* //qianlong 注释控件的只读功能
                            else if (input.is('[readonly]')) {
				                classes.push('xdsoft_disabled');
				            }*/

				            if (customDateSettings && customDateSettings[1] !== "") {
				                classes.push(customDateSettings[1]);
				            }

				            if (_xdsoft_datetime.currentTime.getMonth() !== m) {
				                classes.push('xdsoft_other_month');
				            }

				            if ((options.defaultSelect || datetimepicker.data('changed')) && dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
				                classes.push('xdsoft_current');
				            }

				            if (dateHelper.formatDate(today, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
				                classes.push('xdsoft_today');
				            }

				            if (start.getDay() === 0 || start.getDay() === 6 || options.weekends.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
				                classes.push('xdsoft_weekend');
				            }

				            if (options.highlightedDates[dateHelper.formatDate(start, options.formatDate)] !== undefined) {
				                hDate = options.highlightedDates[dateHelper.formatDate(start, options.formatDate)];
				                classes.push(hDate.style === undefined ? 'xdsoft_highlighted_default' : hDate.style);
				                description = hDate.desc === undefined ? '' : hDate.desc;
				            }

				            if (options.beforeShowDay && $.isFunction(options.beforeShowDay)) {
				                classes.push(options.beforeShowDay(start));
				            }

				            if (newRow) {
				                table += '<tr>';
				                newRow = false;
				                if (options.weeks) {//显示周
				                    table += '<th>' + w + '</th>';
				                }
				            }

				            table += '<td data-date="' + d + '" data-month="' + m + '" data-year="' + y + '"' + ' class="xdsoft_date xdsoft_day_of_week' + start.getDay() + ' ' + classes.join(' ') + '" title="' + description + '">' +
										'<div>' + d + '</div>' +
									'</td>';

				            if (start.getDay() === options.dayOfWeekStartPrev) {
				                table += '</tr>';
				                newRow = true;
				            }

				            start.setDate(d + 1);
				        }
				        table += '</tbody></table>';

				        calendar.html(table);

				        month_picker.find('.xdsoft_label span').eq(0).text(options.i18n[globalLocale].months[_xdsoft_datetime.currentTime.getMonth()]);
				        month_picker.find('.xdsoft_label span').eq(1).text(_xdsoft_datetime.currentTime.getFullYear());
				        //qianlong
				        var wy = parseInt(options.yearEnd, 10) > _xdsoft_datetime.currentTime.getFullYear() ? _xdsoft_datetime.currentTime.getFullYear() : options.yearEnd;//取出最大的年份
				        wy = wy + options.yearOffset;
				        yearmonthpicker.find('.xdsoft_label span').eq(0).text(wy);
				        weekpicker.find('.xdsoft_label span').eq(0).text(wy);
				        // generate timebox
				        time = '';
				        h = '';
				        m = '';

				        line_time = function line_time(h, m) {
				            var now = _xdsoft_datetime.now(), optionDateTime, current_time,
								isALlowTimesInit = options.allowTimes && $.isArray(options.allowTimes) && options.allowTimes.length;
				            now.setHours(h);
				            h = parseInt(now.getHours(), 10);
				            now.setMinutes(m);
				            m = parseInt(now.getMinutes(), 10);
				            optionDateTime = new Date(_xdsoft_datetime.currentTime);
				            optionDateTime.setHours(h);
				            optionDateTime.setMinutes(m);
				            classes = [];
				            if ((options.minDateTime !== false && options.minDateTime > optionDateTime) || (options.maxTime !== false && _xdsoft_datetime.strtotime(options.maxTime).getTime() < now.getTime()) || (options.minTime !== false && _xdsoft_datetime.strtotime(options.minTime).getTime() > now.getTime())) {
				                classes.push('xdsoft_disabled');
				            } else if ((options.minDateTime !== false && options.minDateTime > optionDateTime) || ((options.disabledMinTime !== false && now.getTime() > _xdsoft_datetime.strtotime(options.disabledMinTime).getTime()) && (options.disabledMaxTime !== false && now.getTime() < _xdsoft_datetime.strtotime(options.disabledMaxTime).getTime()))) {
				                classes.push('xdsoft_disabled');
				            } /*else if (input.is('[readonly]')) {
				                classes.push('xdsoft_disabled');
				            }*/

				            current_time = new Date(_xdsoft_datetime.currentTime);
				            current_time.setHours(parseInt(_xdsoft_datetime.currentTime.getHours(), 10));

				            if (!isALlowTimesInit) {
				                current_time.setMinutes(Math[options.roundTime](_xdsoft_datetime.currentTime.getMinutes() / options.step) * options.step);
				            }

				            if ((options.initTime || options.defaultSelect || datetimepicker.data('changed')) && current_time.getHours() === parseInt(h, 10) && ((!isALlowTimesInit && options.step > 59) || current_time.getMinutes() === parseInt(m, 10))) {
				                if (options.defaultSelect || datetimepicker.data('changed')) {
				                    classes.push('xdsoft_current');
				                } else if (options.initTime) {
				                    classes.push('xdsoft_init_time');
				                }
				            }
				            if (parseInt(today.getHours(), 10) === parseInt(h, 10) && parseInt(today.getMinutes(), 10) === parseInt(m, 10)) {
				                classes.push('xdsoft_today');
				            }
				            time += '<div class="xdsoft_time ' + classes.join(' ') + '" data-hour="' + h + '" data-minute="' + m + '">' + dateHelper.formatDate(now, options.formatTime) + '</div>';
				        };

				        if (!options.allowTimes || !$.isArray(options.allowTimes) || !options.allowTimes.length) {
				            for (i = 0, j = 0; i < (options.hours12 ? 12 : 24) ; i += 1) {
				                for (j = 0; j < 60; j += options.step) {
				                    h = (i < 10 ? '0' : '') + i;
				                    m = (j < 10 ? '0' : '') + j;
				                    line_time(h, m);
				                }
				            }
				        } else {
				            for (i = 0; i < options.allowTimes.length; i += 1) {
				                h = _xdsoft_datetime.strtotime(options.allowTimes[i]).getHours();
				                m = _xdsoft_datetime.strtotime(options.allowTimes[i]).getMinutes();
				                line_time(h, m);
				            }
				        }

				        timebox.html(time);

				        //qianlong begin
				        //年
				        year = '';
				        line_year = function line_year(y) {
				            var classes = [];
				            var now = _xdsoft_datetime.now();
				            var nowYear = now.getFullYear();
				            if (y == nowYear) {
				                classes.push('xdsoft_current');
				            }
				            year += '<div class="xdsoft_time ' + classes.join(' ') + '" data-year="' + y + '年">' + y + '年</div>';
				        }

				        for (i = options.yearStart; i <= options.yearEnd; i++) {
				            line_year(i);
				        }

				        yearbox.html(year);
				        //年月
				        yearmonth = '';
				        line_yearmonth = function line_yearmonth(y, m) {
				            m = m + 1;
				            var classes = [];
				            var curTime = _xdsoft_datetime.now();
				            var year = curTime.getFullYear();
				            var month = curTime.getMonth() + 1;
				            if (y == year && m == month) classes.push('xdsoft_current');
				            yearmonth += '<div class="xdsoft_time ' + classes.join(' ') + '" data-yearmonth="' + y + '年' + m + '月">' + y + '年' + m + '月</div>';
				        }

				        var sm = options.monthStart <= 0 || options.monthStart > 11 || options.monthStart > options.monthEnd ? 0 : options.monthStart;
				        var em = options.monthEnd > 11 ? 11 : options.monthEnd;
				        if (_xdsoft_datetime.currentTime.getFullYear() < _xdsoft_datetime.now().getFullYear()) {
				            sm = 0;
				            em = 11;
				        }
				        for (i = sm; i <= em; i++) {
				            line_yearmonth(wy, i);
				        }

				        yearmonthbox.html(yearmonth);
				        yearmonthboxparent.xdsoftScrollerYearMonth();
				        //周
				        line_week = function (y, w) {
				            var classes = [];
				            var curTime = _xdsoft_datetime.now();
				            var year = curTime.getFullYear();
				            var curWeek = _xdsoft_datetime.getWeekOfYear(curTime);
				            //取出1月1号的时间
				            var t = new Date(y, 0, 1);
				            var d1 = t.getDay();
                            var nw = w;
				            var d;
                            //计算第一周和最后一周的时间  暂时去掉计算方式
				            /*if (d1 == 0 && nw == 1) d = 1;
				            else if (d1 != 0 && nw == 1) {
				                if (7 - d1 < 3) d = 7 - d1 + 1;
				                else if (7 - d1 == 4) d = 3;
				                else d = d1 - 7;
				            }
				            else {
				                if (7 - d1 < 3)
				                    d = nw * 7 + 1;
				                else
				                    d = (nw - 1) * 7 + 1;
				            }

				            t.setDate(d);
				            //console.log(d1 + "------" + d + "--------" + t);
				            //对应周的开始时间
				            var st = new Date(t.getFullYear(), t.getMonth(), t.getDate() + (1 - t.getDay()));
				            //console.log(st);
				            //对应周的结束时间
				            var et = new Date(t.getFullYear(), t.getMonth(), t.getDate() + (7 - t.getDay()));*/

                            //2017-09-02 qianlong 第一周为当前年的第一个礼拜一开始 确保安卓、IOS客户端与PC上一致
				            while (t.getDay() != 1) {
				                t.setDate(t.getDate() + 1);
				            }

                            

				            //对应周的开始时间
				            var st = t;
				            if (w == 1) {
				                st.setDate(t.getDate() + (w - 1) * 6);
				            }
				            else
                                st.setDate(t.getDate() + (w - 1) * 7);

                            var temp_st = st;
                            //console.log("st:" + st);
                            //对应周的结束时间
                            var et = new Date(st.getFullYear(), st.getMonth(), st.getDate());
                            et.setDate(et.getDate() + 6);
				            //console.log("et:" + st);
				            if (y == year && w == curWeek) classes.push('xdsoft_current');
				            var ssy = st.getFullYear() < y ? st.getFullYear() + "年" : "";
                            var esy = et.getFullYear() > y ? et.getFullYear() + "年" : "";
                            //console.log("st:" + et);
                            //console.log("et:" + et);
                            var showdate = '(' + ssy + (st.getMonth() + 1) + '月' + st.getDate() + '日-' + esy + (et.getMonth() + 1) + '月' + et.getDate() + '日)';
                            //console.log(showdate);
				            week += '<div class="xdsoft_time ' + classes.join(' ') + '" data-week="' + y + '年 第' + w + '周 ' + showdate + '"><b>第' + w + '周</b> ' + showdate + '</div>';
				        }

				        var wm = options.monthEnd > 11 ? 11 : options.monthEnd;
				        var wd = wm == 11 ? 31 : 1;
				        var t = options.maxDate && wy == _xdsoft_datetime.now().getFullYear() ? new Date(_xdsoft_datetime.now()) : new Date(wy, wm, wd);
				        var iWeek = _xdsoft_datetime.getWeekOfYear(t);
				        console.log("iWeek:" + iWeek);
				        for (var i = 1; i <= iWeek; i++) {
				            line_week(wy, i);
				        }
				        weekbox.html(week);
				        weekboxparent.xdsoftScrollerWeek();
				        //end

				        opt = '';
				        i = 0;

				        for (i = parseInt(options.yearStart, 10) + options.yearOffset; i <= parseInt(options.yearEnd, 10) + options.yearOffset; i += 1) {
				            opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getFullYear() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + i + '</div>';
				        }
				        yearselect.children().eq(0).html(opt);
				        //qianlong
				        yearmonth_yearselect.children().eq(0).html(opt);
				        week_yearselect.children().eq(0).html(opt);

				        for (i = parseInt(options.monthStart, 10), opt = ''; i <= parseInt(options.monthEnd, 10) ; i += 1) {
				            opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getMonth() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + options.i18n[globalLocale].months[i] + '</div>';
				        }
				        monthselect.children().eq(0).html(opt);
				        $(datetimepicker)
							.trigger('generate.xdsoft');
				    }, 10);
				    event.stopPropagation();
				})
				.on('afterOpen.xdsoft', function () {
				    if (options.timepicker) {
				        var classType, pheight, height, top;
				        if (timebox.find('.xdsoft_current').length) {
				            classType = '.xdsoft_current';
				        } else if (timebox.find('.xdsoft_init_time').length) {
				            classType = '.xdsoft_init_time';
				        }
				        if (classType) {
				            pheight = timeboxparent[0].clientHeight;
				            height = timebox[0].offsetHeight;
				            top = timebox.find(classType).index() * options.timeHeightInTimePicker + 1;
				            if ((height - pheight) < top) {
				                top = height - pheight;
				            }
				            timeboxparent.trigger('scroll_element.xdsoft_scroller', [parseInt(top, 10) / (height - pheight)]);
				        } else {
				            timeboxparent.trigger('scroll_element.xdsoft_scroller', [0]);
				        }
				    }
				    //qianlong
				    if (options.yearpicker) {
				        var classType, pheight, height, top;
				        if (yearbox.find('.xdsoft_current').length) {
				            classType = '.xdsoft_current';
				        } else if (yearbox.find('.xdsoft_init_time').length) {
				            classType = '.xdsoft_init_time';
				        }
				        if (classType) {
				            pheight = yearboxparent[0].clientHeight;
				            height = yearbox[0].offsetHeight;
				            top = yearbox.find(classType).index() * options.timeHeightInTimePicker + 1;
				            if ((height - pheight) < top) {
				                top = height - pheight;
				            }
				            yearboxparent.trigger('scroll_element.xdsoft_scroller', [parseInt(top, 10) / (height - pheight)]);
				        } else {
				            yearboxparent.trigger('scroll_element.xdsoft_scroller', [0]);
				        }
				    }
				    if (options.yearmonthpicker) {
				        var classType, pheight, height, top;
				        if (yearmonthbox.find('.xdsoft_current').length) {
				            classType = '.xdsoft_current';
				        } else if (yearmonthbox.find('.xdsoft_init_time').length) {
				            classType = '.xdsoft_init_time';
				        }
				        if (classType) {
				            pheight = yearmonthboxparent[0].clientHeight;
				            height = yearmonthbox[0].offsetHeight;
				            top = yearmonthbox.find(classType).index() * options.timeHeightInTimePicker + 1;
				            if ((height - pheight) < top) {
				                top = height - pheight;
				            }
				            yearmonthboxparent.trigger('scroll_element.xdsoft_scroller', [parseInt(top, 10) / (height - pheight)]);
				        } else {
				            yearmonthboxparent.trigger('scroll_element.xdsoft_scroller', [0]);
				        }
				    }

				    if (options.weekpicker) {
				        var classType, pheight, height, top;
				        if (weekbox.find('.xdsoft_current').length) {
				            classType = '.xdsoft_current';
				        } else if (weekbox.find('.xdsoft_init_time').length) {
				            classType = '.xdsoft_init_time';
				        }
				        if (classType) {
				            pheight = weekboxparent[0].clientHeight;
				            height = weekbox[0].offsetHeight;
				            top = weekbox.find(classType).index() * options.timeHeightInTimePicker + 1;
				            if ((height - pheight) < top) {
				                top = height - pheight;
				            }
				            weekboxparent.trigger('scroll_element.xdsoft_scroller', [parseInt(top, 10) / (height - pheight)]);
				        } else {
				            weekboxparent.trigger('scroll_element.xdsoft_scroller', [0]);
				        }
				    }
				    //End
				});

            timerclick = 0;
            calendar
				.on('touchend click.xdsoft', 'td', function (xdevent) {
				    xdevent.stopPropagation();  // Prevents closing of Pop-ups, Modals and Flyouts in Bootstrap
				    timerclick += 1;
				    var $this = $(this),
						currentTime = _xdsoft_datetime.currentTime;

				    if (currentTime === undefined || currentTime === null) {
				        _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
				        currentTime = _xdsoft_datetime.currentTime;
				    }

				    if ($this.hasClass('xdsoft_disabled')) {
				        return false;
				    }

				    currentTime.setDate(1);
				    currentTime.setFullYear($this.data('year'));
				    currentTime.setMonth($this.data('month'));
				    currentTime.setDate($this.data('date'));

				    datetimepicker.trigger('select.xdsoft', [currentTime]);

				    input.val(_xdsoft_datetime.str());

				    if (options.onSelectDate && $.isFunction(options.onSelectDate)) {
				        options.onSelectDate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
				    }

				    datetimepicker.data('changed', true);
				    datetimepicker.trigger('xchange.xdsoft');
				    datetimepicker.trigger('changedatetime.xdsoft');
				    if ((timerclick > 1 || (options.closeOnDateSelect === true || (options.closeOnDateSelect === false && !options.timepicker))) && !options.inline) {
				        datetimepicker.trigger('close.xdsoft');
				    }
				    setTimeout(function () {
				        timerclick = 0;
				    }, 200);
				});

            timebox
				.on('touchmove', 'div', function () { currentlyScrollingTimeDiv = true; })
				.on('touchend click.xdsoft', 'div', function (xdevent) {
				    xdevent.stopPropagation();
				    if (currentlyScrollingTimeDiv) {
				        currentlyScrollingTimeDiv = false;
				        return;
				    }
				    var $this = $(this),
						currentTime = _xdsoft_datetime.currentTime;

				    if (currentTime === undefined || currentTime === null) {
				        _xdsoft_datetime.currentTime = _xdsoft_datetime.now();
				        currentTime = _xdsoft_datetime.currentTime;
				    }

				    if ($this.hasClass('xdsoft_disabled')) {
				        return false;
				    }
				    currentTime.setHours($this.data('hour'));
				    currentTime.setMinutes($this.data('minute'));
				    datetimepicker.trigger('select.xdsoft', [currentTime]);

				    datetimepicker.data('input').val(_xdsoft_datetime.str());

				    if (options.onSelectTime && $.isFunction(options.onSelectTime)) {
				        options.onSelectTime.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
				    }
				    datetimepicker.data('changed', true);
				    datetimepicker.trigger('xchange.xdsoft');
				    datetimepicker.trigger('changedatetime.xdsoft');
				    if (options.inline !== true && options.closeOnTimeSelect === true) {
				        datetimepicker.trigger('close.xdsoft');
				    }
				});
            //qianlong
            yearbox
				.on('touchmove', 'div', function () { currentlyScrollingYearDiv = true; })
				.on('touchend click.xdsoft', 'div', function (xdevent) {
				    xdevent.stopPropagation();
				    if (currentlyScrollingYearDiv) {
				        currentlyScrollingYearDiv = false;
				        return;
				    }
				    var $this = $(this),
						currentYear = _xdsoft_datetime.now().getFullYear();

				    if (currentYear === undefined || currentYear === null) {
				        currentYear = new Date().getFullYear();
				    }

				    if ($this.hasClass('xdsoft_disabled')) {
				        return false;
				    }
				    currentYear = $this.data('year');
				    datetimepicker.trigger('select.xdsoft', [currentYear]);

				    datetimepicker.data('input').val(currentYear);

				    if (options.onSelectYear && $.isFunction(options.onSelectYear)) {
				        options.onSelectYear.call(datetimepicker, currentYear, datetimepicker.data('input'), xdevent);
				    }
				    datetimepicker.data('changed', true);
				    datetimepicker.trigger('xchange.xdsoft');
				    datetimepicker.trigger('changedatetime.xdsoft');
				    if (options.inline !== true && options.closeOnYearSelect === true) {
				        datetimepicker.trigger('close.xdsoft');
				    }
				});

            yearmonthbox
				.on('touchmove', 'div', function () { currentlyScrollingYearMonthDiv = true; })
				.on('touchend click.xdsoft', 'div', function (xdevent) {
				    xdevent.stopPropagation();
				    if (currentlyScrollingYearMonthDiv) {
				        currentlyScrollingYearMonthDiv = false;
				        return;
				    }
				    var $this = $(this),
						currentYear = _xdsoft_datetime.currentTime.getFullYear(), currentMonth = _xdsoft_datetime.currentTime.getMonth() + 1;

				    if (currentYear === undefined || currentYear === null) {
				        currentYear = new Date().getFullYear();
				    }
				    if (currentMonth === undefined || currentMonth === null) {
				        currentMonth = new Date().getMonth + 1;
				    }

				    if ($this.hasClass('xdsoft_disabled')) {
				        return false;
				    }
				    var currentYearMonth = currentYear + '年' + currentMonth + '月';

				    currentYearMonth = $this.data('yearmonth');
				    datetimepicker.trigger('select.xdsoft', [currentYearMonth]);

				    datetimepicker.data('input').val(currentYearMonth);

				    if (options.onSelectYearMonth && $.isFunction(options.onSelectYearMonth)) {
				        options.onSelectYearMonth.call(datetimepicker, currentYearMonth, datetimepicker.data('input'), xdevent);
				    }
				    datetimepicker.data('changed', true);
				    datetimepicker.trigger('xchange.xdsoft');
				    datetimepicker.trigger('changedatetime.xdsoft');
				    if (options.inline !== true && options.closeOnYearMonthSelect === true) {
				        datetimepicker.trigger('close.xdsoft');
				    }
				});

            weekbox
				.on('touchmove', 'div', function () { currentlyScrollingYearMonthDiv = true; })
				.on('touchend click.xdsoft', 'div', function (xdevent) {
				    xdevent.stopPropagation();
				    if (currentlyScrollingYearMonthDiv) {
				        currentlyScrollingYearMonthDiv = false;
				        return;
				    }
				    var $this = $(this), currentTime = _xdsoft_datetime.currentTime;
				    if (currentTime == null) {
				        currentTime = _xdsoft_datetime.now();
				    }

				    if ($this.hasClass('xdsoft_disabled')) {
				        return false;
				    }
				    var year = currentTime.getFullYear();
				    var currentWeek = year + '年 第' + _xdsoft_datetime.getWeekOfYear(currentTime) + '周';

				    currentWeek = $this.data('week');
				    datetimepicker.trigger('select.xdsoft', [currentWeek]);

				    datetimepicker.data('input').val(currentWeek);

				    if (options.onSelectWeek && $.isFunction(options.onSelectWeek)) {
				        options.onSelectWeek.call(datetimepicker, currentWeek, datetimepicker.data('input'), xdevent);
				    }
				    datetimepicker.data('changed', true);
				    datetimepicker.trigger('xchange.xdsoft');
				    datetimepicker.trigger('changedatetime.xdsoft');
				    if (options.inline !== true && options.closeOnYearMonthSelect === true) {
				        datetimepicker.trigger('close.xdsoft');
				    }
				});
            //End

            datepicker
				.on('mousewheel.xdsoft', function (event) {
				    if (!options.scrollMonth) {
				        return true;
				    }
                    //判断控件是否获取焦点  此处设置无效，因为控件的focus事件已经被占用，会执行日期选择的方法
				    /*var isFocus = $(input).is(":focus");
				    if (!isFouces) return true;*/
				    

				    if (event.deltaY < 0) {
				        _xdsoft_datetime.nextMonth();
				    } else {
				        _xdsoft_datetime.prevMonth();
				    }
				    return false;
				});

            input
				.on('mousewheel.xdsoft', function (event) {
				    if (!options.scrollInput) {
				        return true;
				    }
				    if (!options.datepicker && options.timepicker) {
				        current_time_index = timebox.find('.xdsoft_current').length ? timebox.find('.xdsoft_current').eq(0).index() : 0;
				        if (current_time_index + event.deltaY >= 0 && current_time_index + event.deltaY < timebox.children().length) {
				            current_time_index += event.deltaY;
				        }
				        if (timebox.children().eq(current_time_index).length) {
				            timebox.children().eq(current_time_index).trigger('mousedown');
				        }
				        return false;
				    }
				    if (options.datepicker && !options.timepicker) {
				        datepicker.trigger(event, [event.deltaY, event.deltaX, event.deltaY]);
				        if (input.val) {
				            input.val(_xdsoft_datetime.str());
				        }
				        datetimepicker.trigger('changedatetime.xdsoft');
				        return false;
				    }
				});

            datetimepicker
				.on('changedatetime.xdsoft', function (event) {
				    if (options.onChangeDateTime && $.isFunction(options.onChangeDateTime)) {
				        var $input = datetimepicker.data('input');
				        options.onChangeDateTime.call(datetimepicker, _xdsoft_datetime.currentTime, $input, event);
				        delete options.value;
				        $input.trigger('change');
				    }
				})
				.on('generate.xdsoft', function () {
				    if (options.onGenerate && $.isFunction(options.onGenerate)) {
				        options.onGenerate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
				    }
				    if (triggerAfterOpen) {
				        datetimepicker.trigger('afterOpen.xdsoft');
				        triggerAfterOpen = false;
				    }
				})
				.on('click.xdsoft', function (xdevent) {
				    xdevent.stopPropagation();
				});

            current_time_index = 0;

            /**
			 * Runs the callback for each of the specified node's ancestors.
			 *
			 * Return FALSE from the callback to stop ascending.
			 *
			 * @param {DOMNode} node
			 * @param {Function} callback
			 * @returns {undefined}
			 */
            forEachAncestorOf = function (node, callback) {
                do {
                    node = node.parentNode;

                    if (callback(node) === false) {
                        break;
                    }
                } while (node.nodeName !== 'HTML');
            };

            /**
			 * Sets the position of the picker.
			 *
			 * @returns {undefined}
			 */
            setPos = function () {
                var dateInputOffset,
					dateInputElem,
					verticalPosition,
					left,
					position,
					datetimepickerElem,
					dateInputHasFixedAncestor,
					$dateInput,
					windowWidth,
					verticalAnchorEdge,
					datetimepickerCss,
					windowHeight,
					windowScrollTop;

                $dateInput = datetimepicker.data('input');
                dateInputOffset = $dateInput.offset();
                dateInputElem = $dateInput[0];

                verticalAnchorEdge = 'top';
                verticalPosition = (dateInputOffset.top + dateInputElem.offsetHeight) - 1;
                left = dateInputOffset.left;
                position = "absolute";

                windowWidth = $(window).width();
                windowHeight = $(window).height();
                windowScrollTop = $(window).scrollTop();

                var sHeight = 194;
                if (options.timepicker) sHeight = 207;

                if ((document.documentElement.clientWidth - dateInputOffset.left) < datepicker.parent().outerWidth(true)) {
                    var diff = datepicker.parent().outerWidth(true) - dateInputElem.offsetWidth;
                    left = left - diff;
                }

                if ($dateInput.parent().css('direction') === 'rtl') {
                    left -= (datetimepicker.outerWidth() - $dateInput.outerWidth());
                }

                if (options.fixed) {
                    verticalPosition -= windowScrollTop;
                    left -= $(window).scrollLeft();
                    position = "fixed";
                } else {
                    dateInputHasFixedAncestor = false;

                    forEachAncestorOf(dateInputElem, function (ancestorNode) {
                        if (window.getComputedStyle(ancestorNode).getPropertyValue('position') === 'fixed') {
                            dateInputHasFixedAncestor = true;
                            return false;
                        }
                    });

                    if (dateInputHasFixedAncestor) {
                        position = 'fixed';

                        //If the picker won't fit entirely within the viewport then display it above the date input.
                        if (verticalPosition + datetimepicker.outerHeight() > windowHeight + windowScrollTop) {
                            verticalAnchorEdge = 'bottom';
                            verticalPosition = (windowHeight + windowScrollTop) - dateInputOffset.top;
                        } else {
                            verticalPosition -= windowScrollTop;
                        }
                    } else {
                        if (verticalPosition + 218 > windowHeight + windowScrollTop) {
                            verticalPosition = dateInputOffset.top - sHeight;
                        }
                    }

                    if (verticalPosition < 0) {
                        verticalPosition = 0;
                    }

                    if (left + dateInputElem.offsetWidth > windowWidth) {
                        left = windowWidth - dateInputElem.offsetWidth;
                    }
                }

                datetimepickerElem = datetimepicker[0];

                forEachAncestorOf(datetimepickerElem, function (ancestorNode) {
                    var ancestorNodePosition;

                    ancestorNodePosition = window.getComputedStyle(ancestorNode).getPropertyValue('position');

                    if (ancestorNodePosition === 'relative' && windowWidth >= ancestorNode.offsetWidth) {
                        left = left - ((windowWidth - ancestorNode.offsetWidth) / 2);
                        return false;
                    }
                });

                datetimepickerCss = {
                    position: position,
                    left: left,
                    top: '',  //Initialize to prevent previous values interfering with new ones.
                    bottom: ''  //Initialize to prevent previous values interfering with new ones.
                };

                datetimepickerCss[verticalAnchorEdge] = verticalPosition;

                datetimepicker.css(datetimepickerCss);
            };

            datetimepicker
				.on('open.xdsoft', function (event) {
				    var onShow = true;
				    if (options.onShow && $.isFunction(options.onShow)) {
				        onShow = options.onShow.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
				    }
				    if (onShow !== false) {
				        datetimepicker.show();
				        setPos();
				        $(window)
							.off('resize.xdsoft', setPos)
							.on('resize.xdsoft', setPos);

				        if (options.closeOnWithoutClick) {
				            $([document.body, window]).on('touchstart mousedown.xdsoft', function arguments_callee6() {
				                datetimepicker.trigger('close.xdsoft');
				                $([document.body, window]).off('touchstart mousedown.xdsoft', arguments_callee6);
				            });
				        }
				    }
				})
				.on('close.xdsoft', function (event) {
				    var onClose = true;
				    month_picker
						.find('.xdsoft_month,.xdsoft_year')
							.find('.xdsoft_select')
								.hide();
				    if (options.onClose && $.isFunction(options.onClose)) {
				        onClose = options.onClose.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
				    }
				    if (onClose !== false && !options.opened && !options.inline) {
				        datetimepicker.hide();
				    }
				    event.stopPropagation();
				})
				.on('toggle.xdsoft', function () {
				    if (datetimepicker.is(':visible')) {
				        datetimepicker.trigger('close.xdsoft');
				    } else {
				        datetimepicker.trigger('open.xdsoft');
				    }
				})
				.data('input', input);

            timer = 0;

            datetimepicker.data('xdsoft_datetime', _xdsoft_datetime);
            datetimepicker.setOptions(options);

            function getCurrentValue() {
                var ct = false, time;

                if (options.startDate) {
                    ct = _xdsoft_datetime.strToDate(options.startDate);
                } else {
                    ct = options.value || ((input && input.val && input.val()) ? input.val() : '');
                    if (ct) {
                        ct = _xdsoft_datetime.strToDateTime(ct);
                    } else if (options.defaultDate) {
                        ct = _xdsoft_datetime.strToDateTime(options.defaultDate);
                        if (options.defaultTime) {
                            time = _xdsoft_datetime.strtotime(options.defaultTime);
                            ct.setHours(time.getHours());
                            ct.setMinutes(time.getMinutes());
                        }
                    }
                }

                if (ct && _xdsoft_datetime.isValidDate(ct)) {
                    datetimepicker.data('changed', true);
                } else {
                    ct = '';
                }

                return ct || 0;
            }

            function setMask(options) {

                var isValidValue = function (mask, value) {
                    var reg = mask
						.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, '\\$1')
						.replace(/_/g, '{digit+}')
						.replace(/([0-9]{1})/g, '{digit$1}')
						.replace(/\{digit([0-9]{1})\}/g, '[0-$1_]{1}')
						.replace(/\{digit[\+]\}/g, '[0-9_]{1}');
                    return (new RegExp(reg)).test(value);
                },
				getCaretPos = function (input) {
				    try {
				        if (document.selection && document.selection.createRange) {
				            var range = document.selection.createRange();
				            return range.getBookmark().charCodeAt(2) - 2;
				        }
				        if (input.setSelectionRange) {
				            return input.selectionStart;
				        }
				    } catch (e) {
				        return 0;
				    }
				},
				setCaretPos = function (node, pos) {
				    node = (typeof node === "string" || node instanceof String) ? document.getElementById(node) : node;
				    if (!node) {
				        return false;
				    }
				    if (node.createTextRange) {
				        var textRange = node.createTextRange();
				        textRange.collapse(true);
				        textRange.moveEnd('character', pos);
				        textRange.moveStart('character', pos);
				        textRange.select();
				        return true;
				    }
				    if (node.setSelectionRange) {
				        node.setSelectionRange(pos, pos);
				        return true;
				    }
				    return false;
				};
                if (options.mask) {
                    input.off('keydown.xdsoft');
                }
                if (options.mask === true) {
                    if (typeof moment != 'undefined') {
                        options.mask = options.format
                                .replace(/Y{4}/g, '9999')
                                .replace(/Y{2}/g, '99')
                                .replace(/M{2}/g, '19')
                                .replace(/D{2}/g, '39')
                                .replace(/H{2}/g, '29')
                                .replace(/m{2}/g, '59')
                                .replace(/s{2}/g, '59');
                    } else {
                        options.mask = options.format
                                .replace(/Y/g, '9999')
                                .replace(/F/g, '9999')
                                .replace(/m/g, '19')
                                .replace(/d/g, '39')
                                .replace(/H/g, '29')
                                .replace(/i/g, '59')
                                .replace(/s/g, '59');
                    }
                }

                if ($.type(options.mask) === 'string') {
                    if (!isValidValue(options.mask, input.val())) {
                        input.val(options.mask.replace(/[0-9]/g, '_'));
                        setCaretPos(input[0], 0);
                    }

                    input.on('keydown.xdsoft', function (event) {
                        var val = this.value,
							key = event.which,
							pos,
							digit;

                        if (((key >= KEY0 && key <= KEY9) || (key >= _KEY0 && key <= _KEY9)) || (key === BACKSPACE || key === DEL)) {
                            pos = getCaretPos(this);
                            digit = (key !== BACKSPACE && key !== DEL) ? String.fromCharCode((_KEY0 <= key && key <= _KEY9) ? key - KEY0 : key) : '_';

                            if ((key === BACKSPACE || key === DEL) && pos) {
                                pos -= 1;
                                digit = '_';
                            }

                            while (/[^0-9_]/.test(options.mask.substr(pos, 1)) && pos < options.mask.length && pos > 0) {
                                pos += (key === BACKSPACE || key === DEL) ? -1 : 1;
                            }

                            val = val.substr(0, pos) + digit + val.substr(pos + 1);
                            if ($.trim(val) === '') {
                                val = options.mask.replace(/[0-9]/g, '_');
                            } else {
                                if (pos === options.mask.length) {
                                    event.preventDefault();
                                    return false;
                                }
                            }

                            pos += (key === BACKSPACE || key === DEL) ? 0 : 1;
                            while (/[^0-9_]/.test(options.mask.substr(pos, 1)) && pos < options.mask.length && pos > 0) {
                                pos += (key === BACKSPACE || key === DEL) ? -1 : 1;
                            }

                            if (isValidValue(options.mask, val)) {
                                this.value = val;
                                setCaretPos(this, pos);
                            } else if ($.trim(val) === '') {
                                this.value = options.mask.replace(/[0-9]/g, '_');
                            } else {
                                input.trigger('error_input.xdsoft');
                            }
                        } else {
                            if (([AKEY, CKEY, VKEY, ZKEY, YKEY].indexOf(key) !== -1 && ctrlDown) || [ESC, ARROWUP, ARROWDOWN, ARROWLEFT, ARROWRIGHT, F5, CTRLKEY, TAB, ENTER].indexOf(key) !== -1) {
                                return true;
                            }
                        }

                        event.preventDefault();
                        return false;
                    });
                }
            }

            _xdsoft_datetime.setCurrentTime(getCurrentValue());

            input
				.data('xdsoft_datetimepicker', datetimepicker)
				.on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function () {
				    if (input.is(':disabled') || (input.data('xdsoft_datetimepicker').is(':visible') && options.closeOnInputClick)) {
				        return;
				    }
				    clearTimeout(timer);
				    timer = setTimeout(function () {
				        if (input.is(':disabled')) {
				            return;
				        }

				        triggerAfterOpen = true;
				        _xdsoft_datetime.setCurrentTime(getCurrentValue(), true);
				        if (options.mask) {
				            setMask(options);
				        }
				        datetimepicker.trigger('open.xdsoft');
				    }, 100);
				})
				.on('keydown.xdsoft', function (event) {
				    var elementSelector,
						key = event.which;
				    if ([ENTER].indexOf(key) !== -1 && options.enterLikeTab) {
				        elementSelector = $("input:visible,textarea:visible,button:visible,a:visible");
				        datetimepicker.trigger('close.xdsoft');
				        elementSelector.eq(elementSelector.index(this) + 1).focus();
				        return false;
				    }
				    if ([TAB].indexOf(key) !== -1) {
				        datetimepicker.trigger('close.xdsoft');
				        return true;
				    }
				})
				.on('blur.xdsoft', function () {
				    datetimepicker.trigger('close.xdsoft');
				});
        };
        destroyDateTimePicker = function (input) {
            var datetimepicker = input.data('xdsoft_datetimepicker');
            if (datetimepicker) {
                datetimepicker.data('xdsoft_datetime', null);
                datetimepicker.remove();
                input
					.data('xdsoft_datetimepicker', null)
					.off('.xdsoft');
                $(window).off('resize.xdsoft');
                $([window, document.body]).off('mousedown.xdsoft touchstart');
                if (input.unmousewheel) {
                    input.unmousewheel();
                }
            }
        };
        $(document)
			.off('keydown.xdsoftctrl keyup.xdsoftctrl')
			.on('keydown.xdsoftctrl', function (e) {
			    if (e.keyCode === CTRLKEY) {
			        ctrlDown = true;
			    }
			})
			.on('keyup.xdsoftctrl', function (e) {
			    if (e.keyCode === CTRLKEY) {
			        ctrlDown = false;
			    }
			});

        this.each(function () {
            var datetimepicker = $(this).data('xdsoft_datetimepicker'), $input;
            if (datetimepicker) {
                if ($.type(opt) === 'string') {
                    switch (opt) {
                        case 'show':
                            $(this).select().focus();
                            datetimepicker.trigger('open.xdsoft');
                            break;
                        case 'hide':
                            datetimepicker.trigger('close.xdsoft');
                            break;
                        case 'toggle':
                            datetimepicker.trigger('toggle.xdsoft');
                            break;
                        case 'destroy':
                            destroyDateTimePicker($(this));
                            break;
                        case 'reset':
                            this.value = this.defaultValue;
                            if (!this.value || !datetimepicker.data('xdsoft_datetime').isValidDate(dateHelper.parseDate(this.value, options.format))) {
                                datetimepicker.data('changed', false);
                            }
                            datetimepicker.data('xdsoft_datetime').setCurrentTime(this.value);
                            break;
                        case 'validate':
                            $input = datetimepicker.data('input');
                            $input.trigger('blur.xdsoft');
                            break;
                        default:
                            if (datetimepicker[opt] && $.isFunction(datetimepicker[opt])) {
                                result = datetimepicker[opt](opt2);
                            }
                    }
                } else {
                    datetimepicker
						.setOptions(opt);
                }
                return 0;
            }
            if ($.type(opt) !== 'string') {
                if (!options.lazyInit || options.open || options.inline) {
                    createDateTimePicker($(this));
                } else {
                    lazyInit($(this));
                }
            }
        });

        //$(this).attr("readonly", options.readOnly);

        return result;
    };

    $.fn.datetimepicker.defaults = default_options;

    function HighlightedDate(date, desc, style) {
        "use strict";
        this.date = date;
        this.desc = desc;
        this.style = style;
    }
}));

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
        toBind = ('onwheel' in document || document.documentMode >= 9) ?
                    ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
        slice = Array.prototype.slice,
        nullLowestDeltaTimeout, lowestDelta;

    if ($.event.fixHooks) {
        for (var i = toFix.length; i;) {
            $.event.fixHooks[toFix[--i]] = $.event.mouseHooks;
        }
    }

    var special = $.event.special.mousewheel = {
        version: '3.1.12',

        setup: function () {
            if (this.addEventListener) {
                for (var i = toBind.length; i;) {
                    this.addEventListener(toBind[--i], handler, false);
                }
            } else {
                this.onmousewheel = handler;
            }
            // Store the line height and page height for this particular element
            $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
            $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
        },

        teardown: function () {
            if (this.removeEventListener) {
                for (var i = toBind.length; i;) {
                    this.removeEventListener(toBind[--i], handler, false);
                }
            } else {
                this.onmousewheel = null;
            }
            // Clean up the data we added to the element
            $.removeData(this, 'mousewheel-line-height');
            $.removeData(this, 'mousewheel-page-height');
        },

        getLineHeight: function (elem) {
            var $elem = $(elem),
                $parent = $elem['offsetParent' in $.fn ? 'offsetParent' : 'parent']();
            if (!$parent.length) {
                $parent = $('body');
            }
            return parseInt($parent.css('fontSize'), 10) || parseInt($elem.css('fontSize'), 10) || 16;
        },

        getPageHeight: function (elem) {
            return $(elem).height();
        },

        settings: {
            adjustOldDeltas: true, // see shouldAdjustOldDeltas() below
            normalizeOffset: true  // calls getBoundingClientRect for each event
        }
    };

    $.fn.extend({
        mousewheel: function (fn) {
            return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
        },

        unmousewheel: function (fn) {
            return this.unbind('mousewheel', fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event,
            args = slice.call(arguments, 1),
            delta = 0,
            deltaX = 0,
            deltaY = 0,
            absDelta = 0,
            offsetX = 0,
            offsetY = 0;
        event = $.event.fix(orgEvent);
        event.type = 'mousewheel';

        // Old school scrollwheel delta
        if ('detail' in orgEvent) { deltaY = orgEvent.detail * -1; }
        if ('wheelDelta' in orgEvent) { deltaY = orgEvent.wheelDelta; }
        if ('wheelDeltaY' in orgEvent) { deltaY = orgEvent.wheelDeltaY; }
        if ('wheelDeltaX' in orgEvent) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
        if ('axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
            deltaX = deltaY * -1;
            deltaY = 0;
        }

        // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
        delta = deltaY === 0 ? deltaX : deltaY;

        // New school wheel delta (wheel event)
        if ('deltaY' in orgEvent) {
            deltaY = orgEvent.deltaY * -1;
            delta = deltaY;
        }
        if ('deltaX' in orgEvent) {
            deltaX = orgEvent.deltaX;
            if (deltaY === 0) { delta = deltaX * -1; }
        }

        // No change actually happened, no reason to go any further
        if (deltaY === 0 && deltaX === 0) { return; }

        // Need to convert lines and pages to pixels if we aren't already in pixels
        // There are three delta modes:
        //   * deltaMode 0 is by pixels, nothing to do
        //   * deltaMode 1 is by lines
        //   * deltaMode 2 is by pages
        if (orgEvent.deltaMode === 1) {
            var lineHeight = $.data(this, 'mousewheel-line-height');
            delta *= lineHeight;
            deltaY *= lineHeight;
            deltaX *= lineHeight;
        } else if (orgEvent.deltaMode === 2) {
            var pageHeight = $.data(this, 'mousewheel-page-height');
            delta *= pageHeight;
            deltaY *= pageHeight;
            deltaX *= pageHeight;
        }

        // Store lowest absolute delta to normalize the delta values
        absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

        if (!lowestDelta || absDelta < lowestDelta) {
            lowestDelta = absDelta;

            // Adjust older deltas if necessary
            if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
                lowestDelta /= 40;
            }
        }

        // Adjust older deltas if necessary
        if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
            // Divide all the things by 40!
            delta /= 40;
            deltaX /= 40;
            deltaY /= 40;
        }

        // Get a whole, normalized value for the deltas
        delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / lowestDelta);
        deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / lowestDelta);
        deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / lowestDelta);

        // Normalise offsetX and offsetY properties
        if (special.settings.normalizeOffset && this.getBoundingClientRect) {
            var boundingRect = this.getBoundingClientRect();
            offsetX = event.clientX - boundingRect.left;
            offsetY = event.clientY - boundingRect.top;
        }

        // Add information to the event object
        event.deltaX = deltaX;
        event.deltaY = deltaY;
        event.deltaFactor = lowestDelta;
        event.offsetX = offsetX;
        event.offsetY = offsetY;
        // Go ahead and set deltaMode to 0 since we converted to pixels
        // Although this is a little odd since we overwrite the deltaX/Y
        // properties with normalized deltas.
        event.deltaMode = 0;

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        // Clearout lowestDelta after sometime to better
        // handle multiple device types that give different
        // a different lowestDelta
        // Ex: trackpad = 3 and mouse wheel = 120
        if (nullLowestDeltaTimeout) { clearTimeout(nullLowestDeltaTimeout); }
        nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

    function nullLowestDelta() {
        lowestDelta = null;
    }

    function shouldAdjustOldDeltas(orgEvent, absDelta) {
        // If this is an older event and the delta is divisable by 120,
        // then we are assuming that the browser is treating this as an
        // older mouse wheel event and that we should divide the deltas
        // by 40 to try and get a more usable deltaFactor.
        // Side note, this actually impacts the reported scroll distance
        // in older browsers and can cause scrolling to be slower than native.
        // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
        return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
    }

}));
