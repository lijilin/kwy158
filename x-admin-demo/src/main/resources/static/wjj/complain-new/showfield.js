﻿function checkNum(str) {
    var re = /^-?[1-9]*(\.\d*)?$|^-?d^(\.\d*)?$/;
    if (!re.test(str)) {
        if (isNaN(str)) {
            return false;
        }

    }
    return true;
}



function ShowFieldCheck() {

    var arr2, text,datatype, fieldname;
    var obj;

    var mustList = document.getElementById("rptMustList").value;

    //单独获取sex
    var sexValue = GetRadioValue("rptSex");

    if (mustList != "") {
        var arrMust1 = mustList.split('|');

        for (var index = 0; index < arrMust1.length; index++) {

            arr2 = arrMust1[index].split(',');

            text = arr2[0];
            fieldname = arr2[1];
            datatype = arr2[2];



            if (datatype == "list_multistage") {
                var id = parseInt(GetMultiListValue("rpt" + fieldname));

                if (id == 0) {
                    Dialog.showError(text + "为必选项，还没有选择！");
                    return false;
                }
            } else if (datatype == "checkbox") {
                /*obj = document.getElementsByName("rpt" + fieldname);

                var noinput = 0;
                for (var n = 0; n < obj.length; n++) {
                    if (!obj[n].checked) noinput += 1;
                }

                if (noinput == obj.length) {
                    Dialog.showError(text + "为必选项，至少要选择一个！");
                    return false;
                }*/
                //2017-01-17 qianlong 
                obj = GetCheckedID(fieldname);
                if (obj == "") {
                    Dialog.showError(text + "为必选项，至少要选择一个！");
                    return false;
                }

            } else if (datatype == "textarea_edit") {
                if (eval("editor_" + fieldname).isEmpty()) {
                    Dialog.showError(text + "为必填项，不可留空！", function () {
                        eval("editor_" + fieldname).focus();
                    });
                    return false;
                }

            } else if (datatype == "productselect_batch") {

                if (document.getElementById("rptCommonMultiSelectCount").innerHTML == "0") {

                    Dialog.showError(text + "没有选择，不可留空！");
                    return false;
                }

            } else if (datatype == "int") {

                obj = document.getElementById("rpt" + fieldname);
                if (obj.value == "" || obj.value == "0") {
                    Dialog.showError(text + "为必填项，不可留空！", function () { obj.focus(); });
                    return false;
                }

                if (!checkNum(obj.value)) {
                    Dialog.showError(text + "为数字输入框，只能输入数字！", function () { obj.focus(); });
                    return false;
                }

            } else if (datatype == "dealselect_single") {

                if (document.getElementById("rptDealID").value == "0" || document.getElementById("rptDealID").value == "") {

                    Dialog.showError(text + "没有选择，不可留空！");
                    return false;
                }

            } else if (datatype == "customersselect_single") {
                if (document.getElementById("rptCusID").value == "0" || document.getElementById("rptCusName").value == "名称或拼音模糊查找" || document.getElementById("rptCusName").value == "") {
                    Dialog.showError(text + "为必选项，还没有选择！");
                    return false;
                }
            }
            else if (datatype == "birthday") {

                var birtype = document.getElementById("rptBirthDayType").value;
                if (birtype == "1")//如果选择是公历生日
                {
                    var y = document.getElementById("rptBirthDay_Y1").value;
                    var m = document.getElementById("rptBirthDay_M1").value;
                    var d = document.getElementById("rptBirthDay_D1").value;

                    if (y == 0 || m == 0 || d == 0) {
                        Dialog.showError("生日为必选项，您还没有输入！");
                        return false;
                    }
                }
                else {
                    var y = document.getElementById("rptBirthDay_Y2").value;
                    var m = document.getElementById("rptBirthDay_M2").value;
                    var d = document.getElementById("rptBirthDay_D2").value;

                    if (y == 0 || m == 0 || d == 0) {
                        Dialog.showError("生日为必选项，您还没有输入！");
                        return false;
                    }
                }

            } else if (datatype == "sex") {
                //var sex1 = document.getElementById("rptSex1").checked;
                //var sex2 = document.getElementById("rptSex2").checked;
                if (sexValue == "") {
                    Dialog.showError("性别 为必选项，请选择！");
                    return false;
                }

            } else if (datatype == "userlist") {
                if (document.getElementsByName("rpt" + fieldname).length > 0 && document.getElementsByName("rpt" + fieldname)[0].value == "0" || document.getElementsByName("rpt" + fieldname)[0].value == "") {
                    Dialog.showError(text + "为必选项，还没有选择！");
                    return false;
                }

            } else {
                obj = document.getElementById("rpt" + fieldname);
                if (obj.value == "" || obj.value == "0") {
                    Dialog.showError(text + "为必填项，不可留空！", function () { obj.focus(); });
                    return false;
                }
            }
        }
    }

    $("#rptSex").val(sexValue);

    //校验是否设置了logo的封面图片
    var imageUL = document.getElementById("rptImageUL");
    var fieldName = document.getElementById("hidFieldName");
    var isSetFirstPage = false;
    if (imageUL && imageUL.getElementsByTagName("li").length > 0) {
        var inputObj = imageUL.getElementsByTagName("input");
        for (var i = 0; i < inputObj.length; i++) {
            if (inputObj[i].type == "checkbox") {
                if (inputObj[i].checked) { isSetFirstPage = true; break; }
            }
        }
        //如果不是logo字段，不需要进行封面校验
        if (fieldName&&fieldName=="logo"&&!isSetFirstPage) {
            Dialog.showError("请设置封面图片");
            return false;
        }
    }

    //2016-05-18 qianlong 添加审批流程判断
    if (document.getElementById("rptFlowList")) {
        var selValue = document.getElementById("rptFlowList").value;
        if (selValue == "0") {
            Dialog.showError("请选择审批流程！");
            return false;
        }
    }
   //添加选择审批人判断
    var checkUseidObj = document.getElementById("rptUserIDAll_TagID1");
    if (checkUseidObj) {
        var checkUserIDList = checkUseidObj.value.replace("|", "");
        if (checkUserIDList == "") {
            Dialog.showError("请选择审批人！");
            return false;
        }
    }

    SetCheckBoxValue();
    
    return true;
}

function ShowFieldCheckFrom() {

    var isCheck = ShowFieldCheck();
    if (!isCheck) return isCheck;
    else {
        CommitButton(null);
        return isCheck;
    }
}

function ShowFieldCheckFromList(buttonNameList) {
    var isCheck = ShowFieldCheck();
    if (!isCheck) return isCheck;
    else{
        CommitButtonList(buttonNameList);
    }
}

var fieldName, strText;
var checkObj;
function checkRepeat(obj, text, fieldname,dataType, currentIndexID) {
    checkObj = obj;
    var value = obj.value;
    if (value == "" && fieldname != "ProductModel") return;//产品型号不输入，直接校验产品名称

    fieldName = fieldname;
    //2018-03-14 qianlong 如果是产品名称的重复校验，那么需要添加名称中需要添加产品型号
    if (tableID == 72) {
        if (fieldname == "ProductName") {            
            if (document.getElementById("rptProductModel")) {
                var model = document.getElementById("rptProductModel").value;
                if (model != "") value += "-" + document.getElementById("rptProductModel").value;
            }
        }
        else if (fieldname == "ProductModel") {
            if (value.indexOf("-") > -1) {
                Dialog.showError("产品型号中不能包含 - 符号");
                return;
            }
            fieldName = "ProductName";
            //编辑界面
            if (document.getElementById("rptProductName"))
                value = document.getElementById("rptProductName").value;
            else
                value = document.getElementById("rptText").value;//单项编辑界面
            if (value == "") return;
            if (obj.value != "") value += "-" + obj.value;
        }
    }

    //alert("currentIndexID:"+currentIndexID);
    //alert("CusID:"+currentCusID);
        
    strText = text;    

    try{
        var lodingSpan = document.getElementById("rptRepeatSpan_" + fieldName);
        lodingSpan.style.display = "";
    }
    catch(err){
        var lodingSpan = document.getElementById("rptLinkMan_RepeatSpan_" + fieldName);//这个地方兼容一下录入企业客户联系人检查重复
        lodingSpan.style.display = "";
    }
    //2016-05-10 qianlong 校验是否重复报错的问题
    if (typeof (currentCusID) == "undefined") {
        Kehu51.Team.UI.Web.App.Ajax.GetData.IsExistsContent(tableID, fieldName, dataType, text, value, currentIndexID, 0, checkRepeatCallback);
    }
    else
        Kehu51.Team.UI.Web.App.Ajax.GetData.IsExistsContent(tableID, fieldName, dataType, text, value, currentIndexID, currentCusID, checkRepeatCallback);
    
}
function checkRepeatCallback(res) {
          
    try {
        var lodingSpan = document.getElementById("rptRepeatSpan_" + fieldName);
        lodingSpan.style.display = "none";
    }
    catch (err) {
        var lodingSpan = document.getElementById("rptLinkMan_RepeatSpan_" + fieldName);//这个地方兼容一下录入企业客户联系人检查重复
        lodingSpan.style.display = "none";
    }


    if (res.value != "") {

        //var obj = $("rpt" + fieldName);
        //if(obj == null) obj = $("rptLinkMan_" + fieldName);//兼容录入企业客户时联系人的检查
        if (fieldName != "ProductName")
            Dialog.showError(res.value, function () { checkObj.focus(); });
        else
            Dialog.showError(res.value);
    }
}


//检测手机号
/*var checkphoneing = false;
function CheckMobilePhone(mobilePhone) {
    if (mode != "Add") return;

    if (checkphoneing == false) {
        //if (mode == "Add") {
        //if (mobilePhone == "") return;
        //if(mobilePhone.length != 11) return;
        checkphoneing = true;//标记为正在检查，否则两年事件会同时执行

        alert(fieldName);

        var lodingSpan = $("rptRepeatSpan_" + fieldName);
        lodingSpan.style.display = "";
        Kehu51.Team.UI.Web.App.Ajax.GetData.ExistsMobilePhone(mobilePhone, mobilephonecallback); //2为个人客户
        //}
    }
}

function mobilephonecallback(res)  //回调函数,显示结果   
{
    var cusType = 1;
    var inputPhone;
    if (cusType == 2) {
        inputPhone = document.getElementById("rptMobilePhone");
    } else {
        inputPhone = document.getElementById("rptLinkMan_MobilePhone");
    }

    var lodingSpan = $("rptRepeatSpan_" + fieldName);
    lodingSpan.style.display = "none";

    checkphoneing = false;//检测完成
    if (res.value != "") {
        var msg = res.value;
        Dialog.alert(msg, function () { inputPhone.select(); });
    }
}*/



//2017-01-17 qianlong 获取checkbox的值
function SetCheckBoxValue() {
    var checkboxFieldList = $("#rptCheckBoxList").val();
    var arrFieldName = checkboxFieldList.split(',');
    for (var i = 0; i < arrFieldName.length; i++) {
        var fieldName = arrFieldName[i];
        if (fieldName == "") continue;
        var value = GetCheckedID(fieldName);
        $("#rpt" + fieldName).val(value);
    }

    return false;
}

//2017-06-07 qianlong 表单内容是否为空
//目前只校验了 textarea、text、select、checkbox、radio
function CheckFormContent() {
    var conLen = 0;
    var flag = false;
    $(document).find("textarea").each(function (index) {
        conLen++;
        var content = $(this).val();
        if (content != "") {
            flag = true;
            return;
        }
    });
    //文本框
    if (!flag) {
        $("input[type=text]").each(function (index) {
            var content2 = $(this).val();
            var id = $(this).attr("id");
            var isShow = $(this).is(":hidden");
            if (id != undefined && !isShow && id != "rptGetTime" && content2 != "") {
                flag = true;
                return;
            }
        });
    }
    //下拉选择框 暂时注释掉下拉框
    //if (!flag) {
    //    $("select").each(function () {
    //        var v = $(this).val();
    //        var id = $(this).attr("id");
    //        if (v != 0 && id != "rptLinkMan_BirthDayType" && id != "rptBirthDayType") {
    //            flag = true;
    //            return;
    //        }
    //    });
    //}
    //多选框
    if (!flag) {
        var checkList = "";
        $(".checkbox-item").each(function (index) {
            var type = $(this).attr("data-type");
            if (checkList.indexOf(type) == -1) checkList += type + ",";
        });
        if (checkList != "") {
            checkList = checkList.substr(0, checkList.length - 1);
            var arrCheckBox = checkList.split(',');
            for (var i = 0; i < arrCheckBox.length; i++) {
                var type = arrCheckBox[i]
                var chkValue = GetValue(type);
                if (chkValue != "") {
                    flag = true;
                    return;
                }
            }
        }
    }
    //单选框
    if (!flag) {
        var radioList = "";
        //获取单选框的内容
        $(".radiobox-item").each(function (index) {
            var type = $(this).attr("data-type");
            if (radioList.indexOf(type) == -1 && type != "share") radioList += type + ",";
        });
        if (radioList != "") {
            radioList = radioList.substr(0, radioList.length - 1);
            var arrRadio = radioList.split(',');
            for (var i = 0; i < arrRadio.length; i++) {
                var type = arrRadio[i]
                var chkValue = GetRadioValue(type);
                if (chkValue != "") {
                    flag = true;
                    return;
                }
            }
        }
    }
    return flag;
}

//不再提醒
function NoTips(tableid) {
    //不在提醒就是删除该文件
    var url = "/app/ajax/SaveFormData.aspx?action=delete&tableid="+tableid;
    $.ajax({
        type: "GET",
        url: url,
        success: function (result) {
            //暂时不做任务处理
            $(".tips").fadeOut(200, function () {
                StartSetSize();
            });
            
        },
        error: function (result) {
            //暂时不做任务处理
            //parent.Dialog.close();
        }
    });
    
}
//自动保存表单内容
var autoCount = 0;
function autoSaveFormContent(tableID, moduletype) {
    if (moduletype == undefined) moduletype = 0;
    if (CheckFormContent()) {
        //跟进
        if (tableID == 28) {
            //多功能编辑框
            if ($("textarea").hasClass("ke-edit-textarea")) {
                $("#rptFollowResults").val(encodeURI(decodeURI($("#rptFollowResults").val())));
                if ($("#rptFollowResults").val() == "") return;
            }
        }
        else if (tableID == 72) {
            //产品
            if ($("textarea").hasClass("ke-edit-textarea")) {
                $("#rptcustom_textarea_1").val(encodeURI(decodeURI($("#rptcustom_textarea_1").val())));

                $("#rptcustom_textarea_2").val(encodeURI(decodeURI($("#rptcustom_textarea_2").val())));

                $("#rptcustom_textarea_3").val(encodeURI(decodeURI($("#rptcustom_textarea_3").val())));
            }
        }

        //获取多选框的内容
        var checkList = "";
        var radioList = "";
        $(".checkbox-item").each(function (index) {
            var type = $(this).attr("data-type");
            if (checkList.indexOf(type) == -1) checkList += type + ",";
        });
        if (checkList != "") {
            checkList = checkList.substr(0, checkList.length - 1);

            var arrCheckBox = checkList.split(',');
            for (var i = 0; i < arrCheckBox.length; i++) {
                var type = arrCheckBox[i]
                var chkValue = GetValue(type);
                if (chkValue != "")
                    $("#rpt" + type).val(chkValue);
            }
        }
        //获取单选框的内容
        $(".radiobox-item").each(function (index) {
            var type = $(this).attr("data-type");
            if (radioList.indexOf(type) == -1) radioList += type + ",";
        });
        if (radioList != "") {
            radioList = radioList.substr(0, radioList.length - 1);
            var arrRadio = radioList.split(',');
            for (var i = 0; i < arrRadio.length; i++) {
                var type = arrRadio[i]
                var chkValue = GetRadioValue(type);
                if (chkValue != "") {
                    if (type == "rptLinkMan_Sex")
                        $("#rptLinkMan_Sex").val(chkValue);
                    else if (type == "rptSex")
                        $("#" + type).val(chkValue);
                    else
                        $("#rpt" + type).val(chkValue);
                }
            }

        }
        if (autoCount <= 5) {
            var url = "/app/ajax/SaveFormData.aspx?action=save&tableid=" + tableID + "&type=" + moduletype;
            $.ajax({
                type: "POST",
                url: url,
                data: $('#form1').serialize(),
                success: function (result) {
                    if (result == "1") {
                        if (autoCount > 0)
                            autoCount = 0;
                        $("#autosavetips").css({ "bottom": "38px", "left": "0px;" });
                        //暂时不做任何操作
                        $("#autosavetips").fadeIn(300, function () {
                            $("#tipsMsg").text("当前内容已自动保存！");
                        }).fadeOut(6000);
                    } else {
                        autoCount++;
                    }
                },
                error: function (result) {
                    //暂时不做任何操作
                }
            });
        }
    }
}