if(typeof Kehu51 == "undefined") Kehu51={};
if(typeof Kehu51.Team == "undefined") Kehu51.Team={};
if(typeof Kehu51.Team.UI == "undefined") Kehu51.Team.UI={};
if(typeof Kehu51.Team.UI.Web == "undefined") Kehu51.Team.UI.Web={};
if(typeof Kehu51.Team.UI.Web.App == "undefined") Kehu51.Team.UI.Web.App={};
if(typeof Kehu51.Team.UI.Web.App.Ajax == "undefined") Kehu51.Team.UI.Web.App.Ajax={};
if(typeof Kehu51.Team.UI.Web.App.Ajax.GetListMultiData_class == "undefined") Kehu51.Team.UI.Web.App.Ajax.GetListMultiData_class={};
Kehu51.Team.UI.Web.App.Ajax.GetListMultiData_class = function() {};
Object.extend(Kehu51.Team.UI.Web.App.Ajax.GetListMultiData_class.prototype, Object.extend(new AjaxPro.AjaxClass(), {
	GetNextData: function(parameterName, id, classID) {
		return this.invoke("GetNextData", {"parameterName":parameterName, "id":id, "classID":classID}, this.GetNextData.getArguments().slice(3));
	},
	GetNextDataNew: function(parameterName, id, classID, configGuid) {
		return this.invoke("GetNextDataNew", {"parameterName":parameterName, "id":id, "classID":classID, "configGuid":configGuid}, this.GetNextDataNew.getArguments().slice(4));
	},
	url: '/ajaxpro/Kehu51.Team.UI.Web.App.Ajax.GetListMultiData,Kehu51.Team.UI.Web.ashx'
}));
Kehu51.Team.UI.Web.App.Ajax.GetListMultiData = new Kehu51.Team.UI.Web.App.Ajax.GetListMultiData_class();

