$(function () {

    // 管理员登录url
    var adminUrl="/admin/login";

    // 员工登录url
    var emplUrl="/empl/login";

    // 登录事件
    $("#login").click(function () {

        // 获取表单数据
        var username=$("#username").val();
        var pwd=$("#password").val();

        // 判断登录类型
        var type = loginType(username);

        // 管理员登录
        if(type==1){
            // 组装一个register对象
            var register={};
            register.userName=username;
            register.userEmail=null;
            register.contactTel=null;
            register.userPwd=pwd;

            // 填充邮箱
            var reg = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)/;
            if(reg.test(username)){
                register.userEmail=username;
            }
            // 填充手机号
            var phoneReg = /(^1[3|4|5|7|8]\d{9}$)|(^09\d{8}$)/;
            if(phoneReg.test(username)){
                register.contactTel=username;
            }

            // 通过 ajax 发送数据
            $.ajax({
                url:adminUrl,
                type:"post",
                dataType:"json",
                data:JSON.stringify(register),
                cache: false,                      // 不缓存
                processData: false,                // jQuery不要去处理发送的数据
                contentType: "application/json",
                success:function (data) {           //成功回调
                    console.log(data);
                    if(data.code == 9010201){
                        layer.msg(data.msg,function () {
                            //跳转首页
                            window.location.href="/hello";
                        });
                    }else{
                        layer.msg(data.msg)
                    }
                }
            })
        }


        // 员工登录
        if(type==2){
            // 组装一个empl对象
        }


        // 未注册
        if(type==0){
            layer.msg("没有该账户,请先去注册!");
        }


    });






})

/**
 *  判断根据账号是管理员登录还是员工登录
 *   0: 未注册 1: 管理员 2 : 员工
 * @param username
 */
function loginType(username) {
    var type;
    // 发送 ajax 验证
    $.ajax({
        url : "/login/type",
        type : "post",
        dataType : "json",
        data : {"username":username},
        async:false, // 同步请求
        contentType: "application/json",
        success : function (data) {
            type=data;
        }
    });
    return type;
}
