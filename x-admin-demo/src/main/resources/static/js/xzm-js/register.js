$(function () {

    // 注册的url
    var regUrl="/register/do";

    // 判断用户名是否被注册
    $("#username").change(function () {
        // 获取用户名
        var username=$("#username").val();
        // 验证用户名是否含有特殊字符
        if(check_other_char(username)){
            layer.msg("用户名不能包含特殊字符");
            return;
        }
        // 发送 ajax 验证
            $.ajax({
                url : "/check/username",
                type : "post",
                dataType : "json",
                data : {"username":username},
                contentType: "application/json",
                success : function (data) {
                    console.log(data);
                    if(data.code==9010405){
                        $("#ts").text(data.msg);
                        $("#ts").css("color","red");
                    }else{
                        $("#ts").text(data.msg);
                        $("#ts").css("color","green");
                    }
                }
            });
    });

    // 验证邮箱格式
    // $("#email").change(function () {
    //     // 获取邮箱
    //     var email=$("#email").val();
    //     // 验证邮箱格式
    //     if(!judgeEmail(email)){
    //         layer.msg("邮箱格式不正确");
    //         return;
    //     }
    // });

    // 验证确认密码
    // $("#Rpassword").change(function () {
    //     // 获取密码
    //     var pwd=$("#password").val();
    //     // 获取确认密码
    //     var rPwd=$("#Rpassword").val();
    //     if(rPwd!=pwd){
    //         layer.msg("密码不一致!");
    //         return;
    //     }
    // });

    // 验证联系人的合法性

    // 验证联系号码
    // $("#contactNumber").change(function () {
    //     // 获取手机号
    //     var contactNum=$("#contactNumber").val();
    //     // 手机号正则
    //     var phoneReg = /(^1[3|4|5|7|8]\d{9}$)|(^09\d{8}$)/;
    //     // 验证
    //     if(!phoneReg.test(contactNum.trim())){
    //         layer.msg("请输入合法的手机号");
    //     }
    // });


    // 注册事件
    $("#register").click(function () {

        // 获取表单数据
        var version=$("#version").val();
        var useNum=$("#use_number").val();
        var username=$("#username").val();
        var email=$("#email").val();
        var pwd=$("#password").val();
        var rPwd=$("#Rpassword").val();
        var contacts=$("#contacts").val();
        var contactNum=$("#contactNumber").val();

       // 验证数据的合法性
        if(username.trim().length==0) {
            layer.msg("用户名不能为空");
            return ;
        }

        // 验证邮箱格式
        if(!judgeEmail(email)){
            layer.msg("邮箱格式不正确");
            return;
        }

        if(pwd.trim().length==0){
            layer.msg("密码不能为空");
            return ;
        }

        if(pwd!=rPwd){
            layer.msg("密码不一致");
            return ;
        }

        if(contacts.trim().length==0){
            layer.msg("联系人不能为空");
            return ;
        }

        if(contactNum.trim().length==0){
            layer.msg("联系方式不能为空");
            return ;
        }

        // 手机号正则
        var phoneReg = /(^1[3|4|5|7|8]\d{9}$)|(^09\d{8}$)/;
        // 验证
        if(!phoneReg.test(contactNum.trim())){
            layer.msg("请输入合法的手机号");
        }

        // 组装一个 register 对象
        var register={};
        register.versionId=version;
        register.userNumber=useNum;
        register.userName=username;
        register.userEmail=email;
        register.userPwd=pwd;
        register.contacts=contacts;
        register.contactTel=contactNum;

        // 通过 ajax 发送数据
        $.ajax({
            url:regUrl,
            type:"post",
            dataType:"json",
            data:JSON.stringify(register),
            cache: false,                      // 不缓存
            processData: false,                // jQuery不要去处理发送的数据
            contentType: "application/json",
            success:function (data) {           //成功回调
                if(data.code == 9010200){
                    layer.msg(data.msg,function () {
                        //跳转登录页面
                        window.location.href="/login/page";
                    });
                }else{
                    layer.msg(data.msg)
                }
            }
        })
    });
});

// 验证邮箱格式的方法
function judgeEmail(email) {
    // 定义正则表达式
    var reg = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)/;
    // 判断
    if(   reg.test(email) == false ) {
        layer.msg('邮箱格式不正确');
        return false;
    }
    return true;
}


// 验证用户名是否含有特殊字符
function check_other_char(str)
{
    var arr = ["&", "\\", "/", "*", ">", "<", "@", "!"];
    for (var i = 0; i < arr.length; i++)
    {
        for (var j = 0; j < str.length; j++)
        {
            if (arr[i] == str.charAt(j))
            {
                return true;
            }
        }
    }
    return false;
}