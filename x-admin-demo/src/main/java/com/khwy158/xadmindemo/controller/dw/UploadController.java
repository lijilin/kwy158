package com.khwy158.xadmindemo.controller.dw;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.dw.JsonResultList;
import com.khwy158.modeldemo.pojo.dw.DwDocument;
import com.khwy158.xadmindemo.service.dw.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @program: khwy-158
 * @description:
 * @author: Mr.Wang
 * @create: 2018-09-01 11:18
 **/
@RestController
public class UploadController {
    @Autowired
    DocumentService service;

    @RequestMapping("/dw/docmy/list/{parentid}")
    public JsonResultList dlist(Integer page, Integer limit, @PathVariable Integer parentid){
        JsonResultList listd = service.listd(page, limit, 10, 0,parentid);
        return  listd;
    }

    @RequestMapping("/dw/docmy/download")
    public void download(Integer id,HttpServletResponse res) throws IOException {
        DwDocument dwDocument = service.testDownload(id);
        String path="E:/javamavenproject/kfwy/kwy158/dw-demo/src/main/resources/upload/"+dwDocument.getPath();
        String name=dwDocument.getFileName();
        UploadUtil.downLoad(path,name,res);

    }
@RequestMapping("/dw/docmy/upload")
    public ResultDtoUtile upload (@RequestParam(value = "file" ,required = false) MultipartFile file, @RequestParam(value = "ispublic") Integer ispublic ){
    System.out.println(file.getOriginalFilename());

       return service.upload(file,ispublic);
    }
}
