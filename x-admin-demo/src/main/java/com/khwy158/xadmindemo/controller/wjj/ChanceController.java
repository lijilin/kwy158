package com.khwy158.xadmindemo.controller.wjj;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.WjjChance;
import com.khwy158.xadmindemo.service.wjj.ChanceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ChanceController {

    @Autowired
    private ChanceService cs;


    @ApiOperation(value = "插入机会", notes = "数据库插入机会")
    @PostMapping("/j-chanceAdd")
    public ResultDtoUtile chanceAdd(@RequestBody WjjChance wjjChance){
        //进行模块调用返回数据
        ResultDto chanceAdd = cs.chanceAdd(wjjChance);

        //返回
        return new ResultDtoUtile(chanceAdd.getCode(),chanceAdd.getMsg(),chanceAdd);
    }

    @ApiOperation(value = "成交可能性，机会阶段，来源表--所有客户共享字段返回", notes = "成交可能性，机会阶段，来源表--都返回")
    @PostMapping("/j-chanceStage")
    public ResultDtoUtile chanceStage(){
        //机会阶段
        ResultDto chanceStage = cs.chanceStage();
        //成交可能性
        ResultDto chanceGrade = cs.chanceGrade();
        //来源表
        ResultDto chanceSource = cs.chanceSource();
        //失败
        if (chanceGrade.getCode()!=5200||chanceSource.getCode()!=5200||chanceStage.getCode()!=5200){
            return new ResultDtoUtile(5201,"loser");
        }

        //组装返回对象
        Map<String,Object> map = new HashMap<>(16);
        map.put("chanceStage",chanceStage);
        map.put("chanceGrade",chanceGrade);
        map.put("chanceSource",chanceSource);

        //成功
        return new ResultDtoUtile(chanceStage.getCode(),chanceStage.getMsg(),map);
    }
}
