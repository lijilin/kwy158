package com.khwy158.xadmindemo.controller.dw;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.dw.JsonResultList;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.dw.DwNotice;
import com.khwy158.modeldemo.pojo.wjj.WjjComplain;
import com.khwy158.xadmindemo.service.dw.NoticeService;
import com.khwy158.xadmindemo.service.wjj.ComplainService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: ComplainController
 * @Author: wujunjie
 * @Description: TODO
 * @Date: Created in 2018-8-29
 * @Modified By:
 **/
@RestController
public class NoticeController {

    @Autowired
    private NoticeService nc;

    @ApiOperation(value = "返回所有公司客户", notes = "查询数据库中所有的公司客户")
    @RequestMapping("/send")
    public ResultDtoUtile send(@RequestBody DwNotice notice){

        //返回
        return  nc.sendNotice(notice);
    }
//    @RequestMapping("/get/{id}")
//    public ResultDtoUtile get(@PathVariable Integer id){
//        return nc.find(id);
//    }
    @RequestMapping("/list")
    public JsonResultList getlist(Integer page, Integer limit){
        return  nc.list(page,limit);
    }


}
