package com.khwy158.xadmindemo.controller.wjj;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @ClassName: IndexController
 * @Author: wujunjie
 * @Description: 所有index页面
 * @Date: Created in 2018-8-31
 * @Modified By:
 **/
@Controller
public class IndexController {

    /**
     * 投诉主页
     * @return
     */
    @GetMapping("/j/complain/index")
    public String complainIndex(){
        return "wjj/complain";
    }

    /**
     * 新建投诉
     * @return
     */
    @GetMapping("/j/complain/new")
    public String complainNew(){
        return "wjj/complain-new";
    }

}
