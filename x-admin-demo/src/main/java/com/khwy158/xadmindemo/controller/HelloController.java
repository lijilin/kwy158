package com.khwy158.xadmindemo.controller;

import com.khwy158.xadmindemo.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 描述:
 *
 * @author lijilin
 * @create 2018-08-27 16:22
 */
@Controller
public class HelloController {
    @Autowired
    HelloService helloService;

    @RequestMapping("/hello")
    public String hello(){
        System.out.println("进来了");
        return "index";
    }

    @RequestMapping("hello2")
    @ResponseBody
    public String hello2(){
        return helloService.hello();
    }


}