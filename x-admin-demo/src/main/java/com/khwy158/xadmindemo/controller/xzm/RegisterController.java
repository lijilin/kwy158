package com.khwy158.xadmindemo.controller.xzm;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.xadmindemo.service.xzm.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xzm
 * @Date 2018/8/28 14:25
 *  企业注册
 */
@Controller
public class RegisterController {

    @Autowired
    private RegisterService rs;

    /**
     *  注册页面
     * @return
     */
    @GetMapping("/register/page")
    public String registerPage(){
        return "xzm-pages/register-login/regest";
    }



    /**
     * 修改密码
     * @return
     */
    @GetMapping("/modifyPwd/page")
    public String modifyPwdPage(){
        return "xzm-pages/register-login/modifyPwd";
    }


    /**
     *  验证用户名是否被注册
     * @param nameStr
     * @return
     */
    @PostMapping("/check/username")
    @ResponseBody
    public ResultDtoUtile checkUser(@RequestBody String nameStr){
        // 截取用户名
        String username=nameStr.substring(nameStr.lastIndexOf("=")+1);
        ResultDtoUtile rd = rs.checkUser(username);
        return rd;
    }

    /**
     *  企业注册
     * @param register
     * @return
     */
    @RequestMapping("/register/do")
    @ResponseBody
    public ResultDtoUtile register(@RequestBody Register register){
        ResultDtoUtile rd = rs.register(register);
        return rd;
    }

}
