package com.khwy158.xadmindemo.controller.lj;

import com.khwy158.ljdemo.pojo.LjStockin;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.xadmindemo.service.lj.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author:luojun
 * @date:2018/8/28
 */
@Controller
public class StockController {

    @Autowired
    private StockService ss;
    /**
     * 入库单
     * @return
     */
    @RequestMapping("/stock_add.action")
    public String stockAdd(){
        return "/lj/stock_add";
    }

    /**
     * 入库操作
     * @param ljStockin
     * @return
     */
    @RequestMapping("addStockin.action")
    public ResultDtoUtile addStock(LjStockin ljStockin){
        return ss.addStock(ljStockin);
    }
    /**
     * 入库记录
     * @return
     */
    @RequestMapping("/stock.action")
    public String stock(){
        return "/lj/stock-list";
    }

    /**
     * 出库记录
     * @return
     */
    @RequestMapping("/stock_out.action")
    public String stockOut(){
        return "/lj/stock-out_list";
    }

    /**
     * 锁定记录
     * @return
     */
    @RequestMapping("/stock_lock.action")
    public String stockLock(){
        return "/lj/stock-lock_list";
    }

}
