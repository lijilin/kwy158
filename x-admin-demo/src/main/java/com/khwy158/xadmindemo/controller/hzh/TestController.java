package com.khwy158.xadmindemo.controller.hzh;


import com.khwy158.xadmindemo.service.hzh.Test01;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {


    @Autowired
    Test01 test01;


        @RequestMapping("test101")
        @ResponseBody
        public String test101(){

            return test01.test101();
        }

    @RequestMapping("/test02")
    public String test102(){

        return test01.test102();
    }


}
