package com.khwy158.xadmindemo.controller.dw;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @program: khwy-158
 * @description:
 * @author: Mr.Wang
 * @create: 2018-09-01 11:34
 **/
@Controller
public class PageController {
    @RequestMapping("/dw/page/docmy")
    public String docmypage(){

        return "dw/myDocument";
    }
}
