package com.khwy158.xadmindemo.controller.ljl;

import com.khwy158.modeldemo.pojo.ljl.Page;
import com.khwy158.modeldemo.utils.ResultEexptionDto;
import com.khwy158.xadmindemo.service.ljl.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 描述:
 *
 * @author lijilin
 * @create 2018-08-30 9:38
 */
@Controller
public class DealController {

    @Autowired
    DealService dl;


    @GetMapping("/get_deal_page")
    public  String getDeal(){
        return "ljl/deal";
    }

    @RequestMapping("/get_deal")
    public String getdeal(HttpServletRequest request, Model model){
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        Page pages=new Page(1,10);
        System.out.println(pages);
        ResultEexptionDto resultEexptionDto = dl.companyClientList(pages);
        model.addAttribute("lists",resultEexptionDto.getData());
        return "lil/deal";
    }
}