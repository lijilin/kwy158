package com.khwy158.xadmindemo.controller.hzh;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 产品分类页面
 */
@Controller
@RequestMapping("/hzh")
public class ProductPageController {

    /**
     * 产品名称统计页面
     * @return
     */
    @RequestMapping("product_name_page")
    public String ProductNamePage(){
        return "/hzh/product/product-name";
    }
    /**
     * 产品分类统计页面
     * @return
     */
    @RequestMapping("/product_classify_page")
    public String ProductClassify_Page(){
        return "/hzh/product/product-classify";
    }
    /**
     * 所在城市统计页面
     * @return
     */
    @RequestMapping("/product_city_page")
    public String ProductCity_Page(){
        return "/hzh/product/product-city";
    }
    /**
     * 客户状态统计页面
     * @return
     */
    @RequestMapping("/product_status_page")
    public String ProductStatus_Page(){
        return "/hzh/product/product-status";
    }

    /**
     * 时间统计过页面
     * @return
     */
    @RequestMapping("/product_data_page")
    public String DataEcharts_Page(){
        return "/hzh/product/data-echarts";
    }
    /**
     * 类型图表过页面
     * @return
     */
    @RequestMapping("/product_type_page")
    public String TypeEcharts_Page(){
        return "/hzh/product/type-echarts";
    }

}
