package com.khwy158.xadmindemo.controller.ldx;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.ldx.JsonResult;
import com.khwy158.modeldemo.pojo.wjj.LdxCompanyClient;
import com.khwy158.xadmindemo.service.ldx.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

    @Autowired
    private ClientService cs;


    @RequestMapping("/ldx/newclient")
    public ResultDtoUtile addNewClient(@RequestBody LdxCompanyClient ldxCompanyClient){

        JsonResult jsonResult=cs.savePersonalClient(ldxCompanyClient);

        return new ResultDtoUtile(jsonResult.getCode(),jsonResult.getMsg());
    }
}
