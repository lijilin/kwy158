package com.khwy158.xadmindemo.controller.hzh;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.xadmindemo.service.hzh.ProductNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 产品名称分类
 */
@RestController
public class ProductNameController {

    @Autowired
    ProductNameService service;

    @RequestMapping("/hzh/city_list")
    public ResultDtoUtile selectCityList(){
        return service.selectCityList();
    }



}
