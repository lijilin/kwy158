package com.khwy158.xadmindemo.controller.kzl;


import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.ljl.Page;
import com.khwy158.xadmindemo.service.kzl.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService ps;

    @RequestMapping("/listPage")
    public String productListPage(){
        return "kzl/product/product";
    }

    @RequestMapping("/list1")
    @ResponseBody
    public JsonResult productList(HttpServletRequest request){
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        Page pages=new Page(Integer.parseInt(page),Integer.parseInt(limit));
        JsonResult jsonResult = ps.list1(pages);
        System.out.println(jsonResult);
        return jsonResult;
    }
}
