package com.khwy158.xadmindemo.controller.wjj;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.wjj.FenYeShuJu;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.LdxCompanyClient;
import com.khwy158.modeldemo.pojo.wjj.WjjComplain;
import com.khwy158.xadmindemo.service.wjj.ComplainService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: ComplainController
 * @Author: wujunjie
 * @Description: TODO
 * @Date: Created in 2018-8-29
 * @Modified By:
 **/
@RestController
public class ComplainController {

    @Autowired
    private ComplainService sc;

    @ApiOperation(value = "返回所有公司客户", notes = "查询数据库中所有的公司客户")
    @GetMapping("/j-company")
    public ResultDtoUtile companyAll(){
        //接收返回所有公司
        ResultDto company = sc.company();
        //返回
        return new ResultDtoUtile(company.getCode(),company.getMsg(),company);
    }
    @ApiOperation(value = "返回当前登录用户的所有投诉类型和方式", notes = "查询数据库中所有投诉类型和方式")
    @GetMapping("/j-typeAndMode")
    public ResultDtoUtile complainType(){
        //接收返回所有投诉类型
        ResultDto companyType = sc.companyType();
        ResultDto complainMode = sc.complainMode();
        //失败
        if (companyType.getCode()!=5200||complainMode.getCode()!=5200){
            return new ResultDtoUtile(5201,"loser");
        }
        Map<String,Object> map= new HashMap<>(16);
        map.put("companyType",companyType);
        map.put("complainMode",complainMode);
        //返回成功
        return new ResultDtoUtile(complainMode.getCode(),complainMode.getMsg(),map);
    }

    @ApiOperation(value = "增加投诉", notes = "增加投诉用户到数据库")
    @PostMapping("/j-complainAdd")
    public ResultDtoUtile complainAdd(@RequestBody WjjComplain wjjComplain){
        //返回增加是否成功
        ResultDto complainAdd = sc.complainAdd(wjjComplain);
        //返回
        return new ResultDtoUtile(complainAdd.getCode(),complainAdd.getMsg(),complainAdd);
    }
    @ApiOperation(value = "返回所有当前登录的投诉客户", notes = "返回所有当前登录的投诉客户，后台获取登陆用户")
    @GetMapping("/j-complainAll")
    public FenYeShuJu complainAll(@RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value = "limit",defaultValue = "1") Integer limit){
        //返回增加是否成功
        FenYeShuJu rustFenYe = sc.companyAll(page,limit);

        //返回
        return rustFenYe;
    }
}
