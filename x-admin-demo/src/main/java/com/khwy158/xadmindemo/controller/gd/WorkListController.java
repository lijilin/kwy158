package com.khwy158.xadmindemo.controller.gd;

import com.khwy158.modeldemo.dto.gd.JsonResult;
import com.khwy158.modeldemo.pojo.ljl.Page;
import com.khwy158.xadmindemo.service.gd.WorkListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Controller
//和页面来打交道
public class WorkListController {
    @Autowired
   private WorkListService ws;

    //请求工单页面
    @RequestMapping("/worklistpage")
//    @ResponseBody
    public String workListPage(){
        return "/gd/worklist/worklist-list";
    }

    //获取到所有的工单数据
    @RequestMapping("/worklistpage/list")
    @ResponseBody
    public JsonResult workListList(HttpServletRequest request){
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        Page pages=new Page(Integer.parseInt(page),Integer.parseInt(limit));
        JsonResult jsonResult = ws.selectEmplWorkList(pages);
        return jsonResult;
    }
    //请求工单的查看界面

    @RequestMapping("/worklist/detail/page")
    public String workListDetail(){
        return "/gd/worklist/workListDetail.html";
    }

    }

