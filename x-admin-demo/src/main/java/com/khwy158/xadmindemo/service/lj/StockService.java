package com.khwy158.xadmindemo.service.lj;

import com.khwy158.ljdemo.pojo.LjStockin;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("LJ-CONSUMER")
@Service
public interface StockService {

    /**
     * 入库
     * @param ljStockin
     * @return
     */
    @RequestMapping("/add.action")
    @ResponseBody
    public ResultDtoUtile addStock(@RequestBody LjStockin ljStockin);
}
