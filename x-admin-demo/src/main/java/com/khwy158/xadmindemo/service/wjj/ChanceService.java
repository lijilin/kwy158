package com.khwy158.xadmindemo.service.wjj;

import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.WjjChance;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("WJJ-CONSUMER")
@Service
public interface ChanceService {
    /**
     * 机会客户增加
     * @param wjjChance
     * @return
     */
    @PostMapping("/chance-add")
    ResultDto chanceAdd(@RequestBody WjjChance wjjChance);

    /**
     * 成交可能性返回接口
     * @return
     */
    @GetMapping("/chance-grade")
    ResultDto chanceGrade();
    /**
     * 来源返回接口
     * @return
     */
    @GetMapping("/chance-source")
    ResultDto chanceSource();
    /**
     * 机会阶段返回接口
     * @return
     */
    @GetMapping("/chance-Stage")
    ResultDto chanceStage();
}
