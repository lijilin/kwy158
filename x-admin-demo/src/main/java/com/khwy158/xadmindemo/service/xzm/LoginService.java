package com.khwy158.xadmindemo.service.xzm;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.xzm.Register;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author xzm
 * @Date 2018/8/30 10:35
 */
@FeignClient("XZM-CONSUMER")
@Service
public interface LoginService {

    /**
     *  判断登录类型
     * @param nameStr
     * @return
     */
    @RequestMapping("/login/type")
    @ResponseBody
    public Integer loginType(@RequestBody String nameStr);

    /**
     *  企业登录
     * @param register
     * @return
     */
    @RequestMapping("/admin/login")
    @ResponseBody
    public ResultDtoUtile login(@RequestBody Register register);

}
