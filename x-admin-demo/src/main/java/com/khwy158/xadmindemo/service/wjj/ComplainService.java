package com.khwy158.xadmindemo.service.wjj;

import com.khwy158.modeldemo.dto.wjj.FenYeShuJu;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.WjjComplain;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@FeignClient("WJJ-CONSUMER")
@Service
public interface ComplainService {
    /**
     * 分页返回所有客户
     * @return
     */
    @RequestMapping("/company-all")
    ResultDto company();

    /**
     * 返回所有的投诉类型
     * @return
     */
    @GetMapping("/complain-type")
    ResultDto companyType();
    /**
     * 返回所有的投诉方式
     * @return
     */
    @GetMapping("/complain-mode")
    ResultDto complainMode();
    /**
     * 进行投诉插入
     * @param wjjComplain
     * @return
     */
    @PostMapping("/complain-add")
    ResultDto complainAdd(@RequestBody WjjComplain wjjComplain);

    /**
     * 返回所有投诉客户
     * @return
     */
    @GetMapping("/complain-all")
    FenYeShuJu companyAll(@RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value = "limit",defaultValue = "1") Integer limit);
}
