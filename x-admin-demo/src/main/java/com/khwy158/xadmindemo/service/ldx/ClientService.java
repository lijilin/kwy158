package com.khwy158.xadmindemo.service.ldx;


import com.khwy158.modeldemo.dto.ldx.JsonResult;
import com.khwy158.modeldemo.pojo.wjj.LdxCompanyClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("LDX-SERVICE")
@Service
public interface ClientService {

    @RequestMapping("/ldx/savepersonal")
    JsonResult savePersonalClient(@RequestBody LdxCompanyClient ldxCompanyClient);
}
