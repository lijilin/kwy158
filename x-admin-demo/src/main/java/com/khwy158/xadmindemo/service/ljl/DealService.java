package com.khwy158.xadmindemo.service.ljl;

import com.github.pagehelper.PageInfo;
import com.khwy158.modeldemo.pojo.ljl.Page;
import com.khwy158.modeldemo.utils.ResultEexptionDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@FeignClient("LJL-CONSUMER")
@Service
public interface DealService {

    /**
     * 成交客户
     * @return
     */
    @RequestMapping("/ljl-deal")
    @ResponseBody
    ResultEexptionDto companyClientList(@RequestBody Page page);

}
