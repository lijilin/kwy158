package com.khwy158.xadmindemo.service.gd;

import com.github.pagehelper.PageInfo;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.gd.JsonResult;
import com.khwy158.modeldemo.pojo.gd.GdWorklist;
import com.khwy158.modeldemo.pojo.ljl.Page;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Service
@FeignClient("GD-CONSUMER")
//放自己模块的controller 不要方法体
public interface WorkListService {

    /**
     * 修改工单的状态
     * @param gdWorklist
     * @return
     */
    @RequestMapping("WorkList/change")
    public ResultDtoUtile changeWorkListStatus(@RequestBody GdWorklist gdWorklist);

    /**
     * 批量删除工单
     * @param ids
     * @return
     */
    @RequestMapping("/workList/delete")
    public ResultDtoUtile deleteTravel(@RequestBody List<Integer> ids);

    /**
     * 查询登录的用户来查询工单
     * @param page
     * @return
     */
    @RequestMapping("/workList/all")
    public JsonResult selectEmplWorkList(@RequestBody Page page);



}
