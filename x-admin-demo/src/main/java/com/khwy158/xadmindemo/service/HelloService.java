package com.khwy158.xadmindemo.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("LJL-CONSUMER")
@Service
public interface HelloService {

    @GetMapping("/hello")
    public String hello();
}
