package com.khwy158.xadmindemo.service.hzh;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
@FeignClient("hzh-consumer")
public interface Test01 {

    @RequestMapping("test101")
    @ResponseBody
    public String test101();

    @RequestMapping("/test02")
    public String test102();

}
