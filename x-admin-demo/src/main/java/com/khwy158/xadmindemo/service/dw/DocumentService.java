package com.khwy158.xadmindemo.service.dw;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.dw.JsonResultList;
import com.khwy158.modeldemo.pojo.dw.DwDocument;
import com.khwy158.xadmindemo.controller.dw.MultipartSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @program: khwy-158
 * @description:
 * @author: Mr.Wang
 * @create: 2018-09-01 11:11
 **/
@FeignClient(name="DW-CONSUMER",configuration = MultipartSupportConfig.class)
@Service
public interface DocumentService {

    /**
     * 上传文件
     * @param file 上传的文件
     * @param ispublic 是否是公共文档的
     * @return
     */
    @RequestMapping(value = "/upload",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResultDtoUtile upload(@RequestPart(value = "file") MultipartFile file, @RequestParam(value = "ispublic") Integer ispublic);

    /**
     * 创建文件夹
     * @param name 文件夹名字
     * @param ispublic 是否是公共文档那一块的
     * @return
     */
    @RequestMapping("/file")
    public ResultDtoUtile file(@RequestParam(value = "name") String name, @RequestParam(value = "ispublic") Integer ispublic) ;

    /**
     * 下载
     * @param id 要下载文件的id
     * @param
     * @throws IOException
     */
    @RequestMapping(value = "/testDownload", method = RequestMethod.GET)
    public DwDocument testDownload(@RequestParam(value = "id") Integer id) ;

    /**
     * 移动文件夹
     * @param ids 要被移动的文件的id
     * @param id  移动的目的地的id
     * @return
     */
    @RequestMapping(value = "/move", method = RequestMethod.POST)
    public ResultDtoUtile move(@RequestBody List<Integer> ids, @RequestParam(value = "id")Integer id );

    @RequestMapping("/docmy/list")
    public JsonResultList listd(@RequestParam(value = "page") Integer page
            ,@RequestParam(value = "limit") Integer limit
            ,@RequestParam(value = "mid") Integer mid
            ,@RequestParam(value = "ispublic") Integer ispublic
            ,@RequestParam(value = "parentid") Integer parentid
    );
}
