package com.khwy158.xadmindemo.service.kzl;


import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlProduct;
import com.khwy158.modeldemo.pojo.ljl.Page;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;


@FeignClient("KZL-CONSUMER")
@Service
public interface ProductService {
    /**
     * 产品增加
     * @param product
     * @return
     */
    @PostMapping("/product_add")
    JsonResult add(@RequestBody KzlProduct product);

    /**
     * 产品删除
     * @param id
     * @return
     */
//    @DeleteMapping("product_delete/{id}")
//     ResultDtoUtile delete(@PathVariable Integer id);

    /**
     * 产品更新
     * @param product
     * @return
     */
    @PutMapping("/product_update")
    JsonResult update(@RequestBody KzlProduct product);

    /**
     * 产品列表
     * @return
     */
    @GetMapping("/product_list")
    @ResponseBody
    JsonResult list();


    @RequestMapping("/product_list1")
    @ResponseBody
    JsonResult list1(@RequestBody Page page);
}
