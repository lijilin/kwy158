package com.khwy158.xadmindemo.service.hzh;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 产品名称分类
 */
@Service
@FeignClient("hzh-consumer")
public interface ProductNameService {

    @RequestMapping("/hzh/city_list")
    @ResponseBody
    public ResultDtoUtile selectCityList();

}
