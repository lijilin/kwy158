package com.khwy158.xadmindemo.service.dw;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.dw.JsonResultList;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.dw.DwNotice;
import com.khwy158.modeldemo.pojo.wjj.WjjComplain;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("DW-CONSUMER")
@Service
public interface NoticeService {

    @RequestMapping("/advice/post")
    ResultDtoUtile sendNotice(@RequestBody DwNotice notice);
    @RequestMapping("/advice/get")
     JsonResultList list(@RequestParam(value = "page") Integer page, @RequestParam(value = "limit") Integer limit);
    @RequestMapping("/advice/delete")
    ResultDtoUtile delete(@RequestBody List<Integer> list);
//    @RequestMapping("/advice/get/{id}")
//    public ResultDtoUtile find(@PathVariable Integer id);
    @RequestMapping("/advice/put")
    public ResultDtoUtile update(@RequestBody DwNotice dwNotice );


}
