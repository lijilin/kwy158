package com.khwy158.xadmindemo.service.xzm;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.xzm.Register;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xzm
 * @Date 2018/8/28 14:25
 */
@FeignClient("XZM-CONSUMER")
@Service
public interface RegisterService {

    /**
     *  验证用户名是否被注册
     * @param nameStr
     * @return
     */
    @RequestMapping("/check/username")
    @ResponseBody
    public ResultDtoUtile checkUser(@RequestBody String nameStr);

    /**
     *  企业注册
     * @param register
     * @return
     */
    @RequestMapping("/register/do")
    @ResponseBody
    public ResultDtoUtile register(@RequestBody Register register);

}
