package com.khwy158.sgdemo.dao;

import com.khwy158.sgdemo.pojo.LdxCity;

public interface LdxCityMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int insert(LdxCity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int insertSelective(LdxCity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    LdxCity selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(LdxCity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(LdxCity record);
}