package com.khwy158.sgdemo.pojo;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table ldx_client_grade
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class LdxClientGrade {
    /** */
    private Integer id;

    /** 客户等级名字*/
    private String clientGradeName;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_client_grade.id
     *
     * @return the value of ldx_client_grade.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_client_grade.id
     *
     * @param id the value for ldx_client_grade.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_client_grade.client_grade_name
     *
     * @return the value of ldx_client_grade.client_grade_name
     *
     * @mbg.generated
     */
    public String getClientGradeName() {
        return clientGradeName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_client_grade.client_grade_name
     *
     * @param clientGradeName the value for ldx_client_grade.client_grade_name
     *
     * @mbg.generated
     */
    public void setClientGradeName(String clientGradeName) {
        this.clientGradeName = clientGradeName;
    }
}