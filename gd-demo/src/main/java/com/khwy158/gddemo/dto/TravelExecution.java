package com.khwy158.gddemo.dto;

import com.khwy158.gddemo.enums.LeafStatusEnum;
import com.khwy158.gddemo.enums.TravelStatusEnum;
import com.khwy158.modeldemo.pojo.gd.GdLeaf;
import com.khwy158.modeldemo.pojo.gd.GdTravel;


import java.util.List;

public class TravelExecution {
    private Integer code;
    private String msg;
    private GdTravel gdTravel;
    private List<GdTravel> gdTravelList;

    //失败的构造
    public TravelExecution(TravelStatusEnum travelStatusEnum) {
        this.code = travelStatusEnum.getCode();
        this.msg = travelStatusEnum.getMsg();
    }

    //成功的构造 返回单个请假审批对象
    public TravelExecution(TravelStatusEnum travelStatusEnum, GdTravel gdTravel) {
        this.code = travelStatusEnum.getCode();
        this.msg = travelStatusEnum.getMsg();
        this.gdTravel = gdTravel;
    }

    //成功的构造 返回的是请假审批列表
    public TravelExecution(TravelStatusEnum travelStatusEnum, List<GdLeaf> gdLeafList) {
        this.code = travelStatusEnum.getCode();
        this.msg = travelStatusEnum.getMsg();
        this.gdTravelList = gdTravelList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public GdTravel getGdTravel() {
        return gdTravel;
    }

    public void setGdTravel(GdTravel gdTravel) {
        this.gdTravel = gdTravel;
    }

    public List<GdTravel> getGdTravelList() {
        return gdTravelList;
    }

    public void setGdTravelList(List<GdTravel> gdTravelList) {
        this.gdTravelList = gdTravelList;
    }
}
