package com.khwy158.gddemo.dto;

import com.khwy158.gddemo.enums.WorkListStatusEnum;
import com.khwy158.modeldemo.pojo.gd.GdWorklist;
;

import java.util.List;

public class WorkListExecution {
    private Integer code;
    private String msg;
    private GdWorklist gdWorklist;
    private List<GdWorklist> gdWorklists;

    //失败的构造
    public WorkListExecution(WorkListStatusEnum workListStatusEnum) {
        this.code = workListStatusEnum.getCode();
        this.msg = workListStatusEnum.getMsg();
    }

    //成功的构造 返回单个请假审批对象
    public WorkListExecution(WorkListStatusEnum workListStatusEnum,GdWorklist gdWorklist ) {
        this.code = workListStatusEnum.getCode();
        this.msg = workListStatusEnum.getMsg();
        this.gdWorklist = gdWorklist;
    }

    //成功的构造 返回的是请假审批列表
    public WorkListExecution(WorkListStatusEnum workListStatusEnum, List<GdWorklist> gdWorklists) {
        this.code = workListStatusEnum.getCode();
        this.msg = workListStatusEnum.getMsg();
        this.gdWorklists = gdWorklists;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public GdWorklist getGdWorklist() {
        return gdWorklist;
    }

    public void setGdWorklist(GdWorklist gdWorklist) {
        this.gdWorklist = gdWorklist;
    }

    public List<GdWorklist> getGdWorklists() {
        return gdWorklists;
    }

    public void setGdWorklists(List<GdWorklist> gdWorklists) {
        this.gdWorklists = gdWorklists;
    }
}
