package com.khwy158.gddemo.dto;

import com.khwy158.gddemo.enums.LeafStatusEnum;
import com.khwy158.modeldemo.pojo.gd.GdLeaf;

import java.util.List;

public class LeafExecution {
    private Integer code;
    private String msg;
    private GdLeaf gdLeaf;
    private List<GdLeaf> gdLeafList;

    //失败的构造
    public LeafExecution(LeafStatusEnum leafStatusEnum) {
        this.code = leafStatusEnum.getCode();
        this.msg = leafStatusEnum.getMsg();
    }

    //成功的构造 返回单个请假审批对象
    public LeafExecution(LeafStatusEnum leafStatusEnum, GdLeaf gdLeaf) {
        this.code = leafStatusEnum.getCode();
        this.msg = leafStatusEnum.getMsg();
        this.gdLeaf = gdLeaf;
    }

    //成功的构造 返回的是请假审批列表
    public LeafExecution(LeafStatusEnum leafStatusEnum, List<GdLeaf> gdLeafList) {
        this.code = leafStatusEnum.getCode();
        this.msg = leafStatusEnum.getMsg();
        this.gdLeafList = gdLeafList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public GdLeaf getGdLeaf() {
        return gdLeaf;
    }

    public void setGdLeaf(GdLeaf gdLeaf) {
        this.gdLeaf = gdLeaf;
    }

    public List<GdLeaf> getGdLeafList() {
        return gdLeafList;
    }

    public void setGdLeafList(List<GdLeaf> gdLeafList) {
        this.gdLeafList = gdLeafList;
    }
}
