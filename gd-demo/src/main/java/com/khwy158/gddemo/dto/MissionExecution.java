package com.khwy158.gddemo.dto;

import com.khwy158.gddemo.enums.LeafStatusEnum;
import com.khwy158.gddemo.enums.MissionStatusEnum;
import com.khwy158.modeldemo.pojo.gd.GdMission;


import java.util.List;

public class MissionExecution {
    private Integer code;
    private String msg;
    private GdMission gdMission;
    private List<GdMission> gdMissions;

    //失败的构造
    public MissionExecution(MissionStatusEnum missionStatusEnum) {
        this.code = missionStatusEnum.getCode();
        this.msg = missionStatusEnum.getMsg();
    }

    //成功的构造 返回单个请假审批对象
    public MissionExecution(MissionStatusEnum missionStatusEnum, GdMission gdMission) {
        this.code = missionStatusEnum.getCode();
        this.msg = missionStatusEnum.getMsg();
        this.gdMission = gdMission;
    }

    //成功的构造 返回的是请假审批列表
    public MissionExecution(MissionStatusEnum missionStatusEnum,List<GdMission> gdMissions ) {
        this.code = missionStatusEnum.getCode();
        this.msg = missionStatusEnum.getMsg();
        this.gdMissions = gdMissions;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public GdMission getGdMission() {
        return gdMission;
    }

    public void setGdMission(GdMission gdMission) {
        this.gdMission = gdMission;
    }

    public List<GdMission> getGdMissions() {
        return gdMissions;
    }

    public void setGdMissions(List<GdMission> gdMissions) {
        this.gdMissions = gdMissions;
    }
}
