package com.khwy158.gddemo.controller;

import com.khwy158.gddemo.dto.MissionExecution;
import com.khwy158.gddemo.service.MissionService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.gd.GdMission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MissionController {

    @Autowired
    private MissionService ms;
    /**
     * 添加代办任务
     * @param gdMission
     * @return
     */
    @RequestMapping("/mission/add")
    public ResultDtoUtile addMission(@RequestBody GdMission gdMission){
        MissionExecution missionExecution = ms.addMission(gdMission);
        //必要信息为空 创建失败 空信息
        if (missionExecution.getCode()==1216 ||missionExecution.getCode()==1217 ||missionExecution.getCode()==1218){
            return new ResultDtoUtile(missionExecution.getCode(),missionExecution.getMsg());
        }
        return new ResultDtoUtile(missionExecution.getCode(),missionExecution.getMsg(),missionExecution.getGdMission());
    }

    /**
     * 根据id来删除待办任务
     * @param id
     * @return
     */
    @RequestMapping("mission/delete")
    public ResultDtoUtile deleteMission(@RequestParam(value = "id") Integer id){
        //删除待办任务
        MissionExecution missionExecution = ms.deleteMission(id);
        return new ResultDtoUtile(missionExecution.getCode(),missionExecution.getMsg());
    }

    /**
     * 未完成的待办任务 更新未完成的原因
     * @param gdMission
     * @return
     */
    @RequestMapping("/mission/nocomplete")
    public ResultDtoUtile missionNoComplete(@RequestBody GdMission gdMission){
        //添加待办任务未完成原因
        MissionExecution missionExecution = ms.missionNoComplete(gdMission);
        return new ResultDtoUtile(missionExecution.getCode(),missionExecution.getMsg());
    }

    /**
     * 任务完成时修改待办任务的状态和填写待办任务的完成进度
     * @param gdMission
     * @return
     */
    @RequestMapping("/mission/complete")
    public ResultDtoUtile missionComplete(@RequestBody GdMission gdMission){
        //添加待办任务未完成原因
        MissionExecution missionExecution = ms.missionComplete(gdMission);
        return new ResultDtoUtile(missionExecution.getCode(),missionExecution.getMsg());
    }


}
