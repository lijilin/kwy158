package com.khwy158.gddemo.controller;

import com.khwy158.gddemo.dto.LeafExecution;
import com.khwy158.gddemo.service.LeafService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.gd.GdLeaf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LeafController {
    @Autowired
    private LeafService ls;

    @RequestMapping("/leaf/add")
    public ResultDtoUtile addLeaf(@RequestBody GdLeaf gdLeaf){
        LeafExecution leafExecution = ls.addLeaf(gdLeaf);
        //添加失败
        if(leafExecution.getCode()==1201){
            return new ResultDtoUtile(leafExecution.getCode(),leafExecution.getMsg());
        }
        if(leafExecution.getCode()==1202){
            return new ResultDtoUtile(leafExecution.getCode(),leafExecution.getMsg());
        }
        if(leafExecution.getCode()==1203){
            return new ResultDtoUtile(leafExecution.getCode(),leafExecution.getMsg());
        }

        return new ResultDtoUtile(leafExecution.getCode(),leafExecution.getMsg(),leafExecution.getGdLeaf());


    }
}
