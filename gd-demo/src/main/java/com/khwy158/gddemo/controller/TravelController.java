package com.khwy158.gddemo.controller;

import com.khwy158.gddemo.dto.TravelExecution;
import com.khwy158.gddemo.service.TravelService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.gd.GdTravel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class TravelController {

    @Autowired
    private TravelService ts;

    /**
     * 添加出差审批
     * @return
     */
    @RequestMapping("/travel/add")
    public ResultDtoUtile addTravel(@RequestBody GdTravel gdTravel){
        TravelExecution travelExecution = ts.addTravel(gdTravel);
        //创建失败
        if (travelExecution.getCode()==1205){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }
        //空的信息
        if (travelExecution.getCode()==1206){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }
        //必要的信息为空
        if (travelExecution.getCode()==1207){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }
        return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg(),travelExecution.getGdTravel());
    }

    /**
     * 出差审批的审核
     * @param gdTravel
     * @return
     */
    @RequestMapping("/travel/change")
    public ResultDtoUtile changeTravelStatus(@RequestBody GdTravel gdTravel){
        TravelExecution travelExecution = ts.changeTravelStatus(gdTravel);
        //map是否为空
        if (travelExecution.getCode()==1206){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }
        //必要信息是否为空
        if (travelExecution.getCode()==1207){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }
        //更新失败
        if (travelExecution.getCode()==1208){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }
        //否则就是更新成功
        return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());

    }
    /**
     * 根据id来进行对travel的批量删除
     * @param ids
     * @return
     */
    @RequestMapping("/travel/delete")
    public ResultDtoUtile deleteTravel(@RequestBody List<Integer> ids){
        TravelExecution travelExecution = ts.deleteTravel(ids);
        //判断ids是否为空
        if (travelExecution.getCode()==1206){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }
        //删除失败
        if (travelExecution.getCode()==1209){
            return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
        }

        return new ResultDtoUtile(travelExecution.getCode(),travelExecution.getMsg());
    }


}
