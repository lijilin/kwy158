package com.khwy158.gddemo.controller;

import com.github.pagehelper.PageInfo;
import com.khwy158.gddemo.dao.GdWorklistMapper;
import com.khwy158.gddemo.dto.WorkListExecution;
import com.khwy158.gddemo.service.WorkListService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.dto.gd.JsonResult;
import com.khwy158.modeldemo.pojo.gd.GdWorklist;
import com.khwy158.modeldemo.pojo.ljl.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WorkListController {
    @Autowired
    private WorkListService ws;
    @Autowired
    @SuppressWarnings("all")
    private GdWorklistMapper gdwm;
    /**
     * 发起工单
     * @param gdWorklist
     * @return
     */
    @RequestMapping("WorkList/add")
    public ResultDtoUtile addWorkList(@RequestBody GdWorklist gdWorklist){
        WorkListExecution workListExecution = ws.addWorkList(gdWorklist);
        if (workListExecution.getCode()==1210){
            return new ResultDtoUtile(workListExecution.getCode(),workListExecution.getMsg());
        }
        if (workListExecution.getCode()==1211){
            return new ResultDtoUtile(workListExecution.getCode(),workListExecution.getMsg());
        }
        if (workListExecution.getCode()==1212){
            return new ResultDtoUtile(workListExecution.getCode(),workListExecution.getMsg());
        }
        return new ResultDtoUtile(workListExecution.getCode(),workListExecution.getMsg(),workListExecution.getGdWorklist());
    }

    /**
     * 更新工单的状态
     * @param gdWorklist
     * @return
     */
    @RequestMapping("WorkList/change")
    public ResultDtoUtile changeWorkListStatus(@RequestBody GdWorklist gdWorklist){
        WorkListExecution workListExecution = ws.changeWorkListStatus(gdWorklist);
        if (workListExecution.getCode()==1211|| workListExecution.getCode()==1212 ||workListExecution.getCode()==1213){
            return new ResultDtoUtile(workListExecution.getCode(),workListExecution.getMsg());
        }
        return new ResultDtoUtile(workListExecution.getCode(),workListExecution.getMsg());

    }

    /**
     * 批量删除工单
     * @param ids
     * @return
     */
    @RequestMapping("/workList/delete")
    public ResultDtoUtile deleteTravel(@RequestBody List<Integer> ids) {
        WorkListExecution workListExecution = ws.deleteWorkList(ids);
        return new ResultDtoUtile(workListExecution.getCode(),workListExecution.getMsg());
    }

    /**
     * 查询登录的用户来查询工单
     * @param page
     * @return
     */
    @RequestMapping("/workList/all")
   public JsonResult selectEmplWorkList(@RequestBody Page page){
        //从cookie中获取到登录的用户的empl_id
        ////////////未完成
        WorkListExecution workListExecution = ws.selectAll(1, page);
        //得到所有的工单
        PageInfo pageInfo=new PageInfo(workListExecution.getGdWorklists());
        System.out.println(pageInfo);
        //获取到工单的总条数
        long total = pageInfo.getTotal();

        return new JsonResult(workListExecution.getCode(),workListExecution.getMsg(),total,workListExecution.getGdWorklists());
    }

}
