package com.khwy158.gddemo.dao;


import com.khwy158.modeldemo.pojo.gd.GdMission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GdMissionMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table gd_mission
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table gd_mission
     *
     * @mbg.generated
     */
    int insert(GdMission record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table gd_mission
     *
     * @mbg.generated
     */
    int insertSelective(GdMission record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table gd_mission
     *
     * @mbg.generated
     */
    GdMission selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table gd_mission
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(GdMission record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table gd_mission
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(GdMission record);

    /**
     * 任务未完成
     * @param gdMission
     * @return
     */
    int MissionNoComplete(GdMission gdMission);

    /**
     * 任务完成 修改状态和添加完成进度
     * @param gdMission
     * @return
     */
    int MissionComplete(GdMission gdMission);


}