package com.khwy158.gddemo.enums;

public enum LeafStatusEnum {
    /**
     * 状态
     */
    CREATE_SUCCESS(200,"创建成功"),
    GET_INFO_SUCCESS(201,"获取信息成功!"),
    UPDATE_SUCCESS(202,"更新成功!"),
    CREATE_FAILED(1201,"创建失败"),
    NULL_INFO(1202,"空的信息"),
    NULL_REQUIRED_INFO(1203,"必要的信息为空"),
    UPDATE_FAILED(1204,"更新失败!");

    private Integer code;
    private String msg;


    LeafStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
