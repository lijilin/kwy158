package com.khwy158.gddemo.service.impl;

import com.khwy158.gddemo.dao.GdMissionMapper;
import com.khwy158.gddemo.dto.MissionExecution;
import com.khwy158.gddemo.enums.MissionStatusEnum;

import com.khwy158.gddemo.service.MissionService;
import com.khwy158.modeldemo.pojo.gd.GdMission;
import com.khwy158.modeldemo.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MissionServiceImpl implements MissionService {
    @Autowired
    @SuppressWarnings("all")
    private GdMissionMapper gdmm;
    //添加待办任务
    @Override
    public MissionExecution addMission(GdMission gdMission) {
        //1217判断传入的对象是否为空
        if (gdMission==null){
            return new MissionExecution(MissionStatusEnum.NULL_INFO);
        }
        //任务类型1：客户跟进任务2：普通任务3：客户投诉处理
        //根据不同的任务类型有不同的判断
        //如果是1：客户跟进任务的话
        if (gdMission.getMissionType()==1){
            //判断任务相关联的客户、时间、任务内容、执行人
            if (gdMission.getMissionCustomerId()==null || gdMission.getMissionBeginTime() ==null ||
                    gdMission.getMissionContext()==null || gdMission.getMissionStaffId()==null){
              //1218 判断必要的信息是否为空
                return new MissionExecution(MissionStatusEnum.NULL_REQUIRED_INFO);
            }
        }
        //如果是2：普通任务
        if (gdMission.getMissionType()==2){
            //判断执行时间、内容、执行人
            if (gdMission.getMissionBeginTime()==null || gdMission.getMissionContext()==null ||
                    gdMission.getMissionStaffId()==null){
                //1218 判断必要的信息是否为空
                return new MissionExecution(MissionStatusEnum.NULL_REQUIRED_INFO);
            }
        }

        //自己添加余下的字段
        //创建时间
        String timeStr = TimeUtil.getTimeStr();
        gdMission.setMissionCreateTime(timeStr);

        // 任务状态 1：未完成2：已完成  自动写为未完成
        gdMission.setMissionStatus(1);

        //存入数据库
        int i = gdmm.insertSelective(gdMission);
        //1216 i<=0即是添加失败
        if (i<=0){
            return new MissionExecution(MissionStatusEnum.CREATE_FAILED);
        }
        //200 添加成功
        return new MissionExecution(MissionStatusEnum.CREATE_SUCCESS,gdMission);
    }

    //删除待办任务
    @Override
    public MissionExecution deleteMission(Integer id) {
        //1217 判断id是否为空
        if (id==null){
            return new MissionExecution(MissionStatusEnum.NULL_INFO);
        }
        //如果不为空 则删除
        int i = gdmm.deleteByPrimaryKey(id);

        //1210 删除失败
        if (i<=0){
            return new MissionExecution(MissionStatusEnum.DELETE_FAILED);
        }
        //203 删除成功
        return new MissionExecution(MissionStatusEnum.DELETE_SUCCESS);
    }

    //待办任务未完成时 要添加未完成的原因
    @Override
    public MissionExecution missionNoComplete(GdMission gdMission) {
       //1217 判断传进来的对象是否为空
        if (gdMission==null){
            return new MissionExecution(MissionStatusEnum.NULL_INFO);
        }
        //1218 判断未完成的任务的id和未完成原因是否为空
        if (gdMission.getId()==null || gdMission.getMissionNocompleteReason()==null){
            return new MissionExecution(MissionStatusEnum.NULL_REQUIRED_INFO);
        }
        //更新
        int i = gdmm.MissionNoComplete(gdMission);
        //1219 更新失败
        if (i<=0){
            return new MissionExecution(MissionStatusEnum.UPDATE_FAILED);
        }
        //202 更新成功
        return new MissionExecution(MissionStatusEnum.UPDATE_SUCCESS);
    }

    /**
     * 任务完成 修改待办任务的状态和添加任务的完成进度
     * @param gdMission
     * @return
     */
    @Override
    public MissionExecution missionComplete(GdMission gdMission) {
        //1217 判断传进来的对象是否为空
        if (gdMission==null){
            return new MissionExecution(MissionStatusEnum.NULL_INFO);
        }
        //1218 判断未完成的任务的id和未完成原因是否为空
        if (gdMission.getId()==null || gdMission.getMissionCompletePro()==null){
            return new MissionExecution(MissionStatusEnum.NULL_REQUIRED_INFO);
        }
        int i = gdmm.MissionComplete(gdMission);
        //1219 更新失败
        if (i<=0){
            return new MissionExecution(MissionStatusEnum.UPDATE_FAILED);
        }
        //202 更新成功
        return new MissionExecution(MissionStatusEnum.UPDATE_SUCCESS);

    }
}
