package com.khwy158.gddemo.service;

import com.khwy158.gddemo.dto.MissionExecution;
import com.khwy158.modeldemo.pojo.gd.GdMission;

public interface MissionService {
    /**
     * 添加待办任务
     * @param gdMission
     * @return
     */
    MissionExecution addMission(GdMission gdMission);

    /**
     * 根据id来删除待办任务
     * @param id
     * @return
     */
    MissionExecution deleteMission(Integer id);

    /**
     * 待办任务未完成时要添加未完成原因
     * @param gdMission
     * @return
     */
    MissionExecution missionNoComplete(GdMission gdMission);

    /**
     * 待办任务完成 修改待办任务的状态和填写完成进度
     * @param gdMission
     * @return
     */
    MissionExecution missionComplete(GdMission gdMission);
}
