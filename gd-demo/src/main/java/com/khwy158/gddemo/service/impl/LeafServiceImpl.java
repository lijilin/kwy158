package com.khwy158.gddemo.service.impl;


import com.khwy158.gddemo.dao.GdLeafEmplMapper;
import com.khwy158.gddemo.dao.GdLeafFileMapper;
import com.khwy158.gddemo.dao.GdLeafMapper;
import com.khwy158.gddemo.dto.LeafExecution;
import com.khwy158.gddemo.enums.LeafStatusEnum;
import com.khwy158.gddemo.service.LeafService;
import com.khwy158.modeldemo.pojo.gd.GdLeaf;
import com.khwy158.modeldemo.pojo.gd.GdLeafEmpl;
import com.khwy158.modeldemo.pojo.gd.GdLeafFile;
import com.khwy158.modeldemo.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeafServiceImpl implements LeafService {

    @Autowired
    @SuppressWarnings("all")
    private GdLeafMapper gdlm;

    @Autowired
    @SuppressWarnings("all")
    private GdLeafFileMapper gdfm;

    @Autowired
    @SuppressWarnings("all")
    private GdLeafEmplMapper gdlem;

    /**
     * 添加请假审批
     * @return
     */
    @Override
    public LeafExecution addLeaf(GdLeaf gdLeaf) {
        //判断传入的请假审批对象是否为空
        if (gdLeaf==null){
           return new LeafExecution(LeafStatusEnum.NULL_INFO);
        }
        Integer leafEmplId = gdLeaf.getLeafEmplId();
        //判断请假审批中的所填信息是否为空 请假类型，请假开始时间，请假结束时间，请假理由，请假审批人
        if(gdLeaf.getLeafType()==null|| gdLeaf.getLeafBeginTime()==null || gdLeaf.getLeafEndTime()==null ||
                gdLeaf.getLeafReason()==null || leafEmplId==null){
            return new LeafExecution(LeafStatusEnum.NULL_REQUIRED_INFO);
        }
        //添加余下的字段
        //添加创建时间
        String timeStr = TimeUtil.getTimeStr();
        gdLeaf.setLeafCreateTime(timeStr);


        // 未完成的*************
        //       /** 请假申请创建人*/
        //        private Integer leafCreatemanId;
        //假装有一个session 放入创建人
        //从session中获取到登录者
        gdLeaf.setLeafCreatemanId(1);
        //添加请假审批的状态 1：未审核 2：审核通过 3：审核未通过
        gdLeaf.setLeafStatus(1);
        //保存在数据库
        int i = gdlm.insertSelective(gdLeaf);
        //保存的影响行数小于等于0 即保存失败
        if (i<=0){
            return new LeafExecution(LeafStatusEnum.CREATE_FAILED);
        }

        //在保存leaf表的同时要保存leaf_file ,leaf_empl关联表
        /**
         * leaf_file关联表
         */
        Integer leafFileId = gdLeaf.getLeafFileId();
        //插入leaf_file的时候需要存入leaf_id file_id
        //把总条数来作为leaf_id
//        int leafCount = gdlm.selectCount();
        int leafId = gdlm.selectLeafId();
        GdLeafFile gdLeafFile=new GdLeafFile();
        gdLeafFile.setLeafId(leafId);
        gdLeafFile.setFileId(leafFileId);
        //存入出差申请和file的关联表
        gdfm.insertSelective(gdLeafFile);

        /**
         * leaf_empl关联表
         */
        GdLeafEmpl gdLeafEmpl=new GdLeafEmpl();
        gdLeafEmpl.setLeafId(leafId);
        gdLeafEmpl.setLeafStaffId(leafEmplId);
        gdlem.insertSelective(gdLeafEmpl);
        return new LeafExecution(LeafStatusEnum.CREATE_SUCCESS,gdLeaf);
    }
}
