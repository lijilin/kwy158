package com.khwy158.gddemo.service;

import com.khwy158.gddemo.dto.LeafExecution;
import com.khwy158.modeldemo.pojo.gd.GdLeaf;

public interface LeafService {
//    添加请假审批
    LeafExecution addLeaf(GdLeaf gdLeaf);
}
