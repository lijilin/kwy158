package com.khwy158.gddemo.service;

import com.khwy158.gddemo.dto.WorkListExecution;
import com.khwy158.modeldemo.pojo.gd.GdWorklist;
import com.khwy158.modeldemo.pojo.ljl.Page;

import java.util.List;
import java.util.Map;

public interface WorkListService {
    /**
     * 发起工单
     * @param gdWorklist
     * @return
     */
    WorkListExecution addWorkList(GdWorklist gdWorklist);

    /**
     * 更改工单的状态和添加评论
     * @param gdWorklist
     * @return
     */
    WorkListExecution changeWorkListStatus(GdWorklist gdWorklist);

    /**
     * 根据id来批量删除工单
     * @param ids
     * @return
     */
    WorkListExecution deleteWorkList(List<Integer> ids);

    /**
     * 查询所有的工单
     * @return
     */
    WorkListExecution selectAll(Integer id, Page page );


}
