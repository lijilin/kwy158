package com.khwy158.gddemo.service.impl;

import com.github.pagehelper.PageHelper;
import com.khwy158.gddemo.dao.GdWorklistMapper;
import com.khwy158.gddemo.dto.WorkListExecution;
import com.khwy158.gddemo.enums.WorkListStatusEnum;
import com.khwy158.gddemo.service.WorkListService;
import com.khwy158.modeldemo.pojo.gd.GdWorklist;
import com.khwy158.modeldemo.pojo.ljl.Page;
import com.khwy158.modeldemo.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WorkListServiceImpl implements WorkListService {

    @Autowired
    @SuppressWarnings("all")
    private GdWorklistMapper gdwlm;
    //发起工单
    @Override
    public WorkListExecution addWorkList(GdWorklist gdWorklist) {
        //1211判断是否为空
        if (gdWorklist==null){
            return new WorkListExecution(WorkListStatusEnum.NULL_INFO);
        }
        //1212判断必要的信息是否为空
        if (gdWorklist.getWorklistCustomerId()==null || gdWorklist.getWorklistContext()==null || gdWorklist.getWorklistStaffId()==null ||
                gdWorklist.getWorklistType()==null){
            return new WorkListExecution(WorkListStatusEnum.NULL_REQUIRED_INFO);
        }
        //设置余下的字段
        // 工单状态1：等待处理 2：已处理 3：未处理
        gdWorklist.setWorklistStatus(1);
        /////////未完成 创建人是从session中获取的
        //创建人
        gdWorklist.setWorklistCreatemanId(1);
        //创建时间
        String timeStr = TimeUtil.getTimeStr();
        gdWorklist.setWorklistCreateTime(timeStr);
        //保存到数据库
        int i = gdwlm.insertSelective(gdWorklist);
       //1210创建失败
        if (i<=0){
            return new WorkListExecution(WorkListStatusEnum.CREATE_FAILED);
        }
        //200 添加成功
        return new WorkListExecution(WorkListStatusEnum.CREATE_SUCCESS,gdWorklist);
    }
    //更改工单的状态和内容
    @Override
    public WorkListExecution changeWorkListStatus(GdWorklist gdWorklist) {
        //1211判断传进来的对象是否为空
        if (gdWorklist==null){
            return new WorkListExecution(WorkListStatusEnum.NULL_INFO);
        }
        //1212 判断必要的信息之id 和状态
        if (gdWorklist.getId()==null ||gdWorklist.getWorklistStatus()==null){
            return new WorkListExecution(WorkListStatusEnum.NULL_REQUIRED_INFO);
        }
        //更改状态
        int i = gdwlm.changeWorkListStatus(gdWorklist);

        //1213如果更改失败
        if (i<=0){
            return new WorkListExecution(WorkListStatusEnum.UPDATE_FAILED);
        }
        //202更新成功
        return new WorkListExecution(WorkListStatusEnum.UPDATE_SUCCESS);
    }
//根据id来批量删除工单
    @Override
    public WorkListExecution deleteWorkList(List<Integer> ids) {
        //1212判断返回的id集合是否为空
        if (ids.isEmpty()){
            return new WorkListExecution(WorkListStatusEnum.NULL_INFO);
        }
        int i = gdwlm.deleteWorkList(ids);
       //1214删除失败
        if (i<=0){
            return new WorkListExecution(WorkListStatusEnum.DELETE_FAILED);
        }
        //203删除成功
        return new WorkListExecution(WorkListStatusEnum.DELETE_SUCCESS);
    }
//根据登录用户的id来查询所有的工单
    @Override
    public WorkListExecution selectAll(Integer id, Page page) {
        PageHelper.startPage(page.getPage(),page.getPageSzie());

        //查询到所有的
        List<GdWorklist> gdWorklists = gdwlm.selectEmplWorkLIst(id);
       //1211 空的信息
        if (gdWorklists==null){
            return new WorkListExecution(WorkListStatusEnum.NULL_INFO);
        }
        //0 获取成功
        return new WorkListExecution(WorkListStatusEnum.GET_SUCCESS,gdWorklists);
    }


}
