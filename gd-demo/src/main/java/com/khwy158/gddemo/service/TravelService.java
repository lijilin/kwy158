package com.khwy158.gddemo.service;

import com.khwy158.gddemo.dto.TravelExecution;
import com.khwy158.modeldemo.pojo.gd.GdTravel;

import java.util.List;
import java.util.Map;

public interface TravelService {

    /**
     *
     * @param gdTravel
     * @return
     * 添加出差审批
     */
    TravelExecution addTravel(GdTravel gdTravel);
    /**
     * 对出差审批申请进行审批即审核 就是修改出差审批的状态 1：未审核 2：审核通过 3：审核未通过
     *      * 需要别人传入选择的状态和评论 如果有评论  则存入travel表中
     * @param
     * @return
     */
    TravelExecution changeTravelStatus(GdTravel gdTravel);

    /**
     * 根据id来批量删除travel
     * @param ids
     * @return
     */
    TravelExecution deleteTravel(List<Integer> ids);

}
