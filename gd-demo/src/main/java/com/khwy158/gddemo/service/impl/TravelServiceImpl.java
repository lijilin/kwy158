package com.khwy158.gddemo.service.impl;

import com.khwy158.gddemo.dao.GdTravelEmplMapper;
import com.khwy158.gddemo.dao.GdTravelFileMapper;
import com.khwy158.gddemo.dao.GdTravelMapper;
import com.khwy158.gddemo.dto.TravelExecution;
import com.khwy158.gddemo.enums.TravelStatusEnum;
import com.khwy158.gddemo.service.TravelService;
import com.khwy158.modeldemo.pojo.gd.GdTravel;
import com.khwy158.modeldemo.pojo.gd.GdTravelEmpl;
import com.khwy158.modeldemo.pojo.gd.GdTravelFile;
import com.khwy158.modeldemo.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TravelServiceImpl implements TravelService {

    @Autowired
    @SuppressWarnings("all")
    private GdTravelMapper gdtm;
    @Autowired
    @SuppressWarnings("all")
    private GdTravelEmplMapper gdtem;
    @Autowired
    @SuppressWarnings("all")
    private GdTravelFileMapper gdtfm;

    //添加出差审批
    @Override
    public TravelExecution addTravel(GdTravel gdTravel) {
        //判断传进来的是否为空
        if (gdTravel==null){
            return new TravelExecution(TravelStatusEnum.NULL_INFO);
        }
        //判断必要信息是否为空
        Integer travelEmplId = gdTravel.getTravelEmplId();
        if (gdTravel.getTravelBeginTime()==null || gdTravel.getTravelEndTime()==null || gdTravel.getTravelAddress()==null ||
                gdTravel.getTravelReason()==null || gdTravel.getTravelFee()==null|| travelEmplId==null){
            return new TravelExecution(TravelStatusEnum.NULL_REQUIRED_INFO);
        }
        //添加余下的东西
        //创建时间
        String timeStr = TimeUtil.getTimeStr();
        gdTravel.setTravelCreateTime(timeStr);
        /////////////未完成
        //创建人 是从session中获取到的 假装有一个
        gdTravel.setTravelCreatemanId(1);
        //设置出差审批的状态 1：未审核 2：审核通过 3：审核未通过
        gdTravel.setTravelStatus(1);

        //存入gdtravel表
        int i = gdtm.insertSelective(gdTravel);
        //存入失败 影响行数<=0
        if (i<=0){
            return new TravelExecution(TravelStatusEnum.CREATE_FAILED);
        }

        //在存gdtravel表的同时要要存入和travel相关的表
        //travel_file表
        Integer travelFileId = gdTravel.getTravelFileId();
        GdTravelFile gdTravelFile=new GdTravelFile();
        gdTravelFile.setFileId(travelFileId);

        int travelid = gdtm.selectTravelid();
        //把travel的总条数作为travel_file中的travel_id
       // int travelCount = gdtm.selectCount();
        gdTravelFile.setTravelId(travelid);
        gdtfm.insertSelective(gdTravelFile);

        //travel_empl表
        GdTravelEmpl gdTravelEmpl=new GdTravelEmpl();
        gdTravelEmpl.setTravelStaffId(travelEmplId);
        gdTravelEmpl.setTravelId(travelid);
        gdtem.insertSelective(gdTravelEmpl);

        //出差审批增加成功
        return new TravelExecution(TravelStatusEnum.CREATE_SUCCESS,gdTravel);
    }

    //对出差审批进行修改状态和添加评论 status1:未审核 2：审核通过 3：审核未通过
    @Override
    public TravelExecution changeTravelStatus(GdTravel gdTravel) {
        ////////////////未完成
        ////////////////假装传入了一个map
//        travelMap.put("id",1);
//        travelMap.put("travel_status",2);
//        travelMap.put("travel_comment","好气哦");
       //1206判断map是否为空
        if (gdTravel==null){
            return new TravelExecution(TravelStatusEnum.NULL_INFO);
        }
        Integer id = gdTravel.getId();
        //1207判断id是否为空
        if (id==null){
            return new TravelExecution(TravelStatusEnum.NULL_REQUIRED_INFO);
        }
        Integer travelStatus = gdTravel.getTravelStatus();
        if (travelStatus==null){
            return new TravelExecution(TravelStatusEnum.NULL_REQUIRED_INFO);
        }
        int i = gdtm.changeTravelStatus(gdTravel);

        //1208 更新失败
        if (i<=0){
            return new TravelExecution(TravelStatusEnum.UPDATE_FAILED);
        }
//        //添加travel_comment
//        gdtm.addTravelComment(id,travel_comment);
        //202更新成功
        return new TravelExecution(TravelStatusEnum.UPDATE_SUCCESS);
    }

    //根据id来批量删除travel
    @Override
    public TravelExecution deleteTravel(List<Integer> ids) {
        //////未完成
        //////////假装有一个list
       //1206 判断传入的id集合为空
        if (ids.isEmpty()){
            return new TravelExecution(TravelStatusEnum.NULL_INFO);
        }
        int i = gdtm.deleteTravel(ids);
       //1209删除失败
        if (i<=0){
            return new TravelExecution(TravelStatusEnum.DELETE_FAILED);
        }
        //203删除成功
        return new TravelExecution(TravelStatusEnum.delete_success);
    }
}
