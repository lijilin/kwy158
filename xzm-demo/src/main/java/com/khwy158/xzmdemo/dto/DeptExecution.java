package com.khwy158.xzmdemo.dto;

import com.khwy158.modeldemo.pojo.xzm.Dept;
import com.khwy158.xzmdemo.enums.DeptStatusEnum;

import java.util.List;

/**
 * @Author xzm
 * @Date 2018/8/29 10:16
 */
public class DeptExecution {

    private Integer code;

    private String msg;

    private Dept dept;

    private List<Dept> depts;

    /**
     *  失败的构造器
     * @param deptStatusEnum
     */
    public DeptExecution(DeptStatusEnum deptStatusEnum){
        this.code=deptStatusEnum.getCode();
        this.msg=deptStatusEnum.getMsg();
    }

    /**
     *  执行增删改成功的构造器
     * @param deptStatusEnum
     * @param dept
     */
    public DeptExecution(DeptStatusEnum deptStatusEnum , Dept dept){
        this.code=deptStatusEnum.getCode();
        this.msg=deptStatusEnum.getMsg();
        this.dept=dept;
    }

    /**
     *  执行查询成功的构造器
     * @param deptStatusEnum
     * @param depts
     */
    public DeptExecution(DeptStatusEnum deptStatusEnum , List<Dept> depts){
        this.code=deptStatusEnum.getCode();
        this.msg=deptStatusEnum.getMsg();
        this.depts=depts;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    public List<Dept> getDepts() {
        return depts;
    }

    public void setDepts(List<Dept> Depts) {
        this.depts = Depts;
    }

    @Override
    public String toString() {
        return "DeptExecution{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", dept=" + dept +
                ", depts=" + depts +
                '}';
    }
}
