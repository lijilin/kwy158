package com.khwy158.xzmdemo.dto;

import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.xzmdemo.enums.RegisterStatusEnum;

import javax.servlet.http.Cookie;
import java.util.List;

/**
 * @Author xzm
 * @Date 2018/7/23 18:30
 *  用于member相关数据的传输
 */
public class RegisterExecution {

    private Integer code;

    private String msg;

    private Register register;

    private Cookie cookie;

    private List<Register> registers;

    /**
     *  失败的构造器
     * @param registerStatusEnum
     */
    public RegisterExecution(RegisterStatusEnum registerStatusEnum){
        this.code=registerStatusEnum.getCode();
        this.msg=registerStatusEnum.getMsg();
    }

    /**
     *  执行增删改成功的构造器
     * @param registerStatusEnum
     * @param register
     */
    public RegisterExecution(RegisterStatusEnum registerStatusEnum , Register register){
        this.code=registerStatusEnum.getCode();
        this.msg=registerStatusEnum.getMsg();
        this.register=register;
    }

    /**
     *  执行增删改成功的构造器
     * @param registerStatusEnum
     * @param cookie
     */
    public RegisterExecution(RegisterStatusEnum registerStatusEnum , Cookie cookie){
        this.code=registerStatusEnum.getCode();
        this.msg=registerStatusEnum.getMsg();
        this.cookie=cookie;
    }

    /**
     *  执行查询成功的构造器
     * @param registerStatusEnum
     * @param registers
     */
    public RegisterExecution(RegisterStatusEnum registerStatusEnum , List<Register> registers){
        this.code=registerStatusEnum.getCode();
        this.msg=registerStatusEnum.getMsg();
        this.registers=registers;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    public List<Register> getRegisters() {
        return registers;
    }

    public void setRegisters(List<Register> registers) {
        this.registers = registers;
    }

    public Cookie getCookie() {
        return cookie;
    }

    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }

    @Override
    public String toString() {
        return "RegisterExecution{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", register=" + register +
                ", registers=" + registers +
                '}';
    }
}
