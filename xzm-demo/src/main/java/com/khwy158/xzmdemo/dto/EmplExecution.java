package com.khwy158.xzmdemo.dto;

import com.khwy158.modeldemo.pojo.xzm.Empl;
import com.khwy158.xzmdemo.enums.EmplStatusEnum;

import java.util.List;

/**
 * @Author xzm
 * @Date 2018/8/29 11:28
 */
public class EmplExecution {

    private Integer code;

    private String msg;

    private Empl empl;

    private List<Empl> empls;

    /**
     *  失败的构造器
     * @param emplStatusEnum
     */
    public EmplExecution(EmplStatusEnum emplStatusEnum){
        this.code=emplStatusEnum.getCode();
        this.msg=emplStatusEnum.getMsg();
    }

    /**
     *  执行增删改成功的构造器
     * @param emplStatusEnum
     * @param empl
     */
    public EmplExecution(EmplStatusEnum emplStatusEnum , Empl empl){
        this.code=emplStatusEnum.getCode();
        this.msg=emplStatusEnum.getMsg();
        this.empl=empl;
    }

    /**
     *  执行查询成功的构造器
     * @param emplStatusEnum
     * @param empls
     */
    public EmplExecution(EmplStatusEnum emplStatusEnum , List<Empl> empls){
        this.code=emplStatusEnum.getCode();
        this.msg=emplStatusEnum.getMsg();
        this.empls=empls;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Empl getEmpl() {
        return empl;
    }

    public void setEmpl(Empl empl) {
        this.empl = empl;
    }

    public List<Empl> getEmpls() {
        return empls;
    }

    public void setEmpls(List<Empl> empls) {
        this.empls = empls;
    }

    @Override
    public String toString() {
        return "EmplExecution{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", empl=" + empl +
                ", empls=" + empls +
                '}';
    }
}
