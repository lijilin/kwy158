package com.khwy158.xzmdemo.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author xzm
 * @Date 2018/8/2 19:49
 */
@Component
public class RedisUtils<T>{

    @Autowired
    private StringRedisTemplate redis;

    /**
     * 设置值
     * @param key
     * @param value
     * @return
     */
    public Boolean set(String key, String value) {
        try {
            redis.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     *  获取值
     * @param key
     * @return
     */
    public String get(String key) {
        return redis.opsForValue().get(key);
    }

    /**
     * 设置过时时间
     * @param key
     * @param time
     * @return
     */
    public Boolean expire(String key, long time) {
        try {
            if (time > 0) {

                redis.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 功能描述：根据key 获取过期时间
     * @param key
     * @return
     */
    public Long getExpire(String key) {
        return redis.getExpire(key, TimeUnit.SECONDS);
    }


    /**
     * 递增
     * @param key 键
     * @return
     */
    public Long incr(String key, long delta) {
        return redis.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     * @param key   键
     * @param delta 要减少几
     * @return
     */
    public Long decr(String key, long delta) {
        return redis.opsForValue().increment(key, -delta);
    }


    /**
     * 存放一个map
     * @param key
     * @param map
     * @return
     */
    public Boolean setMap(String key, Map<String, Object> map) {
        redis.opsForHash().putAll(key, map);
        return true;
    }


    /**
     * 获取map
     * @param key
     * @return
     */
    public Map<Object,Object> getMap(String key){
        return redis.opsForHash().entries(key);
    }


    /**
     * 加入队列
     * @param key
     * @param value
     * @return
     */
    public Boolean  setList(String key, String value ){
        redis.opsForList().leftPush(key,value);
        return true;
    }


    /**
     * 出列
     * @param key
     * @return
     */
    public String outList(String key){
        return redis.opsForList().rightPop(key);
    }

    /**
     *  存list
     * @param depts
     */
    public boolean setList(String key,List<T> depts){
        try {
            String str=JSON.toJSON(depts).toString();
            redis.opsForValue().set(key,str);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     *  取出 list
     * @param clazz
     */
    public List<T> getList(String key,Class<T> clazz){
        // 从 redis里面获取 json字符串
        String json = redis.opsForValue().get(key);
        // 声明一个list
        List<T> list=new ArrayList<>();
        // 把字符串解析成list
        list = JSONObject.parseArray(json,clazz);
        return list;
    }
}
