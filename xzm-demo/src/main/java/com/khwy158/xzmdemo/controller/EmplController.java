package com.khwy158.xzmdemo.controller;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.xzm.Empl;
import com.khwy158.xzmdemo.dto.EmplExecution;
import com.khwy158.xzmdemo.service.EmplService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author xzm
 * @Date 2018/8/29 11:40
 */
@Controller
public class EmplController {

    @Autowired
    @SuppressWarnings("all")
    private EmplService es;

    @RequestMapping("/empl/add")
    @ResponseBody
    public ResultDtoUtile addEmpl(@RequestBody Empl empl){
        EmplExecution ee = es.addEmpl(empl);
        return new ResultDtoUtile(ee.getCode(),ee.getMsg(),ee.getEmpl());
    }

    @RequestMapping("/empl/delete/{id}")
    @ResponseBody
    public ResultDtoUtile deleteEmpl(@PathVariable Integer id){
        EmplExecution ee = es.deleteEmpl(id);
        return new ResultDtoUtile(ee.getCode(),ee.getMsg());
    }

    @RequestMapping("/empl/edit")
    @ResponseBody
    public ResultDtoUtile editEmpl(@RequestBody Empl empl){
        EmplExecution ee = es.modifyEmpl(empl);
        return new ResultDtoUtile(ee.getCode(),ee.getMsg(),ee.getEmpl());
    }

    @RequestMapping("/empl/select/{id}")
    @ResponseBody
    public ResultDtoUtile selectEmpl(@PathVariable Integer id){
        EmplExecution ee = es.selectEmpl(id);
        return new ResultDtoUtile(ee.getCode(),ee.getMsg(),ee.getEmpl());
    }

    @RequestMapping("/empl/selectAll")
    @ResponseBody
    public ResultDtoUtile selectAllEmpls(){
        EmplExecution ee = es.selectAllEmpl();
        return new ResultDtoUtile(ee.getCode(),ee.getMsg(),ee.getEmpls());
    }

}
