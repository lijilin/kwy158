package com.khwy158.xzmdemo.controller;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.xzm.Empl;
import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.modeldemo.utils.JwtUtil;
import com.khwy158.xzmdemo.dto.EmplExecution;
import com.khwy158.xzmdemo.dto.RegisterExecution;
import com.khwy158.xzmdemo.service.LoginService;
import com.khwy158.xzmdemo.utils.RedisUtils;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author xzm
 * @Date 2018/8/28 21:26
 */
@Controller
public class LoginController {

    @Autowired
    @SuppressWarnings("all")
    private LoginService ls;

    @Autowired
    private RedisUtils redisUtils;

    /**
     *  判断用户登录类型
     * @param username
     * @return
     */
    @RequestMapping("/login/type")
    @ResponseBody
    public Integer loginType(@RequestBody String username){
        return ls.loginType(username);
    }

    /**
     *  管理员登录
     * @param register
     * @return
     */
    @RequestMapping("/admin/login")
    @ResponseBody
    public ResultDtoUtile adminLogin(@RequestBody Register register, HttpServletRequest request){
        RegisterExecution re = ls.login(register);
        return new ResultDtoUtile(re.getCode(),re.getMsg(),re.getCookie());
    }

    @RequestMapping("/empl/login")
    @ResponseBody
    public ResultDtoUtile emplLogin(@RequestBody Empl empl , HttpServletResponse response){

//        EmplExecution ee = ls.login(empl, response);
//        // 准备一个key值
//        String key="login:empl:token";
//        // 获取token
//        String token = redisUtils.get(key);
//        // 解析jwt
//        Empl reg = JwtUtil.unsign(token, Empl.class);
//        System.out.println(reg);
//
//        return new ResultDtoUtile(ee.getCode(),ee.getMsg(),ee.getEmpl());
        return null;
    }

}
