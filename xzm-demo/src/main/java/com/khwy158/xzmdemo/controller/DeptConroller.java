package com.khwy158.xzmdemo.controller;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.xzm.Dept;
import com.khwy158.xzmdemo.dto.DeptExecution;
import com.khwy158.xzmdemo.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xzm
 * @Date 2018/8/29 10:51
 */
@Controller
public class DeptConroller {

    @Autowired
    @SuppressWarnings("all")
    private DeptService ds;

    @RequestMapping("/dept/add")
    @ResponseBody
    public ResultDtoUtile addDept(@RequestBody Dept dept){
        DeptExecution de = ds.addDept(dept);
        return new ResultDtoUtile(de.getCode(),de.getMsg(),de.getDept());
    }

    @RequestMapping("/dept/delete/{id}")
    @ResponseBody
    public ResultDtoUtile deleteDept(@PathVariable Integer id){
        DeptExecution de = ds.deleteDept(id);
        return new ResultDtoUtile(de.getCode(),de.getMsg());
    }

    @RequestMapping("/dept/edit")
    @ResponseBody
    public ResultDtoUtile editDept(@RequestBody Dept dept){
        DeptExecution de = ds.modifyDept(dept);
        return new ResultDtoUtile(de.getCode(),de.getMsg(),de.getDept());
    }

    @RequestMapping("/dept/select/{id}")
    @ResponseBody
    public ResultDtoUtile selectDept(@PathVariable Integer id){
        DeptExecution de = ds.selectDept(id);
        return new ResultDtoUtile(de.getCode(),de.getMsg(),de.getDept());
    }

    @RequestMapping("/dept/selectAll")
    @ResponseBody
    public ResultDtoUtile selectAllDepts(){
        DeptExecution de = ds.selectAllDept();
        return new ResultDtoUtile(de.getCode(),de.getMsg(),de.getDepts());
    }


}
