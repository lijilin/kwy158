package com.khwy158.xzmdemo.controller;

import com.khwy158.modeldemo.dto.ResultDtoUtile;
import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.xzmdemo.dto.RegisterExecution;
import com.khwy158.xzmdemo.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xzm
 * @Date 2018/8/28 14:23
 */
@Controller
public class RegisterController {

    @Autowired
    @SuppressWarnings("all")
    private RegisterService rs;

    /**
     *  检查用户名是否被注册
     * @param username
     * @return
     */
    @RequestMapping("/check/username")
    @ResponseBody
    public ResultDtoUtile checkUser(@RequestBody String username){
        RegisterExecution re = rs.checkUser(username);
        return new ResultDtoUtile(re.getCode(),re.getMsg());
    }

    /**
     *  企业注册
     * @param register
     * @return
     */
    @RequestMapping("/register/do")
    @ResponseBody
    public ResultDtoUtile register(@RequestBody Register register){
        RegisterExecution re = rs.register(register);
        return new ResultDtoUtile(re.getCode(),re.getMsg(),re.getRegister());
    }


}
