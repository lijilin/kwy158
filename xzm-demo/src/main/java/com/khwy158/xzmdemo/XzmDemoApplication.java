package com.khwy158.xzmdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.khwy158.xzmdemo.dao")
public class XzmDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(XzmDemoApplication.class, args);
    }
}
