package com.khwy158.xzmdemo.service;

import com.khwy158.modeldemo.pojo.xzm.Empl;
import com.khwy158.xzmdemo.dto.EmplExecution;

/**
 * @Author xzm
 * @Date 2018/8/29 11:33
 */
public interface EmplService {

    /**
     *  添加员工
     * @param empl
     * @return
     */
    EmplExecution addEmpl(Empl empl);

    /**
     *  根据id 删除员工
     * @param id
     * @return
     */
    EmplExecution deleteEmpl(Integer id);

    /**
     *  修改员工信息
     * @param empl
     * @return
     */
    EmplExecution modifyEmpl(Empl empl);

    /**
     *  根据id查询员工
     * @param id
     * @return
     */
    EmplExecution selectEmpl(Integer id);

    /**
     *  查询所有员工
     * @return
     */
    EmplExecution selectAllEmpl();
}
