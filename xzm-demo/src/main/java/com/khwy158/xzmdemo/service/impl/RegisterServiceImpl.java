package com.khwy158.xzmdemo.service.impl;

import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.modeldemo.utils.MD5Util;
import com.khwy158.modeldemo.utils.TimeUtil;
import com.khwy158.xzmdemo.dao.RegisterMapper;
import com.khwy158.xzmdemo.dto.RegisterExecution;
import com.khwy158.xzmdemo.enums.RegisterStatusEnum;
import com.khwy158.xzmdemo.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author xzm
 * @Date 2018/8/28 16:38
 */
@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    @SuppressWarnings("all")
    private RegisterMapper rm;

    @Override
    public RegisterExecution checkUser(String username) {
        // 通过user去查询数据库
        Register dbRegister = rm.selectByUsername(username);
        if(dbRegister!=null){
            return new RegisterExecution(RegisterStatusEnum.USERNAME_EXIST);
        }
        return new RegisterExecution(RegisterStatusEnum.USERNAME_AVAILABLE);
    }

    @Override
    public RegisterExecution register(Register register) {

        System.out.println(register);

        // 判断对象是否为空
        if(register==null){
            return new RegisterExecution(RegisterStatusEnum.INFORMATION_NULL);
        }

        //  加密密码
        register.setUserPwd(MD5Util.MD5(register.getUserPwd()));

        // 设置注册时间
        register.setRegisterDate(TimeUtil.getTimeStr());

        // 保存数据库
        int res = rm.insertSelective(register);
        if(res<0){
            return new RegisterExecution(RegisterStatusEnum.REGISTER_FAILED);
        }
        return new RegisterExecution(RegisterStatusEnum.REGISTER_SUSSESS,register);
    }

    @Override
    public RegisterExecution selectAllRegisters() {
        List<Register> registers = rm.selectAll();
        if(registers.size()<0){
            return new RegisterExecution(RegisterStatusEnum.SELECT_FAILED);
        }
        return new RegisterExecution(RegisterStatusEnum.SELECT_SUSSESS,registers);
    }
}
