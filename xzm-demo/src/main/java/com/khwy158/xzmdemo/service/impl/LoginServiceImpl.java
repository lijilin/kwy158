package com.khwy158.xzmdemo.service.impl;

import com.khwy158.modeldemo.pojo.xzm.Empl;
import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.modeldemo.utils.JwtUtil;
import com.khwy158.modeldemo.utils.MD5Util;
import com.khwy158.xzmdemo.dao.EmplMapper;
import com.khwy158.xzmdemo.dao.RegisterMapper;
import com.khwy158.xzmdemo.dto.EmplExecution;
import com.khwy158.xzmdemo.dto.RegisterExecution;
import com.khwy158.xzmdemo.enums.EmplStatusEnum;
import com.khwy158.xzmdemo.enums.RegisterStatusEnum;
import com.khwy158.xzmdemo.service.LoginService;
import com.khwy158.xzmdemo.utils.RedisUtils;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author xzm
 * @Date 2018/8/28 20:54
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    @SuppressWarnings("all")
    private RegisterMapper rm;

    @Autowired
    @SuppressWarnings("all")
    private EmplMapper em;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public Integer loginType(String username) {
        // 如果在注册表里面查询到
        if((rm.selectByUsername(username)!=null)||(rm.selectByEmail(username)!=null)||(rm.selectByTel(Long.valueOf(username))!=null)){
            return 1;
        }
        // 如果在员工表找到
        if((em.selectByUser(username)!=null)||(em.selectByEmail(username)!=null)||(em.selectByTel(Long.valueOf(username))!=null)){
            return 2;
        }
        return 0;
    }

    @Override
    public RegisterExecution login(Register register){

        // 判断对象是否为空
        if(register==null){
            return new RegisterExecution(RegisterStatusEnum.INFORMATION_NULL);
        }

        // 判断用户名是否存在 如果通过用户名,邮箱,电话均找不到,则提示没有改用户
        if(rm.selectByUsername(register.getUserName())==null&&
                rm.selectByEmail(register.getUserEmail())==null&&
                rm.selectByTel(register.getContactTel())==null){
            return new RegisterExecution(RegisterStatusEnum.NO_REGISTER);
        }

        // 加密密码
        register.setUserPwd(MD5Util.MD5(register.getUserPwd()));

        // 用户名登录
        Register nRegister = rm.selectByUsername(register.getUserName());
        if(nRegister!=null){
            Cookie cookie = loginMethod(nRegister);
            return new RegisterExecution(RegisterStatusEnum.LOGIN_SUSSESS,cookie);
            }

        // 用邮箱登录
        Register eRegister = rm.selectByEmail(register.getUserEmail());
        if(eRegister!=null){
            Cookie cookie = loginMethod(eRegister);
            return new RegisterExecution(RegisterStatusEnum.LOGIN_SUSSESS,cookie);
        }

        // 手机号登录
        Register tRegister = rm.selectByTel(register.getContactTel());
        if(tRegister!=null){
            Cookie cookie = loginMethod(tRegister);
            return new RegisterExecution(RegisterStatusEnum.LOGIN_SUSSESS,cookie);
        }
        return new RegisterExecution(RegisterStatusEnum.LOGIN_FAILED);
    }

    @Override
    public EmplExecution login(Empl empl) {

        // 判断对象是否为空
        if(empl==null){
            return new EmplExecution(EmplStatusEnum.INFORMATION_NULL);
        }

        // 判断用户名是否存在
        Empl dbEmpl = em.selectByUser(empl.getEmplUser());
        if(dbEmpl==null){
            return new EmplExecution(EmplStatusEnum.NO_REGISTER);
        }

        // 加密密码并验证
        empl.setEmplPwd(empl.getEmplPwd());
        if(!dbEmpl.getEmplPwd().equals(empl.getEmplPwd())){
            return new EmplExecution(EmplStatusEnum.LOGIN_FAILED);
        }

        // 生成token并保存
        String token = JwtUtil.sign(dbEmpl, 1000 * 60 * 5);

        // 准备一个key值
        String key="login:empl:token";

        // 保存redis
        redisUtils.set(key,token);

       return new EmplExecution(EmplStatusEnum.LOGIN_SUSSESS,dbEmpl);
    }

    /**
     *  生成cookie并返回
     * @param register
     */
    public Cookie loginMethod(Register register ) {
        if (register != null) {
            // 校验密码
            if (register.getUserPwd().equals(register.getUserPwd())) {
                // 生成token,有效期为30分钟
                String token = JwtUtil.sign(register, 1000 * 60 * 30);
                // 保存到cookie里面
                Cookie adminCook = new Cookie("token", token);
                adminCook.setMaxAge(30 * 24 * 60 * 60);   //存活期为一个月 30*24*60*60
                adminCook.setPath("/");
                // 更新数据库
                register.setId(register.getId());
                rm.updateByPrimaryKeySelective(register);
                return adminCook;
            }
        }
        return null;
    }

}

