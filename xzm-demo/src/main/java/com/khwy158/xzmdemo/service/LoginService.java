package com.khwy158.xzmdemo.service;

import com.khwy158.modeldemo.pojo.xzm.Empl;
import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.xzmdemo.dto.EmplExecution;
import com.khwy158.xzmdemo.dto.RegisterExecution;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author xzm
 * @Date 2018/8/28 20:52
 */
public interface LoginService {

    /**
     *  判断登录类型 0,没有这个账号 , 1 , 管理员 2 , 员工
     * @param username
     * @return
     */
    Integer loginType(String username);

    /**
     *  管理员登录
     * @param register
     * @return
     */
    RegisterExecution login(Register register);

    /**
     *  员工登录
     * @param empl
     * @return
     */
    EmplExecution login(Empl empl);

}
