package com.khwy158.xzmdemo.service.impl;

import com.khwy158.modeldemo.pojo.xzm.Empl;
import com.khwy158.xzmdemo.dao.EmplMapper;
import com.khwy158.xzmdemo.dto.EmplExecution;
import com.khwy158.xzmdemo.enums.EmplStatusEnum;
import com.khwy158.xzmdemo.service.EmplService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author xzm
 * @Date 2018/8/29 11:36
 */
@Service
public class EmplServiceImpl implements EmplService {

    @Autowired
    @SuppressWarnings("all")
    private EmplMapper em;

    @Override
    public EmplExecution addEmpl(Empl empl) {
        int res = em.insertSelective(empl);
        if(res<0){
            return new EmplExecution(EmplStatusEnum.CREATE_FAILED);
        }
        return new EmplExecution(EmplStatusEnum.CREATE_SUSSESS,empl);
    }

    @Override
    public EmplExecution deleteEmpl(Integer id) {
        int res = em.deleteByPrimaryKey(id);
        if(res<0){
            return new EmplExecution(EmplStatusEnum.DELETE_FAILED);
        }
        return new EmplExecution(EmplStatusEnum.DELETE_SUSSESS);
    }

    @Override
    public EmplExecution modifyEmpl(Empl empl) {
        int res = em.updateByPrimaryKeySelective(empl);
        if(res<0){
            return new EmplExecution(EmplStatusEnum.MODIFY_FAILED);
        }
        return new EmplExecution(EmplStatusEnum.MODIFY_SUSSESS,empl);
    }

    @Override
    public EmplExecution selectEmpl(Integer id) {
        Empl empl = em.selectByPrimaryKey(id);
        if(empl==null){
            return new EmplExecution(EmplStatusEnum.SELECT_FAILED);
        }
        return new EmplExecution(EmplStatusEnum.SELECT_SUSSESS,empl);
    }

    @Override
    public EmplExecution selectAllEmpl() {
        List<Empl> empls = em.selectAll();
        if(empls.size()<0){
            return new EmplExecution(EmplStatusEnum.SELECT_FAILED);
        }
        return new EmplExecution(EmplStatusEnum.SELECT_SUSSESS,empls);
    }
}
