package com.khwy158.xzmdemo.service;

import com.khwy158.modeldemo.pojo.xzm.Dept;
import com.khwy158.xzmdemo.dto.DeptExecution;

import java.util.zip.InflaterInputStream;

/**
 * @Author xzm
 * @Date 2018/8/29 10:18
 */
public interface DeptService {

    /**
     *  增加部门
     * @return
     */
    DeptExecution addDept(Dept dept);

    /**
     *  根据id删除部门
     * @return
     */
    DeptExecution deleteDept(Integer id);

    /**
     *  修改部门信息
     * @return
     */
    DeptExecution modifyDept(Dept dept);

    /**
     *  根据id查询部门
     * @return
     */
    DeptExecution selectDept(Integer id);

    /**
     *  查询所有部门
     * @return
     */
    DeptExecution selectAllDept();
}
