package com.khwy158.xzmdemo.service.impl;

import com.khwy158.modeldemo.pojo.xzm.Dept;
import com.khwy158.xzmdemo.dao.DeptMapper;
import com.khwy158.xzmdemo.dto.DeptExecution;
import com.khwy158.xzmdemo.enums.DeptStatusEnum;
import com.khwy158.xzmdemo.service.DeptService;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author xzm
 * @Date 2018/8/29 10:22
 */
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    @SuppressWarnings("all")
    private DeptMapper dm;

    @Override
    public DeptExecution addDept(Dept dept) {

        int res = dm.insertSelective(dept);
        if(res<0){
            return new DeptExecution(DeptStatusEnum.CREATE_FAILED);
        }
        return new DeptExecution(DeptStatusEnum.CREATE_SUSSESS,dept);
    }

    @Override
    public DeptExecution deleteDept(Integer id) {
        int res = dm.deleteByPrimaryKey(id);
        if(res<0){
            return new DeptExecution(DeptStatusEnum.DELETE_FAILED);
        }
        return new DeptExecution(DeptStatusEnum.DELETE_SUSSESS);
    }

    @Override
    public DeptExecution modifyDept(Dept dept) {

        int res = dm.updateByPrimaryKeySelective(dept);
        if(res<0){
            return new DeptExecution(DeptStatusEnum.MODIFY_FAILED);
        }
        return new DeptExecution(DeptStatusEnum.MODIFY_SUSSESS,dept);
    }

    @Override
    public DeptExecution selectDept(Integer id) {
        Dept dept = dm.selectByPrimaryKey(id);
        if(dept==null) {
            return new DeptExecution(DeptStatusEnum.SELECT_FAILED);
        }
        return new DeptExecution(DeptStatusEnum.SELECT_SUSSESS,dept);
    }

    @Override
    public DeptExecution selectAllDept() {
        List<Dept> depts = dm.selectAll();
        if(depts.size()<0) {
            return new DeptExecution(DeptStatusEnum.SELECT_FAILED);
        }
        return new DeptExecution(DeptStatusEnum.SELECT_SUSSESS,depts);
    }
}
