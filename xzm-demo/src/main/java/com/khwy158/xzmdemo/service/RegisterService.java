package com.khwy158.xzmdemo.service;

import com.khwy158.modeldemo.pojo.xzm.Register;
import com.khwy158.xzmdemo.dto.RegisterExecution;

/**
 * @Author xzm
 * @Date 2018/8/28 16:33
 */
public interface RegisterService {

    /**
     *  验证用户名是否被注册
     * @param user
     * @return
     */
    RegisterExecution checkUser(String user);

    /**
     *  客户注册
     * @param register
     * @return
     */
    RegisterExecution register(Register register);

    /**
     *  查询所有注册公司
     * @return
     */
    RegisterExecution selectAllRegisters();

}
