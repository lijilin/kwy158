package com.khwy158.xzmdemo.enums;

/**
 * @Author xzm
 * @Date 2018/8/30 10:52
 */
public enum  LoginStausEnum {

    /**
     *  状态相关
     */
    LOGIN_SUSSESS(9010201,"登录成功"),
    LOGIN_FAILED(9010400,"登录失败"),
    NO_REGISTER(9010401,"用户名不存在")
    ;

    LoginStausEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

