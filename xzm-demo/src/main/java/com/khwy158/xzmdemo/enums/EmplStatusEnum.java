package com.khwy158.xzmdemo.enums;

/**
 * @Author xzm
 * @Date 2018/8/29 11:26
 */
public enum  EmplStatusEnum {

        /**
         *  状态相关
         */
        CREATE_SUSSESS(9010200,"创建成功"),
        CREATE_FAILED(9010400,"创建失败"),
        DELETE_FAILED(9010401,"删除失败"),
        DELETE_SUSSESS(9010201,"删除成功"),
        MODIFY_FAILED(9010402,"修改失败"),
        MODIFY_SUSSESS(9010202,"修改成功"),
        SELECT_FAILED(9010403,"查询失败"),
        SELECT_SUSSESS(9010203,"查询成功"),
        LOGIN_SUSSESS(9010204,"登录成功"),
        LOGIN_FAILED(9010404,"登录失败"),
    NO_REGISTER(9010405,"用户名不存在"),
    INFORMATION_NULL(9010406,"重要信息为空"),
        ;

    EmplStatusEnum(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        private Integer code;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

}
