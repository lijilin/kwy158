package com.khwy158.xzmdemo.enums;

/**
 * @Author xzm
 * @Date 2018/7/25 19:54
 */
public enum RegisterStatusEnum {

    /**
     *  状态相关
     */
    REGISTER_SUSSESS(9010200,"注册成功"),
    REGISTER_FAILED(9010400,"注册失败"),
    INFORMATION_NULL(9010401,"重要信息为空"),
    LOGIN_SUSSESS(9010201,"登录成功"),
    LOGIN_FAILED(9010402,"登录失败"),
    NO_REGISTER(9010403,"用户名不存在"),
    SELECT_FAILED(9010404,"查询失败"),
    SELECT_SUSSESS(9010203,"查询失败"),
    USERNAME_AVAILABLE(9010204,"用户名可用"),
    USERNAME_EXIST(9010405,"用户名已存在")
    ;

    RegisterStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
