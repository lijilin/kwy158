package com.khwy158.xzmdemo.enums;

/**
 * @Author xzm
 * @Date 2018/8/29 10:13
 */
public enum  DeptStatusEnum {

    /**
     *  状态相关
     */
    CREATE_SUSSESS(9010200,"创建成功"),
    CREATE_FAILED(9010400,"创建失败"),
    DELETE_FAILED(9010401,"删除失败"),
    DELETE_SUSSESS(9010201,"删除成功"),
    MODIFY_FAILED(9010402,"修改失败"),
    MODIFY_SUSSESS(9010202,"修改成功"),
    SELECT_FAILED(9010403,"查询失败"),
    SELECT_SUSSESS(9010203,"查询成功"),
//    EXIST_DEPT(9010403,"部门已经存在")
            ;

    DeptStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
