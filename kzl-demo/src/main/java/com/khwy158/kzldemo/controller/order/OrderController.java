package com.khwy158.kzldemo.controller.order;

import com.khwy158.kzldemo.dto.OrderExecution;
import com.khwy158.kzldemo.service.order.OrderService;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderService os;

    @PostMapping("/order_add")
    public JsonResult add(@RequestBody KzlOrder order){
        OrderExecution orderExecution = os.add(order);
        return new JsonResult(orderExecution.getCode(),orderExecution.getMsg(),orderExecution.getOrder(),20L);
    }
    @DeleteMapping("/order_delete/{id}")
    public JsonResult delete(@PathVariable Integer id){
        OrderExecution orderExecution = os.deleteById(id);
        return new JsonResult(orderExecution.getCode(),orderExecution.getMsg(),null,20L);
    }
    @PutMapping("/order_update")
    public JsonResult update(@RequestBody KzlOrder order){
        OrderExecution orderExecution = os.add(order);
        return new JsonResult(orderExecution.getCode(),orderExecution.getMsg(),orderExecution.getOrder(),20L);
    }
    @GetMapping("/order_list")
    public JsonResult orderList(){
        OrderExecution orderExecution = os.orderList();
        return new JsonResult(orderExecution.getCode(),orderExecution.getMsg(),orderExecution.getOrderList(),20L);
    }


}
