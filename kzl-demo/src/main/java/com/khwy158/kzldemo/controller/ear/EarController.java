package com.khwy158.kzldemo.controller.ear;

import com.khwy158.kzldemo.dto.EarExecution;
import com.khwy158.kzldemo.enums.EarEnum;
import com.khwy158.kzldemo.service.ear.EarService;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlEar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EarController {

    @Autowired
    private EarService es;

    public JsonResult add(@RequestBody KzlEar ear){
        EarExecution earExecution = es.add(ear);
        return new JsonResult(earExecution.getCode(),earExecution.getMsg(),earExecution.getEar(),20L);
    }

    public JsonResult delete(@PathVariable Integer id){
        EarExecution earExecution = es.deleteById(id);
        return new JsonResult(earExecution.getCode(),earExecution.getMsg(),null,20L);
    }

    public JsonResult update(@RequestBody KzlEar ear){
        EarExecution earExecution = es.update(ear);
        return new JsonResult(earExecution.getCode(),earExecution.getMsg(),earExecution.getEar(),20L);
    }

    public JsonResult earList(){
        EarExecution earExecution = es.earList();
        return new JsonResult(earExecution.getCode(),earExecution.getMsg(),earExecution.getEarList(),20L);
    }
}
