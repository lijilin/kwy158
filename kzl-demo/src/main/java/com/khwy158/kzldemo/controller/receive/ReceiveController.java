package com.khwy158.kzldemo.controller.receive;

import com.khwy158.kzldemo.dto.ReceiveExecution;
import com.khwy158.kzldemo.service.receive.ReceiveService;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlReceive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReceiveController {

    @Autowired
    private ReceiveService rs;

    @PostMapping("/receive_add")
    public JsonResult add(@RequestBody KzlReceive receive){
        ReceiveExecution receiveExecution = rs.add(receive);
        return new JsonResult(receiveExecution.getCode(),receiveExecution.getMsg(),receiveExecution.getReceive(),20L);
    }
    @DeleteMapping("/receive_delete/{id}")
    public JsonResult delete(@PathVariable Integer id){
        ReceiveExecution receiveExecution = rs.deleteById(id);
        return new JsonResult(receiveExecution.getCode(),receiveExecution.getMsg(),null,20L);
    }
    @PutMapping("/receive_update")
    public JsonResult update(@RequestBody KzlReceive receive){
        ReceiveExecution receiveExecution = rs.update(receive);
        return new JsonResult(receiveExecution.getCode(),receiveExecution.getMsg(),receiveExecution.getReceive(),20L);
    }
    @GetMapping("/receive_list")
    public JsonResult receiveList(){
        ReceiveExecution receiveExecution = rs.receiveList();
        return new JsonResult(receiveExecution.getCode(),receiveExecution.getMsg(),receiveExecution.getReceiveList(),20L);
    }
}
