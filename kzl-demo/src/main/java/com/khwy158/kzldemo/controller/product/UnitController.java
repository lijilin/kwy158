package com.khwy158.kzldemo.controller.product;


import com.khwy158.kzldemo.dto.ProductExecution;
import com.khwy158.kzldemo.dto.UnitExecution;
import com.khwy158.kzldemo.service.product.UnitService;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlProductUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UnitController {

    @Autowired
    private UnitService us;

    /**
     * 增加单位
     * @param unit
     * @return
     */
    @PostMapping("/unit_add")
    public JsonResult add(@RequestBody KzlProductUnit unit){
        UnitExecution unitExecution = us.insert(unit);
        return new JsonResult(unitExecution.getCode(),unitExecution.getMsg(),unitExecution.getUnit(),20L);
    }

    /**
     * 删除单位
     * @param id
     * @return
     */
    @DeleteMapping("/unit_delete/{id}")
    public JsonResult delete(@PathVariable Integer id){
        UnitExecution unitExecution = us.deleteById(id);
        return new JsonResult(unitExecution.getCode(),unitExecution.getMsg(),null,20L);
    }

    /**
     * 更新单位
     * @param unit
     * @return
     */
    @PutMapping("/unit_update")
    public JsonResult update(@RequestBody KzlProductUnit unit){
        UnitExecution unitExecution = us.update(unit);
        return new JsonResult(unitExecution.getCode(),unitExecution.getMsg(),unitExecution.getUnit(),20L);
    }

    /**
     * 查询所有单位
     * @return
     */
    @GetMapping ("/unit_selectList")
    public JsonResult selectList(){
        UnitExecution unitExecution = us.unitlist();
        return new JsonResult(unitExecution.getCode(),unitExecution.getMsg(),unitExecution.getUnitList(),20L);
    }

}
