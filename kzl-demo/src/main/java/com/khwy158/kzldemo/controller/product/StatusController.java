package com.khwy158.kzldemo.controller.product;


import com.khwy158.kzldemo.dto.StatusExecution;
import com.khwy158.kzldemo.service.product.StatusService;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlProductStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StatusController {

    @Autowired
    private StatusService ss;

    /**
     * 产品状态增加
     * @param status
     * @return
     */
    @PostMapping("/status_add")
    public JsonResult add(@RequestBody KzlProductStatus status){
        StatusExecution statusExecution = ss.add(status);
        return new JsonResult(statusExecution.getCode(),statusExecution.getMsg(),statusExecution.getStatus(),20L);
    }

    /**
     * 删除产品状态
     * @param id
     * @return
     */
    @DeleteMapping("/status_delete/{id}")
    public JsonResult delete(@PathVariable Integer id){
        StatusExecution statusExecution = ss.deleteById(id);
        return new JsonResult(statusExecution.getCode(),statusExecution.getMsg(),null,20L);
    }

    /**
     * 更新产品状态
     * @param status
     * @return
     */
    @PutMapping("/status_update")
    public JsonResult update(@RequestBody KzlProductStatus status){
        StatusExecution statusExecution = ss.update(status);
        return new JsonResult(statusExecution.getCode(),statusExecution.getMsg(),statusExecution.getStatus(),20L);
    }

    /**
     * 产品状态列表
     * @return
     */
    @GetMapping ("/status_list")
    public JsonResult list(){
        StatusExecution statusExecution = ss.statusList();
        return new JsonResult(statusExecution.getCode(),statusExecution.getMsg(),statusExecution.getStatusList(),20L);
    }
}
