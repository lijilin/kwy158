package com.khwy158.kzldemo.controller.product;

import com.github.pagehelper.PageInfo;
import com.khwy158.kzldemo.dto.ProductExecution;
import com.khwy158.kzldemo.service.product.ProductService;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlProduct;
import com.khwy158.modeldemo.pojo.ljl.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class ProductController {

    @Autowired
    private ProductService ps;
    @PostMapping("/product_add")
    public JsonResult add(@RequestBody KzlProduct product){
        ProductExecution categoryExecution =ps.add(product);
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),categoryExecution.getProduct(),20L);
    }

    @DeleteMapping("product_delete/{id}")
    public JsonResult delete(@PathVariable Integer id){
        ProductExecution categoryExecution =ps.deleteById(id);
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),categoryExecution.getProduct(),20L);
    }

    @PutMapping("/product_update")
    public JsonResult update(@RequestBody KzlProduct product){
        ProductExecution categoryExecution =ps.update(product);
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),categoryExecution.getProduct(),20L);
    }

    @GetMapping("/product_list")
    public JsonResult list(){
        ProductExecution categoryExecution =ps.list();
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),categoryExecution.getProducts(),20L);
    }

    @RequestMapping("/product_list1")
    public JsonResult list1(@RequestBody Page page){
        //从cookie中获取到登录的用户的empl_id
        ////////////未完成
        ProductExecution productExecution = ps.proAndOrder(1, page);
        //得到所有的产品
        PageInfo pageInfo=new PageInfo(productExecution.getProducts());
        //获取到产品的总条数
        long total = pageInfo.getTotal();
        return new JsonResult(0,productExecution.getMsg(),productExecution.getProducts(),total);
    }
}
