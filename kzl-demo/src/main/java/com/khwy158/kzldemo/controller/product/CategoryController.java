package com.khwy158.kzldemo.controller.product;


import com.khwy158.kzldemo.dto.CategoryExecution;
import com.khwy158.kzldemo.service.product.CategoryService;
import com.khwy158.modeldemo.dto.kzl.JsonResult;
import com.khwy158.modeldemo.pojo.kzl.KzlProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService cs;

    /**
     * 增加种类
     * @return
     */
    @PostMapping("/category_add")
    @ResponseBody
    public JsonResult add(@RequestBody KzlProductCategory category){
        CategoryExecution categoryExecution = cs.add(category);
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),categoryExecution.getCategory(),20L);
    }

    /**
     * 删除种类
     * @param id
     * @return
     */
    @DeleteMapping("/category_delete/{id}")
    public JsonResult delete(@PathVariable Integer id){
        CategoryExecution categoryExecution = cs.deleteById(id);
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),null,20L);
    }

    /**
     * 更新
     * @param
     * @return
     */
    @PutMapping("/category_update")
    public JsonResult update(@RequestBody KzlProductCategory category){
        CategoryExecution categoryExecution = cs.edit(category);
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),categoryExecution.getCategory(),20L);
    }

    /**
     * 列表
     * @param
     * @return
     */
    @GetMapping("/category_list")
    public JsonResult list(){
        CategoryExecution categoryExecution = cs.selectAll();
        return new JsonResult(categoryExecution.getCode(),categoryExecution.getMsg(),categoryExecution.getCategoryList(),20L);
    }
}
