package com.khwy158.kzldemo.dto;

import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.modeldemo.pojo.kzl.KzlProduct;

import java.util.List;

public class ProductExecution {

    private Integer code;

    private String msg;

    private KzlProduct product;

    private List<KzlProduct> products;


    /**
     * 失败的构造器
     * @param ProductEnum
     */
    public ProductExecution(ProductEnum ProductEnum) {
        this.code = ProductEnum.getCode();
        this.msg = ProductEnum.getMsg();
    }



    /**
     * 执行增删改成功的构造器
     * @param
     * @param
     */

    public ProductExecution(ProductEnum ProductEnum, KzlProduct product) {
        this.code = ProductEnum.getCode();
        this.msg = ProductEnum.getMsg();
        this.product = product;
    }

    /**
     * 执行查询成功的构造器
     * @param ProductEnum
     * @param
     */

    public ProductExecution(ProductEnum ProductEnum, List<KzlProduct> products) {
        this.code = ProductEnum.getCode();
        this.msg = ProductEnum.getMsg();
        this.products = products;
    }



    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public KzlProduct getProduct() {
        return product;
    }

    public void setProduct(KzlProduct product) {
        this.product = product;
    }

    public List<KzlProduct> getProducts() {
        return products;
    }

    public void setProducts(List<KzlProduct> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "ProductExecution{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", product=" + product +
                ", products=" + products +
                '}';
    }
}
