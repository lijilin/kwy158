package com.khwy158.kzldemo.dto;

import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.enums.ReceiveEnum;
import com.khwy158.modeldemo.pojo.kzl.KzlReceive;

import java.util.List;

public class ReceiveExecution {

    private Integer code;

    private String msg;

    private KzlReceive receive;

    private List<KzlReceive> receiveList;

    /**
     * 失败的构造器
     * @param receiveEnum
     */
    public ReceiveExecution(ReceiveEnum receiveEnum) {
        this.code = receiveEnum.getCode();
        this.msg = receiveEnum.getMsg();
    }

    /**
     * 执行增删改成功的构造器
     * @param receiveEnum
     * @param receive
     */
    public ReceiveExecution(ReceiveEnum receiveEnum, KzlReceive receive) {
        this.code = receiveEnum.getCode();
        this.msg = receiveEnum.getMsg();
        this.receive = receive;
    }

    /**
     * 执行查询成功的构造器
     * @param receiveEnum
     * @param receiveList
     */
    public ReceiveExecution(ReceiveEnum receiveEnum, List<KzlReceive> receiveList) {
        this.code = receiveEnum.getCode();
        this.msg = receiveEnum.getMsg();
        this.receiveList = receiveList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public KzlReceive getReceive() {
        return receive;
    }

    public void setReceive(KzlReceive receive) {
        this.receive = receive;
    }

    public List<KzlReceive> getReceiveList() {
        return receiveList;
    }

    public void setReceiveList(List<KzlReceive> receiveList) {
        this.receiveList = receiveList;
    }
}
