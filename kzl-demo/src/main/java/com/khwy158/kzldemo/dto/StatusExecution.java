package com.khwy158.kzldemo.dto;

import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.enums.StatusEnum;
import com.khwy158.modeldemo.pojo.kzl.KzlProductStatus;

import java.util.List;

public class StatusExecution {

    private Integer code;

    private String msg;

    private KzlProductStatus status;

    private List<KzlProductStatus> statusList;

    public StatusExecution(StatusEnum statusEnum) {
        this.code = statusEnum.getCode();
        this.msg = statusEnum.getMsg();
    }

    public StatusExecution(StatusEnum statusEnum, KzlProductStatus status) {
        this.code = statusEnum.getCode();
        this.msg = statusEnum.getMsg();
        this.status = status;
    }

    public StatusExecution(StatusEnum statusEnum, List<KzlProductStatus> statusList) {
        this.code = statusEnum.getCode();
        this.msg = statusEnum.getMsg();
        this.statusList = statusList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public KzlProductStatus getStatus() {
        return status;
    }

    public void setStatus(KzlProductStatus status) {
        this.status = status;
    }

    public List<KzlProductStatus> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<KzlProductStatus> statusList) {
        this.statusList = statusList;
    }
}
