package com.khwy158.kzldemo.dto;

import com.khwy158.kzldemo.enums.EarEnum;
import com.khwy158.modeldemo.pojo.kzl.KzlEar;

import java.util.List;

public class EarExecution {

    private Integer code;

    private String msg;

    private KzlEar ear;

    private List<KzlEar> earList;

    public EarExecution(EarEnum earEnum) {
        this.code = earEnum.getCode();
        this.msg = earEnum.getMsg();
    }

    public EarExecution(EarEnum earEnum, KzlEar ear) {
        this.code = earEnum.getCode();
        this.msg = earEnum.getMsg();
        this.ear = ear;
    }

    public EarExecution(EarEnum earEnum, List<KzlEar> earList) {
        this.code = earEnum.getCode();
        this.msg = earEnum.getMsg();
        this.earList = earList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public KzlEar getEar() {
        return ear;
    }

    public void setEar(KzlEar ear) {
        this.ear = ear;
    }

    public List<KzlEar> getEarList() {
        return earList;
    }

    public void setEarList(List<KzlEar> earList) {
        this.earList = earList;
    }
}
