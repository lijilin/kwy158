package com.khwy158.kzldemo.dto;

import com.khwy158.kzldemo.enums.OrderEnum;
import com.khwy158.modeldemo.pojo.kzl.KzlOrder;

import java.util.List;

public class OrderExecution {

    private Integer code;

    private String msg;

    private KzlOrder order;

    private List<KzlOrder> orderList;

    public OrderExecution(OrderEnum orderEnum) {
        this.code = orderEnum.getCode();
        this.msg = orderEnum.getMsg();
    }

    public OrderExecution(OrderEnum orderEnum, KzlOrder order) {
        this.code = orderEnum.getCode();
        this.msg = orderEnum.getMsg();
        this.order = order;
    }

    public OrderExecution(OrderEnum orderEnum, List<KzlOrder> orderList) {
        this.code = orderEnum.getCode();
        this.msg = orderEnum.getMsg();
        this.orderList = orderList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public KzlOrder getOrder() {
        return order;
    }

    public void setOrder(KzlOrder order) {
        this.order = order;
    }

    public List<KzlOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<KzlOrder> orderList) {
        this.orderList = orderList;
    }
}
