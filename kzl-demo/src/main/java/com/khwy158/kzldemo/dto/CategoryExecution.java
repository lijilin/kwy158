package com.khwy158.kzldemo.dto;

import com.khwy158.kzldemo.enums.CategoryEnum;
import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.modeldemo.pojo.kzl.KzlProductCategory;

import java.util.List;

public class CategoryExecution {

    private Integer code;

    private String msg;

    private KzlProductCategory category;

    private List<KzlProductCategory> categoryList;

    /**
     * 失败的构造器
     * @param categoryEnum
     */
    public CategoryExecution(CategoryEnum categoryEnum) {
        this.code = categoryEnum.getCode();
        this.msg = categoryEnum.getMsg();
    }

    /**
     * 执行增删改成功的构造器
     * @param categoryEnum
     * @param category
     */
    public CategoryExecution(CategoryEnum categoryEnum, KzlProductCategory category) {
        this.code = categoryEnum.getCode();
        this.msg = categoryEnum.getMsg();
        this.category = category;
    }

    /**
     * 执行查询成功的构造器
     * @param categoryEnum
     * @param categoryList
     */
    public CategoryExecution(CategoryEnum categoryEnum, List<KzlProductCategory> categoryList) {
        this.code = categoryEnum.getCode();
        this.msg = categoryEnum.getMsg();
        this.categoryList = categoryList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public KzlProductCategory getCategory() {
        return category;
    }

    public void setCategory(KzlProductCategory category) {
        this.category = category;
    }

    public List<KzlProductCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<KzlProductCategory> categoryList) {
        this.categoryList = categoryList;
    }
}
