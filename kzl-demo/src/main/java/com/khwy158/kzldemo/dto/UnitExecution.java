package com.khwy158.kzldemo.dto;

import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.enums.UnitEnum;
import com.khwy158.modeldemo.pojo.kzl.KzlProductUnit;

import java.util.List;

public class UnitExecution {

    private Integer code;

    private String msg;

    private KzlProductUnit unit;

    private List<KzlProductUnit> unitList;

    /**
     * 失败的构造器
     * @param unitEnum
     */
    public UnitExecution(UnitEnum unitEnum) {
        this.code = unitEnum.getCode();
        this.msg = unitEnum.getMsg();
    }

    /**
     * 执行增删改成功的构造器
     * @param unitEnum
     * @param unit
     */
    public UnitExecution(UnitEnum unitEnum, KzlProductUnit unit) {
        this.code = unitEnum.getCode();
        this.msg = unitEnum.getMsg();
        this.unit = unit;
    }

    /**
     * 执行查询成功的构造器
     * @param unitEnum
     * @param unitList
     */
    public UnitExecution(UnitEnum unitEnum, List<KzlProductUnit> unitList) {
        this.code = unitEnum.getCode();
        this.msg = unitEnum.getMsg();
        this.unitList = unitList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public KzlProductUnit getUnit() {
        return unit;
    }

    public void setUnit(KzlProductUnit unit) {
        this.unit = unit;
    }

    public List<KzlProductUnit> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<KzlProductUnit> unitList) {
        this.unitList = unitList;
    }
}
