package com.khwy158.kzldemo.dto;

public class JsonResult {


    /**
     * 状态码
     */
    private Integer code;

    /**
     * 信息
     */
    private String msg;

    /**
     * 数据
     */
    private Object data;

    private Integer count;



    /**
     * 失败的构造器
     * @param code
     * @param msg
     */
    public JsonResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 成功的构造器
     * @param code
     * @param msg
     * @param data
     */
    public JsonResult(Integer code, String msg, Object data,Integer count) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count=count;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
