package com.khwy158.kzldemo.service.product.impl;

import com.khwy158.kzldemo.dao.KzlProductStatusMapper;
import com.khwy158.kzldemo.dto.StatusExecution;
import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.enums.StatusEnum;
import com.khwy158.kzldemo.service.product.StatusService;
import com.khwy158.modeldemo.pojo.kzl.KzlProductStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService{

    @Autowired
    @SuppressWarnings("all")
    private KzlProductStatusMapper mapper;

    @Override
    public StatusExecution add(KzlProductStatus status) {
        mapper.insert(status);
        return new StatusExecution(StatusEnum.OPERATE_SUCCESS);
    }

    @Override
    public StatusExecution deleteById(Integer id) {
        mapper.deleteByPrimaryKey(id);
        return new StatusExecution(StatusEnum.DELETE_SUCCESS);
    }

    @Override
    public StatusExecution update(KzlProductStatus status) {
        mapper.updateByPrimaryKey(status);
        return new StatusExecution(StatusEnum.OPERATE_SUCCESS);
    }

    @Override
    public StatusExecution statusList() {
        List<KzlProductStatus> list = mapper.selectAll();
        return new StatusExecution(StatusEnum.OPERATE_SUCCESS);
    }
}
