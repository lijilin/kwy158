package com.khwy158.kzldemo.service.order.impl;

import com.khwy158.kzldemo.dao.KzlOrderMapper;
import com.khwy158.kzldemo.dto.OrderExecution;
import com.khwy158.kzldemo.enums.OrderEnum;
import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.service.order.OrderService;
import com.khwy158.modeldemo.pojo.kzl.KzlOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    @SuppressWarnings("all")
    private KzlOrderMapper mapper;

    @Override
    public OrderExecution add(KzlOrder order) {
        mapper.insertSelective(order);
        return new OrderExecution(OrderEnum.OPERATE_SUCCESS,order);
    }

    @Override
    public OrderExecution deleteById(Integer id) {
        mapper.deleteByPrimaryKey(id);
        return new OrderExecution(OrderEnum.DELETE_SUCCESS);
    }

    @Override
    public OrderExecution update(KzlOrder order) {
        mapper.updateByPrimaryKeySelective(order);
        return new OrderExecution(OrderEnum.OPERATE_SUCCESS,order);
    }

    @Override
    public OrderExecution orderList() {
        List<KzlOrder> kzlOrders = mapper.selectAll();
        return new OrderExecution(OrderEnum.OPERATE_SUCCESS,kzlOrders);
    }
}
