package com.khwy158.kzldemo.service.product.impl;

import com.khwy158.kzldemo.dao.KzlProductUnitMapper;
import com.khwy158.kzldemo.dto.UnitExecution;
import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.enums.UnitEnum;
import com.khwy158.kzldemo.service.product.UnitService;
import com.khwy158.modeldemo.pojo.kzl.KzlProductUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitServiceImpl implements UnitService{

    @Autowired
    @SuppressWarnings("all")
    private KzlProductUnitMapper mapper;

    @Override
    public UnitExecution insert(KzlProductUnit unit) {
        mapper.insertSelective(unit);
        return new UnitExecution(UnitEnum.OPERATE_SUCCESS);
    }

    @Override
    public UnitExecution deleteById(Integer id) {
        mapper.deleteByPrimaryKey(id);
        return new UnitExecution(UnitEnum.DELETE_SUCCESS);
    }

    @Override
    public UnitExecution update(KzlProductUnit unit) {
        mapper.updateByPrimaryKeySelective(unit);
        return new UnitExecution(UnitEnum.OPERATE_SUCCESS);
    }

    @Override
    public UnitExecution unitlist() {
        List<KzlProductUnit> list = mapper.selectAll();

        return new UnitExecution(UnitEnum.OPERATE_SUCCESS);
    }
}
