package com.khwy158.kzldemo.service.product;


import com.khwy158.kzldemo.dto.CategoryExecution;
import com.khwy158.kzldemo.dto.ProductExecution;
import com.khwy158.modeldemo.pojo.kzl.KzlProductCategory;
import org.springframework.data.annotation.Id;

import java.util.List;

public interface CategoryService {

    /**
     * 增加种类
     */
    CategoryExecution add(KzlProductCategory category);

    /**
     * 根据id删除
     */
    CategoryExecution deleteById(Integer id);

    /**
     * 修改
     * @return
     */
    CategoryExecution edit(KzlProductCategory category);

    /**
     * 查询全部
     * @return
     */
    CategoryExecution selectAll();

}
