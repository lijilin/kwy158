package com.khwy158.kzldemo.service.product;

import com.khwy158.kzldemo.dto.UnitExecution;
import com.khwy158.modeldemo.pojo.kzl.KzlProductUnit;


public interface UnitService {
    /**
     * 产品单位增加
     *
     * @param unit
     */
    UnitExecution insert(KzlProductUnit unit);

    /**
     * 删除产品单位
     *
     * @param id
     */
    UnitExecution deleteById(Integer id);

    /**
     * 更新
     *
     * @param unit
     */
    UnitExecution update(KzlProductUnit unit);

    /**
     * 单位列表
     * @return
     */
    UnitExecution unitlist();
}
