package com.khwy158.kzldemo.service.ear;

import com.khwy158.kzldemo.dto.EarExecution;
import com.khwy158.modeldemo.pojo.kzl.KzlEar;

import java.util.List;

public interface EarService {
    /**
     * 增加收支记录
     * @param ear
     */
    EarExecution add(KzlEar ear);

    /**
     * 删除收支记录
     * @param id
     */
    EarExecution deleteById(Integer id);

    /**
     * 更新收支记录
     * @param ear
     */
    EarExecution update(KzlEar ear);

    /**
     * 收支记录列表
     * @return
     */
    EarExecution earList();
}
