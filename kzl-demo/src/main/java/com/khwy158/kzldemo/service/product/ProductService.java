package com.khwy158.kzldemo.service.product;

import com.khwy158.kzldemo.dto.ProductExecution;
import com.khwy158.modeldemo.pojo.kzl.KzlProduct;
import com.khwy158.modeldemo.pojo.ljl.Page;


public interface ProductService {
    /**
     * 增加产品
     * @param product
     */
    ProductExecution add(KzlProduct product);

    /**
     * 删除产品
     * @param id
     */
    ProductExecution deleteById(Integer id);

    /**
     * 更新产品
     * @param product
     */
    ProductExecution update(KzlProduct product);

    /**
     * 查询单个产品
     * @param product
     */
    ProductExecution selectById(KzlProduct product);

    /**
     * 查询所有产品
     * @return
     */
    ProductExecution list();

    /**
     * 链表查询订单表
     * @return
     */
    ProductExecution proAndOrder(Integer id, Page page);
}
