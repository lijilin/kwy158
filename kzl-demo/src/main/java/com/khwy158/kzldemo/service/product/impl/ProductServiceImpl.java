package com.khwy158.kzldemo.service.product.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.khwy158.kzldemo.dao.KzlProductMapper;
import com.khwy158.kzldemo.dto.ProductExecution;
import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.service.product.ProductService;
import com.khwy158.modeldemo.pojo.kzl.KzlProduct;
import com.khwy158.modeldemo.pojo.ljl.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    @SuppressWarnings("all")
    private KzlProductMapper mapper;

    @Override
    public ProductExecution add(KzlProduct product) {
        mapper.insertSelective(product);
        return new  ProductExecution(ProductEnum.OPERATE_SUCCESS,product);
    }

    @Override
    public ProductExecution deleteById(Integer id) {
        mapper.deleteByPrimaryKey(id);
        return new ProductExecution(ProductEnum.OPERATE_SUCCESS);
    }

    @Override
    public ProductExecution update(KzlProduct product) {
        mapper.updateByPrimaryKeySelective(product);
        return new ProductExecution(ProductEnum.OPERATE_SUCCESS,product);
    }

    @Override
    public ProductExecution selectById(KzlProduct product) {
        return null;
    }


    @Override
    public ProductExecution list() {
        List<KzlProduct> kzlProducts = mapper.selectAll();
        System.out.println(kzlProducts.size());
        if (kzlProducts==null){
            return new ProductExecution(ProductEnum.OPERATE_FAILED);
        }
        return new  ProductExecution(ProductEnum.OPERATE_SUCCESS,kzlProducts);
    }

    @Override
    public ProductExecution proAndOrder(Integer id, Page page) {
        PageHelper.startPage(page.getPage(),page.getPageSzie());

        List<KzlProduct> kzlProducts = mapper.selectProOrder(id);

        if (kzlProducts==null){
            return new ProductExecution(ProductEnum.OPERATE_FAILED);
        }
        return new  ProductExecution(ProductEnum.OPERATE_SUCCESS,kzlProducts);
    }
}
