package com.khwy158.kzldemo.service.product.impl;

import com.khwy158.kzldemo.dao.KzlProductCategoryMapper;
import com.khwy158.kzldemo.dto.CategoryExecution;
import com.khwy158.kzldemo.dto.ProductExecution;
import com.khwy158.kzldemo.enums.CategoryEnum;
import com.khwy158.kzldemo.enums.ProductEnum;
import com.khwy158.kzldemo.service.product.CategoryService;
import com.khwy158.modeldemo.pojo.kzl.KzlProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    @SuppressWarnings("all")
    private KzlProductCategoryMapper mapper;

    @Override
    public CategoryExecution add(KzlProductCategory category) {
        mapper.insertSelective(category);
        return new CategoryExecution(CategoryEnum.OPERATE_SUCCESS);
    }

    @Override
    public CategoryExecution deleteById(Integer id) {
        mapper.deleteByPrimaryKey(id);
        return new CategoryExecution(CategoryEnum.DELETE_SUCCESS);
    }

    @Override
    public CategoryExecution edit(KzlProductCategory category) {
        mapper.updateByPrimaryKeySelective(category);
        return new CategoryExecution(CategoryEnum.OPERATE_SUCCESS);
    }

    @Override
    public CategoryExecution selectAll() {
        List<KzlProductCategory> list = mapper.selectAll();
        return new CategoryExecution(CategoryEnum.OPERATE_SUCCESS);
    }
}
