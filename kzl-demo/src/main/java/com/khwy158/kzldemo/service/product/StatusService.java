package com.khwy158.kzldemo.service.product;

import com.khwy158.kzldemo.dto.StatusExecution;
import com.khwy158.modeldemo.pojo.kzl.KzlProductStatus;


public interface StatusService {

    /**
     * 增加产品状态
     * @param status
     */
    StatusExecution add(KzlProductStatus status);

    /**
     * 删除产品状态
     * @param id
     */
    StatusExecution  deleteById(Integer id);

    /**
     * 更新产品状态
     * @param status
     */
    StatusExecution  update(KzlProductStatus status);

    /**
     * 产品状态列表
     * @return
     */
    StatusExecution statusList();

}
