package com.khwy158.kzldemo.service.receive;

import com.khwy158.kzldemo.dto.ReceiveExecution;
import com.khwy158.modeldemo.pojo.kzl.KzlReceive;

import java.util.List;

public interface ReceiveService {
    /**
     * 增加回款记录
     * @param receive
     */
    ReceiveExecution add(KzlReceive receive);

    /**
     * 删除回款记录
     * @param id
     */
    ReceiveExecution deleteById(Integer id);

    /**
     * 更新回款记录
     * @param receive
     */
    ReceiveExecution update(KzlReceive receive);

    /**
     * 回款记录列表
     * @return
     */
    ReceiveExecution receiveList();
}
