package com.khwy158.kzldemo.service.order;

import com.khwy158.kzldemo.dto.OrderExecution;
import com.khwy158.modeldemo.pojo.kzl.KzlOrder;

import java.util.List;

public interface OrderService {
    /**
     * 增加订单
     * @param order
     */
    OrderExecution add(KzlOrder order);

    /**
     * 删除订单
     * @param id
     */
    OrderExecution deleteById(Integer id);

    /**
     * 更新订单
     * @param order
     */
    OrderExecution update(KzlOrder order);

    /**
     * 订单列表
     * @return
     */
    OrderExecution orderList();
}
