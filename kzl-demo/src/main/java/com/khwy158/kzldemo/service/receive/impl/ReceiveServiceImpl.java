package com.khwy158.kzldemo.service.receive.impl;

import com.khwy158.kzldemo.dao.KzlReceiveMapper;
import com.khwy158.kzldemo.dto.ReceiveExecution;
import com.khwy158.kzldemo.enums.ReceiveEnum;
import com.khwy158.kzldemo.service.receive.ReceiveService;
import com.khwy158.modeldemo.pojo.kzl.KzlReceive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReceiveServiceImpl implements ReceiveService{

    @Autowired
    @SuppressWarnings("all")
    private KzlReceiveMapper mapper;

    @Override
    public ReceiveExecution add(KzlReceive receive) {
        mapper.insertSelective(receive);
        return new ReceiveExecution(ReceiveEnum.OPERATE_SUCCESS,receive);
    }

    @Override
    public ReceiveExecution deleteById(Integer id) {
        mapper.deleteByPrimaryKey(id);
        return new ReceiveExecution(ReceiveEnum.DELETE_SUCCESS);
    }

    @Override
    public ReceiveExecution update(KzlReceive receive) {
        mapper.updateByPrimaryKeySelective(receive);
        return new ReceiveExecution(ReceiveEnum.OPERATE_SUCCESS,receive);
    }

    @Override
    public ReceiveExecution receiveList() {
        List<KzlReceive> kzlReceives = mapper.selectAll();

        return new ReceiveExecution(ReceiveEnum.OPERATE_SUCCESS,kzlReceives);
    }
}
