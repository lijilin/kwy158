package com.khwy158.kzldemo.service.ear.impl;

import com.khwy158.kzldemo.dao.KzlEarMapper;
import com.khwy158.kzldemo.dto.EarExecution;
import com.khwy158.kzldemo.enums.EarEnum;
import com.khwy158.kzldemo.service.ear.EarService;
import com.khwy158.modeldemo.pojo.kzl.KzlEar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EarServiceImpl implements EarService{

    @Autowired
    @SuppressWarnings("all")
    private KzlEarMapper mapper;

    @Override
    public EarExecution add(KzlEar ear) {
        mapper.insert(ear);
        return new EarExecution(EarEnum.OPERATE_SUCCESS,ear);
    }

    @Override
    public EarExecution deleteById(Integer id) {
        mapper.deleteByPrimaryKey(id);
        return new EarExecution(EarEnum.DELETE_SUCCESS);
    }

    @Override
    public EarExecution update(KzlEar ear) {
        mapper.updateByPrimaryKeySelective(ear);
        return new EarExecution(EarEnum.OPERATE_SUCCESS,ear);
    }

    @Override
    public EarExecution earList() {
        List<KzlEar> kzlEars = mapper.selectAll();
        return new EarExecution(EarEnum.OPERATE_SUCCESS,kzlEars);
    }
}
