package com.khwy158.kzldemo.enums;

public enum ReceiveEnum {
    /**
     * 状态相关
     */
    ALLREADY_EXIT(6000,"该产品已经存在!"),
    SUCCESS(200,"保存成功！"),
    OPERATE_SUCCESS(201,"操作成功！"),
    REQUIRED_NULL_INFO(6001,"客户为必选项，还没有选择！"),
    NO_CHOICE(6002,"未选中任何记录!"),
    OPERATE_FAILED(6005,"操作失败"),
    NO_DELETE(6003,"请选择要删除的记录！"),
    DELETE_SUCCESS(6004,"删除成功!"),
    ;


    private Integer code;
    private String msg;

    ReceiveEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
