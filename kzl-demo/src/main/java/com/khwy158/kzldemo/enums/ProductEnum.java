package com.khwy158.kzldemo.enums;

public enum ProductEnum {
    /**
     * 状态相关
     */
    ALLREADY_EXIT(6000,"该产品已经存在!"),
    REQUIRED_NULL_INFO(6001,"产品名称为必填项，不可留空！"),
    SUCCESS(200,"保存成功！"),
    OPERATE_SUCCESS(201,"操作成功！"),
    NO_CHOICE(6002,"未选中任何记录!"),
    OPERATE_FAILED(8001,"操作失败"),
    NO_DELETE(6003,"请选择要删除的记录！"),
    DELETE_SUCCESS(6004,"删除成功!"),
    ;


    private Integer code;
    private String msg;


    ProductEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
