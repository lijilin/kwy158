package com.khwy158.kzldemo;

import com.khwy158.kzldemo.dao.KzlProductCategoryMapper;
import com.khwy158.modeldemo.pojo.kzl.KzlProductCategory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.khwy158.kzldemo.dao")
public class KzlDemoApplication {

    @Bean
    public KzlProductCategory Mapper(){
        return new KzlProductCategory();
    }
    public static void main(String[] args) {
        SpringApplication.run(KzlDemoApplication.class, args);
    }
}
