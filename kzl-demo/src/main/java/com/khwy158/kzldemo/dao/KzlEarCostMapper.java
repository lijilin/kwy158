package com.khwy158.kzldemo.dao;

import com.khwy158.modeldemo.pojo.kzl.KzlEarCost;
import com.khwy158.modeldemo.pojo.kzl.KzlProductCategory;

import java.util.List;

public interface KzlEarCostMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table kzl_ear_cost
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table kzl_ear_cost
     *
     * @mbg.generated
     */
    int insert(KzlEarCost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table kzl_ear_cost
     *
     * @mbg.generated
     */
    int insertSelective(KzlEarCost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table kzl_ear_cost
     *
     * @mbg.generated
     */
    KzlEarCost selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table kzl_ear_cost
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(KzlEarCost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table kzl_ear_cost
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(KzlEarCost record);

    /**
     * 查询所有
     * @return
     */
    List<KzlEarCost> selectAll();
}