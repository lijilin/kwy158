package com.khwy158.hzhdemo.pojo;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table ldx_company_client
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class LdxCompanyClient {
    /** */
    private Integer id;

    /** 客户名称*/
    private String companyClientName;

    /** 所在城市ID*/
    private Integer cityId;

    /** 行业*/
    private String profession;

    /** 意向产品ID*/
    private Integer productId;

    /** 公司座机*/
    private String companyPhone;

    /** 客户状态*/
    private Integer clientStatusId;

    /** 客户类别*/
    private Integer clientCategoryId;

    /** 客户等级ID*/
    private Integer clientGradeId;

    /** 客户来源*/
    private String clientSource;

    /** 客户成熟度ID*/
    private Integer maturityId;

    /** 公司地址*/
    private String companyAddress;

    /** 公司客户介绍*/
    private String companyDesc;

    /** 获取企业客户时间*/
    private String acquisitionTime;

    /** 注册公司ID*/
    private Integer registerId;

    /** 所属员工*/
    private Integer emplId;

    /** 联系人姓名*/
    private String contactsName;

    /** 联系人性别0为男1为女默认0*/
    private Integer contactsSex;

    /** 联系人生日*/
    private String contactsBirthday;

    /** 联系人职务*/
    private String contactsJob;

    /** 联系人电话*/
    private Long contactsPhone;

    /** 联系人邮箱*/
    private String contactsEmail;

    /** 相关文档地址*/
    private String relatedDocuments;

    /** 公司客户还是个人客户0为公司1为个人客户*/
    private Integer clientComOrPerson;

    /** 个人客户所在公司*/
    private String personalClientCompany;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyClientName() {
        return companyClientName;
    }

    public void setCompanyClientName(String companyClientName) {
        this.companyClientName = companyClientName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public Integer getClientStatusId() {
        return clientStatusId;
    }

    public void setClientStatusId(Integer clientStatusId) {
        this.clientStatusId = clientStatusId;
    }

    public Integer getClientCategoryId() {
        return clientCategoryId;
    }

    public void setClientCategoryId(Integer clientCategoryId) {
        this.clientCategoryId = clientCategoryId;
    }

    public Integer getClientGradeId() {
        return clientGradeId;
    }

    public void setClientGradeId(Integer clientGradeId) {
        this.clientGradeId = clientGradeId;
    }

    public String getClientSource() {
        return clientSource;
    }

    public void setClientSource(String clientSource) {
        this.clientSource = clientSource;
    }

    public Integer getMaturityId() {
        return maturityId;
    }

    public void setMaturityId(Integer maturityId) {
        this.maturityId = maturityId;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyDesc() {
        return companyDesc;
    }

    public void setCompanyDesc(String companyDesc) {
        this.companyDesc = companyDesc;
    }

    public String getAcquisitionTime() {
        return acquisitionTime;
    }

    public void setAcquisitionTime(String acquisitionTime) {
        this.acquisitionTime = acquisitionTime;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public Integer getEmplId() {
        return emplId;
    }

    public void setEmplId(Integer emplId) {
        this.emplId = emplId;
    }

    public String getContactsName() {
        return contactsName;
    }

    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    public Integer getContactsSex() {
        return contactsSex;
    }

    public void setContactsSex(Integer contactsSex) {
        this.contactsSex = contactsSex;
    }

    public String getContactsBirthday() {
        return contactsBirthday;
    }

    public void setContactsBirthday(String contactsBirthday) {
        this.contactsBirthday = contactsBirthday;
    }

    public String getContactsJob() {
        return contactsJob;
    }

    public void setContactsJob(String contactsJob) {
        this.contactsJob = contactsJob;
    }

    public Long getContactsPhone() {
        return contactsPhone;
    }

    public void setContactsPhone(Long contactsPhone) {
        this.contactsPhone = contactsPhone;
    }

    public String getContactsEmail() {
        return contactsEmail;
    }

    public void setContactsEmail(String contactsEmail) {
        this.contactsEmail = contactsEmail;
    }

    public String getRelatedDocuments() {
        return relatedDocuments;
    }

    public void setRelatedDocuments(String relatedDocuments) {
        this.relatedDocuments = relatedDocuments;
    }

    public Integer getClientComOrPerson() {
        return clientComOrPerson;
    }

    public void setClientComOrPerson(Integer clientComOrPerson) {
        this.clientComOrPerson = clientComOrPerson;
    }

    public String getPersonalClientCompany() {
        return personalClientCompany;
    }

    public void setPersonalClientCompany(String personalClientCompany) {
        this.personalClientCompany = personalClientCompany;
    }

    @Override
    public String toString() {
        return "LdxCompanyClient{" +
                "id=" + id +
                ", companyClientName='" + companyClientName + '\'' +
                ", cityId=" + cityId +
                ", profession='" + profession + '\'' +
                ", productId=" + productId +
                ", companyPhone='" + companyPhone + '\'' +
                ", clientStatusId=" + clientStatusId +
                ", clientCategoryId=" + clientCategoryId +
                ", clientGradeId=" + clientGradeId +
                ", clientSource='" + clientSource + '\'' +
                ", maturityId=" + maturityId +
                ", companyAddress='" + companyAddress + '\'' +
                ", companyDesc='" + companyDesc + '\'' +
                ", acquisitionTime='" + acquisitionTime + '\'' +
                ", registerId=" + registerId +
                ", emplId=" + emplId +
                ", contactsName='" + contactsName + '\'' +
                ", contactsSex=" + contactsSex +
                ", contactsBirthday='" + contactsBirthday + '\'' +
                ", contactsJob='" + contactsJob + '\'' +
                ", contactsPhone=" + contactsPhone +
                ", contactsEmail='" + contactsEmail + '\'' +
                ", relatedDocuments='" + relatedDocuments + '\'' +
                ", clientComOrPerson=" + clientComOrPerson +
                ", personalClientCompany='" + personalClientCompany + '\'' +
                '}';
    }
}