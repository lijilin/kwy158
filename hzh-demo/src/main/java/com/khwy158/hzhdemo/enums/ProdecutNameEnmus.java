package com.khwy158.hzhdemo.enums;

public enum ProdecutNameEnmus {
    /**
     * 枚举相关
     */
    CREATE_SUCCESS(6200,"查询成功"),
    UPDATE_SUCCESS(6201,"出库成功"),
    SELECT_SUCCESS(6203,"查询成功"),
    SELECTLIST_SUCCESS(6204,"查询列表成功"),
    UPDATE_FAILED(64001,"入库失败!"),
    NULL_INFO(64001,"空的信息"),
    ;
    private Integer code;
    private String msg;

    ProdecutNameEnmus(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ProdecutNameEnmus{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
