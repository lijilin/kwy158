package com.khwy158.hzhdemo.service;

import com.khwy158.hzhdemo.dto.ProductCityExecution;

/**
 * @author:Mr.胡
 * @date:Created at 2018/8/29
 *
 * 产品分类
 */
public interface productClassifyService {
    /**
     * 根据城市 查询列表
     * @return
     */
    public ProductCityExecution selectCityList();

}
