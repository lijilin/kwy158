package com.khwy158.hzhdemo.service.impl;

import com.khwy158.hzhdemo.dao.LdxCityMapper;
import com.khwy158.hzhdemo.dto.ProductCityExecution;
import com.khwy158.hzhdemo.enums.ProdecutNameEnmus;
import com.khwy158.hzhdemo.pojo.LdxCity;
import com.khwy158.hzhdemo.service.productClassifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author:Mr.胡
 * @date:Created at 2018/8/29
 */
@Service
public class productClassifyServiceImpl implements productClassifyService {

    @Autowired
    @SuppressWarnings("all")
    private LdxCityMapper mapper;

    /**
     * 城市列表数量
     * @return
     */
    @Override
    public ProductCityExecution selectCityList() {


//        List<LdxCity> ldxCities = mapper.selectAllCity();
        List<LdxCity> ldxCities = mapper.selectAllCity();
        System.out.println("********************"+ldxCities.size()+"********************");

        return new ProductCityExecution(ProdecutNameEnmus.SELECT_SUCCESS, ldxCities);
    }




}
