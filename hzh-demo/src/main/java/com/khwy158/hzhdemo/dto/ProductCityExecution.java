package com.khwy158.hzhdemo.dto;

import com.khwy158.hzhdemo.enums.ProdecutNameEnmus;
import com.khwy158.hzhdemo.pojo.LdxCity;

import java.util.List;

/**
 * @author:Mr.胡
 * @date:Created at 2018/8/29
 */
public class ProductCityExecution {

    private  String msg;

    private Integer code;

    private LdxCity Company;  // 客户表

    private List<LdxCity> list;

    /**
     * 失败
     * @param msg
     * @param code
     */
    public ProductCityExecution(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }

    /**
     * 成功
     * @param company
     */
    public ProductCityExecution(ProdecutNameEnmus enmus, LdxCity company) {
        this.msg = enmus.getMsg();
        this.code = enmus.getCode();
        Company = company;
    }

    /**
     * 成功
     * @param list
     */
    public ProductCityExecution(ProdecutNameEnmus enmus, List<LdxCity> list) {
        this.msg = enmus.getMsg();
        this.code = enmus.getCode();
        this.list = list;
    }

    public ProductCityExecution(String success, String s, List<LdxCity> mapperList) {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public LdxCity getCompany() {
        return Company;
    }

    public void setCompany(LdxCity company) {
        Company = company;
    }

    public List<LdxCity> getList() {
        return list;
    }

    public void setList(List<LdxCity> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ProductCityExecution{" +
                "msg='" + msg + '\'' +
                ", code=" + code +
                ", Company=" + Company +
                ", list=" + list +
                '}';
    }
}
