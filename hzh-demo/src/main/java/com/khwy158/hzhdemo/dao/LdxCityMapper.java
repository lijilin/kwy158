package com.khwy158.hzhdemo.dao;

import com.khwy158.hzhdemo.pojo.LdxCity;

import java.util.List;

public interface LdxCityMapper {

    /**
     * 查询所有
     * @param
     * @return
     */
    List<LdxCity> selectAllCity();

    /**
     * 查询2
     * @param id
     * @return
     */
    List<LdxCity> selectAllCity2(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int insert(LdxCity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int insertSelective(LdxCity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    LdxCity selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(LdxCity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ldx_city
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(LdxCity record);
}