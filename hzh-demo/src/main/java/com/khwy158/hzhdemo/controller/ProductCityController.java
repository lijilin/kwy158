package com.khwy158.hzhdemo.controller;

import com.khwy158.hzhdemo.dto.ProductCityExecution;
import com.khwy158.hzhdemo.service.productClassifyService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 分类页面
 */
@Controller
@RequestMapping("/hzh")
public class ProductCityController {

    @Autowired
    private productClassifyService service;

    /**
     * 查询城市列表
     * @return
     */
    @RequestMapping("/city_list")
    @ResponseBody
    public ResultDtoUtile selectCityList(){



        ProductCityExecution CityExecution = service.selectCityList();
        System.out.println(CityExecution.toString()+"####################3");


        return new ResultDtoUtile(CityExecution.getCode(),CityExecution.getMsg(),CityExecution.getList());
    }


}
