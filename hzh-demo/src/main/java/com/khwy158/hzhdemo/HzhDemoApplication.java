package com.khwy158.hzhdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.khwy158.hzhdemo.dao")
public class HzhDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HzhDemoApplication.class, args);
    }
}
