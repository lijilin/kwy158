package com.khwy158.wjjdemo.service;

import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.WjjChance;

public interface ChanceService {

    /**
     * 新建销售机会
     * @return
     */
    ResultDto chanceAdd(WjjChance wjjChance);

    /**
     * 成交可能性
     * @return
     */
    ResultDto chanceGrade();
    /**
     * 机会阶段
     * @return
     */
    ResultDto chanceStage();

    /**
     * 机会来源
     * @return
     */
    ResultDto chanceSource();
}
