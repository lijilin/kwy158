package com.khwy158.wjjdemo.service.impl;

import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.dto.wjj.ResultEnum;
import com.khwy158.modeldemo.pojo.wjj.WjjChance;
import com.khwy158.modeldemo.pojo.wjj.WjjChanceGrade;
import com.khwy158.modeldemo.pojo.wjj.WjjChanceSource;
import com.khwy158.modeldemo.pojo.wjj.WjjChanceStage;
import com.khwy158.wjjdemo.config.MyConfig;
import com.khwy158.wjjdemo.dao.WjjChanceGradeMapper;
import com.khwy158.wjjdemo.dao.WjjChanceMapper;
import com.khwy158.wjjdemo.dao.WjjChanceSourceMapper;
import com.khwy158.wjjdemo.dao.WjjChanceStageMapper;
import com.khwy158.wjjdemo.service.ChanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChanceServiceImpl implements ChanceService {

    @Autowired
    private MyConfig myConfig;
    /**
     * 机会表
     */
    @Autowired
    @SuppressWarnings("all")
    private WjjChanceMapper wcm;
    /**
     * 成交可能性Mapper
     */
    @Autowired
    @SuppressWarnings("all")
    private WjjChanceGradeMapper wcgm;
    /**
     * 来源表Mapper
     */
    @Autowired
    @SuppressWarnings("all")
    private WjjChanceSourceMapper wcsm;
    /**
     * 机会阶段mapper
     */
    @Autowired
    @SuppressWarnings("all")
    private WjjChanceStageMapper wcst;




    @Override
    public ResultDto chanceAdd(WjjChance wjjChance) {
        //关键信息不能为空
        if (wjjChance.getChanceName().isEmpty()
                ||wjjChance.getCompanyClient()==null
                ||wjjChance.getChanceStageId()==null){
            return new ResultDto(ResultEnum.NULL);
        }


        //模拟用户
        wjjChance.setRegisterId(myConfig.getAccount());
        //插入机会信息 到数据库
        int i = wcm.insertSelective(wjjChance);

        //失败
        if (i<=0){
            return new ResultDto(ResultEnum.LOSER);
        }

        //成功
        return new ResultDto(ResultEnum.SUCCESS);
    }

    @Override
    public ResultDto chanceGrade() {
        //数据库查询
        List<WjjChanceGrade> wjjChanceGrade = wcgm.selectByPrimaryKey();
        //失败
        if (wjjChanceGrade == null){
            return new ResultDto(ResultEnum.LOSER);
        }
        //成功
        return new ResultDto(ResultEnum.SUCCESS,wjjChanceGrade);
    }

    @Override
    public ResultDto chanceStage() {
        //数据库查询
        List<WjjChanceStage> wjjChanceStages = wcst.selectByPrimaryKey();
        //失败
        if (wjjChanceStages == null){
            return new ResultDto(ResultEnum.LOSER);
        }
        //成功
        return new ResultDto(ResultEnum.SUCCESS,wjjChanceStages);
    }

    @Override
    public ResultDto chanceSource() {
        //数据库查询
        List<WjjChanceSource> wjjChanceSources = wcsm.selectByPrimaryKey();
        //失败
        if (wjjChanceSources == null){
            return new ResultDto(ResultEnum.LOSER);
        }
        //成功
        return new ResultDto(ResultEnum.SUCCESS,wjjChanceSources);
    }
}
