package com.khwy158.wjjdemo.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.khwy158.modeldemo.dto.wjj.FenYeShuJu;
import com.khwy158.modeldemo.pojo.wjj.WjjComplain;
import com.khwy158.modeldemo.pojo.wjj.WjjComplainMode;
import com.khwy158.modeldemo.pojo.wjj.WjjComplainType;
import com.khwy158.modeldemo.utils.TimeUtil;
import com.khwy158.wjjdemo.dao.LdxCompanyClientMapper;
import com.khwy158.modeldemo.pojo.wjj.LdxCompanyClient;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.dto.wjj.ResultEnum;
import com.khwy158.wjjdemo.config.MyConfig;
import com.khwy158.wjjdemo.dao.WjjComplainMapper;
import com.khwy158.wjjdemo.dao.WjjComplainModeMapper;
import com.khwy158.wjjdemo.dao.WjjComplainTypeMapper;
import com.khwy158.wjjdemo.service.ComplainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: ComplainServiceImpl
 * @Author: wujunjie
 * @Description: ComplainService实现类
 * @Date: Created in 2018-8-28
 * @Modified By:
 **/
@Service
public class ComplainServiceImpl implements ComplainService {

    /**
     * 控制分页查询数量
     */
    @Autowired
    private MyConfig myConfig;
    /**
     * 客户mapper
     */
    @Autowired
    @SuppressWarnings("all")
    private LdxCompanyClientMapper lcm;
    /**
     * 投诉类型mapper
     */
    @Autowired
    @SuppressWarnings("all")
    private WjjComplainTypeMapper wctm;
    /**
     * 投诉方式mapper
     */
    @Autowired
    @SuppressWarnings("all")
    private WjjComplainModeMapper wcmm;
    /**
     * 新建投诉mapper
     */
    @Autowired
    @SuppressWarnings("all")
    private WjjComplainMapper wcm;

    @Override
    public ResultDto findByAll() {

        //查询所有的公司用户返回
        PageHelper.startPage(1,myConfig.getPage());
        List<LdxCompanyClient> companybyAll = lcm.findByAll(myConfig.getAccount());
        //判断数据库是否查询到了公司客户
        if(companybyAll==null){
            return new ResultDto(ResultEnum.LOSER);
        }
        //所有分页信息包装
        PageInfo pageInfo = new PageInfo(companybyAll);
        //成功返回
        return new ResultDto(ResultEnum.SUCCESS,pageInfo);
    }

    @Override
    public ResultDto complainType(){
        //数据库查找返回所有类型
        List<WjjComplainType> wjjComplainType = wctm.selectByPrimaryKey();
        //错误
        if (wjjComplainType==null){
            return new ResultDto(ResultEnum.LOSER);
        }
        //成功
        return new ResultDto(ResultEnum.SUCCESS,wjjComplainType);
    }

    @Override
    public ResultDto complainMode() {
        //数据库查找返回所有投诉方式
        List<WjjComplainMode> wjjComplainMode = wcmm.selectByPrimaryKey();
        //错误
        if (wjjComplainMode==null){
            return new ResultDto(ResultEnum.LOSER);
        }
        //成功
        return new ResultDto(ResultEnum.SUCCESS,wjjComplainMode);
    }

    @Override
    public FenYeShuJu complainAll(Integer page,Integer limit) {
        //查询所有的公司用户返回
        Page objects = PageHelper.startPage(page,limit);
        List<WjjComplain> wjjComplain = wcm.selectByPrimaryKey(myConfig.getAccount());
        //错误
        if (wjjComplain==null){
            return new FenYeShuJu(ResultEnum.LOSER);
        }

        //成功
        return new FenYeShuJu(0,"success",objects.getTotal(),wjjComplain);
    }

    @Override
    public ResultDto complainAdd(WjjComplain wjjComplain) {
        //判断关键信息是否为空
        if(wjjComplain.getComplainName()==null
                ||wjjComplain.getComplainTheme()==null
                ||wjjComplain.getComplainEnteringTime()==null
                ||wjjComplain.getComplainTypeId()==null){
            //关键信息为空
            return new ResultDto(ResultEnum.NULL);
        }
        //当前登录账号的ID
        wjjComplain.setRegisterId(myConfig.getAccount());
        //投诉录入时间
        wjjComplain.setComplainEnteringTime(TimeUtil.getTimeStr());
        //执行插入操作
        int complainInsert = wcm.insertSelective(wjjComplain);

        //判断是否插入成功
        if(complainInsert <= 0){
            //失败
            return new ResultDto(ResultEnum.LOSER);
        }


        //成功
        return new ResultDto(ResultEnum.SUCCESS);
    }




    
}
