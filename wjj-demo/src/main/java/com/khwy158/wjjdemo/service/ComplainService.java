package com.khwy158.wjjdemo.service;

import com.khwy158.modeldemo.dto.wjj.FenYeShuJu;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.WjjComplain;

/**
 * ClassName: ComplainService
 * Author: wujunjie
 * Description: ComplainService api
 * Date:Created in 2018/8/28
 * Modified By:
 **/
public interface ComplainService {
    /**
     * 返回所有的个人客户和企业客户
     * @return
     */
    ResultDto findByAll();

    /**
     * 返回所有投诉类型
     * @return
     */
    ResultDto complainType();

    /**
     * 返回所有投诉方式
     * @return
     */
    ResultDto complainMode();

    /**
     * 当前登录账号的所有投诉用户
     * @return
     */
    FenYeShuJu complainAll(Integer page,Integer limit);

    /**
     * 增加投诉用户
     * @return
     */
    ResultDto complainAdd(WjjComplain wjjComplain);
}
