package com.khwy158.wjjdemo.controller;

import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.WjjChance;
import com.khwy158.wjjdemo.service.ChanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChanceController {

    @Autowired
    private ChanceService cs;

    /**
     * 机会客户增加
     * @param wjjChance
     * @return
     */
    @PostMapping("/chance-add")
    public ResultDto chanceAdd(@RequestBody WjjChance wjjChance){
        //调用service方法进行插入返回
        ResultDto chanceAdd = cs.chanceAdd(wjjChance);
        //失败
        if (chanceAdd.getCode()!=5200){
            return chanceAdd ;
        }
        //成功
        return chanceAdd;
    }
    /**
     * 成交可能性返回接口
     * @return
     */
    @GetMapping("/chance-grade")
    public ResultDto chanceGrade(){

        ResultDto companyMode = cs.chanceGrade();
        //返回失败，没有数据，或者出错了
        if (companyMode.getCode() != 5200){
            return companyMode ;
        }
        //返回成功
        return companyMode;
    }
    /**
     * 来源返回接口
     * @return
     */
    @GetMapping("/chance-source")
    public ResultDto chanceSource(){

        ResultDto companyMode = cs.chanceSource();
        //返回失败，没有数据，或者出错了
        if (companyMode.getCode() != 5200){
            return companyMode ;
        }
        //返回成功
        return companyMode;
    }
    /**
     * 机会阶段返回接口
     * @return
     */
    @GetMapping("/chance-Stage")
    public ResultDto chanceStage(){
        //进行service方法查找所有公司客户
        ResultDto companyMode = cs.chanceStage();
        //返回失败，没有数据，或者出错了
        if (companyMode.getCode() != 5200){
            return companyMode ;
        }
        //返回成功
        return companyMode;
    }
}
