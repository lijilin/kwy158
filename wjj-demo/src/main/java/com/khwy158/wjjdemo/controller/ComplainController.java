package com.khwy158.wjjdemo.controller;

import com.khwy158.modeldemo.dto.wjj.FenYeShuJu;
import com.khwy158.modeldemo.dto.wjj.ResultDto;
import com.khwy158.modeldemo.pojo.wjj.WjjComplain;
import com.khwy158.wjjdemo.service.ComplainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author :投诉控制层
 */
@RestController
public class ComplainController {

    @Autowired
    private ComplainService cs;


    /**
     * 公司客户名称返回接口
     * @return
     */
    @RequestMapping("/company-all")
    public ResultDto company(){
        //进行service方法查找所有公司客户
        ResultDto byAll = cs.findByAll();
        //返回失败，没有数据，或者出错了
        if (byAll.getCode() != 5200){
            return byAll ;
        }
        //返回成功
        return byAll;
    }

    /**
     * 投诉类型返回接口
     * @return
     */
    @GetMapping("/complain-type")
    public ResultDto companyType(){
        //进行service方法查找所有公司客户
        ResultDto companyType = cs.complainType();
        //返回失败，没有数据，或者出错了
        if (companyType.getCode() != 5200){
            return companyType ;
        }
        //返回成功
        return companyType;
    }
    /**
     * 投诉方式返回接口
     * @return
     */
    @GetMapping("/complain-mode")
    public ResultDto companyMode(){
        //进行service方法查找所有公司客户
        ResultDto companyMode = cs.complainMode();
        //返回失败，没有数据，或者出错了
        if (companyMode.getCode() != 5200){
            return companyMode ;
        }
        //返回成功
        return companyMode;
    }

    /**
     * 投诉客户添加
     * @param wjjComplain
     * @return
     */
    @PostMapping("/complain-add")
    public ResultDto complainAdd(@RequestBody WjjComplain wjjComplain){
        //这里进行对当前登录用户的账号进行装填


        //进行service查询
        ResultDto complainAdd = cs.complainAdd(wjjComplain);
        //失败
        if (complainAdd.getCode()!=5200){
            return complainAdd ;
        }
        //成功
        return complainAdd;
    }

    /**
     * 所有当前登录用户的投诉客户
     * @return
     */
    @GetMapping("/complain-all")
    public FenYeShuJu companyAll(Integer page,Integer limit){
        //进行service方法查找所有公司客户
        FenYeShuJu fenYeShuJu = cs.complainAll(page,limit);
        //返回失败，没有数据，或者出错了
        if (fenYeShuJu.getCode() != 0){
            return fenYeShuJu ;
        }
        //返回成功
        return fenYeShuJu;
    }
}
