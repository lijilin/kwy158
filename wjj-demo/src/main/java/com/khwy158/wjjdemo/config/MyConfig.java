package com.khwy158.wjjdemo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName: MyConfig
 * @Author: wujunjie
 * @Description: TODO
 * @Date: Created in 2018-8-29
 * @Modified By:
 **/
@Component
@ConfigurationProperties(prefix = "myconfig")
public class MyConfig {
    /**
     * 分页数据
     */
    private int page;
    /**
     * 登陆用户账户
     */
    private int account;

    public MyConfig() {
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "MyConfig{" +
                "page=" + page +
                ", account='" + account + '\'' +
                '}';
    }
}
