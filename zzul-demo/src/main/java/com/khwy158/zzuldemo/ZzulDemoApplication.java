package com.khwy158.zzuldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class ZzulDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZzulDemoApplication.class, args);
    }
}
