package com.khwy158.ljdemo.dao;

import com.khwy158.ljdemo.pojo.LjStockin;

public interface LjStockinMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lj_stockin
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lj_stockin
     *
     * @mbg.generated
     */
    int insert(LjStockin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lj_stockin
     *
     * @mbg.generated
     */
    int insertSelective(LjStockin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lj_stockin
     *
     * @mbg.generated
     */
    LjStockin selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lj_stockin
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(LjStockin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lj_stockin
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(LjStockin record);
}