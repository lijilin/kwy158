package com.khwy158.ljdemo.service.impl;

import com.khwy158.ljdemo.dao.LjStockinMapper;
import com.khwy158.ljdemo.dto.StockinExecution;
import com.khwy158.ljdemo.enums.StockinStatusEnum;
import com.khwy158.ljdemo.pojo.LjStockin;
import com.khwy158.ljdemo.service.LjStockinService;
import com.khwy158.modeldemo.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:luojun
 * @date:2018/8/28
 */
@Service
public class LjStockinServiceImpl implements LjStockinService {

    @Autowired
    @SuppressWarnings("all")
    private LjStockinMapper lm;

    @Override
    public StockinExecution add(LjStockin ljStockin) {
        //判断数据是否为空
        if (ljStockin == null){
            return new StockinExecution(StockinStatusEnum.NULL_INFO);
        }
        //存入数据库
        int i = lm.insert(ljStockin);
        if (i > 0){
            //成功返回
            return new StockinExecution(StockinStatusEnum.CREATE_SUCCESS,ljStockin);
        }
        //失败返回
        return new StockinExecution(StockinStatusEnum.CREATE_FAILED);
    }
}
