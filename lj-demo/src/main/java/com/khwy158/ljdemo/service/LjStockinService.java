package com.khwy158.ljdemo.service;

import com.khwy158.ljdemo.dto.StockinExecution;
import com.khwy158.ljdemo.pojo.LjStockin;

public interface LjStockinService {

    /**
     * 入库
     * @param ljStockin
     * @return
     */
    StockinExecution add(LjStockin ljStockin);
}
