package com.khwy158.ljdemo.controller;

import com.khwy158.ljdemo.dto.StockinExecution;
import com.khwy158.ljdemo.pojo.LjStockin;
import com.khwy158.ljdemo.service.LjStockinService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author:luojun
 * @date:2018/8/28
 */
@Controller
public class StockinController {

    @Autowired
    private LjStockinService ls;

    /**
     * 入库
     * @param ljStockin
     * @return
     */
    @RequestMapping("/add.action")
    @ResponseBody
    public ResultDtoUtile addStock(@RequestBody LjStockin ljStockin, HttpServletRequest request){
        //获取前端选择的产品信息
        //获取入库数量
//        String nubmer = request.getParameter("nubmer");
//        Integer addnumber = Integer.parseInt(nubmer);
//        if (addnumber <= 0 || !(addnumber instanceof Integer)){
//            return new ResultDtoUtile(1106,"输入数据必须大于0的整数");
//        }
        //判断必要数据是否为空
        if (ljStockin.getTitle() == null || ljStockin.getInTime() == null
            || ljStockin.getPrincipalId() == null)
        {
            return new ResultDtoUtile(1105,"必要的信息为空");
        }
        //调用service
        StockinExecution stockinExecution = ls.add(ljStockin);
        if (stockinExecution.getCode() == 1100){
            Integer id = ljStockin.getId();
            System.out.println(id);
            //成功返回
            return new ResultDtoUtile(stockinExecution.getCode(),stockinExecution.getMsg(),stockinExecution.getLjStockin());
        }

        //失败返回
        return new ResultDtoUtile(stockinExecution.getCode(),stockinExecution.getMsg());

    }

}
