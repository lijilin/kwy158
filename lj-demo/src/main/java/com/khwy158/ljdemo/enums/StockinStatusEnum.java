package com.khwy158.ljdemo.enums;

/**
 * @author:luojun
 * @date:2018/8/28
 */
public enum  StockinStatusEnum {

    /**
     * 状态相关
     */
    CREATE_SUCCESS(1100,"创建成功"),
    GET_INFO_SUCCESS(1101,"获取信息成功!"),
    UPDATE_SUCCESS(1102,"更新成功!"),
    GET_INFO_FAILED(1103,"获取信息失败!"),
    CREATE_FAILED(1104,"创建失败"),
    NULL_REQUIRED_INFO(1105,"必要的信息为空"),
    NOT_MATCH(1006,"数据不符合要求"),
    NULL_INFO(1007,"空的信息"),
    UPDATE_FAILED(1108,"更新失败!");

    private Integer code;
    private String msg;

    StockinStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }
}
