package com.khwy158.ljdemo.dto;


import com.khwy158.ljdemo.enums.StockinStatusEnum;
import com.khwy158.ljdemo.pojo.LjStockin;

import java.util.List;

/**
 * @author:luojun
 * @date:2018/8/6
 */
public class StockinExecution {

    private Integer code;
    private String msg;
    private LjStockin ljStockin;
    private List<LjStockin> ljStockinList;

    /**
     * 失败的构造器
     * @param stockinStatusEnum
     */
    public StockinExecution(StockinStatusEnum stockinStatusEnum) {
        this.code = stockinStatusEnum.getCode();
        this.msg = stockinStatusEnum.getMsg();
    }

    /**
     * 成功的构造器
     * @param stockinStatusEnum
     */
    public StockinExecution(StockinStatusEnum stockinStatusEnum, LjStockin ljStockin) {
        this.code = stockinStatusEnum.getCode();
        this.msg = stockinStatusEnum.getMsg();
        this.ljStockin = ljStockin;
    }

    /**
     * 成功的构造器
     * @param stockinStatusEnum
     */
    public StockinExecution(StockinStatusEnum stockinStatusEnum, List<LjStockin> ljStockinList) {
        this.code = stockinStatusEnum.getCode();
        this.msg = stockinStatusEnum.getMsg();
        this.ljStockinList = ljStockinList ;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LjStockin getLjStockin() {
        return ljStockin;
    }

    public void setLjStockin(LjStockin ljStockin) {
        this.ljStockin = ljStockin;
    }

    public List<LjStockin> getStockinList() {
        return ljStockinList;
    }

    public void setStockinList(List<LjStockin> stockinList) {
        this.ljStockinList = stockinList;
    }
}

