package com.khwy158.modeldemo.pojo.dw;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table dw_log_chance
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class DwLogChance {
    /** 主键*/
    private Integer id;

    /** 用户id*/
    private Integer registerId;

    /** 员工id*/
    private Integer emplId;

    /** 日志表id关联工作日志表*/
    private Integer logId;

    /** 关联机会表*/
    private Integer chanceId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_chance.id
     *
     * @return the value of dw_log_chance.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_chance.id
     *
     * @param id the value for dw_log_chance.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_chance.register_id
     *
     * @return the value of dw_log_chance.register_id
     *
     * @mbg.generated
     */
    public Integer getRegisterId() {
        return registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_chance.register_id
     *
     * @param registerId the value for dw_log_chance.register_id
     *
     * @mbg.generated
     */
    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_chance.empl_id
     *
     * @return the value of dw_log_chance.empl_id
     *
     * @mbg.generated
     */
    public Integer getEmplId() {
        return emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_chance.empl_id
     *
     * @param emplId the value for dw_log_chance.empl_id
     *
     * @mbg.generated
     */
    public void setEmplId(Integer emplId) {
        this.emplId = emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_chance.log_id
     *
     * @return the value of dw_log_chance.log_id
     *
     * @mbg.generated
     */
    public Integer getLogId() {
        return logId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_chance.log_id
     *
     * @param logId the value for dw_log_chance.log_id
     *
     * @mbg.generated
     */
    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_chance.chance_id
     *
     * @return the value of dw_log_chance.chance_id
     *
     * @mbg.generated
     */
    public Integer getChanceId() {
        return chanceId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_chance.chance_id
     *
     * @param chanceId the value for dw_log_chance.chance_id
     *
     * @mbg.generated
     */
    public void setChanceId(Integer chanceId) {
        this.chanceId = chanceId;
    }
}