package com.khwy158.modeldemo.pojo.wjj;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table ldx_company_client
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class LdxCompanyClient {
    /** */
    private Integer id;

    /** 客户名称*/
    private String companyClientName;

    /** 所在城市ID*/
    private Integer cityId;

    /** 行业*/
    private String profession;

    /** 意向产品ID*/
    private Integer productId;

    /** 公司座机*/
    private String companyPhone;

    /** 客户状态*/
    private Integer clientStatusId;

    /** 客户类别*/
    private Integer clientCategoryId;

    /** 客户等级ID*/
    private Integer clientGradeId;

    /** 客户来源*/
    private String clientSource;

    /** 客户成熟度ID*/
    private Integer maturityId;

    /** 公司地址*/
    private String companyAddress;

    /** 公司客户介绍*/
    private String companyDesc;

    /** 获取企业客户时间*/
    private String acquisitionTime;

    /** 注册公司ID*/
    private Integer registerId;

    /** 所属员工*/
    private Integer emplId;

    /** 联系人姓名*/
    private String contactsName;

    /** 联系人性别0为男1为女默认0*/
    private Integer contactsSex;

    /** 联系人生日*/
    private String contactsBirthday;

    /** 联系人职务*/
    private String contactsJob;

    /** 联系人电话*/
    private Long contactsPhone;

    /** 联系人邮箱*/
    private String contactsEmail;

    /** 相关文档地址*/
    private String relatedDocuments;

    /** 公司客户还是个人客户0为公司1为个人客户*/
    private Integer clientComOrPerson;

    /** 个人客户所在公司*/
    private String personalClientCompany;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.id
     *
     * @return the value of ldx_company_client.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.id
     *
     * @param id the value for ldx_company_client.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.company_client_name
     *
     * @return the value of ldx_company_client.company_client_name
     *
     * @mbg.generated
     */
    public String getCompanyClientName() {
        return companyClientName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.company_client_name
     *
     * @param companyClientName the value for ldx_company_client.company_client_name
     *
     * @mbg.generated
     */
    public void setCompanyClientName(String companyClientName) {
        this.companyClientName = companyClientName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.city_id
     *
     * @return the value of ldx_company_client.city_id
     *
     * @mbg.generated
     */
    public Integer getCityId() {
        return cityId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.city_id
     *
     * @param cityId the value for ldx_company_client.city_id
     *
     * @mbg.generated
     */
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.profession
     *
     * @return the value of ldx_company_client.profession
     *
     * @mbg.generated
     */
    public String getProfession() {
        return profession;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.profession
     *
     * @param profession the value for ldx_company_client.profession
     *
     * @mbg.generated
     */
    public void setProfession(String profession) {
        this.profession = profession;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.product_id
     *
     * @return the value of ldx_company_client.product_id
     *
     * @mbg.generated
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.product_id
     *
     * @param productId the value for ldx_company_client.product_id
     *
     * @mbg.generated
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.company_phone
     *
     * @return the value of ldx_company_client.company_phone
     *
     * @mbg.generated
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.company_phone
     *
     * @param companyPhone the value for ldx_company_client.company_phone
     *
     * @mbg.generated
     */
    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.client_status_id
     *
     * @return the value of ldx_company_client.client_status_id
     *
     * @mbg.generated
     */
    public Integer getClientStatusId() {
        return clientStatusId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.client_status_id
     *
     * @param clientStatusId the value for ldx_company_client.client_status_id
     *
     * @mbg.generated
     */
    public void setClientStatusId(Integer clientStatusId) {
        this.clientStatusId = clientStatusId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.client_category_id
     *
     * @return the value of ldx_company_client.client_category_id
     *
     * @mbg.generated
     */
    public Integer getClientCategoryId() {
        return clientCategoryId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.client_category_id
     *
     * @param clientCategoryId the value for ldx_company_client.client_category_id
     *
     * @mbg.generated
     */
    public void setClientCategoryId(Integer clientCategoryId) {
        this.clientCategoryId = clientCategoryId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.client_grade_id
     *
     * @return the value of ldx_company_client.client_grade_id
     *
     * @mbg.generated
     */
    public Integer getClientGradeId() {
        return clientGradeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.client_grade_id
     *
     * @param clientGradeId the value for ldx_company_client.client_grade_id
     *
     * @mbg.generated
     */
    public void setClientGradeId(Integer clientGradeId) {
        this.clientGradeId = clientGradeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.client_source
     *
     * @return the value of ldx_company_client.client_source
     *
     * @mbg.generated
     */
    public String getClientSource() {
        return clientSource;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.client_source
     *
     * @param clientSource the value for ldx_company_client.client_source
     *
     * @mbg.generated
     */
    public void setClientSource(String clientSource) {
        this.clientSource = clientSource;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.maturity_id
     *
     * @return the value of ldx_company_client.maturity_id
     *
     * @mbg.generated
     */
    public Integer getMaturityId() {
        return maturityId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.maturity_id
     *
     * @param maturityId the value for ldx_company_client.maturity_id
     *
     * @mbg.generated
     */
    public void setMaturityId(Integer maturityId) {
        this.maturityId = maturityId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.company_address
     *
     * @return the value of ldx_company_client.company_address
     *
     * @mbg.generated
     */
    public String getCompanyAddress() {
        return companyAddress;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.company_address
     *
     * @param companyAddress the value for ldx_company_client.company_address
     *
     * @mbg.generated
     */
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.company_desc
     *
     * @return the value of ldx_company_client.company_desc
     *
     * @mbg.generated
     */
    public String getCompanyDesc() {
        return companyDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.company_desc
     *
     * @param companyDesc the value for ldx_company_client.company_desc
     *
     * @mbg.generated
     */
    public void setCompanyDesc(String companyDesc) {
        this.companyDesc = companyDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.acquisition_time
     *
     * @return the value of ldx_company_client.acquisition_time
     *
     * @mbg.generated
     */
    public String getAcquisitionTime() {
        return acquisitionTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.acquisition_time
     *
     * @param acquisitionTime the value for ldx_company_client.acquisition_time
     *
     * @mbg.generated
     */
    public void setAcquisitionTime(String acquisitionTime) {
        this.acquisitionTime = acquisitionTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.register_id
     *
     * @return the value of ldx_company_client.register_id
     *
     * @mbg.generated
     */
    public Integer getRegisterId() {
        return registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.register_id
     *
     * @param registerId the value for ldx_company_client.register_id
     *
     * @mbg.generated
     */
    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.empl_id
     *
     * @return the value of ldx_company_client.empl_id
     *
     * @mbg.generated
     */
    public Integer getEmplId() {
        return emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.empl_id
     *
     * @param emplId the value for ldx_company_client.empl_id
     *
     * @mbg.generated
     */
    public void setEmplId(Integer emplId) {
        this.emplId = emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.contacts_name
     *
     * @return the value of ldx_company_client.contacts_name
     *
     * @mbg.generated
     */
    public String getContactsName() {
        return contactsName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.contacts_name
     *
     * @param contactsName the value for ldx_company_client.contacts_name
     *
     * @mbg.generated
     */
    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.contacts_sex
     *
     * @return the value of ldx_company_client.contacts_sex
     *
     * @mbg.generated
     */
    public Integer getContactsSex() {
        return contactsSex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.contacts_sex
     *
     * @param contactsSex the value for ldx_company_client.contacts_sex
     *
     * @mbg.generated
     */
    public void setContactsSex(Integer contactsSex) {
        this.contactsSex = contactsSex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.contacts_birthday
     *
     * @return the value of ldx_company_client.contacts_birthday
     *
     * @mbg.generated
     */
    public String getContactsBirthday() {
        return contactsBirthday;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.contacts_birthday
     *
     * @param contactsBirthday the value for ldx_company_client.contacts_birthday
     *
     * @mbg.generated
     */
    public void setContactsBirthday(String contactsBirthday) {
        this.contactsBirthday = contactsBirthday;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.contacts_job
     *
     * @return the value of ldx_company_client.contacts_job
     *
     * @mbg.generated
     */
    public String getContactsJob() {
        return contactsJob;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.contacts_job
     *
     * @param contactsJob the value for ldx_company_client.contacts_job
     *
     * @mbg.generated
     */
    public void setContactsJob(String contactsJob) {
        this.contactsJob = contactsJob;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.contacts_phone
     *
     * @return the value of ldx_company_client.contacts_phone
     *
     * @mbg.generated
     */
    public Long getContactsPhone() {
        return contactsPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.contacts_phone
     *
     * @param contactsPhone the value for ldx_company_client.contacts_phone
     *
     * @mbg.generated
     */
    public void setContactsPhone(Long contactsPhone) {
        this.contactsPhone = contactsPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.contacts_email
     *
     * @return the value of ldx_company_client.contacts_email
     *
     * @mbg.generated
     */
    public String getContactsEmail() {
        return contactsEmail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.contacts_email
     *
     * @param contactsEmail the value for ldx_company_client.contacts_email
     *
     * @mbg.generated
     */
    public void setContactsEmail(String contactsEmail) {
        this.contactsEmail = contactsEmail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.related_documents
     *
     * @return the value of ldx_company_client.related_documents
     *
     * @mbg.generated
     */
    public String getRelatedDocuments() {
        return relatedDocuments;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.related_documents
     *
     * @param relatedDocuments the value for ldx_company_client.related_documents
     *
     * @mbg.generated
     */
    public void setRelatedDocuments(String relatedDocuments) {
        this.relatedDocuments = relatedDocuments;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.client_com_or_person
     *
     * @return the value of ldx_company_client.client_com_or_person
     *
     * @mbg.generated
     */
    public Integer getClientComOrPerson() {
        return clientComOrPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.client_com_or_person
     *
     * @param clientComOrPerson the value for ldx_company_client.client_com_or_person
     *
     * @mbg.generated
     */
    public void setClientComOrPerson(Integer clientComOrPerson) {
        this.clientComOrPerson = clientComOrPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ldx_company_client.personal_client_company
     *
     * @return the value of ldx_company_client.personal_client_company
     *
     * @mbg.generated
     */
    public String getPersonalClientCompany() {
        return personalClientCompany;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ldx_company_client.personal_client_company
     *
     * @param personalClientCompany the value for ldx_company_client.personal_client_company
     *
     * @mbg.generated
     */
    public void setPersonalClientCompany(String personalClientCompany) {
        this.personalClientCompany = personalClientCompany;
    }
}