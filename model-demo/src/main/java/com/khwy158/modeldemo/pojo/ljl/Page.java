package com.khwy158.modeldemo.pojo.ljl;

/**
 * 描述:
 *
 * @author lijilin
 * @create 2018-08-31 11:07
 */
public class Page {

    private Integer page;

    private Integer pageSzie;

    public Page() {
    }

    public Page(Integer page, Integer pageSzie) {
        this.page = page;
        this.pageSzie = pageSzie;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSzie() {
        return pageSzie;
    }

    public void setPageSzie(Integer pageSzie) {
        this.pageSzie = pageSzie;
    }

    @Override
    public String toString() {
        return "Page{" +
                "page=" + page +
                ", pageSzie=" + pageSzie +
                '}';
    }
}