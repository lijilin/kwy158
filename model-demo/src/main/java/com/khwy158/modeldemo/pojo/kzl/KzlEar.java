package com.khwy158.modeldemo.pojo.kzl;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table kzl_ear
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class KzlEar {
    /** */
    private Integer id;

    /** 收支类型*/
    private String earType;

    /** 收支金额*/
    private Double earSum;

    /** 收支日期*/
    private String earTime;

    /** 收支客户名字*/
    private String earCusName;

    /** 收支名称*/
    private String earName;

    /** 说明*/
    private String earExplain;

    /** 员工id*/
    private Integer emplId;

    /** 注册id*/
    private Integer registerId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.id
     *
     * @return the value of kzl_ear.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.id
     *
     * @param id the value for kzl_ear.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.ear_type
     *
     * @return the value of kzl_ear.ear_type
     *
     * @mbg.generated
     */
    public String getEarType() {
        return earType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.ear_type
     *
     * @param earType the value for kzl_ear.ear_type
     *
     * @mbg.generated
     */
    public void setEarType(String earType) {
        this.earType = earType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.ear_sum
     *
     * @return the value of kzl_ear.ear_sum
     *
     * @mbg.generated
     */
    public Double getEarSum() {
        return earSum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.ear_sum
     *
     * @param earSum the value for kzl_ear.ear_sum
     *
     * @mbg.generated
     */
    public void setEarSum(Double earSum) {
        this.earSum = earSum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.ear_time
     *
     * @return the value of kzl_ear.ear_time
     *
     * @mbg.generated
     */
    public String getEarTime() {
        return earTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.ear_time
     *
     * @param earTime the value for kzl_ear.ear_time
     *
     * @mbg.generated
     */
    public void setEarTime(String earTime) {
        this.earTime = earTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.ear_cus_name
     *
     * @return the value of kzl_ear.ear_cus_name
     *
     * @mbg.generated
     */
    public String getEarCusName() {
        return earCusName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.ear_cus_name
     *
     * @param earCusName the value for kzl_ear.ear_cus_name
     *
     * @mbg.generated
     */
    public void setEarCusName(String earCusName) {
        this.earCusName = earCusName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.ear_name
     *
     * @return the value of kzl_ear.ear_name
     *
     * @mbg.generated
     */
    public String getEarName() {
        return earName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.ear_name
     *
     * @param earName the value for kzl_ear.ear_name
     *
     * @mbg.generated
     */
    public void setEarName(String earName) {
        this.earName = earName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.ear_explain
     *
     * @return the value of kzl_ear.ear_explain
     *
     * @mbg.generated
     */
    public String getEarExplain() {
        return earExplain;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.ear_explain
     *
     * @param earExplain the value for kzl_ear.ear_explain
     *
     * @mbg.generated
     */
    public void setEarExplain(String earExplain) {
        this.earExplain = earExplain;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.empl_id
     *
     * @return the value of kzl_ear.empl_id
     *
     * @mbg.generated
     */
    public Integer getEmplId() {
        return emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.empl_id
     *
     * @param emplId the value for kzl_ear.empl_id
     *
     * @mbg.generated
     */
    public void setEmplId(Integer emplId) {
        this.emplId = emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_ear.register_id
     *
     * @return the value of kzl_ear.register_id
     *
     * @mbg.generated
     */
    public Integer getRegisterId() {
        return registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_ear.register_id
     *
     * @param registerId the value for kzl_ear.register_id
     *
     * @mbg.generated
     */
    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }
}