package com.khwy158.modeldemo.utils;

/**
 * 描述:
 *
 * @author lijilin
 * @create 2018-08-30 11:34
 */
public class ResultEexptionDto {

    private Integer code;

    private String msg;

    private Long count;

    private Object data;

    public ResultEexptionDto() {
    }

    public ResultEexptionDto(Integer code, String msg, Long count, Object data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}