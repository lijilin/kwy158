package com.khwy158.modeldemo.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author:Teacher黄
 * @date:Created at 2018/07/22
 */
public class TimeUtil {

    /**
     * 获取当前时间字符串
     * @return
     */
    public static String getTimeStr(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }
}
