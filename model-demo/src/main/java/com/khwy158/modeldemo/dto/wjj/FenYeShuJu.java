package com.khwy158.modeldemo.dto.wjj;

import com.khwy158.modeldemo.pojo.wjj.WjjComplain;

import java.util.List;

/**
 * @ClassName: FenYeShuJu
 * @Author: wujunjie
 * @Description: TODO
 * @Date: Created in 2018-8-31
 * @Modified By:
 **/
public class FenYeShuJu {
    /**
     * 状态
     */
    private Integer code;
    /**
     * 信息
     */
    private String msg;
    /**
     * 数据
     */
    private List<WjjComplain> data;
    /**
     * 总条数
     */
    private Long count;

    public FenYeShuJu() {
    }

    public FenYeShuJu(Integer code, String msg, Long count ,List<WjjComplain> data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }

    public FenYeShuJu(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<WjjComplain> getData() {
        return data;
    }

    public void setData(List<WjjComplain> data) {
        this.data = data;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "FenYeShuJu{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", count=" + count +
                '}';
    }
}
