package com.khwy158.modeldemo.dto.wjj;

/**
 * wjj
 * 2018-8-28
 * 用于返回，所有的状态码
 */
public enum ResultEnum {
    //返回成功
    SUCCESS(5200,"success"),
    LOSER(5201,"loser"),
    NULL(5202,"关键信息不能为空")
    ;
    /**
     * 状态码
     */
    private Integer code;

    /**
     * 状态信息
     */
    private String msg;


    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
