package com.khwy158.modeldemo.dto.gd;


public class JsonResult {

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 信息
     */
    private String msg;
    /**
     * 总条数
     */
    private Long count;
    /**
     * 数据
     */
    private Object data;

    public JsonResult() {
    }

    /**
     * 失败构造器
     * @param code
     * @param msg
     */
    public JsonResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 成功的构造器
     * @param code
     * @param msg
     * @param data
     */
    public JsonResult(Integer code, String msg, Long count, Object data) {
        this.code = code;
        this.msg = msg;
        this.count=count;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
