package com.khwy158.modeldemo.dto;

import java.util.List;
import java.util.Map;

/**
 * 公共的对象传输类
 * wjj:2018-8-28
 */
public class ResultDtoUtile {
    /**
     * code:状态码
     */
    private Integer code;
    /**
     * msg:状态码信息
     */
    private String msg;
    /**
     * obj:单个对象返回
     */
    private Object obj;
    /**
     * listobj: 多个对象返回
     */
    private List<Object> listobj;
    /**
     * mapobj: map 对象返回
     */
    private Map<String,Object> mapobj;

    /**
     * 失败返回函数
     * @param code
     * @param msg
     */
    public ResultDtoUtile(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultDtoUtile() {
    }

    /**
     * 成功单个对象返回函数
     * @param code
     * @param msg
     * @param obj
     */
    public ResultDtoUtile(Integer code, String msg, Object obj) {
        this.code = code;
        this.msg = msg;
        this.obj = obj;
    }

    /**
     * 成功多个对象返回
     * @param code
     * @param msg
     * @param listobj
     */
    public ResultDtoUtile(Integer code, String msg, List<Object> listobj) {
        this.code = code;
        this.msg = msg;
        this.listobj = listobj;
    }

    /**
     * 成都Map对象返回
     * @param code
     * @param msg
     * @param mapobj
     */
    public ResultDtoUtile(Integer code, String msg, Map<String, Object> mapobj) {
        this.code = code;
        this.msg = msg;
        this.mapobj = mapobj;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public List<Object> getListobj() {
        return listobj;
    }

    public void setListobj(List<Object> listobj) {
        this.listobj = listobj;
    }

    public Map<String, Object> getMapobj() {
        return mapobj;
    }

    public void setMapobj(Map<String, Object> mapobj) {
        this.mapobj = mapobj;
    }


    @Override
    public String toString() {
        return "ResultDtoUtile{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", obj=" + obj +
                ", listobj=" + listobj +
                ", mapobj=" + mapobj +
                '}';
    }
}
