package com.khwy158.modeldemo.dto.wjj;

import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * @Author: wjj:2018-8-28
 *
 */
public class ResultDto {
    /**
     * code:状态码
     */
    private Integer code;
    /**
     * msg:状态码信息
     */
    private String msg;
    /**
     * obj:单个对象返回
     */
    private Object obj;
    /**
     * listobj: 多个对象返回
     */
    private List<Object> listobj;
    /**
     * mapobj: map 对象返回
     */
    private Map<String,Object> mapobj;

    /**
     * 返回分页数据 包括数据
     */
    private PageInfo pageInfo;

    public ResultDto() {

    }

    /**
     * 成功返回分页数据
     * @param code
     * @param msg
     * @param pageInfo
     */
    public ResultDto(Integer code, String msg, PageInfo pageInfo) {
        this.code = code;
        this.msg = msg;
        this.pageInfo = pageInfo;
    }

    /**
     * 失败返回函数
     * @param resultEnum
     */
    public ResultDto(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
    }

    /**
     * 成功单个对象返回函数
     * @param resultEnum
     * @param obj
     */
    /**
     * 失败返回函数
     * @param resultEnum
     */
    public ResultDto(ResultEnum resultEnum,Object obj) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.obj = obj;
    }

    /**
     * 成功多个对象返回
     * @param resultEnum
     * @param listobj
     */
    public ResultDto(ResultEnum resultEnum, List<Object> listobj) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.listobj = listobj;
    }

    /**
     * 成都Map对象返回
     * @param resultEnum
     * @param mapobj
     */
    public ResultDto(ResultEnum resultEnum, Map<String, Object> mapobj) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.mapobj = mapobj;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public List<Object> getListobj() {
        return listobj;
    }

    public void setListobj(List<Object> listobj) {
        this.listobj = listobj;
    }

    public Map<String, Object> getMapobj() {
        return mapobj;
    }

    public void setMapobj(Map<String, Object> mapobj) {
        this.mapobj = mapobj;
    }


    @Override
    public String toString() {
        return "ResultDto{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", obj=" + obj +
                ", listobj=" + listobj +
                ", mapobj=" + mapobj +
                '}';
    }
}
