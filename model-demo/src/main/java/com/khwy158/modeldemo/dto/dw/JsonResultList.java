package com.khwy158.modeldemo.dto.dw;

/**
 * @author:Teacher黄
 * @date:Created at 2018/07/23
 */
public class JsonResultList {

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 信息
     */
    private String msg;
    private int count;
    /**
     * 数据
     */
    private Object data;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 失败构造器
     * @param code
     * @param msg
     */
    public JsonResultList(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public JsonResultList() {
    }

    /**
     * 成功的构造器
     * @param code
     * @param msg
     * @param data
     */
    public JsonResultList(Integer code, String msg, int count, Object data) {
        this.code = code;
        this.msg = msg;
        this.count=count;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
