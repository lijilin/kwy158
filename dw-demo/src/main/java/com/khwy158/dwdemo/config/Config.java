package com.khwy158.dwdemo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author:Teacher黄
 * @date:Created at 2018/07/24
 */
@Component
@PropertySource("classpath:config.properties")
public class Config {

    /**
     * 上传文件的路径
     */
    @Value("${file.upload.path}")
    private String path;

    @Value("${baiduapi.url}")
    private String url;
    @Value("${baiduapi.ak}")
    private String ak;

    public Config() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }
}
