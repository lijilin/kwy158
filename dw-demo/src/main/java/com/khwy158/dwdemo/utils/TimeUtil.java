package com.khwy158.dwdemo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author:Teacher黄
 * @date:Created at 2018/07/22
 */
public class TimeUtil {

    /**
     * 获取当前时间字符串
     * @return
     */
    public static String getTimeStr(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }
    /**
     * 日期转时间戳
     * @param date
     *
     * @return
     */
    public static String dateTotimeStamp(String date){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return String.valueOf(sdf.parse(date).getTime()/1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  "";
    }
    /**
     * 获取时间戳 单位秒
     * @return
     */
    public static long getTimeStamp(){
        Date date = new Date();
        return date.getTime()/1000;
    }
}
