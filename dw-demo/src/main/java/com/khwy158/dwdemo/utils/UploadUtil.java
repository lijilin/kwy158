package com.khwy158.dwdemo.utils;

import com.netflix.client.http.HttpResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author:Teacher黄
 * @date:Created at 2018/07/24
 */
public class UploadUtil {


    /**
     * 文件上传的方法
     *
     * @param file       : 文件
     * @param memberId   : 会员的id
     * @param uploadPath : 文件上传的根路径
     * @return
     */
    public static String upload(MultipartFile file, Integer memberId, String uploadPath) {
        if (file.isEmpty()) {
            //为空就直接返回null
            return null;
        }
        // 得到文件名字
        String fileName = file.getOriginalFilename();
        // 得到后缀
        String ext = fileName.substring(fileName.lastIndexOf("."));

        // 装一个唯一的名字
        String uniqueName = UUID.randomUUID().toString().replaceAll("-", "");

        //组装一个上传路径
        String temPath = "member/header/" + memberId + "/";

        String dbPath = temPath + uniqueName + ext;

        String upPath = uploadPath + dbPath;
        // 创建文件夹
        createDir(uploadPath + temPath);

        //抽象出需要上传的文件对象
        File upFile = new File(upPath);

        //上传
        try {
            file.transferTo(upFile);

            return dbPath;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 创建文件夹的方法
     *
     * @param path
     */
    private static void createDir(String path) {
        // 抽象出一个文件对象
        File file = new File(path);

        // 判断是否存在
        if (!file.exists()) {
            //递归的创建所有的文件夹
            file.mkdirs();
        }

    }

    /**
     * 外部创建文件
     *
     * @param uploadPath
     * @param memberId
     * @return
     */
    public static String create(String uploadPath, Integer memberId) {
        //组装一个上传路径
        String uniqueName = UUID.randomUUID().toString().replaceAll("-", "");
        String temPath = "member/header/" + memberId + "/" + uniqueName + "/";
        // 抽象出一个文件对象
        File file = new File(uploadPath + temPath);
        // 判断是否存在
        if (!file.exists()) {
            //递归的创建所有的文件夹
            file.mkdirs();
        }
        return temPath;

    }

    /**
     * 计算文件大小
     *
     * @param file
     * @return
     */
    public static String getFileSize(MultipartFile file) {
        DecimalFormat df1 = new DecimalFormat("0.00");
        String fileSizeString = "";
        long fileSize = file.getSize();
        if (fileSize < 1024) {
            fileSizeString = df1.format((double) fileSize) + "B";
        } else if (fileSize < 1048576) {
            fileSizeString = df1.format((double) fileSize / 1024) + "K";
        } else if (fileSize < 1073741824) {
            fileSizeString = df1.format((double) fileSize / 1048576) + "M";
        } else {
            fileSizeString = df1.format((double) fileSize / 1073741824) + "G";
        }
        return fileSizeString;
    }

    /**
     * 下载文件
     * @param path 文件的路径
     * @param name  文件的名字
     * @param res   HttpServletResponse
     */
    public static void downLoad(String path, String name, HttpServletResponse res) {

        File file = new File(path);
        res.setHeader("content-type", "application/octet-stream");
        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=" + name);
        byte[] buff = new byte[1024];
        BufferedInputStream bis = null;
        OutputStream os = null;
        try {
            os = res.getOutputStream();
            bis = new BufferedInputStream(new FileInputStream(file));
            int i = bis.read(buff);
            while (i != -1) {
                os.write(buff, 0, buff.length);
                os.flush();
                i = bis.read(buff);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static String moveFile(String startPath,String endPath){
        File file=new File(startPath);
        String fileName=file.getName();
        //判断目标是否是一个文件
        File endFile=new File(endPath);
        if (endFile.isFile()){
            System.out.println("目标路径是个文件，请检查目标路径！");
            return null;
        }

        //移动文件

        System.out.println(endPath + fileName);
        boolean b = file.renameTo(new File(endPath + fileName));
        return endPath + fileName;

    }
}