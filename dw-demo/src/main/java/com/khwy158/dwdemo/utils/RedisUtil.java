package com.khwy158.dwdemo.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author:Teacher黄
 * @date:Created at 2018/08/02
 */
@Component
public class RedisUtil {

    @Autowired
    private StringRedisTemplate redis;


    /**
     * 设置字符串
     * @param key
     * @param value
     * @return
     */
    public Boolean set(String key,String value){

        try{
            redis.opsForValue().set(key,value);
            return true;
        }catch (Exception e){
            return false;
        }
    }


    /**
     * 获取字符串
     * @param key
     * @return
     */
    public String get(String key){
        if(key == null){
            return null;
        }

        return redis.opsForValue().get(key);
    }


    /**
     * 设置过期的时间
     * @param key
     * @param time
     * @return
     */
    public Boolean expire(String key,Long time){
        if(time <= 0){
            return false;
        }

        redis.expire(key,time,TimeUnit.SECONDS);
        return true;
    }
    public Boolean delete(String key){
       return redis.delete(key);
    }




}
