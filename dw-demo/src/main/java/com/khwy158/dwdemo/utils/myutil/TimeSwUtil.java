package com.khwy158.dwdemo.utils.myutil;


import com.khwy158.dwdemo.utils.TimeUtil;

/**
 * @program: depot-hx
 * @description: 用于时间的转换
 * @author: Mr.Wang
 * @create: 2018-08-27 20:21
 **/
public class TimeSwUtil {
    public static String getTimefor(String time){
        long s = Long.valueOf(TimeUtil.dateTotimeStamp(time));
        long timeStr = TimeUtil.getTimeStamp();
        long t=timeStr-s;
        if (t<=10){
            return "刚刚";
        }else if(t<60){
            return t+"秒以前";
        }else if (t<3600){
            return t/60 +"分钟以前";
        }else {
            return time;
        }

    }


}
