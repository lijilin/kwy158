package com.khwy158.dwdemo.pojo;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table dw_log_document
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class DwLogDocument {
    /** 主键*/
    private Integer id;

    /** 用户id*/
    private Integer registerId;

    /** 员工id*/
    private Integer emplId;

    /** 日志表id关联工作日志表*/
    private Integer logId;

    /** 文档表id关联文档表*/
    private Integer documentId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_document.id
     *
     * @return the value of dw_log_document.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_document.id
     *
     * @param id the value for dw_log_document.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_document.register_id
     *
     * @return the value of dw_log_document.register_id
     *
     * @mbg.generated
     */
    public Integer getRegisterId() {
        return registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_document.register_id
     *
     * @param registerId the value for dw_log_document.register_id
     *
     * @mbg.generated
     */
    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_document.empl_id
     *
     * @return the value of dw_log_document.empl_id
     *
     * @mbg.generated
     */
    public Integer getEmplId() {
        return emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_document.empl_id
     *
     * @param emplId the value for dw_log_document.empl_id
     *
     * @mbg.generated
     */
    public void setEmplId(Integer emplId) {
        this.emplId = emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_document.log_id
     *
     * @return the value of dw_log_document.log_id
     *
     * @mbg.generated
     */
    public Integer getLogId() {
        return logId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_document.log_id
     *
     * @param logId the value for dw_log_document.log_id
     *
     * @mbg.generated
     */
    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dw_log_document.document_id
     *
     * @return the value of dw_log_document.document_id
     *
     * @mbg.generated
     */
    public Integer getDocumentId() {
        return documentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dw_log_document.document_id
     *
     * @param documentId the value for dw_log_document.document_id
     *
     * @mbg.generated
     */
    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }
}