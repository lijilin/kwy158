package com.khwy158.dwdemo.eum;

public enum NoticeEnum {
    NULLMESSAGE(9400,"必要信息为空"),
    SUCCESS(200,"成功"),
    FAIL(9401,"失败"),
    ;
    private Integer code;
    private String msg;

    NoticeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
