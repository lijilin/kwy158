package com.khwy158.dwdemo.controller;

import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.pojo.DwNotice;
import com.khwy158.dwdemo.service.NoticeService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: khwy-158
 * @description: 公告的控制器
 * @author: Mr.Wang
 * @create: 2018-08-28 16:31
 **/
@RestController
public class AdviceController {
    @Autowired
    NoticeService service;
    @RequestMapping("/advice/post")
    public ResultDtoUtile send(@RequestBody DwNotice dwNotice){
        ResultDtoUtile resul = service.sendNotice(dwNotice);
        return resul;
    }

    @RequestMapping("/advice/get")
    public JsonResultList list(@RequestParam Integer page, @RequestParam Integer limit){
        JsonResultList jsonResultList = service.noticeList(page, limit, 10, 1);
        return jsonResultList;
    }
    @RequestMapping("/advice/delete")
    public ResultDtoUtile delete(@RequestBody List<Integer> list){
        System.out.println(list.size());
        return service.removeNotice(list);

    }

    @RequestMapping("/advice/get/{id}")
    public ResultDtoUtile find(@PathVariable Integer id){
        return service.findNoticeOne(id);
    }

    @RequestMapping("/advice/put")
    public ResultDtoUtile update(@RequestBody DwNotice dwNotice ){
        return service.updateNotice(dwNotice);

    }
}
