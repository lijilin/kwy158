package com.khwy158.dwdemo.controller;

import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.pojo.DwLog;
import com.khwy158.dwdemo.pojo.DwNotice;
import com.khwy158.dwdemo.service.LogService;
import com.khwy158.dwdemo.service.NoticeService;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: khwy-158
 * @description: 日志的控制器
 * @author: Mr.Wang
 * @create: 2018-08-28 16:31
 **/
@RestController
public class LogController {
    @Autowired
    LogService service;
    @RequestMapping("/log/post")
    public ResultDtoUtile send(@RequestBody DwLog dwlog){
        //先获取登录信息
        dwlog.setRegisterId(10);
        dwlog.setEmplId(10);
        ResultDtoUtile resul = service.sendLog(dwlog);
        return resul;
    }

//    @RequestMapping("/advice/get")
//    public JsonResultList list(@RequestParam Integer page, @RequestParam Integer limit){
//        JsonResultList jsonResultList = service.noticeList(page, limit, 10, 1);
//        return jsonResultList;
//    }
//    @RequestMapping("/advice/delete")
//    public ResultDtoUtile delete(@RequestBody List<Integer> list){
//        System.out.println(list.size());
//        return service.removeNotice(list);
//
//    }
//
//    @RequestMapping("/advice/get/{id}")
//    public ResultDtoUtile find(@PathVariable Integer id){
//        return service.findNoticeOne(id);
//    }
//
//    @RequestMapping("/advice/put")
//    public ResultDtoUtile update(@RequestBody DwNotice dwNotice ){
//        return service.updateNotice(dwNotice);
//
//    }
}
