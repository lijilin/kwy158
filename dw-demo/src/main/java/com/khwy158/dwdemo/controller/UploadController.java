package com.khwy158.dwdemo.controller;

import com.khwy158.dwdemo.config.Config;
import com.khwy158.dwdemo.dao.DwDocumentMapper;
import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.pojo.DwDocument;
import com.khwy158.dwdemo.service.DocumentService;
import com.khwy158.dwdemo.utils.TimeUtil;
import com.khwy158.dwdemo.utils.UploadUtil;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;


/**
 * @program: khwy-158
 * @description: 文档上传下载
 * @author: Mr.Wang
 * @create: 2018-08-30 10:13
 **/
@RestController
public class UploadController {
    @Autowired
    Config config;
    @Autowired
    @SuppressWarnings("all")
    DwDocumentMapper dwDocumentMapper;
    @Autowired
    DocumentService service;

    /**
     * 上传文件
     * @param pic 上传的文件
     * @param ispublic 是否是公共文档的
     * @return
     */
    @RequestMapping("/upload")
    @ResponseBody
    public ResultDtoUtile upload(@RequestParam(value = "file", required = false) MultipartFile pic, @RequestParam(value = "ispublic") Integer ispublic) {
        //获得初识值：两个id
        System.out.println(ispublic);
        System.out.println(pic.getOriginalFilename());
        String path = UploadUtil.upload(pic, 10, config.getPath());
        //存数据库
        //准备一个对像装数据
        DwDocument dwDocument = new DwDocument();
        dwDocument.setRegisterId(10);
        dwDocument.setEmplId(10);
        dwDocument.setFileName(pic.getOriginalFilename());
        dwDocument.setExpandedName(path);
        dwDocument.setPath(path);
        dwDocument.setIsfile(1);
        dwDocument.setIspublicDocument(ispublic);
        dwDocument.setLastTime(TimeUtil.getTimeStr());
        dwDocument.setParentId(0);
        dwDocument.setFileSize(UploadUtil.getFileSize(pic));
        //通过后缀获取类型
        String type = pic.getOriginalFilename().substring(pic.getOriginalFilename().lastIndexOf(".") + 1) + "格式";
        dwDocument.setType(type);
        dwDocumentMapper.insertSelective(dwDocument);
        return new ResultDtoUtile(200, "success");
    }

    /**
     * 创建文件夹
     * @param name 文件夹名字
     * @param ispublic 是否是公共文档那一块的
     * @return
     */
    @RequestMapping("/file")
    public ResultDtoUtile file(@RequestParam(value = "name") String name, @RequestParam(value = "ispublic") Integer ispublic) {
        //获取初始数据
        String path = UploadUtil.create(config.getPath(), 10);
        DwDocument dwDocument = new DwDocument();
        dwDocument.setRegisterId(10);
        dwDocument.setEmplId(10);
        dwDocument.setFileName(name);
        dwDocument.setExpandedName(path);
        dwDocument.setPath(path);
        dwDocument.setIsfile(0);
        dwDocument.setIspublicDocument(ispublic);
        dwDocument.setLastTime(TimeUtil.getTimeStr());
        dwDocument.setParentId(0);
        dwDocument.setType("文件夹");
        dwDocumentMapper.insertSelective(dwDocument);
        return new ResultDtoUtile(200, "success");
    }

    /**
     * 下载
     * @param id 要下载文件的id
     * @param
     * @throws IOException
     */
    @RequestMapping(value = "/testDownload", method = RequestMethod.GET)
    public DwDocument testDownload(@RequestParam Integer id)  {
        DwDocument dwDocument = dwDocumentMapper.selectByPrimaryKey(id);
        return dwDocument;
//        String path=config.getPath()+dwDocument.getPath();
//        String name=dwDocument.getFileName();
//        UploadUtil.downLoad(path,name,res);
    }

    /**
     * 移动文件夹
     * @param ids 要被移动的文件的id
     * @param id  移动的目的地的id
     * @return
     */
    @RequestMapping(value = "/move", method = RequestMethod.POST)
    public ResultDtoUtile move(@RequestBody List<Integer> ids,@RequestParam(value = "id")Integer id ){
        //获得目标文件夹路径
        System.out.println(ids.size());
        DwDocument dwDocument = dwDocumentMapper.selectByPrimaryKey(id);
        String endPath=config.getPath()+dwDocument.getPath();
        System.out.println(endPath);
        String startPath;
        for (Integer startId : ids) {
            DwDocument document=dwDocumentMapper.selectByPrimaryKey(startId);
            startPath=config.getPath()+document.getPath();

            String b = UploadUtil.moveFile(startPath, endPath);
            if (b==null){
                return new ResultDtoUtile(4040,"移动失败了");
            }
            //修改文件的父级目录id
            document.setParentId(dwDocument.getId());
            //修改路径
            document.setPath(b);
            //修改时间
            document.setLastTime(TimeUtil.getTimeStr());
            //执行更改操作
            dwDocumentMapper.updateByPrimaryKeySelective(document);
            }
        return new ResultDtoUtile(200,"成功了");
    }

    /**
     *
     * @param page
     * @param limit
     * @param mid
     * @param ispublic
     * @return
     */
    @RequestMapping("/docmy/list")
    public JsonResultList listd(Integer page,Integer limit,Integer mid,Integer ispublic,Integer parentid){
        System.out.println(ispublic);
        JsonResultList getlist = service.getlist(page, limit, mid, ispublic,parentid);
        return getlist;
    }


}
