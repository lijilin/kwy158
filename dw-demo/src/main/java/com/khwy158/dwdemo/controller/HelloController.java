package com.khwy158.dwdemo.controller;

import com.khwy158.dwdemo.dao.DwNoticeMapper;
import com.khwy158.dwdemo.pojo.DwNotice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: khwy-158
 * @description: sadsa
 * @author: Mr.Wang
 * @create: 2018-08-28 11:03
 **/
@RestController
public class HelloController {
    @Autowired
            @SuppressWarnings("all")
    DwNoticeMapper mapper;

    @RequestMapping("/hello")
    public List<Integer> hello1(@RequestBody List<Integer> lists){

        return lists;
    }
}
