package com.khwy158.dwdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @program: khwy-158
 * @description: 请求page的
 * @author: Mr.Wang
 * @create: 2018-08-30 10:43
 **/
@Controller
public class PageController {

    @RequestMapping("/dw/uploadpg")
    public String upPage(){
    return "dw-page/upload";

    }
}
