package com.khwy158.dwdemo.service;

import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.pojo.DwLog;
import com.khwy158.dwdemo.pojo.DwNotice;
import com.khwy158.modeldemo.dto.ResultDtoUtile;

import java.util.List;

/**
 * @program: khwy-158
 * @description: 日志
 * @author: Mr.Wang
 * @create: 2018-08-28 14:59
 **/

public interface LogService {
    /**
     * 发日志的方法
     * @param dwLog
     * @return
     */
    ResultDtoUtile sendLog(DwLog dwLog);

    /**
     * 我创建的日志列表直接查日志表
     * @param page
     * @param limit
     * @param qid
     * @param mid
     * @return
     */
    JsonResultList logMyList(Integer page, Integer limit, Integer qid, Integer mid);

    /**
     * 提交给我的非草稿日志列表查日志关联表
     * @param page
     * @param limit
     * @param qid
     * @param mid
     * @return
     */
    JsonResultList logMeList(Integer page, Integer limit, Integer qid, Integer mid);


    /**
     * 分批删除草稿
     * @param ids
     * @return
     */
    ResultDtoUtile removeLog(List<Integer> ids);

    /**
     * 编辑我创建的日志
     * @param id
     * @return
     */
    ResultDtoUtile findLogMy(Integer id);

    /**
     *修改草稿
     * @param dwLog
     * @return
     */
    ResultDtoUtile updateLog(DwLog dwLog);
    
}
