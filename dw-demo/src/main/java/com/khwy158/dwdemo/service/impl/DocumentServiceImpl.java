package com.khwy158.dwdemo.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.khwy158.dwdemo.dao.DwDocumentMapper;
import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.pojo.DwDocument;
import com.khwy158.dwdemo.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: khwy-158
 * @description:
 * @author: Mr.Wang
 * @create: 2018-08-31 14:28
 **/
@Service
public class DocumentServiceImpl implements DocumentService {
    @Autowired
            @SuppressWarnings("all")
    DwDocumentMapper mapper;
    @Override
    public JsonResultList getlist(Integer page, Integer limit, Integer mid, Integer ispublic,Integer parentid) {

        //验证数据
        //调用mapper
        System.out.println(mid+"and"+ispublic);
        Page objects = PageHelper.startPage(page, limit);
        List<DwDocument> selectall = mapper.selectall(mid,ispublic,parentid);
        return new JsonResultList(0,"",(int)objects.getTotal(),selectall);
    }
}
