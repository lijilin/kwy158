package com.khwy158.dwdemo.service;

import com.khwy158.dwdemo.dto.JsonResultList;
import org.springframework.data.redis.connection.RedisZSetCommands;

/**
 * @program: khwy-158
 * @description: 文档的接口
 * @author: Mr.Wang
 * @create: 2018-08-31 14:03
 **/
public interface DocumentService {
    /**
     * 查询我的文档的列表
     * @param limit
     * @param page
     * @param
     * @param mid
     * @param
     * @return
     */
    JsonResultList getlist(Integer page,Integer limit,Integer mid,Integer ispublic,Integer parentid);
}
