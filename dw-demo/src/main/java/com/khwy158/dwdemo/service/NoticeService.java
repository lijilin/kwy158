package com.khwy158.dwdemo.service;

import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.pojo.DwNotice;
import com.khwy158.dwdemo.pojo.DwNoticeEmpl;
import com.khwy158.modeldemo.dto.ResultDtoUtile;

import java.util.List;

/**
 * @program: khwy-158
 * @description: 公告
 * @author: Mr.Wang
 * @create: 2018-08-28 14:59
 **/

public interface NoticeService {
    /**
     * 发公告的方法
     * @param dwNotice
     * @return
     */
    ResultDtoUtile sendNotice(DwNotice dwNotice);

    /**
     * 公告列表
     * @param page
     * @param limit
     * @param qid
     * @param mid
     * @return
     */
    JsonResultList noticeList(Integer page, Integer limit, Integer qid, Integer mid);

    /**
     * 分批删除
     * @param ids
     * @return
     */
    ResultDtoUtile removeNotice(List<Integer> ids);

    ResultDtoUtile findNoticeOne(Integer id);

    ResultDtoUtile updateNotice(DwNotice dwNotice);

}
