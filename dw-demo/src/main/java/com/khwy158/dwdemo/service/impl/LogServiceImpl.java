package com.khwy158.dwdemo.service.impl;

import com.khwy158.dwdemo.dao.*;
import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.eum.NoticeEnum;
import com.khwy158.dwdemo.pojo.*;
import com.khwy158.dwdemo.service.LogService;
import com.khwy158.dwdemo.utils.TimeUtil;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: khwy-158
 * @description: 日志接口的实现类
 * @author: Mr.Wang
 * @create: 2018-08-29 15:22
 **/
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    @SuppressWarnings("all")
    DwLogMapper logMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogEmplMapper logEmpMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogDocumentMapper logDocumentMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogChanceMapper logChanceMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogClientMapper logClientMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogComplaintMapper logComplaintMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogExpendMapper logExpendMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogFurtherMapper logFurtherMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogOrderMapper logOrderMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogRefundMapper logRefundMapper;
    @Autowired
    @SuppressWarnings("all")
    DwLogReturnedMapper logReturnedMapper;


    @Override
    public ResultDtoUtile sendLog(DwLog dwLog) {
        //验证数据
        System.out.println(dwLog.getSort());
        System.out.println(dwLog.getContent());
        System.out.println(dwLog.getTitle());
        if (dwLog==null){
            return new ResultDtoUtile(NoticeEnum.NULLMESSAGE.getCode(),NoticeEnum.NULLMESSAGE.getMsg());
        }
        if (dwLog.getTitle()==null||dwLog.getContent()==null||dwLog.getSort()==null){
            return new ResultDtoUtile(NoticeEnum.NULLMESSAGE.getCode(),NoticeEnum.NULLMESSAGE.getMsg());
        }

        //设置发公告时间
        dwLog.setTime(TimeUtil.getTimeStr());
        //插入日志表
        int i = logMapper.insertSelective(dwLog);
        if (i<=0){
            return new ResultDtoUtile(NoticeEnum.FAIL.getCode(),NoticeEnum.FAIL.getMsg());
        }
        //更新联表
            //更新日志通知表
        List<Integer> ids=dwLog.getMemberList();
         if (ids!=null){
             //指定通知
             for (Integer integer : ids) {
                 DwLogEmpl dwLogEmpl=new DwLogEmpl();
                 //设置那两个id
                 dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                 dwLogEmpl.setEmplId(dwLog.getEmplId());
                 //要通知人的id
                 dwLogEmpl.setMemberId(integer);
                 //日志表id
                 dwLogEmpl.setLogId(dwLog.getId());
                 logEmpMapper.insertSelective(dwLogEmpl);
             }
         }
        //更新文档表
         ids=dwLog.getDocumentList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogDocument dwLogEmpl=new DwLogDocument();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //
                dwLogEmpl.setDocumentId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logDocumentMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getChanceList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogChance dwLogEmpl=new DwLogChance();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //
                dwLogEmpl.setChanceId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logChanceMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getClientList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogClient dwLogEmpl=new DwLogClient();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //
                dwLogEmpl.setClientId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logClientMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getComplaintList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogComplaint dwLogEmpl=new DwLogComplaint();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //要通知人的id
                dwLogEmpl.setComplaintId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logComplaintMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getExpendList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogExpend dwLogEmpl=new DwLogExpend();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //要通知人的id
                dwLogEmpl.setExpendId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logExpendMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getFurtherList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogFurther dwLogEmpl=new DwLogFurther();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //要通知人的id
                dwLogEmpl.setFurtherId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logFurtherMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getOrderList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogOrder dwLogEmpl=new DwLogOrder();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //要通知人的id
                dwLogEmpl.setOrderId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logOrderMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getRefundList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogRefund dwLogEmpl=new DwLogRefund();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //要通知人的id
                dwLogEmpl.setRefundId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logRefundMapper.insertSelective(dwLogEmpl);
            }
        }
        //更新表
        ids=dwLog.getReturendList();
        if (ids!=null ){
            //指定通知
            for (Integer integer : ids) {
                DwLogReturned dwLogEmpl=new DwLogReturned();
                //设置那两个id
                dwLogEmpl.setRegisterId(dwLog.getRegisterId());
                dwLogEmpl.setEmplId(dwLog.getEmplId());
                //要通知人的id
                dwLogEmpl.setReturnedId(integer);
                //日志表id
                dwLogEmpl.setLogId(dwLog.getId());
                logReturnedMapper.insertSelective(dwLogEmpl);
            }
        }
        return new ResultDtoUtile(NoticeEnum.SUCCESS.getCode(),NoticeEnum.SUCCESS.getMsg());
    }

    @Override
    public JsonResultList logMyList(Integer page, Integer limit, Integer qid, Integer mid) {
        return null;
    }

    @Override
    public JsonResultList logMeList(Integer page, Integer limit, Integer qid, Integer mid) {
        return null;
    }

    @Override
    public ResultDtoUtile removeLog(List<Integer> ids) {
        return null;
    }

    @Override
    public ResultDtoUtile findLogMy(Integer id) {
        return null;
    }

    @Override
    public ResultDtoUtile updateLog(DwLog dwLog) {
        return null;
    }
}
