package com.khwy158.dwdemo.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.khwy158.dwdemo.dao.DwNoticeEmplMapper;
import com.khwy158.dwdemo.dao.DwNoticeMapper;
import com.khwy158.dwdemo.dto.JsonResultList;
import com.khwy158.dwdemo.eum.NoticeEnum;
import com.khwy158.dwdemo.pojo.DwNotice;
import com.khwy158.dwdemo.pojo.DwNoticeEmpl;
import com.khwy158.dwdemo.service.NoticeService;
import com.khwy158.dwdemo.utils.TimeUtil;
import com.khwy158.dwdemo.utils.myutil.TimeSwUtil;
import com.khwy158.modeldemo.dto.ResultDtoUtile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: khwy-158
 * @description: 公告实现类
 * @author: Mr.Wang
 * @create: 2018-08-28 15:39
 **/
@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
            @SuppressWarnings("all")
    DwNoticeMapper noticeMapper;
    @Autowired
    @SuppressWarnings("all")
    DwNoticeEmplMapper noticeEmplMapper;

    @Override
    public ResultDtoUtile sendNotice(DwNotice dwNotice) {
        //验证数据
        if (dwNotice==null){
            return new ResultDtoUtile(NoticeEnum.NULLMESSAGE.getCode(),NoticeEnum.NULLMESSAGE.getMsg());
        }
        if (dwNotice.getTitle()==null||dwNotice.getContent()==null){
            return new ResultDtoUtile(NoticeEnum.NULLMESSAGE.getCode(),NoticeEnum.NULLMESSAGE.getMsg());
        }
        //获得登录对像

        //对公告设置对像id
        dwNotice.setRegisterId(10);
        dwNotice.setEmplId(10);
        //设置发公告时间
        dwNotice.setTime(TimeUtil.getTimeStr());
        //插入公告表
        int i = noticeMapper.insertSelective(dwNotice);
        if (i<=0){
            return new ResultDtoUtile(NoticeEnum.FAIL.getCode(),NoticeEnum.FAIL.getMsg());
        }
        //更新联表
            //判断list是否为空如果为空全部通知
        List<Integer> ids=dwNotice.getList();
            if (ids==null||ids.size()==0){
                //调接口获得全部成员
                //模拟数据
                List<Integer> list=new ArrayList<>();
                list.add(1);
                list.add(2);
                list.add(3);
                list.add(4);
                //插入公告连表数据库
                for (Integer integer : list) {
                    DwNoticeEmpl dwNoticeEmpl=new DwNoticeEmpl();
                    //为表设置所属人
                    dwNoticeEmpl.setRegisterId(10);
                    dwNoticeEmpl.setEmplId(10);
                    //发布人id
                    dwNoticeEmpl.setNoticeId(dwNotice.getId());
                    //要通知人的id
                    dwNoticeEmpl.setMemberId(integer);
                    noticeEmplMapper.insertSelective(dwNoticeEmpl);
                }
                return new ResultDtoUtile(NoticeEnum.SUCCESS.getCode(),NoticeEnum.SUCCESS.getMsg());
            }
            //指定通知
        for (Integer integer : ids) {
            DwNoticeEmpl dwNoticeEmpl=new DwNoticeEmpl();
            //要通知人的id
            dwNoticeEmpl.setMemberId(integer);
            //为表设置所属人
            dwNoticeEmpl.setRegisterId(10);
            dwNoticeEmpl.setEmplId(10);
            //发布人id
            dwNoticeEmpl.setNoticeId(dwNotice.getId());
            noticeEmplMapper.insertSelective(dwNoticeEmpl);
        }
        return new ResultDtoUtile(NoticeEnum.SUCCESS.getCode(),NoticeEnum.SUCCESS.getMsg());

    }

    @Override
    public JsonResultList noticeList(Integer page, Integer limit, Integer qid, Integer mid) {
        //
        Page page1 = PageHelper.startPage(page, limit);
        List<DwNoticeEmpl> dwNoticeEmpls = noticeEmplMapper.selectAll(qid, mid);
        for (DwNoticeEmpl dwNoticeEmpl : dwNoticeEmpls) {
            //设置显示时间格式
            dwNoticeEmpl.setTime(TimeSwUtil.getTimefor(dwNoticeEmpl.getTime()));
        }
        return new JsonResultList(NoticeEnum.SUCCESS.getCode(),NoticeEnum.SUCCESS.getMsg(),(int)page1.getTotal(),dwNoticeEmpls);
    }

    @Override
    public ResultDtoUtile removeNotice(List<Integer> ids) {
        //验证数据
        if (ids==null||ids.size()==0){
            return new ResultDtoUtile(NoticeEnum.NULLMESSAGE.getCode(),NoticeEnum.NULLMESSAGE.getMsg());
        }
        //执行操作
        int i = noticeEmplMapper.deleteNotice(ids);
        System.out.println(i);
        if (i<=0){
            return new ResultDtoUtile(NoticeEnum.FAIL.getCode(),NoticeEnum.FAIL.getMsg());
        }
        return new ResultDtoUtile(NoticeEnum.SUCCESS.getCode(),NoticeEnum.SUCCESS.getMsg(),i);
    }

    @Override
    public ResultDtoUtile findNoticeOne(Integer noticeId) {
        //根据公告id进行编辑查找
        DwNotice notice = noticeMapper.selectByPrimaryKey(noticeId);
        if (notice==null){
            return new ResultDtoUtile(9401,"无此id的公告");
        }

        return new ResultDtoUtile(NoticeEnum.SUCCESS.getCode(),NoticeEnum.SUCCESS.getMsg(),notice);
    }

    @Override
    public ResultDtoUtile updateNotice(DwNotice dwNotice) {
        //验证数据
        if (dwNotice==null){
            return new ResultDtoUtile(NoticeEnum.NULLMESSAGE.getCode(),NoticeEnum.NULLMESSAGE.getMsg());
        }
        if (dwNotice.getTitle()==null||dwNotice.getContent()==null){
            return new ResultDtoUtile(NoticeEnum.NULLMESSAGE.getCode(),NoticeEnum.NULLMESSAGE.getMsg());
        }
        int i = noticeMapper.updateByPrimaryKeySelective(dwNotice);
        if (i<=0){
            return new ResultDtoUtile(NoticeEnum.FAIL.getCode(),NoticeEnum.FAIL.getMsg());
        }
        return new ResultDtoUtile(NoticeEnum.SUCCESS.getCode(),NoticeEnum.SUCCESS.getMsg(),i);
    }
}
