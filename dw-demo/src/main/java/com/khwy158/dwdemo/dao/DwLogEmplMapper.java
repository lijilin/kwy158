package com.khwy158.dwdemo.dao;

import com.khwy158.dwdemo.pojo.DwLogEmpl;

public interface DwLogEmplMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_log_empl
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_log_empl
     *
     * @mbg.generated
     */
    int insert(DwLogEmpl record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_log_empl
     *
     * @mbg.generated
     */
    int insertSelective(DwLogEmpl record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_log_empl
     *
     * @mbg.generated
     */
    DwLogEmpl selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_log_empl
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(DwLogEmpl record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_log_empl
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(DwLogEmpl record);
}