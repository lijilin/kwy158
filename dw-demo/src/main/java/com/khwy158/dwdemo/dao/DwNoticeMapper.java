package com.khwy158.dwdemo.dao;

import com.khwy158.dwdemo.pojo.DwNotice;

public interface DwNoticeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_notice
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_notice
     *
     * @mbg.generated
     */
    int insert(DwNotice record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_notice
     *
     * @mbg.generated
     */
    int insertSelective(DwNotice record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_notice
     *
     * @mbg.generated
     */
    DwNotice selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_notice
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(DwNotice record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dw_notice
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(DwNotice record);
}