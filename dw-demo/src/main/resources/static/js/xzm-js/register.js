$(function () {

    var regUrl="/test";


    // 获取表单数据
    var version=$("#version").val();
    var useNum=$("#use_number").val();
    var username=$("#username").val();
    var email=$("#email").val();
    var pwd=$("#password").val();
    var rPwd=$("#rPwd").val();
    var contacts=$("#contacts").val();
    var contactNum=$("#contactNumber").val();

    // 验证数据的合法性
    // if(username.trim().length==0) {
    //     layer.msg("用户名不能为空");
    //     return false;
    // }
    //
    // judgeEmail(email);
    //
    // if(pwd.trim().length==0){
    //     layer.msg("密码不能为空");
    //     return false;
    // }
    //
    // if(pwd!=rPwd){
    //     layer.msg("密码不一致");
    //     return false;
    // }
    //
    // if(contacts.trim().length==0){
    //     layer.msg("联系人不能为空");
    //     return false;
    // }
    //
    // if(contactNum.trim().length==0){
    //     layer.msg("联系方式不能为空");
    //     return false;
    // }


    // 组装一个 register 对象
    var register={};
    register.useNum=useNum;
    register.username=username;
    register.email=email;
    register.pwd=pwd;
    register.contacts=contacts;
    register.contactNum=contactNum;

    // 通过 ajax 发送数据
    $.ajax({
                url:regUrl,
                type:"post",
        dataType:"json",
        data:JSON.stringify(register),
        cache: false,                      // 不缓存
        processData: false,                // jQuery不要去处理发送的数据
        contentType: "application/json",
        success:function (data) {           //成功回调
            if(data.code == 200){
                layer.msg(data.msg,function () {
                    //
                });
            }else{
                layer.msg(data.msg)
            }
        }
    })













});

// 验证邮箱格式的方法
function judgeEmail(email) {
    // 定义正则表达式
    var reg = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)/;
    // 判断
    if(   reg.test(email) == false ) {
        layer.msg('邮箱格式不正确');
        return false;
    }
    return true;
}
