package com.khwy158.ljldemo.Utils;

import com.khwy158.ljldemo.enums.EnumsEexption;
import com.khwy158.ljldemo.pojo.LdxCompanyClient;

import java.util.List;

/**
 * 描述:
 *
 * @author lijilin
 * @create 2018-08-30 11:47
 */
public class EexptionUtil {

    private Integer code;

    private String msg;

    private LdxCompanyClient companyClient;

    private List<LdxCompanyClient> companyClientListc;

    /**
     * 失败的
     * @param code
     * @param msg
     */

    public EexptionUtil(EnumsEexption enumsEexption) {
        this.code = enumsEexption.getCode();
        this.msg = enumsEexption.getMsg();
    }

    /**
     * 成功的
     * @param code
     * @param msg
     * @param data
     */
    public EexptionUtil(EnumsEexption enumsEexption,LdxCompanyClient companyClient) {
        this.code = enumsEexption.getCode();
        this.msg = enumsEexption.getMsg();
        this.companyClient=companyClient;
    }

    /**
     * 查询
     * @param code
     * @param msg
     * @param datalist
     */
    public EexptionUtil(EnumsEexption enumsEexption, List<LdxCompanyClient> companyClientListc) {
        this.code = enumsEexption.getCode();
        this.msg = enumsEexption.getMsg();
        this.companyClientListc=companyClientListc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LdxCompanyClient getCompanyClient() {
        return companyClient;
    }

    public void setCompanyClient(LdxCompanyClient companyClient) {
        this.companyClient = companyClient;
    }

    public List<LdxCompanyClient> getCompanyClientListc() {
        return companyClientListc;
    }

    public void setCompanyClientListc(List<LdxCompanyClient> companyClientListc) {
        this.companyClientListc = companyClientListc;
    }
}