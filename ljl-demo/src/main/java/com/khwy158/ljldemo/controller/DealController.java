package com.khwy158.ljldemo.controller;

import com.github.pagehelper.PageInfo;
import com.khwy158.ljldemo.Utils.EexptionUtil;
import com.khwy158.ljldemo.enums.EnumsEexption;
import com.khwy158.ljldemo.pojo.LdxCompanyClient;
import com.khwy158.ljldemo.service.impl.DealserviceImpl;
import com.khwy158.modeldemo.pojo.ljl.Page;
import com.khwy158.modeldemo.utils.ResultEexptionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 描述:
 *
 * @author lijilin
 * @create 2018-08-29 17:29
 */
@Controller
public class DealController {

    @Autowired
    DealserviceImpl dealservice;

    /**
     * 成交客户
     * @return
     */
    @RequestMapping("/ljl-deal")
    @ResponseBody
    public ResultEexptionDto companyClientList(@RequestBody Page page){

        System.out.println("服务器page"+page);
        EexptionUtil eexptionUtil = dealservice.get(3,page);

        PageInfo pageInfo=new PageInfo(eexptionUtil.getCompanyClientListc());

        long total = pageInfo.getTotal();

        System.out.println("controller层面"+eexptionUtil.getCompanyClientListc().toString());

        return new ResultEexptionDto(eexptionUtil.getCode(),eexptionUtil.getMsg(),total,eexptionUtil.getCompanyClientListc());
    }

    /**
     * 失败客户
     * @return
     */
//    @GetMapping("/ljl-loser")
//    @ResponseBody
//    public ResultEexptionDto loser(@RequestBody Page page){
//        EexptionUtil eexptionUtil = dealservice.get(4,page);
//        return new ResultEexptionDto(eexptionUtil.getCode(),eexptionUtil.getMsg(),50,eexptionUtil.getCompanyClientListc());
//    }

    /**
     * 流失客户
     * @return
     */
//    @GetMapping("/ljl-lossing")
//    @ResponseBody
//    public ResultEexptionDto lossing(@RequestBody Page page){
//        EexptionUtil eexptionUtil = dealservice.get(5,page);
//        return new ResultEexptionDto(eexptionUtil.getCode(),eexptionUtil.getMsg(),50,eexptionUtil.getCompanyClientListc());
//    }
}