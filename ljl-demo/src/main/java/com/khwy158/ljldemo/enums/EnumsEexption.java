package com.khwy158.ljldemo.enums;

public enum EnumsEexption {

    SUCCESS(0,""),
    FAILD(4001,"获取信息失败!"),
    ;

    private Integer code;

    private String msg;

    EnumsEexption() {
    }

    EnumsEexption(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
