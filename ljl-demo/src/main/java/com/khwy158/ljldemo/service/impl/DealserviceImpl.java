package com.khwy158.ljldemo.service.impl;

import com.github.pagehelper.PageHelper;
import com.khwy158.ljldemo.Utils.EexptionUtil;
import com.khwy158.ljldemo.dao.LdxCompanyClientMapper;
import com.khwy158.ljldemo.enums.EnumsEexption;
import com.khwy158.ljldemo.pojo.LdxCompanyClient;
import com.khwy158.ljldemo.service.Dealservice;
import com.khwy158.modeldemo.pojo.ljl.CompanyClient;
import com.khwy158.modeldemo.pojo.ljl.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述:
 *
 * @author lijilin
 * @create 2018-08-29 15:54
 */
@Service
public class DealserviceImpl implements Dealservice {

    @Autowired
    @SuppressWarnings("all")
    LdxCompanyClientMapper cd;

    /**
     * 根据客户状态ID 查询
     * @param id
     * @return
     */
    @Override
    public EexptionUtil get(Integer id,Page page) {
        PageHelper.startPage(page.getPage(),page.getPageSzie());
        List<LdxCompanyClient> ldxCompanyClients = cd.get(id);

        List<CompanyClient> companyClientList=null;
        for (LdxCompanyClient client : ldxCompanyClients) {
            String acquisitionTime = client.getAcquisitionTime();
        }
        System.out.println("service曾"+ldxCompanyClients.toString());
        if (ldxCompanyClients==null){
            return new EexptionUtil(EnumsEexption.FAILD);
        }

        return new EexptionUtil(EnumsEexption.SUCCESS,ldxCompanyClients);
    }
}