package com.khwy158.ljldemo.service;

import com.khwy158.ljldemo.Utils.EexptionUtil;
import com.khwy158.ljldemo.pojo.LdxCompanyClient;
import com.khwy158.modeldemo.pojo.ljl.Page;

import java.util.List;

public interface Dealservice {

    /**
     * 根据客户状态ID查询
     * @param id
     * @return
     */
    EexptionUtil get(Integer id, Page page);
}
