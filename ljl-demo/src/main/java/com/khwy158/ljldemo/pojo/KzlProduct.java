package com.khwy158.ljldemo.pojo;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table kzl_product
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class KzlProduct {
    /** 产品id*/
    private Integer id;

    /** 产品分类*/
    private String proCategory;

    /** 产品名字*/
    private String proName;

    /** 产品图片*/
    private String proPicture;

    /** 销售单价*/
    private Double proPrice;

    /** 成本*/
    private Double proCost;

    /** 单位*/
    private String proUnit;

    /** 产品状态状态*/
    private String proStatus;

    /** 销售状态默认上架可选下架*/
    private String saleStatus;

    /** 上市时间*/
    private String listTime;

    /** 介绍*/
    private String proIntroduce;

    /** 员工id*/
    private Integer emplId;

    /** 注册id*/
    private Integer registerId;

    /** */
    private Integer proOrderId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.id
     *
     * @return the value of kzl_product.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.id
     *
     * @param id the value for kzl_product.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_category
     *
     * @return the value of kzl_product.pro_category
     *
     * @mbg.generated
     */
    public String getProCategory() {
        return proCategory;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_category
     *
     * @param proCategory the value for kzl_product.pro_category
     *
     * @mbg.generated
     */
    public void setProCategory(String proCategory) {
        this.proCategory = proCategory;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_name
     *
     * @return the value of kzl_product.pro_name
     *
     * @mbg.generated
     */
    public String getProName() {
        return proName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_name
     *
     * @param proName the value for kzl_product.pro_name
     *
     * @mbg.generated
     */
    public void setProName(String proName) {
        this.proName = proName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_picture
     *
     * @return the value of kzl_product.pro_picture
     *
     * @mbg.generated
     */
    public String getProPicture() {
        return proPicture;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_picture
     *
     * @param proPicture the value for kzl_product.pro_picture
     *
     * @mbg.generated
     */
    public void setProPicture(String proPicture) {
        this.proPicture = proPicture;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_price
     *
     * @return the value of kzl_product.pro_price
     *
     * @mbg.generated
     */
    public Double getProPrice() {
        return proPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_price
     *
     * @param proPrice the value for kzl_product.pro_price
     *
     * @mbg.generated
     */
    public void setProPrice(Double proPrice) {
        this.proPrice = proPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro__cost
     *
     * @return the value of kzl_product.pro__cost
     *
     * @mbg.generated
     */
    public Double getProCost() {
        return proCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro__cost
     *
     * @param proCost the value for kzl_product.pro__cost
     *
     * @mbg.generated
     */
    public void setProCost(Double proCost) {
        this.proCost = proCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_unit
     *
     * @return the value of kzl_product.pro_unit
     *
     * @mbg.generated
     */
    public String getProUnit() {
        return proUnit;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_unit
     *
     * @param proUnit the value for kzl_product.pro_unit
     *
     * @mbg.generated
     */
    public void setProUnit(String proUnit) {
        this.proUnit = proUnit;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_status
     *
     * @return the value of kzl_product.pro_status
     *
     * @mbg.generated
     */
    public String getProStatus() {
        return proStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_status
     *
     * @param proStatus the value for kzl_product.pro_status
     *
     * @mbg.generated
     */
    public void setProStatus(String proStatus) {
        this.proStatus = proStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.sale_status
     *
     * @return the value of kzl_product.sale_status
     *
     * @mbg.generated
     */
    public String getSaleStatus() {
        return saleStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.sale_status
     *
     * @param saleStatus the value for kzl_product.sale_status
     *
     * @mbg.generated
     */
    public void setSaleStatus(String saleStatus) {
        this.saleStatus = saleStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.list_time
     *
     * @return the value of kzl_product.list_time
     *
     * @mbg.generated
     */
    public String getListTime() {
        return listTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.list_time
     *
     * @param listTime the value for kzl_product.list_time
     *
     * @mbg.generated
     */
    public void setListTime(String listTime) {
        this.listTime = listTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_introduce
     *
     * @return the value of kzl_product.pro_introduce
     *
     * @mbg.generated
     */
    public String getProIntroduce() {
        return proIntroduce;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_introduce
     *
     * @param proIntroduce the value for kzl_product.pro_introduce
     *
     * @mbg.generated
     */
    public void setProIntroduce(String proIntroduce) {
        this.proIntroduce = proIntroduce;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.empl_id
     *
     * @return the value of kzl_product.empl_id
     *
     * @mbg.generated
     */
    public Integer getEmplId() {
        return emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.empl_id
     *
     * @param emplId the value for kzl_product.empl_id
     *
     * @mbg.generated
     */
    public void setEmplId(Integer emplId) {
        this.emplId = emplId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.register_id
     *
     * @return the value of kzl_product.register_id
     *
     * @mbg.generated
     */
    public Integer getRegisterId() {
        return registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.register_id
     *
     * @param registerId the value for kzl_product.register_id
     *
     * @mbg.generated
     */
    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column kzl_product.pro_order_id
     *
     * @return the value of kzl_product.pro_order_id
     *
     * @mbg.generated
     */
    public Integer getProOrderId() {
        return proOrderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column kzl_product.pro_order_id
     *
     * @param proOrderId the value for kzl_product.pro_order_id
     *
     * @mbg.generated
     */
    public void setProOrderId(Integer proOrderId) {
        this.proOrderId = proOrderId;
    }
}