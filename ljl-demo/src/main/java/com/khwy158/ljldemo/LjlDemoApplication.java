package com.khwy158.ljldemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.khwy158.ljldemo.dao")
public class LjlDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(LjlDemoApplication.class, args);
    }
}
